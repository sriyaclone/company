<?php
    /* QUOTATION TYPE */
    define('SPECIAL', 1);
    define('NORMAL', 0);

    /* QUOTATON STATUS */
    define('PENDING',0);
    define('APPROVED',1);
    define('REJECTED',-1);
    define('REVISED',2);
    define('CLOSED',3);
    define('INVOICED',4);
    define('PO_PROCESSING',5);

    /* FOLLOWUP STATUS */
    define('FPENDING',0);
    define('FDONE',1);
    define('FCANCEL',-1);

    define('VAT',1);
    define('NBT',2);

    /* INQUIRY EVENT STATUS */
    define('INQUIRY_WEB_CREATE',1);
    define('INQUIRY_APPROVED',2);
    define('INQUIRY_CANCELLED',3);
    define('INQUIRY_REOPENED',4);
    define('INQUIRY_ASSIGNED',5);
    define('INQUIRY_MOBILE_CREATE',6);
    define('INQUIRY_QUOTATION_CREATED',7);
    define('INQUIRY_QUOTATION_REVISED',8);
    define('INQUIRY_QUOTATION_APPROVED',9);
    define('INQUIRY_QUOTATION_REJECTED',10);
    define('INQUIRY_QUOTATION_CLOSED',11);
    define('INQUIRY_PO_UPLOADED',12);

    /* Invoice Status */
    define('INVOICE_PENDING', 0);
    define('INVOICE_DISPATCHED', 1);
    define('INVOICE_LOADED', 2);
    define('INVOICE_DELIVERED', 3);
    define('INVOICE_RETURNED', 4);
    define('DELIVERY_RETURNED', 5);

    /* PAYMENT COLLECTION TYPES */
    define('PAYMENT_CASH',1);
    define('PAYMENT_CHEQUE',2);
    define('PAYMENT_ADVANCE',3);

    /* PAYMENT COLLECTION STATUS */
    define('PAYMENT_PENDING',0);
    define('PAYMENT_APPROVED',1);
    define('PAYMENT_HOLD',2);
    define('PAYMENT_REJECT',-1);


    /** Quotation DISCOUNT TYPE */
    define('CASH',1);
    define('PROJECT',2);

    /** ORDER TYPE */
    define('NORMAL_ORDER',1);

    /** ORDER DISCOUNT TYPE */
    define('UDP_DISCOUNT',1);

     /** CUSTOMER FROM */
    define('BUY_ORANGE_NEW',1);

    /** CUSTOMER BLOCK */
    define('CUSTOMER_ACTIVE',1);

    define('ORDER_PENDING',0);
    define('ORDER_APPROVED',1);
    define('ORDER_GRN',2);
    define('ORDER_PAYMENT_DONE',3);
    define('ORDER_PAYMENT_PARTIAL',4);
    define('ORDER_DELIVERED',5);
    define('ORDER_REJECTED',-1);

    define('DISPATCH_PENDING',1);
    define('DISPATCH_PICKED',2);
    define('DISPATCH_BAYIN',3);
    define('DISPATCH_LOADED',4);
    define('DISPATCH_CHECKOUT',5);
    define('DISPATCH_CHECKIN',6);
    define('DISPATCH_CANCEL',7);

    define('COMPANYDELIVERY',1);
    define('COURIERDELIVERY',2);
    define('ONPREMISES',3);

    /* Dispatch Loading Status */
    define('LOADING_NO',-1);
    define('LOADING_PENDING',0);
    define('LOADING_STARTED',1);
    define('LOADING_DONE',2);

    /* Grn types */
    define('DIRECT_GRN',1);
    define('GRN_BY_ORDER',2);
?>