@extends('layouts.back_master') @section('title','Admin - Product Management')
@section('current_title','MRP List')

<link href="{{asset('assets/inspinia/assets/morris/morris-0.4.3.min.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://www.highcharts.com/media/com_demo/css/highslide.css" />

@section('css')
    <style type="text/css">
        .large-value{
            font-size: 40px;
        }
        .small-value{
            font-size: 20px;
        }
        .no-padding{
            padding: 0px;
        }
        .ibox-content {
            padding: 0px 20px 10px 20px !important;
        }
        .rounded-left {
            border-top-left-radius: 500px !important;
            border-bottom-left-radius: 500px !important;
            border-top-right-radius: 0px!important;
            border-bottom-right-radius: 0px !important;
        }
        .rounded-right {
            border-top-right-radius: 500px !important;
            border-bottom-right-radius: 500px !important;
        }
        .btn-sm {
            padding: 5px 10px !important;
            font-size: 10px !important;
            line-height: 1.5 !important;
        }

        .filter-toggle{
            text-align: right;
            padding-right: 15px;
            padding-bottom: 15px;
        }
    </style>
@stop

@section('content')
    <div class="wrapper wrapper-content">
        <div class="row filter-toggle">
            <label class="btn btn-sm rounded-left white" onclick="load_weekly()">Weekly</label>
            <label class="btn btn-sm white" onclick="load_weekly()">Monthly</label>
            <label class="btn btn-sm rounded-right white" onclick="load_weekly()">Yearly</label>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-primary pull-right">Last Weekly</span>
                        <h5>Sales Order</h5>
                    </div>
                    <div class="ibox-content">

                        <div class="row">
                            <div class="col-md-8">
                                <h1 class="no-margins large-value">406,42</h1>
                                <div class="font-bold text-navy">44% <i class="fa fa-level-up"></i> <small>Rapid pace</small></div>
                            </div>
                            <div class="col-md-4">
                                <h1 class="no-margins small-value">206,12</h1>
                                <div class="font-bold text-navy">22% <i class="fa fa-level-up"></i></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-primary pull-right">Last Weekly</span>
                        <h5>Invoices</h5>
                    </div>
                    <div class="ibox-content">

                        <div class="row">
                            <div class="col-md-8">
                                <h1 class="no-margins large-value">406,42</h1>
                                <div class="font-bold text-navy">44% <i class="fa fa-level-up"></i> <small>Rapid pace</small></div>
                            </div>
                            <div class="col-md-4">
                                <h1 class="no-margins small-value">206,12</h1>
                                <div class="font-bold text-navy">22% <i class="fa fa-level-up"></i></div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-primary pull-right">Last Weekly</span>
                        <h5>Delivered</h5>
                    </div>
                    <div class="ibox-content">

                        <div class="row">
                            <div class="col-md-8">
                                <h1 class="no-margins large-value">406,42</h1>
                                <div class="font-bold text-navy">44% <i class="fa fa-level-up"></i> <small>Rapid pace</small></div>
                            </div>
                            <div class="col-md-4">
                                <h1 class="no-margins small-value">206,12</h1>
                                <div class="font-bold text-navy">22% <i class="fa fa-level-up"></i></div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-primary pull-right">Last Weekly</span>
                        <h5>Returns</h5>
                    </div>
                    <div class="ibox-content">

                        <div class="row">
                            <div class="col-md-8">
                                <h1 class="no-margins large-value">406,42</h1>
                                <div class="font-bold text-navy">44% <i class="fa fa-level-up"></i> <small>Rapid pace</small></div>
                            </div>
                            <div class="col-md-4">
                                <h1 class="no-margins small-value">206,12</h1>
                                <div class="font-bold text-navy">22% <i class="fa fa-level-up"></i></div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>            
        </div>

        <div class="row">
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div style="padding-top: 15px">
                            <h3 class="font-bold no-margins">
                                Today Summary
                            </h3>
                        </div>

                        <div class="m-t-sm">
                            <div class="row">
                                <div class="col-md-12">
                                    <ul class="stat-list">
                                        <li>
                                            <h2 class="no-margins">2,346</h2>
                                            <small>Sales Order in Today</small>
                                            <div class="progress progress-mini">
                                                <div class="progress-bar" style="width: 48%;"></div>
                                            </div>
                                        </li>
                                        <li>
                                            <h2 class="no-margins ">4,422</h2>
                                            <small>Invoice in Today</small>
                                            <div class="progress progress-mini">
                                                <div class="progress-bar" style="width: 60%;"></div>
                                            </div>
                                        </li>
                                        <li>
                                            <h2 class="no-margins ">4,422</h2>
                                            <small>Pending Delivery in Today</small>
                                            <div class="progress progress-mini">
                                                <div class="progress-bar" style="width: 60%;"></div>
                                            </div>
                                        </li>
                                        <li>
                                            <h2 class="no-margins ">4,422</h2>
                                            <small>Delivered in Today</small>
                                            <div class="progress progress-mini">
                                                <div class="progress-bar" style="width: 60%;"></div>
                                            </div>
                                        </li>
                                        <li>
                                            <h2 class="no-margins ">4,422</h2>
                                            <small>Returns in Today</small>
                                            <div class="progress progress-mini">
                                                <div class="progress-bar" style="width: 60%;"></div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                    </div>                    
                </div>
            </div>

        </div>

    </div>
<!-- !!Main content -->
@stop

@section('js')
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/modules/series-label.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://www.highcharts.com/media/com_demo/js/highslide-full.min.js"></script>
    <script src="https://www.highcharts.com/media/com_demo/js/highslide.config.js" charset="utf-8"></script>

    <script type="text/javascript">
        $(document).ready(function() {            


            Highcharts.chart('container', {

                chart: {
                    scrollablePlotArea: {
                        minWidth: 700
                    }
                },

                data: {
                    csvURL: "{{asset('assets/samples/analytics.csv')}}",
                    beforeParse: function (csv) {
                        return csv.replace(/\n\n/g, '\n');
                    }
                },

                title: {
                    text: 'Sales Analysis'
                },

                xAxis: {
                    tickInterval: 7 * 24 * 3600 * 1000, // one week
                    tickWidth: 0,
                    gridLineWidth: 1,
                    labels: {
                        align: 'left',
                        x: 3,
                        y: -3
                    }
                },

                yAxis: [{ // left y axis
                    title: {
                        text: null
                    },
                    labels: {
                        align: 'left',
                        x: 3,
                        y: 16,
                        format: '{value:.,0f}'
                    },
                    showFirstLabel: false
                }, { // right y axis
                    linkedTo: 0,
                    gridLineWidth: 0,
                    opposite: true,
                    title: {
                        text: null
                    },
                    labels: {
                        align: 'right',
                        x: -3,
                        y: 16,
                        format: '{value:.,0f}'
                    },
                    showFirstLabel: false
                }],

                legend: {
                    align: 'left',
                    verticalAlign: 'top',
                    borderWidth: 0
                },

                tooltip: {
                    shared: true,
                    crosshairs: true
                },

                plotOptions: {
                    series: {
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function (e) {
                                    hs.htmlExpand(null, {
                                        pageOrigin: {
                                            x: e.pageX || e.clientX,
                                            y: e.pageY || e.clientY
                                        },
                                        headingText: this.series.name,
                                        maincontentText: Highcharts.dateFormat('%A, %b %e, %Y', this.x) + ':<br/> ' +
                                            this.y + ' invoices',
                                        width: 200
                                    });
                                }
                            }
                        },
                        marker: {
                            lineWidth: 1
                        }
                    }
                },

                series: [{
                    name: 'All sessions',
                    lineWidth: 4,
                    marker: {
                        radius: 4
                    }
                }, {
                    name: 'New users'
                }]
            });


        });
    </script>
@stop
