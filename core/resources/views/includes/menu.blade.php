<section class="sidebar">
	<!-- Sidebar user panel -->
	<div class="user-panel">
		<div class="pull-left image">
			<img src="{{url('assets/adminlte/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
		</div>
		<div class="pull-left info">
			<?php $name = $user->first_name.' '.$user->last_name  ?>
			<p>{{strlen($name) > 17 ? substr($name,0,17)."..." : $name}}</p>
			<a style="color: #f0f0f0;" href="{{url('admin/user/profile')}}">profile</a>
		</div>
	</div>


	<form action="{{url('admin/search')}}" method="get" class="sidebar-form">
		<div class="input-group">
			<input type="text" name="keyword" class="form-control" placeholder="Search...">
			<span class="input-group-btn">
				<button type="submit" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
			</span>
		</div>
	</form>

	<!-- sidebar menu: : style can be found in sidebar.less -->
	<ul class="sidebar-menu">
		<li class="header">MAIN NAVIGATION</li>
		@if(isset($menu))
			{!!$menu!!}
		@endif
		<li class="header">INQUIRY PRIORITY</li>
		<li>
			<a href="{{url('admin/inquiry/list')}}?status=0" style="padding: 4px 5px 4px 15px !important;">
				<i class="fa fa-circle-o" style="color:#3498db"></i> <span>ALL  </span>
			</a>
		</li>

		<li>
			<a href="{{url('admin/inquiry/list')}}?status=1" style="padding: 4px 5px 4px 15px !important;">
				<i class="fa fa-circle-o" style="color:#ff7800"></i> <span>PENDING  </span>
			</a>
		</li>

		<li>
			<a href="{{url('admin/inquiry/list')}}?status=2" style="padding: 4px 5px 4px 15px !important;">
				<i class="fa fa-circle-o" style="color:#2ecc71"></i> <span>APPROVED  </span>
			</a>
		</li>

		<li>
			<a href="{{url('admin/inquiry/list')}}?status=3" style="padding: 4px 5px 4px 15px !important;">
				<i class="fa fa-circle-o" style="color:#f61700"></i> <span>REJECT  </span>
			</a>
		</li>

	</ul>
	
</section>
