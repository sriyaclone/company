<div class="navbar-collapse collapse" id="navbar-collapse">
    <ul class="nav navbar-nav navbar-right">
        <li>
            <a href="/">
                Home
            </a>
        </li>
        <li>
            <a href="archive.html">
                Archive
            </a>
        </li>
        <li>
            <a href="browse.html">
                Browse
            </a>
        </li>
        <li>
            <a href="{{url('/login')}}">
                Login
            </a>
        </li>
        <li>
             <a href="{{url('/signup')}}" class="btn btn-success nav-btn">Sign Up</a>
        </li>
    </ul>
</div>