@extends('layouts.back_master') @section('title','Dashboard')
@section('current_title','Dashboard')

<link rel="stylesheet" type="text/css" src="{{asset('assets/dist/bootstrap-datepicker/css/bootstrap-datepicker.css')}}"/>
@section('css')
<style type="text/css">
    .stats .info-box-icon{
        background: none;
    }

    .stats .info-box-text{
        margin-top: 8px;
    }

    .stats .info-box-number{
        font-weight: 800;
    }

    .sector-performance a{
        padding: 5px 10px !important;
    }

    .dis-excced{
        color: red;
    }

    .btn{
        background: none;
    }

    .box-green{
      border-color: #00a65a;
    }

    .users-list-img {
        border-radius: 50%;
        max-width: 30%;
        height: auto;
    }

    .info-box-content{
        margin-left: 0px !important;
        color: #73879C;
        padding: 12px 10px;
        text-align: center;
        color: #ffffff;
    }

    .blue{
        background-color: #33B2FF;
    }

    .yellow{
        background-color: #FFC733;
    }

    .green{
        background-color: #239D0D;
    }

    .pink{
        background-color: #96228D;
    }

    .navy{
        background-color: #1C2EE2;
    }

    .icon-box{
        font-size: 17px;        
        font-weight: 600;        
    }

    .icon-padding{
        padding-right: 5px;        
    }

    .info-box-number{
        font-weight: 600;
        font-size: 21px;
    }
</style>  
@stop

@section('content')
<!-- Content-->
<section>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Welcome To
            <small>O|Lead</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>
    <!-- !!Content Header (Page header) -->

    <!-- Main content -->
    <section class="content" >
    <!-- Info boxes -->

    </section>

</section>
<!-- !!!Content -->

@stop
@section('js')
  <!-- CORE JS -->
    <script src="{{asset('assets/front/highchart.js')}}"></script>
    <script src="{{asset('assets/dist/jquery-knob/dist/jquery.knob.min.js')}}"></script>
    <script src="{{asset('assets/dist/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>

    <script type="text/javascript">

        
    </script>
  <!-- //CORE JS -->
@stop
