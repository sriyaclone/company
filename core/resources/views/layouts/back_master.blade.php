<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{config('app.app_name')}} | {{(isset($breadcrumbs))?$breadcrumbs['title']:'No Title'}}</title>

    <link href="{{asset('assets/inspinia/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/inspinia/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('assets/inspinia/material-icons/material-icons.css')}}" rel="stylesheet">
    <link href="{{asset('assets/inspinia/assets/select2/select2.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/inspinia/assets/ladda/ladda-themeless.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/inspinia/assets/toastr/toastr.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/inspinia/assets/sweetalert/css/sweet-alert.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/inspinia/css/plugins/multiselect/css/multi-select.css')}}">

    <link href="{{asset('assets/inspinia/assets/chosen/chosen.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/inspinia/assets/datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/inspinia/assets/file/css/fileinput.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/inspinia/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('assets/inspinia/css/style.css')}}" rel="stylesheet">
    <style>
        .required:after{
            content:' *';
            color:#e80909;
        }

        .has-error .select2-container--default .select2-selection--single, 
        .has-error .select2-container--default .select2-selection--multiple{
            border-color: #ed5565;
        }

        #toast-container > div{
            border-radius:0;
            -webkit-box-shadow:none;
            box-shadow:none;
        }

        .toast-success{
            background-color:#1ab373;
        }

        .toast-error{
            background-color:#de2a2a;
        }

        table h4{
            font-size: 20px;
            font-weight: normal;
        }

        table h6{
            font-size: 15px;
        }

        .table > tbody > tr > td{
            vertical-align:middle;
        }
    </style>
    @yield('styles-links')
    @yield('css')

</head>

<body class="fixed-sidebar">
    <div class="showbox">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
            </svg>
        </div>
    </div>
    <div id="wrapper">
        @include('layouts.include.sidebar')
        <div id="page-wrapper" class="gray-bg">
            @include('layouts.include.header')
            <!-- <div class="wrapper wrapper-content"> -->
            @yield('content')
            <!-- </div> -->
            @include('layouts.include.footer')
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="{{asset('assets/inspinia/js/jquery-3.1.1.min.js')}}"></script>
    <script src="{{asset('assets/inspinia/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/inspinia/js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
    <script src="{{asset('assets/inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/inspinia/assets/select2/select2.full.min.js')}}"></script>
    <script src="{{asset('assets/inspinia/assets/toastr/toastr.min.js')}}"></script>
    <script src="{{asset('assets/inspinia/assets/sweetalert/js/sweet-alert.min.js')}}"></script>
    <script src="{{asset('assets/inspinia/js/custom_functions.js')}}"></script>
    <script src="{{asset('assets/inspinia/assets/chosen/chosen.jquery.min.js')}}"></script>
    <script src="{{asset('assets/inspinia/assets/datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('assets/inspinia/assets/file/js/fileinput.min.js')}}"></script>
    <script src="{{asset('assets/inspinia/js/plugins/multiselect/js/jquery.multi-select.js')}}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{asset('assets/inspinia/js/inspinia.js')}}"></script>
    <script>
        $(document).ready(function(){
            $('.showbox').fadeOut();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(".select2").select2();
            $('button[type=submit]').click(function(){
                $('.showbox').show();
            });

            toastr.options = {
                "closeButton": true,
                "debug": false,
                "progressBar": false,
                "preventDuplicates": false,
                "positionClass": "toast-top-right",
                "onclick": null,
                "showDuration": "400",
                "hideDuration": "1000",
                "timeOut": "2500",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            @if(session('success'))
                @if(session('add'))
                    toastr.success('Record Successfully Added.', 'Good Job!');
                @elseif(session('edit'))
                    toastr.success('Record Successfully Updated.', 'Good Job!');
                @endif
            @elseif(session('error'))
                @if(session('add'))
                    toastr.error('Problem occured with adding data.', 'Oh Snap!');
                @elseif(session('edit'))
                    toastr.error('Problem occured with updating data.', 'Oh Snap!');
                @endif
            @endif

            @if(session('success'))
                sweetAlert('{{session('success.title')}}', '{!!session('success.message')!!}',0);
            @elseif(session('error'))
                sweetAlert('{{session('error.title')}}','{!!session('error.message')!!}',2);
            @elseif(session('warning'))
                sweetAlert('{{session('warning.title')}}','{!!session('warning.message')!!}',3);
            @elseif(session('info'))
                sweetAlert('{{session('info.title')}}','{!!session('info.message')!!}',1);
            @elseif(session('link'))
                sweetAlertLink('{{session('link.title')}}','{!!session('link.message')!!}','{!!session('link.link')!!}','{!!session('link.linktitle')!!}',0);
            @endif
        });
    </script>
    @yield('scripts-links')
    @yield('js')
</body>
</html>