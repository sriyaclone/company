<?php
namespace App\Classes\Contracts;


interface BaseStockHandler
{

    const STOCK_IN = 0;
    const FIRST_IN_FIRST_OUT = 1;
    const BATCH_OUT = 2;

    /**
     * Find a resource by id
     *
     * @param $product_id
     * @param $qty
     * @param $warehouse_id
     * @param $batch_id
     * @param $transaction_type_id
     * @param $mrp_id
     * @return Model|null
     */
    public function stockIn($product_id, $qty, $warehouse_id, $batch_id, $transaction_type_id, $mrp_id = 0);

    /**
     * Find a resource by criteria
     *
     * @param $product_id
     * @param $qty
     * @param $batch_id
     * @param $warehouse_id
     * @param $reference_id
     * @param $transaction_type_id
     * @param $type
     * @return Model|null
     */
    public function stockOut($product_id, $qty,$batch_id, $warehouse_id, $reference_id, $transaction_type_id, $type);
}