<?php
namespace App\Classes;

use App\Models\InquiryTransaction;
use App\Models\User;

use Core\EmployeeManage\Models\Employee;

use App\Modules\EmployeeManage\Models\AppUser;
use App\Modules\AdminDispatchManage\Models\DispatchManage;

class Functions
{

    //unixTimePhptoJava
    public static function unixTimePhpToJava($time)
    {
        $conTime = $time * 1000;
        return $conTime;
    }

    public static function unixTimeJavaToPhp($time)
    {
        $conTime = $time / 1000;
        return $conTime;
    }

    public static function formatDateToPhp($date)
    {
        $date_format = substr($date, 0, strlen($date) - 3);
        $time_format = substr($date, 13, strlen($date));
        return date("Y-m-d", strtotime(substr($date_format, 0, strlen($date_format) - 8))).' '.date("H:i:s", strtotime($time_format));
    }

     /**
     * user token check
     * @param $token
     * @return count of users
     *
     */
    public static function checkToken($token)
    {
        $appuser = AppUser::where('app_user.token', $token)->toSql();
        
        return count($appuser) > 0; //TRUE OR false
    }

     /**
     * get employee from mobile token
     * @param $token
     * @return object
     *
     */
    public static function getEmployeeFromToken($token)
    {
        $appuser = AppUser::where('token', $token)->first();
        $emp     = null;
        
        if(count($appuser)>0)
        {
            $emp = EmployeeManage::find($appuser->id);
        }

        return $emp;
    }

    public static function getEmployeeRepIds($employee_id){
        try{
            $pro = Employee::find($employee_id)->descendantsAndSelf()->get()->lists('id');
        }catch(\Exception $e){
            $pro = null;
        }
        return $pro;
    }

    //validate input
    public static function is_valid($request)
    {
        $request = trim($request);

        if(!empty($request) && isset($request))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static function set_transation($inquiry_id,$event_id,$reference)
    {
        InquiryTransaction::where('inquiry_id',$inquiry->id)->delete();
                
        $usr=User::find(Sentinel::getUser()->id);

        $trans=InquiryTransaction::create([
            'inquiry_id'    =>  $inquiry_id,
            'event_id'      =>  $event_id,
            'action_by'     =>  $usr->employee_id,
            'reference'     =>  $reference
        ]);

        if($trans){
            return true;
        }else{
            return false;
        }
    }

    public static function num($val){
        if($val <= 9){
            return "00000" . $val;
        }else if($val > 9 && $val <= 99){
            return "0000" . $val;
        }else if($val > 9 && $val > 99 && $val <= 999){
            return "000" . $val;
        }else if($val > 9 && $val > 99 && $val > 999 && $val <= 9999){
            return "00" . $val;
        }else if($val > 9 && $val > 99 && $val > 999 && $val > 9999 && $val <= 99999){
            return "0" . $val;
        }else if($val > 9 && $val > 99 && $val > 999 && $val > 9999 && $val > 99999 && $val <= 999999){
            return $val;
        }
    }

    public static function dispatchNo(){
            
        $last_id=DispatchManage::whereNull('deleted_at')->orderBy('id','DESC')->limit(1)->first();
        
        if($last_id){
            $num=$last_id->id+1;
            return 'DPT/'.self::num($num);
        }else{
            return 'DPT000001';
        }        
    }

}