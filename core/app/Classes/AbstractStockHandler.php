<?php
namespace App\Classes;

/**
 *
 * Overpayment Tranacstions
 *
 * @author Tharindu Lakshan<tahrinudmac@gmail.com>
 * @version 1.0.0
 * @copyright Copyright (c) 2015, Yasith Samarawickrama
 *
 */


use App\Classes\Contracts\BaseStockHandler;
use App\Models\Stock;
use App\Models\StockTransaction;
use App\Models\StockTransactionType;
use App\Models\StockType;

use App\Modules\Product\Models\Product;
use App\Modules\Warehouse\Models\Warehouse;
use DB;
use Exception;
use Illuminate\Support\Facades\Log;

abstract class AbstractStockHandler implements BaseStockHandler
{

}
