<?php
/**
 * Created by PhpStorm.
 * User: ACER
 * Date: 6/21/2018
 * Time: 9:36 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Customer extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'customer';


    // guard attributes from mass-assignment
    protected $guarded = array('id');

//    public function employee(){
//        return $this->belongsTo('Core\EmployeeManage\Models\Employee','employee_id','id');
//    }

}