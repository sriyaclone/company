<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * WarehouseEmployee Model Class
 *
 *
 * @category   Models
 * @package    Model
 * @author     sriya <csriyarathne@gmail.com>
 * @copyright  Copyright (c) 2015, Yasith Samarawickrama
 * @version    v1.0.0
 */
class WarehouseEmployee extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'warehouse_employee';
	
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = ['id'];

    public function warehouse()
    {
        return $this->HasMany('App\Models\Warehouse','id','warehouse_id');
    }

}
