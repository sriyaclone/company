<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * WarehouseVehicle Model Class
 *
 *
 * @category   Models
 * @package    Model
 * @author     sriya <csriyarathne@gmail.com>
 * @copyright  Copyright (c) 2015, Yasith Samarawickrama
 * @version    v1.0.0
 */
class WarehouseVehicle extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'warehouse_vehicle';
	
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = ['id'];	


	public function warehouse(){
		return $this->belongsTo('App\Models\Warehouse','warehouse_id','id');
	}

}
