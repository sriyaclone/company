<?php namespace App\Models;

//use Illuminate\Database\Eloquent\Model;
use Cartalyst\Sentinel\Users\EloquentUser as CartalystUser;

/**
 * User Model Class
 *
 *
 * @category   Models
 * @package    Model
 * @author     Yasith Samarawickrama <yazith11@gmail.com>
 * @copyright  Copyright (c) 2015, Yasith Samarawickrama
 * @version    v1.0.0
 */
class User extends CartalystUser{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['permissions', 'last_login','username', 'password', 'employee_id'];

	/**
	 * Login column names
	 *
	 * @var array
	 */
	protected $loginNames = ['email', 'username'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password'];

    public function emp(){
        return $this->belongsTo('Core\EmployeeManage\Models\Employee','employee_id','id');
    }

    public function sectors(){
        return $this->hasMany('App\Models\SectorUser');
    }

}
