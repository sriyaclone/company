@extends('layouts.back_master') @section('title','Add Employee Type')
@section('css')
	<style type="text/css">
		.panel.panel-bordered {
            border: 1px solid #ccc;
        }.chosen-container {
            font-family: 'FontAwesome', 'Open Sans', sans-serif;
            width: 100% !important;
        }b, strong {
            font-weight: bold;
        }.top{
            margin-top: 10px;
        }
	</style>
@stop
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Employee Type Management</h2>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{url('employee/type/list')}}">Employee Type Management</a></li>
            <li class="active">Employee Type Create</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a class="btn btn-primary" href="{{ url('employee/type/list') }}"><i class="fa fa-th" aria-hidden="true"></i> Type List</a>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Employee Type Create</h5>
            <div class="ibox-tools">
                
            </div>
        </div>

        <div class="ibox-content" style="display: block;">
        	<form role="form" class="form-horizontal form-validation" method="post">
	        {!!Form::token()!!}			
				
				<div class="row top">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<label class=" control-label required" >Name</label>
						<input type="text" class="form-control @if($errors->has('name')) error @endif" name="name" placeholder="Employee Type" required value="{{Input::old('name')}}">
            			@if($errors->has('name'))
            				<label id="label-error" class="error" for="label">{{$errors->first('name')}}</label>
            			@endif
					</div>
				</div>

				<div class="row top">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<label class=" control-label required" >Parent</label>
						{!! Form::select('parent',$parentList, Input::old('parent'),['class'=>'form-control chosen','style'=>'width:100%;','required','data-placeholder'=>'Choose parent','id'=>'parent']) !!}
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<button type="submit" class="pull-right btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
					</div>
				</div>

			</form>
        </div>
    </div>
</div>

@stop
@section('js')
<script type="text/javascript">
	$(document).ready(function(){
		$(".chosen").chosen();
	});
</script>
@stop