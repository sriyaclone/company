@extends('layouts.back_master') @section('title','Employee Type List')
@section('css')
<style type="text/css">

</style>
@stop
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Employee Type Management</h2>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{url('employee/type/list')}}">Employee Type Management</a></li>
            <li class="active">Employee Type List</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a class="btn btn-warning" href="{{ url('employee/type/add') }}"><i class="fa fa-plus" aria-hidden="true"></i> Type Create</a>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Employee Type List</h5>
            <div class="ibox-tools">
                
            </div>
        </div>

        <div class="ibox-content" style="display: block;">
        	<table class="table table-bordered">
      			<thead style="background:#ddd">
                  	<th class="text-center" width="2%">#</th>
                  	<th class="text-center">Name</th>
                  	<th class="text-center">Parent</th>
                  	<th class="text-center">Status</th>
                  	<th colspan="2" class="text-center" width="6%">Action</th>
      			</thead>
      			<tbody>
              		@foreach($data as $key=>$empType)
              			<tr>
	              			<td>{{$key+1}}</td>
	              			<td>{{$empType->name}}</td>
	              			<td>@if($empType->parentType!=Null){{$empType->parentType->name}}@endif</td>

	              			<td style="text-align: center">
		              			@if($empType->status==1)
					                <span class="label label-success" style="padding: .6em 10px .3em;">Activated</span>
					            @else
					                <span class="label label-danger" style="padding: .6em 10px .3em;">Deactivated</span>
					            @endif
					        </td>

					        <td>
					            @if(Sentinel::hasAnyAccess(['employee.type.status','admin']))
			              			@if($empType->status==1)
					                    <label class="switch switch-sm" data-toggle="tooltip" data-placement="top" title="Click to Deactivate"><input class="type-activate" type="checkbox" checked value="{{$empType->id}}"><span style="position:inherit;"><i class="handle" style="position:inherit;"></i></span></label>
					                @else
					                    <label class="switch switch-sm" data-toggle="tooltip" data-placement="top" title="Click to Activate"><input class="type-activate" type="checkbox" value="{{$empType->id}}"><span style="position:inherit;"><i class="handle" style="position:inherit;"></i></span></label>
					                @endif
					            @else
					                @if($empType->status==1)
					                    <label class="switch switch-sm" data-toggle="tooltip" data-placement="top" title="Deactivate Disabled"><input disabled class="type-activate" type="checkbox" checked value="{{$empType->id}}"><span style="position:inherit;"><i class="handle" style="position:inherit;"></i></span></label>
					                @else
					                    <label class="switch switch-sm" data-toggle="tooltip" data-placement="top" title="Activate Disabled"><input disabled class="type-activate" type="checkbox" value="{{$empType->id}}"><span style="position:inherit;"><i class="handle" style="position:inherit;"></i></span></label>
					                @endif
					            @endif
					        </td>

					        <td>
					            @if(Sentinel::hasAnyAccess(['employee.type.edit','admin']))
					                <a href="{{url('employee/type/edit')}}/{{$empType->id}}" class="blue" data-toggle="tooltip" data-placement="top" title="Edit Type"><i class="fa fa-pencil" style="color: blue"></i></a>
					            @else
					                <a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>
					            @endif
					        </td>

			            </tr>
              		@endforeach
              	</tbody>
      		</table>
      		<?php echo $data->appends(Input::except('page'))->render()?>
        </div>
    </div>
</div>

@stop
@section('js')
<script type="text/javascript">
	var id = 0;
	var table = '';
	$(document).ready(function(){
		
		$('.type-activate').change(function(){
			$('.panel').addClass('panel-refreshing');
			if($(this).prop('checked')==true){					
				ajaxRequest( '{{url('employee/type/status')}}' , { 'id' : $(this).val() , 'status' : 1 }, 'post', successFunc);
			}else{					
				ajaxRequest( '{{url('employee/type/status')}}' , { 'id' : $(this).val() , 'status' : 0 }, 'post', successFunc);
			}			
		});

	});

	function successFunc(data){
        if(data.status=='success'){
            $('.panel').removeClass('panel-refreshing');
            sweetAlert('Success','Action Successfully Done!',0);
    		window.location.reload();
        }else{
            sweetAlert('Error Occured','Please try again!',3);
        }
	}
</script>
@stop