@extends('layouts.back_master') @section('title','Employee List')
@section('css')
	<style type="text/css">
		.top{
			margin-top: 10px;
		}
	</style>
@stop
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Employee Management</h2>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{url('employee/list')}}">Employee Management</a></li>
            <li class="active">Employee List</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a class="btn btn-warning" href="{{ url('employee/add') }}"><i class="fa fa-plus" aria-hidden="true"></i> Employee Create</a>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Employee Create</h5>
            <div class="ibox-tools">
                
            </div>
        </div>

        <div class="ibox-content" style="display: block;">
        	<table class="table table-bordered">
      			<thead style="background:#ddd">
      				<form role="form" class="form-horizontal form-validation" method="get">
            		{!!Form::token()!!}
	      				<tr>
		                  	<th class="text-center" width="2%">#</th>
		                  	<th class="text-center">Type <br>
		                  		<select name="type" id="type" class="form-control chosen" required="required">
									<option value="0">-ALL-</option>
									@foreach($types as $type)
										<option value="{{$type->id}}" @if($type->id==$old['type']) selected @endif>{{$type->name}}</option>
									@endforeach
								</select>
		                  	</th>
		                  	<th class="text-center">Code <br>
		                  		<input type="text" name="code" id="code" class="form-control" value="{{$old['code']}}">
		                  	</th>
		                  	<th class="text-center">Name <br>
		                  		<input type="text" name="name" id="name" class="form-control" value="{{$old['name']}}">
		                  	</th>
		                  	<th class="text-center">NIC</th>
		                  	<th class="text-center">Address</th>
		                  	<th class="text-center">Mobile</th>
		                  	<th class="text-center">Email</th>
		                  	<th class="text-center">Superior</th>
							<th class="text-center">WareHouse</th>
		                  	<th colspan="1" class="text-center" width="2%">Action <br>
		                  		<button type="submit" class="btn btn-default btn-search">
									<i class="fa fa-search"></i> Search
								</button>
		                  	</th>
		                </tr>
		            </form>
      			</thead>
      			<tbody>
              		@foreach($data as $key=>$employee)
              			<tr>
	              			<td>{{$key+1}}</td>
	              			<td>{{$employee->type->name!=null?$employee->type->name:'-'}}</td>
	              			<td>{{$employee->code}}</td>
	              			<td>{{$employee->first_name}} {{$employee->last_name}}</td>
	              			<td>{{$employee->nic}}</td>
	              			<td>{{$employee->address}}</td>
	              			<td>{{$employee->mobile!=null?$employee->mobile:'-'}}</td>
	              			<td>{{$employee->email!=null?$employee->email:'-'}}</td>
	              			<td>@if($employee->parentEmployee){{$employee->parentEmployee->getFullNameAttribute()}}@endif</td>
							<td>@if(sizeof($employee->employeeHaswarehouse)){{$employee->employeeHaswarehouse[0]['warehouse'][0]['name']}}@endif</td>
	              			<td>
	              				@if(Sentinel::hasAnyAccess(['employee.edit','admin']))
					                <a href="{{url('employee/edit')}}/{{$employee->id}}" class="blue" data-toggle="tooltip" data-placement="top" title="Edit Employee"><i class="fa fa-pencil" style="color: #3F51B5"></i></a>
					            @else
					                <a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>
					            @endif
	              			</td>
			            </tr>
              		@endforeach
              	</tbody>
      		</table>
      		<div class="pull-right" style="margin-top: 10px">
      			<?php echo $data->appends(Input::except('page'))->render()?>
      		</div>
      		<div class="overlay" style="display:none;">
				<i class="fa fa-refresh fa-spin"></i>
			</div>
        </div>
    </div>
</div>

@stop
@section('js')
<script type="text/javascript">
	var id = 0;

	$(document).ready(function(){
		$('.employee-activate').change(function(){
			$('.overlay').show();
			if($(this).prop('checked')==true){					
				ajaxRequest( '{{url('employee/status')}}' , { 'id' : $(this).val() , 'status' : 1 }, 'post', successFunc);
			}else{					
				ajaxRequest( '{{url('employee/status')}}' , { 'id' : $(this).val() , 'status' : 0 }, 'post', successFunc);
			}
		});

		$('.employee-delete').click(function(e){
			e.preventDefault();
			id = $(this).data('id');
			sweetAlertConfirm('Delete Employee', 'Are you sure?',2, deleteFunc);
		});

		$('.varify-employee').click(function(e){
			e.preventDefault();
			id = $(this).data('id');
			sweetAlertConfirm('Varify Employee', 'Are you sure?',1, varifyFunc);
		});

		$('.reject-employee').click(function(e){
			e.preventDefault();
			id = $(this).data('id');
			sweetAlertConfirm('Reject Employee', 'Are you sure?',1, rejectFunc);
		});

	});

	/**
	 * Delete the Employee
	 * Call to the ajax request employee/delete.
	 */
	function deleteFunc(){
		ajaxRequest( '{{url('employee/delete')}}' , { 'id' : id  }, 'post', handleData);
	}

	/**
	 * Varify employee
	 * Call to the ajax request employee/varify.
	 */
	function varifyFunc(){
		$('.overlay').show();
		ajaxRequest( '{{url('employee/varify')}}' , { 'id' : id  }, 'post', handleDataVarify);
	}

	/**
	 * Reject Employee
	 * Call to the ajax request employee/reject.
	 */
	function rejectFunc(){
		$('.overlay').show();
		ajaxRequest( '{{url('employee/reject')}}' , { 'id' : id  }, 'post', handleDataReject);
	}

	/**
	 * Delete the employee return function
	 * Return to this function after sending ajax request to the employee/delete
	 */
	function handleData(data){
		if(data.status=='success'){
			sweetAlert('Delete Success','Record Deleted Successfully!',0);
		}else if(data.status=='invalid_id'){
			sweetAlert('Delete Error','Employee Id doesn\'t exists.',3);
		}else if(data.status=='assinged'){
			sweetAlert('Cannot Deactivate','Employee Already has Asset .',3);
		}else{
			sweetAlert('Error Occured','Please try again!',3);
		}
	}

	/**
	 * Varify the employee return function
	 * Return to this function after sending ajax request to the employee/varify
	 */
	function handleDataVarify(data){
		
		$('.overlay').hide();

		if(data.status=='success'){
			swal('Varification Success','Employee Verified Successfully!','success');
			window.location.reload();
		}else if(data.status=='invalid_id'){
			swal('Varification Error','Employee doesn\'t exists.','error');
		}else{
			swal('Error Occured','Please try again!','error');
		}		
	}

	/**
	 * Reject the employee return function
	 * Return to this function after sending ajax request to the employee/reject
	 */
	function handleDataReject(data){
		
		$('.overlay').hide();

		if(data.status=='success'){
			swal('Rejection Completed','Employee Rejected Successfully!','success');
			window.location.reload();
		}else if(data.status=='invalid_id_employee'){
			swal('Varification Error','Employee doesn\'t exists.','error');
		}else{
			swal('Error Occured','Please try again!','error');
		}		
	}

	function successFunc(data){
		$('.overlay').hide();
		if(data.status=='success'){
			sweetAlert('Success','Action Successfully Done!',0);
		}else if(data.status=='invalid_id'){
			sweetAlert('Delete Error','Employee Id doesn\'t exists.',3);
		}else if(data.status=='assinged'){
			sweetAlert('Cannot Deactivate','Employee Already has Asset .',3);
		}else{
			sweetAlert('Error Occured','Please try again!',3);
		}

		window.location.href = "{{url('employee/list')}}";
	}

    function  clear_filters() { //reset filters

        window.location.href = '{{url('employee/list')}}';
    }
</script>
@stop