@extends('layouts.back_master') @section('title','Edit Employee')
@section('css')
    <style type="text/css">
        .panel.panel-bordered {
            border: 1px solid #ccc;
        }

        .chosen-container {
            font-family: 'FontAwesome', 'Open Sans', sans-serif;
        }

        b, strong {
            font-weight: bold;
        }

        .top{
            margin-top: 10px;
        }

    </style>
@stop
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Employee Management</h2>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{url('employee/list')}}">Employee Management</a></li>
            <li class="active">Employee Edit</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a class="btn btn-primary" href="{{ url('employee/list') }}"><i class="fa fa-th" aria-hidden="true"></i> Employee List</a>
            <a class="btn btn-warning" href="{{ url('employee/add') }}"><i class="fa fa-plus" aria-hidden="true"></i> Employee Create</a>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Employee Edit</h5>
            <div class="ibox-tools">
                
            </div>
        </div>

        <div class="ibox-content" style="display: block;">
            <form role="form" class="form-horizontal form-validation" method="post" id="edit_form" name="edit_form">
            {!!Form::token()!!}
                <div class="row top">
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <input type="hidden" name="hidden_emp_id" id="hidden_emp_id" value="{{$employee->id}}">                        
                        <label class="required">Designation</label>
                        {!! Form::select('empType',$typeList, $employee->employee_type_id,['class'=>'form-control chosen','style'=>'width:100%;','required','data-placeholder'=>'Choose Employee Designation','id'=>'empType']) !!}
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">                            
                        <label class="required">First Name</label>
                        <input type="text" class="form-control @if($errors->has('fName')) error @endif" name="fName" placeholder="First Name" required value="{{$employee->first_name}}">
                        @if($errors->has('fName'))
                            <label id="label-error" class="error" for="label">{{$errors->first('fName')}}</label>
                        @endif
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">                        
                        <label class="required">Last Name</label>
                        <input type="text" class="form-control @if($errors->has('lName')) error @endif" name="lName" placeholder="Last Name" value="{{$employee->last_name}}" required>
                        @if($errors->has('lName'))
                            <label id="label-error" class="error" for="label">{{$errors->first('lName')}}</label>
                        @endif
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">                            
                        <label class="required">Code</label>
                        <input type="text" class="form-control @if($errors->has('code')) error @endif" name="code" placeholder="Employee Code" value="{{$employee->code}}" required>
                        @if($errors->has('code'))
                            <label id="label-error" class="error" for="label">{{$errors->first('code')}}</label>
                        @endif
                    </div>
                </div>

                <div class="row top">
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">                            
                        <label >NIC</label>
                        <input type="text" class="form-control @if($errors->has('nic')) error @endif" name="nic" placeholder="NIC" value="{{$employee->nic}}" >
                        @if($errors->has('nic'))
                            <label id="label-error" class="error" for="label">{{$errors->first('nic')}}</label>
                        @endif
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">                                                                
                        <label class="">Address </label>
                        <input type="text" class="form-control @if($errors->has('address')) error @endif" name="address" placeholder="Address" value="{{$employee->address}}">
                        @if($errors->has('address'))
                            <label id="label-error" class="error" for="label">{{$errors->first('address')}}</label>
                        @endif
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">                            
                        <label class="">Email</label>
                        <input type="text" class="form-control @if($errors->has('email')) error @endif" name="email" placeholder="Email" value="{{$employee->email}}">
                        @if($errors->has('email'))
                            <label id="label-error" class="error" for="label">{{$errors->first('email')}}</label>
                        @endif
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <label >Mobile </label>
                        <input type="text" class="form-control @if($errors->has('mobile')) error @endif" name="mobile" placeholder="Mobile" value="{{$employee->mobile}}" >
                       @if($errors->has('mobile'))
                             <label id="label-error" class="error" for="label">{{$errors->first('mobile')}}</label>
                       @endif
                    </div>
                </div>

                <div class="row top">
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <label class="required">Parent Employee</label>
                        {!! Form::select('parent',$parentList, $employee->parent,['class'=>'form-control chosen','style'=>'width:100%;','required','data-placeholder'=>'Choose employee parent','id'=>'parent']) !!}
                    </div>
                </div>
                <div class="row top">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-floppy-o"></i> Save</button>
                    </div>
                </div>
                <div class="overlay" style="display:none;">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>
            </form>
        </div>
    </div>
</div>

@stop
@section('js')
<script type="text/javascript">

    var type = $('select[name="empType"]');
    $(document).ready(function () {
        $(".chosen").chosen();        
        var type = $('select[name="empType"]');
        type.change(function (e) {
            changeParent();
        });
    });

    function changeParent() {
        
        $('.overlay').show();
        $('#parent').html("");
        $.ajax({
            url: "{{url('employee/getParent')}}",
            type: 'GET',
            data: {'type': type.val()},
            success: function(data) {                
                $.each(data,function(key,value){
                    $('#parent').append('<option value="'+key+'">'+value+'</option>');
                });
                $('#parent').trigger("chosen:updated");
                $('.overlay').hide();
            },error: function(data){

            }
        });
    }

</script>
@stop
