@extends('layouts.back_master') @section('title','Add Employee')
@section('css')
    <style type="text/css">
        .panel.panel-bordered {
            border: 1px solid #ccc;
        }
        .chosen-container {
            font-family: 'FontAwesome', 'Open Sans', sans-serif;
            width: 100% !important;
        }
        b, strong {
            font-weight: bold;
        }
        .top{
            margin-top: 10px;
        }
    </style>
@stop
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Employee Management</h2>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{url('employee/list')}}">Employee Management</a></li>
            <li class="active">Employee Create</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a class="btn btn-primary" href="{{ url('employee/list') }}"><i class="fa fa-th" aria-hidden="true"></i> Employee List</a>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Employee Create</h5>
            <div class="ibox-tools">
                
            </div>
        </div>

        <div class="ibox-content" style="display: block;">
            <form role="form" class="form-horizontal form-validation" method="post">
            {!!Form::token()!!}
            
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        
                        <label class="required">Designation</label>
                        @if($errors->has('empType'))
                            {!! Form::select('empType',$typeList, [],['class'=>'form-control chosen error','style'=>'width:100%;','required','data-placeholder'=>'Choose Employee Designation','id'=>'empType']) !!}
                            <label id="label-error" class="error" for="label">{{$errors->first('empType')}}</label>
                        @else
                            {!! Form::select('empType',$typeList, [],['class'=>'form-control chosen','style'=>'width:100%;','required','data-placeholder'=>'Choose Employee Designation','id'=>'empType']) !!}
                        @endif
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        
                        <label class="required">First Name</label>
                        <input type="text" class="form-control @if($errors->has('fName')) error @endif" name="fName" placeholder="First Name" required value="{{Input::old('fName')}}">
                        @if($errors->has('fName'))
                            <label id="label-error" class="error" for="label">{{$errors->first('fName')}}</label>
                        @endif
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        
                        <label class="required">Last Name</label>
                        <input type="text" class="form-control @if($errors->has('lName')) error @endif" name="lName" placeholder="Last Name" value="{{Input::old('lName')}}" required>
                        @if($errors->has('lName'))
                            <label id="label-error" class="error" for="label">{{$errors->first('lName')}}</label>
                        @endif
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        
                        <label class="required">Code</label>
                        <input type="text" class="form-control @if($errors->has('code')) error @endif" name="code" id="code" placeholder="Employee Code" value="{{Input::old('code')}}" required>
                        @if($errors->has('code'))
                            <label id="label-error" class="error" for="label">{{$errors->first('code')}}</label>
                        @endif
                    </div>

                </div>

                <div class="row">

                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        
                        <label style="margin-top: 6px">NIC</label>
                        <input type="text" class="form-control @if($errors->has('nic')) error @endif" name="nic" placeholder="NIC" value="{{Input::old('nic')}}" >
                        @if($errors->has('nic'))
                            <label id="label-error" class="error" for="label">{{$errors->first('nic')}}</label>
                        @endif

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                                            
                        <label class="" style="margin-top: 6px">Address </label>
                        <input type="text" class="form-control @if($errors->has('address')) error @endif" name="address" placeholder="Address" value="{{Input::old('address')}}">
                        @if($errors->has('address'))
                            <label id="label-error" class="error" for="label">{{$errors->first('address')}}</label>
                        @endif

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        
                        <label class="" style="margin-top: 6px">Email</label>
                        <input type="text" class="form-control @if($errors->has('email')) error @endif" name="email" placeholder="Email" value="{{Input::old('email')}}">
                        @if($errors->has('email'))
                            <label id="label-error" class="error" for="label">{{$errors->first('email')}}</label>
                        @endif

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        
                        <label  style="margin-top: 6px">Mobile </label>
                        <input type="text" class="form-control @if($errors->has('mobile')) error @endif" name="mobile" placeholder="Mobile" value="{{Input::old('mobile')}}" >
                        @if($errors->has('mobile'))
                            <label id="label-error" class="error" for="label">{{$errors->first('mobile')}}</label>
                        @endif

                    </div>

                </div>

                <div class="row">

                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <label class="required" style="margin-top: 6px">Parent Employee</label>
                        {!! Form::select('parent',$parentList, Input::old('parent'),['class'=>'form-control chosen','style'=>'width:100%;','required','data-placeholder'=>'Choose parent','id'=>'parent']) !!}
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <label  style="margin-top: 6px">WareHouse</label>
                        {!! Form::select('warehouse',$warehouses, [],['class'=>'form-control chosen','style'=>'width:100%;','required','data-placeholder'=>'Choose Warehouse','id'=>'warehouse','name'=>'warehouse']) !!}
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    </div>
                    
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <button type="submit" style="margin-top: 10%" id="submit" class="btn btn-default pull-right"><i class="fa fa-floppy-o"></i> Save</button>
                    </div>
                </div>

                <div class="overlay" style="display:none;">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>
                

            </form>
        </div>
    </div>
</div>

@stop
@section('js')
<script type="text/javascript">    
    var type = $('select[name="empType"]');    
    $(document).ready(function () {
        $(".chosen").chosen();
        type.change(function (e) {
            changeParent();
        });
    });

    /*
    * Load employee for new employee,
    * Load employee according to selected emp type
    */
    function changeParent() {        
        $('.panel').addClass('panel-refreshing');
        $('.overlay').show();
        $('#parent').html("");
        $.ajax({
            url: "{{url('employee/getParent')}}",
            type: 'GET',
            data: {'type': type.val()},
            success: function(data) {                
                $.each(data,function(key,value){
                    $('#parent').append('<option value="'+key+'">'+value+'</option>');
                });
                $('#parent').trigger("chosen:updated");
                $('.overlay').hide();
            },error: function(data){

            }
        });
    }

</script>
@stop
