<?php
namespace Core\EmployeeManage\Http\Requests;

use App\Http\Requests\Request;

class EmployeeRequest extends Request
{
    public function authorize(){
        return true;
    }

    public function rules(){
        $id = $this->id;
        // $repID = $this->uID;
        if($this->is('employee/add')){
            $rules = [
                'email'             => 'email|unique:dms_employee,email,'.$this->email,
                'short_code'       => 'required|unique:dms_employee,short_code,'.$this->code,
                'mobile'            => 'min:10|max:10',
                'nic'               => 'unique:dms_employee,nic,'.$this->nic,
            ];
        }else if($this->is('employee/edit/'.$id)){
            $rules = [
                'email'             => 'email|unique:dms_employee,email,'.$id,
                'short_code'              => 'required|unique:dms_employee,short_code,'.$id,
                'mobile'            => 'min:10|max:10',
                'nic'               => 'unique:dms_employee,nic,'.$id,
            ];
        }else if($this->is('employee/reset/reset-password')){
            $rules = [
                'user_name'         => 'unique:user,username,'.$id,
                'uName'             => 'unique:dms_app_user,username,'.$id,
                'mobile'            => 'min:10|max:10',
                'password'          => 'required|min:6|regex:/^((?=\S*?[A-Z])(?=\S*?[a-z])(?=\S*?[0-9]).{6,})\S$/',
                'confirm_password'  => 'required|same:password'
            ];
        }

        return $rules;
    }

}