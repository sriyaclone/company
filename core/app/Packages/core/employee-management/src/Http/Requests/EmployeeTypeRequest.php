<?php
namespace Core\EmployeeManage\Http\Requests;

use App\Http\Requests\Request;

class EmployeeTypeRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $id = $this->id;
        $rules = [
            'name' => 'required|unique:dms_employee_type,name,'.$id,
            'parent' => 'required'
        ];
        return $rules;
    }

}