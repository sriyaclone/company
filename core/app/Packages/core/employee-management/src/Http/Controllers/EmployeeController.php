<?php
namespace Core\EmployeeManage\Http\Controllers;

use App\Http\Controllers\Controller;

use Core\UserManage\Models\User;
use Core\Permissions\Models\Permission;

use Core\EmployeeManage\Http\Requests\EmployeeRequest;
use Core\EmployeeManage\Models\Employee;
use Core\EmployeeManage\Models\EmployeeType;
use Core\Sector\Models\Sector;
use App\Modules\AdminProductCategory\Models\AdminProductCategory;
use App\Models\ManagerHasProductCategory;
use Core\UserRoles\Models\UserRole;
use Core\UserManage\Models\RoleUsers;
use Illuminate\Http\Request;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use App\Models\Warehouse;
use App\Models\WarehouseEmployee;
use DB;
use Response;
use Sentinel;
use Hash;
use Reminder;

use App\Exceptions\TransactionException;

class EmployeeController extends Controller
{
    /**
     * Show the menu add screen to the user.
     *
     * @return Response
     */
    public function addView()
    {
        $empType = EmployeeType::where('status',1)->orderBy('parent')->get()->lists('name', 'id');

        $roles = UserRole::all()->lists('name','id');

        $tmp = EmployeeType::first();

        $parentList = Employee::where('depth','>=',0)
                ->where('status', 1)
                ->where('employee_type_id',(($tmp->parent)?$tmp->parent:$tmp->id))
                ->select(DB::raw('CONCAT(first_name," ",last_name) as name'), 'id')
                ->lists('name','id');
        $warehouse = Warehouse::all()->lists('name','id')->prepend('---- Select Warehouse ----',0);

        return view('employeeManage::employee.add')->with(['typeList' => $empType,'roles'=>$roles,'parentList'=>$parentList,'warehouses'=>$warehouse]);
    }

    /**
     * add new employee type data to database
     * @param $request
     * @return Redirect to type add
     */
    public function add(EmployeeRequest $request)
    {
        if ($request->get('empType') > 1) {
            $parent = Employee::find($request->get('parent'));
        } else {
            $parent = Employee::find(4);
        }

        try {
            DB::transaction(function () use ($request,$parent) {

                $employee = Employee::create([
                    'employee_type_id' => $request->get('empType'),
                    'first_name' => $request->get('fName'),
                    'last_name' => $request->get('lName'),
                    'code' => $request->get('code'),
                    'nic' => $request->get('nic'),
                    'address' => $request->get('address'),
                    'email' => $request->get('email'),
                    'mobile' => $request->get('mobile'),
                    'parent' => $request->get('parent')>0?$request->get('parent'):null,
                    'status' => 0
                ]);

                WarehouseEmployee::create([
                    'warehouse_id' => $request->get('warehouse'),
                    'employee_id' =>$employee->id,
                    'employee_type_id' => $request->get('empType')
                ]);

                if($employee){
                    if($request->get('parent')>0){
                        $employee->makeChildOf($parent);
                    }
                }else{
                    throw new TransactionException('Something wrong. Employee wasn\'t created', 100);
                }
            });
            return redirect('employee/add')->with(['success' => true,
                'success.message' => 'Employee added successfully!',
                'success.title' => 'Well Done!']);
        } catch (TransactionException $e) {
            if ($e->getCode() == 100) {
                return redirect('employee/add')->with(['error' => true,
                    'success.message' => $e->getMessage(),
                    'success.title' => 'Error!']);
            }else if ($e->getCode() == 101) {
                return redirect('employee/add')->with(['error' => true,
                    'success.message' => $e->getMessage(),
                    'success.title' => 'Error!']);
            }
        } catch (Exception $e) {
            return redirect('employee/add')->with(['error' => true,
                    'success.message' => $e->getMessage(),
                    'success.title' => 'Error!']);
        }
    }

    /**
     * Show the menu list screen to the user.
     *
     * @return Response
     */
    public function listView(Request $request)
    {
            
        // return $request->all();
        $data    = Employee::whereNull('deleted_at')->with(['parentEmployee','type','employeeHaswarehouse.warehouse']);
        $types = EmployeeType::orderBy('parent')->get();

        $code = $request->code;
        if($code!=null){
            $data  = $data->where('short_code','like','%'.$code.'%');
        }

        $old_status = $request->status;
        if($old_status!=0 && $old_status!=3){
            $data  = $data->where('varified_status',$old_status);
        }

        $name = $request->name;
        if($name!=null){
            $data  = $data->where('first_name','like','%'.$name.'%')
                          ->orWhere('last_name','like','%'.$name.'%');
        } 

        $keyword = $request->keyword;
        if($keyword!=null){
            $data  = $data->where('address','like','%'.$keyword.'%')
                          ->orWhere('mobile','like','%'.$keyword.'%')
                          ->orWhere('email','like','%'.$keyword.'%');
        } 

        $type = $request->type;
        if($type>0){
            $data  = $data->where('employee_type_id',$type);
        }

        $data = $data->paginate(10);
        $count=$data->total();

        $status=array(3=>'All',1=>'Varified',2=>'Rejected',0=>'Not Varified');

        return view('employeeManage::employee.list')->with([
            'data'    => $data,
            'types' => $types,
            'row_count'=>$count,
            'status' => $status,
            'old'     => ['code'=>$code,'name'=>$name,'keyword'=>$keyword,'type'=>$type,'status'=>$old_status]
        ]);
    }

    /**
     * Show the employee list data to the user.
     *
     * @return Response
     */
    public function jsonList(Request $r)
    {
        $data = Employee::where('parent','>',0)->with(['parentEmployee','type'])->get();
        $jsonList = array();
        $i=1;
        foreach ($data as $employee) {
            $dd= array();
            array_push($dd,$i);

            array_push($dd,$employee->first_name);
            array_push($dd,$employee->last_name);
            array_push($dd,$employee->code);
            array_push($dd,$employee->address);
            array_push($dd,$employee->email);
            array_push($dd,$employee->mobile);
            array_push($dd,$employee->type->name);
            array_push($dd,$employee->parentEmployee->getFullNameAttribute());

            if($employee->status==1){                 
                array_push($dd, '<span class="label label-success" style="padding: .6em 10px .3em;">Activated</span>');
            }else{
                array_push($dd, '<span class="label label-danger" style="padding: .6em 10px .3em;">Deactivated</span>');
            }

            if($employee->status==1){
                if(Sentinel::hasAnyAccess(['employee.edit','admin'])){
                    array_push($dd, '<label class="switch switch-sm" data-toggle="tooltip" data-placement="top" title="Click to Deactivate"><input class="employee-activate" type="checkbox" checked value="'.$employee->id.'"><span style="position:inherit;"><i class="handle" style="position:inherit;"></i></span></label>');
                }else{
                    array_push($dd, '<label class="switch switch-sm" data-toggle="tooltip" data-placement="top" title="Click to Deactivate"><input disabled class="employee-activate" type="checkbox" checked value="'.$employee->id.'"><span style="position:inherit;"><i class="handle" style="position:inherit;"></i></span></label>');
                }
            }else{
                array_push($dd, '<label class="switch switch-sm" data-toggle="tooltip" data-placement="top" title="Click to Activate"><input class="employee-activate" type="checkbox" value="'.$employee->id.'"><span style="position:inherit;"><i class="handle" style="position:inherit;"></i></span></label>');
            }

            if (Sentinel::hasAnyAccess(['employee.edit','admin'])) {
                array_push($dd, '<a href="#" class="blue" onclick="window.location.href=\'' . url('employee/edit/' . $employee->id) . '\'" data-toggle="tooltip" data-placement="top" title="Edit Type" style="background: #3F51B5;padding: 5px;border-radius: 2px;"><i class="fa fa-pencil"></i></a>');
            } else {
                array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
            }

            if (Sentinel::hasAnyAccess(['employee.type.delete', 'admin'])) {
                array_push($dd, '<a href="#" class="red employee-delete" data-id="' . $employee->id . '" data-toggle="tooltip" data-placement="top" title="Delete Type" style="background: #CC1A6C;padding: 5px;border-radius: 2px;"><i class="fa fa-trash-o"></i></a>');
            } else {
                array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
            }


            array_push($jsonList, $dd);
            $i++;

        }
        return Response::json(array('data' => $jsonList));
    }

    /**
     * Activate or Deactivate Employee
     * @param  Request $request employee id with status to change
     * @return Json            json object with status of success or failure
     */
    public function status(Request $request)
    {
        if ($request->ajax()) {
            $id = $request->input('id');
            $status = $request->input('status');
            $employee = Employee::find($id);

            if ($employee) {
                
                if ($status == 0) {
                    
                    $employee->status = $status;
                    $employee->save();
                    
                    $web_user = User::where('employee_id', $id)->first();

                    if ($web_user) {
                        $user = Sentinel::findById($web_user->id);
                        
                        $update_user = User::find($web_user->id);
                        $update_user->status = $status;
                        $update_user->save();
                        
                        Activation::remove($user);
                    }

                }else{

                    $employee->status = $status;
                    $employee->save();
                    
                    $web_user = User::where('employee_id', $id)->first();

                    if ($web_user) {
                        $user = Sentinel::findById($web_user->id);
                        
                        $update_user = User::find($web_user->id);
                        $update_user->status = $status;
                        $update_user->save();
                                                
                        $acUser = Activation::create($user);
                        Activation::complete($user, $acUser->code);                        
                    }

                }                

                return response()->json(['status' => 'success']);
            } else {
                return response()->json(['status' => 'invalid_id']);
            }
        } else {
            return response()->json(['status' => 'not_ajax']);
        }
    }

    /**
     * Delete a Employee
     * @param  Request  employee id
     * @return Json     json object with status of success or failure
     */
    public function delete(Request $request)
    {
        if($request->ajax()){
            $id = $request->input('id');
            $employee = Employee::find($id);
            if($employee){
       
                $employee->delete();

                $web_user = User::where('employee_id', $id)->first();
                if ($web_user) {
                    $user = Sentinel::findById($web_user->id);
                    
                    $update_user = User::find($web_user->id);
                    $update_user->status = 0;
                    $update_user->save();
                    
                    Activation::remove($user);

                    $web_user->delete();
                    
                }

                return response()->json(['status' => 'success']);

            }else{
                return response()->json(['status' => 'invalid_id']);
            }
        }else{
            return response()->json(['status' => 'not_ajax']);
        }
    }

    /**
     * Varify a Employee
     * @param  Request  employee id
     * @return Json     json object with status of success or failure
     */
    public function varify(Request $request)
    {
        if($request->ajax()){
            $id = $request->input('id');
            $employee = Employee::find($id);
            if($employee){

                $employee->varified_status=1;
                $employee->varified_by=Sentinel::getUser()->id;
                $employee->save();
                
                return response()->json(['status' => 'success']);

            }else{
                return response()->json(['status' => 'invalid_id']);
            }
        }else{
            return response()->json(['status' => 'not_ajax']);
        }
    }

    /**
     * Reject a Employee
     * @param  Request  employee id
     * @return Json     json object with status of success or failure
     */
    public function reject(Request $request)
    {
        if($request->ajax()){
            $id = $request->input('id');
            $employee = Employee::find($id);
            if($employee){

                $employee->varified_status=2;
                $employee->varified_by=Sentinel::getUser()->id;
                $employee->save();
                
                return response()->json(['status' => 'success']);

            }else{
                return response()->json(['status' => 'invalid_id']);
            }
        }else{
            return response()->json(['status' => 'not_ajax']);
        }
    }

    /**
     * Show the menu edit screen to the user.
     * @param type id
     * @return Response
     */
    public function editView($id)
    {
        $ware_id='';
        $employee = Employee::with(['type'])->find($id);
        $warehouseemployee = WarehouseEmployee::where('employee_id',$employee->id)->get();
        if(sizeof($warehouseemployee) > 0){
            $ware_id = $warehouseemployee[0]->warehouse_id;
        }
        $empType = EmployeeType::whereNotIn('status',[0])->orderBy('parent')->get()->lists('name', 'id')->prepend('---- Select Employee Type ----',0);

        $type = EmployeeType::find($employee->employee_type_id);

        $warehouses = Warehouse::get()->lists('name', 'id')->prepend('---- Select Warehouse ----',0);

        $parentList = Employee::where('employee_type_id',$type->parent)
                ->where('status', 1)
                ->select(DB::raw('CONCAT(first_name," ",last_name) as name'), 'id')
                ->lists('name','id');

        return view('employeeManage::employee.edit')->with([
            'employee'                  => $employee, 
            'typeList'                  => $empType,
            'parentList'                => $parentList,
            'warehouses'                => $warehouses,
            'ware_id'                   =>$ware_id
        ]);
    }

    /**
     * Add new type data to database
     *
     * @return Redirect to menu edit
     */
    public function edit(EmployeeRequest $request, $id)
    {
        try {
            DB::transaction(function () use ($request,$id) {

                $employee=Employee::find($id);

                if($employee){
                    $employee->first_name=$request->get('fName');
                    $employee->last_name = $request->get('lName');
                    $employee->short_code = $request->get('short_code');
                    $employee->nic = $request->get('nic');
                    $employee->email = $request->get('email');
                    $employee->mobile = $request->get('mobile');
                    $employee->address = $request->get('address');
                    $employee->employee_type_id = $request->get('empType');
                    $employee->save();
                    
                    if(($request->get('parent') != $employee->id) && ($request->get('parent') != $employee->parent)){
                        $empParent = Employee::find($request->get('parent'));
                        $employee->makeChildOf($empParent);
                    }

                }else{
                    throw new TransactionException('Something wrong.Employee wasn\'t created', 100);
                }
            });
            return redirect('employee/list')->with(['success' => true,
                'success.message' => 'Employee Edited successfully!',
                'success.title' => 'Well Done!']);
        } catch (TransactionException $e) {
            Log::info($e);
            return redirect('employee/edit/'.$id)->with(['error' => true,
                'error.message' => 'Transaction Error',
                'error.title' => 'Ops!']);

        } catch (Exception $e) {
            Log::info($e);
            return redirect('employee/edit/'.$id)->with(['error' => true,
                'error.message' => 'Transaction Error',
                'error.title' => 'Ops!']);
        }
    }

    /**
     * Load employee for Reset Password view of Web Account
     *
     * @return Response Json List of employee
     */
    public function loadEmployee(Request $request)
    {   
        if($request->ajax()){
            $employee = Employee::where('status', 1)->where('employee_type_id', $request->get('type'))->select(DB::raw('CONCAT(first_name," ",last_name) as name'), 'id')->lists('name','id')->prepend('---- Select Employee Employee ----','0');
            return response()->json($employee);
        }else{
            return response()->json([]);
        }
        
    }

    /**
     * Reset Password view of Web Account
     *
     * @return Redirect to Web Account Password Reset View
     */
    public function viewPasswordReset(Request $request)
    {
        $empType = EmployeeType::orderBy('parent')->get()->lists('name', 'id')->prepend('---- Select Employee Type ----','0');
        $employee = Employee::where('status', 1)->select(DB::raw('CONCAT(first_name," ",last_name) as name'), 'id')->lists('name','id')->prepend('---- Select Employee Employee ----','0');

        $logged_user=Sentinel::getUser();

        $reset_multiple_user="";

        if($logged_user->employee[0]->employee_type_id==1){
            $reset_multiple_user=true;
        }else{
            $reset_multiple_user=false;
        }

        $logged_user_type_id=$logged_user->employee[0]->employee_type_id;
        $logged_user_id=$logged_user->employee[0]->id;

        return view('employeeManage::reset.reset-password')->with(['empTypeList' => $empType,'employeeList' => $employee,'logged_employee_type'=>$logged_user_type_id,'logged_employee'=>$logged_user_id,'reset_multiple'=>$reset_multiple_user]);
    }

    /**
     * Load user credential Detail to view of Reset Password View
     *
     * @return Response JSON List of credential Detail
     */
    public function loadCredentialData(Request $request)
    {        
        if($request->ajax()){            
            $user = User::where('employee_id',$request->get('employee'))->get();
            return response()->json(['user' => $user]);            
        }else{
            return response()->json([]);
        }
    }

    /**
     * Reset Web Account Password data to database
     *
     * @return Response
     */
    public function passwordReset(Request $request)
    {
        try {
            DB::transaction(function () use ($request) {

                $data=$request->get('detail');                

                if($data['web_password']){
                    if($data['web_password'] != null || $data['web_password'] != ""){

                        $user = User::where('employee_id',$request->get('employee'))->first();

                        $user = Sentinel::findById($user->id);

                        $rem_object= Reminder::create($user);

                        if($rem_object){
                            $new_password = Reminder::complete($user, $rem_object->code, $data['web_password']);
                        }else{
                            throw new TransactionException('Something wrong.Dealer wasn\'t created', 100);
                        }
                    }
                }
            });
            return Response::json([1]);
        } catch (TransactionException $e) {
            if ($e->getCode() == 100) {
                return Response::json([0]);
            }else if ($e->getCode() == 101) {
                return Response::json([0]);
            }
        } catch (Exception $e) {
            return Response::json([0]);
        }
    }

    /**
     * Add new type data to database
     *
     * @return Redirect to menu edit
     */
    public function filter(Request $request)
    {
        $filterArr = json_decode($request->get('data'));
        $cur = $request->get('currentPage');
        $size = $request->get('pageSize');
        $skip = ($cur * $size) - ($size);
        $count = 0;
        $sql = 'SELECT emp.first_name,emp.last_name,emp.email,emp_type.type,emp.id,emp.mobile,emp.address FROM dimo_employee as emp INNER  JOIN dimo_employee_type as emp_type ON emp.employee_type_id=emp_type.id ';
        if ($filterArr[0]->value != "") {
            $sql .= 'WHERE ';
            for ($i = 0; $i < count($filterArr); $i++) {
                if ($i > 0) {
                    $sql .= ' AND ';
                }
                $sql .= $filterArr[$i]->name . ' LIKE "%' . $filterArr[$i]->value . '%" ';
            }
            $emp = DB::select($sql);
            $count = count($emp);
            $sql .= ' LIMIT ' . $skip . ',' . ($cur * $size);
            $emp = DB::select($sql);
        } else {
            $emp = Employee::join('dimo_employee_type as type', 'type.id', '=', 'employee_type_id')
                ->select('dimo_employee.first_name', 'dimo_employee.last_name', 'dimo_employee.email', 'dimo_employee_type.type', 'dimo_employee.id', 'dimo_employee.mobile', 'dimo_employee.address')
                ->take($size)
                ->skip($skip)
                ->get();
            $count = Employee::count();
        }
        return response()->json(array('data' => $emp, 'count' => $count));
    }

    /**
     * get data of for selected id
     * @param Request $request location type id
     * @return data object
     */
    public function getViewData(Request $request)
    {

        $employer = Employee::find($request->get('id'));
        if ($employer->type->type == 'Rep') {
            $employer = Employee::join('dimo_rep as r', 'r.employee_id', '=', 'dimo_employee.id')
                ->find($request->get('id'));
            $arr = array(
                'first_name' => $employer->first_name,
                'last_name' => $employer->last_name,
                'email' => $employer->email,
                'type' => $employer->type->type,
                'mobile_user_name' => $employer->mobile_user_name,
                'short_code' => $employer->short_code,
                'mobile' => $employer->mobile,
                'address' => $employer->address,
                'parent' => count($employer->parent()->get()) > 0 ? $employer->parent()->get()[0]->full_name : ''

            );
            $employee = ['first_name', 'last_name', 'address', 'email', 'mobile', 'type', 'parent', 'mobile_user_name', 'short_code'];
            return response()->json(array($arr, $employee));
        }elseif($employer->type->id == 6){
            $employer = Employee::join('dimo_msr as r', 'r.employee_id', '=', 'dimo_employee.id')
                ->find($request->get('id'));
            $arr = array(
                'first_name' => $employer->first_name,
                'last_name' => $employer->last_name,
                'email' => $employer->email,
                'type' => $employer->type->type,
                'mobile_user_name' => $employer->mobile_user_name,
                'mobile' => $employer->mobile,
                'address' => $employer->address,
                'parent' => count($employer->parent()->get()) > 0 ? $employer->parent()->get()[0]->full_name : ''

            );
            $employee = ['first_name', 'last_name', 'address', 'email', 'mobile', 'type', 'parent', 'mobile_user_name'];
            return response()->json(array($arr, $employee));
        }
        $arr = array(
            'first_name' => $employer->first_name,
            'last_name' => $employer->last_name,
            'mobile' => $employer->mobile,
            'address' => $employer->address,
            'email' => $employer->email,
            'type' => $employer->type->type,
            'parent' => count($employer->parent()->get()) > 0 ? $employer->parent()->get()[0]->full_name : ''

        );
        $employee = ['first_name', 'last_name', 'address', 'email', 'mobile', 'type', 'parent'];
        return response()->json(array($arr, $employee));
    }

    /**
     * get location list of filter the type
     * @param Request $request location type id
     * @return location list
     */
    public function getLocation(Request $request)
    {
        if ($request->get('type') > 0) {
            $location = Location::where('type', $request->get('type'))->orderBy('name','ASC')->lists('name','id');            
            return response()->json($location);
        }else{
            return response()->json([]);
        }
    }

    /**
     * get employee list of filter the type
     * @param Request $request employee type id
     * @return parent list
     */
    public function getParent(Request $request)
    {
        $empType=EmployeeType::find($request->get('type'));

        $employee = Employee::where('employee_type_id',(($empType->parent)?$empType->parent:$empType->id))
                ->orderBy('first_name','ASC')
                ->select(DB::raw('CONCAT(first_name," ",last_name) as name'), 'id')
                ->lists('name','id')->prepend('No Parent',0);
     
        return response()->json($employee);
    }

}