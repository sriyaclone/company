@extends('layouts.back_master') @section('title','Menu List')
@section('css')
<link rel="stylesheet" href="{{asset('assets/inspinia/css/plugins/datatables/css/datatables/dataTables.bootstrap.css')}}">
<style type="text/css">	

</style>
@stop
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Menu Management</h2>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{url('menu/list')}}">Menu Management</a></li>
            <li class="active">Menu List</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a class="btn btn-warning" href="{{ url('menu/add') }}"><i class="fa fa-plus" aria-hidden="true"></i> Menu Create</a>
        </div>
    </div>
</div>

<!-- Main content -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Menu List</h5>
            <div class="ibox-tools">
                
            </div>
        </div>

        <div class="ibox-content" style="display: block;">
        	<table class="table table-bordered bordered table-striped table-condensed datatable">
              	<thead>
	                <tr>
	                  	<th rowspan="2" class="text-center" width="4%">#</th>
	                  	<th rowspan="2" class="text-center" width="18%" style="font-weight:normal;">Label</th>
	                  	<th rowspan="2" class="text-center" style="font-weight:normal;">Link</th>
	                  	<th rowspan="2" class="text-center" width="5%" style="font-weight:normal;">Icon</th>
	                  	<th rowspan="2" class="text-center" style="font-weight:normal;">Parent</th>
	                  	<th rowspan="2" style="font-weight:normal;">Permissions</th>
	                  	<th rowspan="2" class="text-center" width="6%" style="font-weight:normal;">Status</th>
	                  	<th colspan="2" class="text-center" width="4%" style="font-weight:normal;">Action</th>
	                </tr>
	                <tr style="display: none;">
	                	<th style="display: none;" width="2%"></th>
	                	<th style="display: none;" width="2%"></th>
	                </tr>
              	</thead>
            </table>
        </div>
    </div>
</div>

@stop
@section('js')

<!-- datatables -->
<script src="{{asset('assets/inspinia/js/plugins/datatables/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/inspinia/js/plugins/datatables/js/dataTables.bootstrap.min.js')}}"></script>

<script type="text/javascript">
	var id = 0;
	var table = '';
	$(document).ready(function(){
		table = generateTable('.datatable', '{{url('menu/json/list')}}',[3,5,6,7,8],[0,2,3,6,7,8],[],[],[0,"asc"]);

		table.on('draw.dt',function(){
			$("[data-toggle=tooltip]").tooltip();

			$('.menu-activate').change(function(){
				if($(this).prop('checked')==true){
					ajaxRequest( '{{url('menu/status')}}' , { 'id' : $(this).val() , 'status' : 1 }, 'post', successFunc);
				}else{
					ajaxRequest( '{{url('menu/status')}}' , { 'id' : $(this).val() , 'status' : 0 }, 'post', successFunc);
				}
			});

			$('.menu-delete').click(function(e){
				e.preventDefault();
				id = $(this).data('id');
				sweetAlertConfirm('Delete Menu', 'Are you sure?',2, deleteFunc);
			});
		});
	});

	/**
	 * Delete the menu
	 * Call to the ajax request menu/delete.
	 */
	function deleteFunc(){
		ajaxRequest( '{{url('menu/delete')}}' , { 'id' : id  }, 'post', handleData);
	}

	/**
	 * Delete the menu return function
	 * Return to this function after sending ajax request to the menu/delete
	 */
	function handleData(data){
		if(data.status=='success'){
			sweetAlert('Delete Success','Record Deleted Successfully!',0);
			table.ajax.reload();
		}else if(data.status=='invalid_id'){
			sweetAlert('Delete Error','Menu Id doesn\'t exists.',3);
		}else{
			sweetAlert('Error Occured','Please try again!',3);
		}
	}

	function successFunc(data){
		table.ajax.reload();
	}
</script>
@stop
