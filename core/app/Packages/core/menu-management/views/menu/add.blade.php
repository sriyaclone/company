@extends('layouts.back_master') @section('title','Add Menu')
@section('css')
	<style type="text/css">
		.ms-container {
	    	background: transparent url("{{asset('assets/inspinia/css/plugins/multiselect/img/switch3.png')}}") no-repeat 50% 50%;
	    	width: 100%;
		}
	</style>
@stop
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Menu Management</h2>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{url('menu/list')}}">Menu Management</a></li>
            <li class="active">Menu Create</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a class="btn btn-primary" href="{{ url('menu/list') }}"><i class="fa fa-th" aria-hidden="true"></i> Menu List</a>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Menu Create</h5>
            <div class="ibox-tools">
                
            </div>
        </div>

        <div class="ibox-content" style="display: block;">
        	<form role="form" class="form-validation" method="post">
          	{!!Form::token()!!}
	          	<div class="row">
	          		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
	          			<div class="form-group">
		            		<label class="control-label required">Menu Label</label>
	            			<input type="text" class="form-control @if($errors->has('label')) error @endif" name="label" placeholder="Menu Label" required value="{{Input::old('label')}}">
	            			@if($errors->has('label'))
	            				<label id="label-error" class="error" for="label">{{$errors->first('label')}}</label>
	            			@endif
		                </div>
	          		</div>

	          		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
	          			<div class="form-group">
		            		<label class="control-label">Menu Icon</label>
		            		<div style="font-family: fontAwesome;">
		            			{!! Form::select('menu_icon', $fonts, Input::old('menu_icon'),['class'=>'chosen','style'=>'width:100%;font-family:\'FontAwesome\'','data-placeholder'=>'Choose Icon']) !!}
		            		</div>
		                </div>
	          		</div>

	          		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
	          			<div class="form-group">
		            		<label class="control-label required">Menu URL</label>
	            			<input type="text" class="form-control @if($errors->has('menu_url')) error @endif" name="menu_url" placeholder="Ex: menu/list" value="{{Input::old('menu_url')}}" required>
	            			@if($errors->has('menu_url'))
	            				<label id="label-error" class="error" for="label">{{$errors->first('menu_url')}}</label>
	            			@endif
		                </div>
	          		</div>

	          		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
		          		<div class="form-group">
		          			<label class="control-label required">Menu Parent</label>
		        			@if($errors->has('parent_menu'))
		        				{!! Form::select('parent_menu',$menus, Input::old('parent_menu'),['class'=>'chosen error','style'=>'width:100%;','required','data-placeholder'=>'Choose Parent Menu']) !!}
		        				<label id="label-error" class="error" for="label">{{$errors->first('parent_menu')}}</label>
		        			@else
		        				{!! Form::select('parent_menu',$menus, Input::old('parent_menu'),['class'=>'chosen','style'=>'width:100%;','required','data-placeholder'=>'Choose Parent Menu']) !!}
		        			@endif
		        		</div>	
	          		</div>

	          		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	          			<div class="form-group">
		            		<label class="control-label">Menu Order</label>
	            			@if($errors->has('menu_order'))
	            				{!! Form::select('menu_order',$menus, Input::old('menu_order'),['class'=>'chosen error','style'=>'width:100%;','required','data-placeholder'=>'Set After']) !!}
	            				<label id="label-error" class="error" for="label">{{$errors->first('menu_order')}}</label>
	            			@else
	            				{!! Form::select('menu_order',$menus, Input::old('menu_order'),['class'=>'chosen','style'=>'width:100%;','required','data-placeholder'=>'Set After']) !!}
	            			@endif
		                </div>
	          		</div>

	          		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	          			<div class="form-group">
		            		<label class="control-label required">Permissions</label>
	            			@if($errors->has('permissions[]'))
	            				{!! Form::select('permissions[]',$permissionArr, null,['class'=>'error', 'multiple','id'=>'permissions','style'=>'width:100%;','required']) !!}
	            				<label id="label-error" class="error" for="label">{{$errors->first('permissions[]')}}</label>
	            			@else
	            				{!! Form::select('permissions[]',$permissionArr, Input::old('permissions[]'),['multiple','id'=>'permissions','style'=>'width:100%;','required']) !!}
	            			@endif
		                </div>
	          		</div>	          		
	          	</div>

	            <div class="row">
	              	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                	<button type="submit" class="btn btn-primary pull-right">
	                		<i class="fa fa-floppy-o"></i> Save
	                	</button>
              	  	</div>  	
                </div> 
       		</form>
        </div>
    </div>
</div>

@stop
@section('js')
	<script type="text/javascript">
		$(document).ready(function() {
	  		$(".chosen").chosen();
	  		$('#permissions').multiSelect();
		});
	</script>
@stop
