@extends('layouts.back_master') @section('title','Add Menu')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
<style type="text/css">
b, strong {
    font-weight: bold;
}
.repeat:hover {
    background-color:#EFEFEF;
}
#scrollArea {
    height: 200px;
    overflow: auto;
}
.switch.switch-sm{
    width: 30px;
    height: 16px;
}
.switch :checked + span {

    border-color: #689A07;
    -webkit-box-shadow: #689A07 0px 0px 0px 21px inset;
    -moz-box-shadow: #2ecc71 0px 0px 0px 21px inset;
    box-shadow: #689A07 0px 0px 0px 21px inset;
    -webkit-transition: border 300ms, box-shadow 300ms, background-color 1.2s;
    -moz-transition: border 300ms, box-shadow 300ms, background-color 1.2s;
    -o-transition: border 300ms, box-shadow 300ms, background-color 1.2s;
    transition: border 300ms, box-shadow 300ms, background-color 1.2s;
    background-color: #689A07;

}

.bootstrap-tagsinput.error{
    border-color: #d96557;
}
    
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
    Permssion Group 
    <small> Management</small>
    </h1>
    <ol class="breadcrumb">
    <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
        <li><a href="{{{url('permission/groups/list')}}}">Groups List</a></li>
        <li class="active">Add Group</li>
    </ol>
</section>

<!-- Main content -->
<section class="content" ng-app="permissionApp" ng-controller="permissionController">
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Add Group</h3>
            <div class="box-tools pull-right">
                <!-- <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> -->
            </div>
        </div>
        <div class="box-body">
             <form role="form" name="form" class="form-validation" method="post">
                {!!Form::token()!!}
                <div class="form-group" >
                    <label class="control-label required" style="padding-right:15px;" required>Group Name</label>
                    <input name="groupName" type="text" class="form-control" placeholder="Group Name" ng-model="groupName" novalidate>
                        <label id="label-error" class="error"
                               for="label" ng-bind = "groupNameError">
                        </label> 
                </div> 
                
                <div class="row">
                    <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                        <label class="control-label required" style="padding-right:15px;">Permissions</label>
                        <input class="form-control" type="text" name="searchText" placeholder="Search.." ng-model="searchText">
                        <label id="label-error" class="error col-sm-offset-1" for="label" ng-bind = "permissionsError">
                        </label>
                     </div> 
                </div><br>

                <div class="form-group">
                    <div class="well col-sm-5" style="padding-top:0px;padding-left:0px;padding-right:0px;padding-bottom:0px">
                        <div id="scrollArea" ng-controller="ScrollController">
                            <ul class="example-animate-container list-group">
                                <li style="height:30px;" class="animate-repeat list-group-item repeat" 
                                ng-repeat="listItem in permissionList | filter:searchText as results" ng-click="push(listItem,permissionList,selectedPermissionList)">@{{listItem.name}}</li>
                                <li  style="list-style: none" ng-if="results.length===0"><span class="label label-danger col-sm-12">No results found...</span></li>
                            </ul>
                        </div>
                    </div>
                    
                    <div class="col-sm-1" style="position:relative;display:block;">
                        <div style="position:absolute;margin-top:100%;margin-left:25%">
                            <img src="{{asset('assets/dist/jquery-multiselect/img/switch.png')}}" alt=""> 
                        </div>
                    </div>

                    <div class="well col-sm-6" style="padding-top:0px;padding-left:0px;padding-right:0px;padding-bottom:0px">
                        <div id="scrollArea" ng-controller="ScrollController">
                            <ul class="list-group">
                                <li style="height:30px;" class="list-group-item repeat" 
                                ng-repeat="selectedItem in selectedPermissionList" ng-click="push(selectedItem,selectedPermissionList,permissionList) | orderBy : selectedItem">@{{selectedItem.name}}</li>
                                <li style="list-style: none" ng-if="selectedPermissionList.length===0"><span class="label label-info col-sm-12">Add permissions here..</span></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <button class="pull-right btn btn-primary" class="btn btn-primary" ng-click="savePermissionGroup()"><i class="fa fa-floppy-o"></i> Save</button>
                    </div>
                </div> 

            </form>


        </div><!-- /.box-body -->
    </div><!-- /.box -->
</section><!-- /.content -->

@stop
@section('js')
    <script src="{{asset('/assets/dist/angular/angular/angular.min.js')}}"></script>
    <script type="text/javascript">
   
    /**
     * Angular
     */

    //initialize angular app
    var app = angular.module("permissionApp",[]);

    //controller
    app.controller("permissionController", function($scope,$http){

        //scope variables

        $scope.groupNameError = "";
        $scope.persmissionsError = "";
        //permission list
        $scope.permissionList = [];
        //user selected permission list
        $scope.selectedPermissionList = [];


         $.ajax({
            url: '{{url('permission/groups/permission/list')}}',
            type: 'get',
            success: function(response){
                $scope.permissionList = response.data;
                $scope.$digest();
            },
            error: function(Response){              
            }
        });

        //functions

        //save permissoin group
        $scope.savePermissionGroup = function(){
            $.ajax({
                url: '{{url('permission/groups/add')}}',
                type: 'POST',
                data: {groupName:$scope.groupName,permissions:$scope.selectedPermissionList},
                success: function(Response){
                    window.location.reload();
                    $scope.$digest();
                    swal("Good Job!", "Permission Group Added Successfully!", "success");
                },
                error: function(Response){                
                }
            });
        }
        //push item to list  and remove from other list
        $scope.push = function (item,from,to){
            var index = getIndex(from,item);
            insertLast(item,to);
            deleteItem(index,from);
            //$scope.permissionList.splice(item,1);
        }
       

        //add item to end of the list
        function insertLast (item,to){
            to.push(item);
        }
        //delete item from permission list
        function deleteItem (index,from){
            from.splice(index,1);
        }
        //get index of list
        function getIndex(array,item){
            return array.indexOf(item);
        }
        // get permissions list - not using
        function getPermissionList(){
            $.ajax({
                url:'{{url('permission/groups/list')}}',
                type: 'GET',
                data: {},
                success : function(Response){
                    console.log("GET permission list success");
                    createList(Response,$scope.permissionList);
                },
                error: function(Response){
                    console.log("GET permission list failed");
                }
            });
        }

        
    });//end controller

    //scroll controller 
    app.controller('ScrollController', ['$scope', '$location', '$anchorScroll',
        function($scope, $location, $anchorScroll){}
    ]);
    </script>
@stop


























