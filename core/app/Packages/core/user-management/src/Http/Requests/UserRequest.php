<?php
namespace Core\UserManage\Http\Requests;

use App\Http\Requests\Request;
use Input;

class UserRequest extends Request {

	public function authorize(){
		return true;
	}

	public function rules(){  
		$rules = [
			'employee'			=> 'required',
			'user_type'				=> 'required',
			'user_name'				=> 'required|min:6|unique:users,username',
			'password' 				=> 'required|confirmed|min:6',
			'password_confirmation'	=> 'required'
		]; 
		
		
		return $rules;
	}

}
