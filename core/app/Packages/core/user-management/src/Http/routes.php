<?php
/**
 * USER MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright 2015 Yasith Samarawickrama
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'user', 'namespace' => 'Core\UserManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
        Route::get('add', [
            'as' => 'user.add', 'uses' => 'UserController@addView'
        ]);

        Route::get('list', [
            'as' => 'user.list', 'uses' => 'UserController@listView'
        ]);

        Route::get('edit/{id}', [
            'as' => 'user.edit', 'uses' => 'UserController@editView'
        ]);
        
        Route::get('view/{id}', [
            'as' => 'user.view', 'uses'  => 'UserController@viewUser'
        ]);

        Route::get('appuser', [
            'as' => 'appuser.add', 'uses' => 'UserController@addappuserView'
        ]);

        Route::get('appuserlist', [
            'as' => 'appuser.list', 'uses' => 'UserController@appuserlistView'
        ]);

        Route::get('appuseredit/{id}', [
            'as' => 'appuser.edit', 'uses' => 'UserController@appusereditView'
        ]);

        Route::get('appuserview/{id}', [
            'as' => 'user.view', 'uses'  => 'UserController@appuserview'
        ]);

        Route::get('qrgenerator', [
            'as' => 'user.add', 'uses'  => 'UserController@qrgenerator'
        ]);

        Route::get('printqr', [
            'as' => 'user.add', 'uses' => 'UserController@printqr'
        ]);

        /*Route::get('json/list', [
            'as' => 'user.list', 'uses' => 'UserController@jsonList'
        ]);*/

        //password reset
        Route::get('password/reset/{id}', [
            'as'    => 'user.password_reset', 'uses'  => 'UserController@password_reset'
        ]);

        //app user password reset
        Route::get('appuserpassword/reset/{id}', [
            'as' => 'user.password_reset', 'uses'  => 'UserController@app_user_password_reset'
        ]);

        Route::get('delete/{id}', [
            'as' => 'user.delete', 'uses' => 'UserController@delete'
        ]);

        Route::get('deleteappuser/{id}', [
            'as' => 'user.delete', 'uses' => 'UserController@deleteappuser'
        ]);
      
        /*** POST Routes*/

        Route::post('add', [
            'as' => 'user.add', 'uses' => 'UserController@add'
        ]);

        Route::post('appuser', [
            'as' => 'appuser.add', 'uses' => 'UserController@appuser'
        ]);

        Route::post('edit/{id}', [
            'as' => 'user.edit', 'uses' => 'UserController@edit'
        ]);

        Route::post('appuseredit/{id}', [
            'as' => 'user.edit', 'uses' => 'UserController@editappuser'
        ]);


        Route::post('status', [
            'as' => 'user.status', 'uses' => 'UserController@status'
        ]);

        Route::post('appuserstatus', [
            'as' => 'user.status', 'uses' => 'UserController@appuserstatus'
        ]);

        Route::post('password/reset/{id}', [
            'as' => 'user.password_reset', 'uses'  => 'UserController@post_password_reset'
        ]);

        Route::post('appuserpassword/reset/{id}', [
            'as'  => 'user.password_reset', 'uses'  => 'UserController@app_password_reset'
        ]);

    });
});