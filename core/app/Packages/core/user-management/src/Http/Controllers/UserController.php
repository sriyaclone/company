<?php
namespace Core\UserManage\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Apps;
use App\Models\AppUser;
// use App\Models\User;

// use Core\UserManage\Models\User;
use Core\UserRoles\Models\UserRole;
use Core\permissions\Models\Permission;

use Core\UserManage\Http\Requests\UserRequest;
use Core\UserManage\Http\Requests\AppUserRequest;
use Core\UserManage\Http\Requests\UserEditRequest;
use Core\UserManage\Http\Requests\AppUserEditRequest;

use Core\UserManage\Http\Requests\UserPasswordResetRequest;
use Core\UserManage\Http\Requests\AppUserPasswordResetRequest;
use Core\UserManage\Models\User;

use Core\EmployeeManage\Models\Employee;
use Core\EmployeeManage\Models\EmployeeType;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use App\Classes\PdfTemplate;
use Response;
use Sentinel;
use Hash;
use DNS2D;
use DB;


class UserController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| User Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('guest');
	}

	/**
	 * Show the User add screen to the user.
	 *
	 * @return Response
	 */
	public function addView()
	{
		//load datat to variable from database
		$employees	= Employee::leftjoin('users','users.employee_id','=','dms_employee.id')
            ->leftjoin('dms_employee_type','dms_employee_type.id','=','dms_employee.employee_type_id')
            ->where('users.employee_id','=',Null)
            ->whereNotIn('dms_employee.employee_type_id',[3,6])
            ->select(DB::raw('CONCAT(dms_employee.first_name, " ", dms_employee.last_name) as name'),'dms_employee.id','dms_employee.short_code as code')
            ->get();
		

		$roles 	= UserRole::select('name' , 'id' )->get();

		return view( 'userManage::user.add' )->with([
			'employees' 	=> $employees,
			'user_types'	=> $roles
		 ]);
	}

	/**
	 * Add new user data to database
	 *
	 * @return Redirect to menu add
	 */
	public function add(UserRequest $request)
	{
		try {
            DB::transaction(function () use ($request) {
				//$password =  md5($request->input('password'));

				$user = Sentinel::registerAndActivate([
					'username'		=> $request->input('user_name'),
					'password'		=> $request->input('password')
				]);	

				
				if(!empty($request->input('employee'))){

					$employee 	= Employee::find($request->input('employee'));

					if($employee){
			            User::where('id', $user->id)->update([
			                'employee_id' 	=> $employee->id,
			                'status'      	=> 1
			            ]);

			            $role = Sentinel::findRoleById($request->input('user_type'));

					   	if(sizeof($role) > 0){
					   		$role->users()->attach($user);
					   	}else{
					   		throw new Exception("Role not found!.");	   		
					   	}
					}else{
		            	throw new \Exception("Employee doesn't exists for this id : ".$request->input('employee'));
		            }
	        	}else{
	            	throw new \Exception("Employee doesn't selected");
	            }
			});

	        return redirect('user/add')->with([
	            'success' 			=> true,
	            'success.message'	=> 'User has been successfully added!.',
	            'success.title' 	=> 'Success...!'
	        ]);

	    } catch (TransactionException $e) {
            if ($e->getCode() == 100) {
                return Response::json($e);
            }else if ($e->getCode() == 101) {
                return Response::json($e);
            }
        } catch (Exception $e) {
            return Response::json($e);
        }
		
    }

    /**
     * Show the App User add screen to the user.
     *
     * @return Response
     */
    public function addappuserView()
    {
        $appPermissionList = Apps::all()->lists('name','id');

         $employees	= Employee::leftjoin('dms_app_user','dms_app_user.employee_id','=','dms_employee.id')
            ->leftjoin('dms_employee_type','dms_employee_type.id','=','dms_employee.employee_type_id')
            ->where('dms_app_user.employee_id','=',Null)
            ->select(DB::raw('CONCAT(dms_employee.first_name, " ", dms_employee.last_name) as name'),'dms_employee.id','dms_employee.short_code as code')
            ->get();
        return view( 'userManage::user.addappuser' )->with([
        	'employees'				=>	$employees,
        	'appPermissionList'		=>	$appPermissionList
        ]);
    }

    /**
     * Add new user data to database
     *
     * @return Redirect to menu add
     */
    public function appuser(Request $request)
    {	
    	try {
            DB::transaction(function () use ($request) {

		        $password =  md5($request->input('password'));
		        
		        $user = AppUser::create([
		            'username'			=> $request->input('user_name'),
		            'password'			=> $password,
		            'app_permission' 	=> json_encode($request->input('appPermission')),
		        ]);

		        if($user){
			        if(!empty($request->input('employee'))){

			            $employee 	= Employee::find($request->input('employee'));

			            if($employee){

			                AppUser::where('id', $user->id)->update([
			                    'employee_id' 		=> $employee->id,
			                    'token'       		=> md5($user->id),
			                    'status'      		=> 1
			                ]);

			            }else{
			            	throw new \Exception("Employee doesn't exists for this id : ".$request->input('employee'));
			            }
			        }
			    }else{
			    	throw new \Exception("User fails");
			    }
		    });

            return redirect('user/appuser')->with([
	            'success' 			=> true,
	            'success.message'	=> 'App User has been successfully Added!.',
	            'success.title' 	=> 'Well Done!'
	        ]);

        } catch (TransactionException $e) {
            if ($e->getCode() == 100) {
                return Response::json($e);
            }else if ($e->getCode() == 101) {
                return Response::json($e);
            }
        } catch (Exception $e) {
            return Response::json($e);
        }
        
    }


	/**
	 * Show the user add screen to the user.
	 *
	 * @return Response
	 */
	public function listView(Request $request)
	{
		$roles = UserRole::all();
		$users = [];

		$name     = $request->input('name');
		$username = $request->input('username');
		$email    = $request->input('email');
		$role     = $request->input('role');
		$status   = $request->input('status');

		$user = User::leftjoin('dms_employee','users.employee_id','=','dms_employee.id')
				->join('role_users as rs', 'rs.user_id', '=', 'users.id')
				->join('roles as r', 'r.id', '=', 'rs.role_id')
				->select(
					'users.id', 
					'r.name as role_name', 
					 DB::raw('CONCAT(dms_employee.first_name, " ", dms_employee.last_name) as name'),
					'users.username', 
					'dms_employee.email', 
					'users.status'
				);

		if(!empty($name) && $name != ''){
			$user->whereIn('employee_id',function($query)use($name){
            	$query->select('id')->from('employee')
            		->where('first_name','like','%'.$name.'%')
            		->orWhere('last_name','like','%'.$name.'%');
            });
		}

		if(!empty($username) && $username != ''){
			$user->where('users.username', 'LIKE', '%'.$username.'%');
		}

		if(!empty($email) && $email != ''){
			$user->whereIn('employee_id',function($query)use($email){
            	$query->select('id')->from('employee')
            		->where('email','like','%'.$email.'%');
            });
		}

		if(!empty($role) && $role != ''){
			$user->where('r.id', $role);
		}

		if($status != 0){
			if($status == 1){
				$user->where('users.status', 1);
			}else{
				$user->where('users.status', 0);
			}
		}

		$users = $user->orderBy('id', 'ASC')->paginate(10);

		$links = $users;

		$links->appends([
			'name'     => $name,
			'username' => $username,
			'email'    => $email,
			'role'     => $role,
			'status'   => $status,
		]);

		return view('userManage::user.list')->with([
			'roles' => $roles,
			'users' => $users,
			'links' => $links,
            'old'   => $request
		]);
	}

    /**
     * Show the appuser add screen to the user.
     *
     * @return Response
     */
    public function appuserlistView(Request $request)
    {
        $roles = UserRole::all();
        $users = [];

        $name     = $request->input('name');
        $username = $request->input('username');
        $email    = $request->input('email');
        $status   = $request->input('status');

        $user = AppUser::leftjoin('dms_employee','dms_app_user.employee_id','=','dms_employee.id')->select(
            'dms_app_user.id',
            'dms_app_user.username',
            'dms_app_user.status',
            DB::raw('CONCAT(dms_employee.first_name, " ", dms_employee.last_name) as name'),
            'dms_employee.email',
            'dms_employee.short_code as code',
            'dms_employee.mobile'
        );

        if(!empty($name) && $name != '')
        {
            $user->whereIn('employee_id',function($query)use($name){
            	$query->select('id')->from('dms_employee')
            		->where('first_name','like','%'.$name.'%')
            		->orWhere('last_name','like','%'.$name.'%');
            });

        }

        if(!empty($username) && $username != '')
        {
            $user->where('dms_app_user.username', 'LIKE', '%'.$username.'%');

        }

        if($status != "" && ($status != 0 || $status != 1))
        {
            $user->where('dms_app_user.status', $status);
        }

        $users = $user->orderBy('id', 'ASC')->paginate(10);
        $links = $users;

        $links->appends([
            'name'     => $name,
            'username' => $username,
            'status'   => $status,
        ]);

        return view('userManage::user.appuserlist')->with([
            'users' => $users,
            'links' => $links,
            'old'   => $request
        ]);
    }

	/**
	 * Activate or Deactivate User
	 * @param  Request $request user id with status to change
	 * @return json object with status of success or failure
	 */
	public function status(Request $request)
	{
		$id     = $request->input('id');
		$status = $request->input('status');
		$user   = User::where('id', $id)->first();

		if(sizeof($user) > 0)
		{
			$user = User::where('id', $id)->update([
				'status' 	=> $status,
				'updated_at'=> date('Y-m-d h:i:s')
			]);

			if(sizeof($user) > 0)
			{
				return response()->json(['status' => 'success']);
			}
			else
			{
				return response()->json(['status' => 'error']);
			}
		}
		else
		{
			return response()->json(['status' => 'invalid_id']);
		}
	}

    /**
     * Activate or Deactivate User
     * @param  Request $request user id with status to change
     * @return json object with status of success or failure
     */
    public function appuserstatus(Request $request)
    {
        $id     = $request->input('id');
        $status = $request->input('status');
        $user   = AppUser::where('id', $id)->first();

        if(sizeof($user) > 0)
        {
            $user = AppUser::where('id', $id)->update([
                'status' 	=> $status,
                'updated_at'=> date('Y-m-d h:i:s')
            ]);

            if(sizeof($user) > 0)
            {
                return response()->json(['status' => 'success']);
            }
            else
            {
                return response()->json(['status' => 'error']);
            }
        }
        else
        {
            return response()->json(['status' => 'invalid_id']);
        }
    }


    /**
	 * Delete a User
	 * @param  Request $request user id
	 * @return Json           	json object with status of success or failure
	 */
	public function delete(Request $request, $id)
	{
		$user   = User::where('id', $id)->first();

		if(sizeof($user) > 0)
		{
			$deleted = User::where('id', $id)->delete();

			if(sizeof($deleted) > 0)
			{
				return redirect()->route('user.list')->with([ 
					'success' 			=> true,
					'success.message'	=> $user->first_name.' '.$user->last_name.' has been successfully deleted!.',
					'success.title' 	=> 'Success...!'
				]);
			}
			else
			{
				return redirect()->route('user.list')->with([ 
					'error' 			=> true,
					'error.message'	=> $user->first_name.' '.$user->last_name.' couldn\'t be deleted!.',
					'error.title' 	=> 'Error!.'
				]);
			}
		}
		else
		{
			return redirect()->route('user.list')->with([ 
					'error' 			=> true,
					'error.message'	=> 'Invalid Id or User not found!.',
					'error.title' 	=> 'Error!.'
				]);
		}
	}

    /**
     * Delete a AppUser
     * @param  Request $request user id
     * @return Json           	json object with status of success or failure
     */
    public function deleteappuser(Request $request, $id)
    {
        $user   = AppUser::where('id', $id)->first();

        if(sizeof($user) > 0)
        {
            $deleted = AppUser::where('id', $id)->delete();

            if(sizeof($deleted) > 0)
            {
                return redirect('user/appuserlist')->with([
                    'success' 			=> true,
                    'success.message'	=> $user->first_name.' '.$user->last_name.' has been Successfully deleted!.',
                    'success.title' 	=> 'Done!.'
                ]);
            }
            else
            {
                return redirect('user/appuserlist')->with([
                    'error' 			=> true,
                    'error.message'	=> $user->first_name.' '.$user->last_name.' couldn\'t be deleted!.',
                    'error.title' 	=> 'Error!.'
                ]);
            }
        }
        else
        {
            return redirect('user/appuserlist')->with([
                'error' 			=> true,
                'error.message'	=> 'Invalid Id or User not found!.',
                'error.title' 	=> 'Error!.'
            ]);
        }
    }

	/**
	 * Show the user edit screen to the user.
	 *
	 * @return Response
	 */
	public function editView($id)
	{
		  $empType=Employee::where('id','=',$id)->first();

		 $employees	= Employee::leftjoin('users','users.employee_id','=','dms_employee.id')
            ->leftjoin('dms_employee_type','dms_employee_type.id','=','dms_employee.employee_type_id')
            ->whereNotIn('dms_employee.employee_type_id',[$empType->employee_type_id])
            ->select(DB::raw('CONCAT(dms_employee.first_name, " ", dms_employee.last_name) as name'),'dms_employee.id','dms_employee.short_code as code')
            ->get();
			
		 $cur_role	= UserRole::select('id')->where('id', function($query) use ($id){
						$query->select('rs.role_id')
							->from('role_users as rs')
							->where('rs.user_id', $id)
							->first();
      	  			})->first();

	    $curUser 		= User::with(['roles'])->find($id);  
	    $srole 			= array();

	    foreach($curUser->roles as $key => $value)
	    {
	    	$srole= $value->id;
	    }
	     $srole;

	    $roles = UserRole::select('name' , 'id' )->get();


		if($curUser)
		{
			return view('userManage::user.edit')->with([ 
				'curUser' 	=> $curUser,
				'employees'	=> $employees,
				'user_types'=> $roles,
				'srole'		=> $srole,
				'cur_role'	=> $cur_role
			]);

		}
		else
		{
			return view('errors.404');
		}
	}

    /**
     * Show the user edit screen to the user.
     *
     * @return Response
     */
    public function appusereditView($id)
    {
        $curUserold 	= AppUser::find($id);

        $curUser 		= $curUserold;

        $appPermissionList = Apps::all()->lists('name','id');

        $tmp=json_decode($curUserold->app_permission);

        $dd=[];

        foreach ($tmp as $value) {
        	$dd[]=(int)$value;
        }

        $appPermissionListSelected = $dd;
        
        return view('userManage::user.editappuser')->with([
            'curUser' 					=> $curUser,
            'appPermissionList' 		=> $appPermissionList,
            'appPermissionListSelected' => $appPermissionListSelected,
        ]);       

    }

	//update user infomation
	public function edit(Request $request, $id)
	{
		try
		{
			//transaction is start to process
			DB::transaction(function () use ($request,$id) {

				$user             	= User::with(['roles'])->find($id);
				$user->username 	= $request->get('user_name');

				//Remove all Roles of current user	
				$user->roles()->detach();	
				$user->save();

				//attach new roles for current user
				$role = Sentinel::findRoleById($request->input('user_type'));
			   	$role->users()->attach($user);

			});			

			return redirect('user/list')->with([
				'success'         => true,
				'success.message' => 'User has been successfully updated!.',
				'success.title'   => 'Success...!'
			]);	
		}catch(Exception $e){
			throw new \Exception($e->getMessage());
		}
	}

    //update user infomation
    public function editappuser(Request $request, $id)
    {
        try{
            //transaction is start to process
            DB::transaction(function () use ($request,$id) {

	            $user = AppUser::find($id);
	            $user->username 		= $request->get('username');
	            $user->app_permission 	= json_encode($request->input('appPermission'));

	            //Remove all Roles of current user
	            $user->save();

	        });
            
            return redirect('user/appuserlist')->with([
                'success'         => true,
                'success.message' => 'App User has been successfully updated!.',
                'success.title'   => 'Done!.'
            ]);

        }catch(Exception $e){
            throw new \Exception($e->getMessage());
        }
    }

	//view user
    public function viewUser($id)
    {
        $user		= User::select('first_name', 'last_name', 'id', 'username', 'email')
            ->where('id', $id)
            ->first();

        $cur_role	= UserRole::select('name')->where('id', function($query) use ($id){
            $query->select('rs.role_id')
                ->from('role_users as rs')
                ->where('rs.user_id', $id)
                ->first();
        })->first();

        if(sizeof($user) > 0)
        {
            return view('userManage::user.view')->with([
                'user' => $user,
                'role' => $cur_role,
            ]);

        }
        else
        {
            return view('userManage::user.view')->with([
                'user' => null,
                'role' => null,
            ]);
        }
    }

    //view appuser
    public function appuserview($id)
    { 
        $user = AppUser::leftjoin('employee','app_user.employee_id','=','employee.id')            
            ->select('employee.first_name', 'employee.last_name', 'app_user.id', 'username', 'employee.email')
            ->where('app_user.id', $id)
            ->first();

        if(sizeof($user) > 0)
        {
            return view('userManage::user.appuserview')->with([
                'user' => $user
            ]);

        }
        else
        {
            return view('userManage::user.appuserview')->with([
                'user' => null,
            ]);
        }
    }

	//password reset
	public function password_reset(Request $request, $id)
	{
		return view('userManage::user.password_reset');
	}

    //password reset
    public function app_user_password_reset(Request $request, $id)
    {
        return view('userManage::user.app_user_password_reset');
    }

	//password reset
	public function post_password_reset(UserPasswordResetRequest $request, $id)
	{
		try
		{
			$password = $request->input('password');
			$user = User::find($id);	

			//check user
			if(sizeof($user) > 0)
			{
				//find and update
				$user = User::find($id)->update([
					'password' => Hash::make($password)
				]);				

				if(sizeof($user) > 0)
				{
					return redirect()->route('user.list')->with([
						'success'         => true,
						'success.message' => 'Password has been successfully reseted',
						'success.title'   => 'Success...!',
					]);
				}
				else
				{
					return redirect()->route('user.list')->with([
						'error'         => true,
						'error.message' => 'Something went wrong!.',
						'error.title'   => 'Error!.',
					]);
				}
			}
			else
			{
				return redirect()->route('user.list')->with([
						'error'         => true,
						'error.message' => 'That user doesn\'t exists!.',
						'error.title'   => 'Error!.',
					]);
			}
		}
		catch (Exception $e)
		{
			throw new Exception($e->getMessage());
			
		}
	}

    //app user password reset
    public function app_password_reset(AppUserPasswordResetRequest $request, $id)
    {
        try
        {
            $password = $request->input('password');
            $user = User::find($id);

            //check user
            if(sizeof($user) > 0)
            {
                //find and update
                $user = AppUser::find($id)->update([
                    'password' => md5($password)
                ]);

                if(sizeof($user) > 0)
                {
                    return redirect('user/appuserlist')->with([
                        'success'         => true,
                        'success.message' => 'Password has been successfully reseted',
                        'success.title'   => 'Success...!',
                    ]);
                }
                else
                {
                    return redirect('user/appuserlist')->with([
                        'error'         => true,
                        'error.message' => 'Something went wrong!.',
                        'error.title'   => 'Error!.',
                    ]);
                }
            }
            else
            {
                return redirect('user/appuserlist')->with([
                    'error'         => true,
                    'error.message' => 'That user doesn\'t exists!.',
                    'error.title'   => 'Error!.',
                ]);
            }
        }
        catch (Exception $e)
        {
            throw new Exception($e->getMessage());

        }
    }

    public function qrgenerator(Request $request)
    {
       $employee = $request->employee;
        $employees	= Employee::leftjoin('dms_app_user','dms_app_user.employee_id','=','dms_employee.id')            
			            ->select(DB::raw('CONCAT(dms_employee.first_name, " ", dms_employee.last_name) as name'),'dms_employee.id','dms_employee.short_code as code','dms_app_user.token')
			            ->get();
        if($employee > 0){
            $employee_data	= Employee::leftjoin('dms_app_user','dms_app_user.employee_id','=','dms_employee.id')
                ->select(DB::raw('CONCAT(dms_employee.first_name, " ", dms_employee.last_name) as name'),'dms_employee.id','dms_employee.short_code as code','dms_app_user.token')
               ->where('dms_employee.id',$employee)
                ->get();
             $current_code = $employee_data[0];
             if(isset($current_code->token)){
                 $barcode =  DNS2D::getBarcodeSVG($current_code->token, "QRCODE",10,10);
                 $current_code->qr= $barcode;
             }else{
                 $current_code = null;
             }
        }else{
           $current_code = null;
        }

        return view('userManage::user.qrgenerator')->with(['employees'=>$employees, 'current_code' => $current_code,'employeer' => $employee]);
    }


    public function printqr(Request $request)
    {
        $id = $request->id;

        if($id > 0){
            $employee_data	= Employee::leftjoin('dms_app_user','dms_app_user.employee_id','=','dms_employee.id')
                ->select(DB::raw('CONCAT(dms_employee.first_name, " ", dms_employee.last_name) as name'),'dms_employee.id','dms_employee.short_code as code','dms_app_user.token')
                ->where('dms_employee.id',$id)
                ->get();
            $current_code = $employee_data[0];
            if(isset($current_code->token)){
                $barcode =  DNS2D::getBarcodeSVG($current_code->token, "QRCODE",5,5);
                $current_code->qr= $barcode;
            }else{
                $current_code = null;
            }
        }else{
            $current_code = null;
        }

        if (count($employee_data) > 0) {
            $page1 = view('userManage::user.printqr')->with(['current_code' => $current_code])->render();
        } else {
            return response()->view("errors.404");
        }

        $data = ['no' => ''];

        $pdf = new PdfTemplate();
        $pdf->SetMargins(10, 15, 10);
        $pdf->setHeaderTemplateAutoreset(true);
        $pdf->SetHeaderData('', 0, '', '', array(), array());
        $pdf->setPrintHeader(true);
        $pdf->SetMargins(28.35 / $pdf->k,35);
        $pdf->SetAutoPageBreak(TRUE, 35);
        $pdf->AddPage();
        $pdf->writeHtml($page1);
        $pdf->output("test.pdf", 'I');

    }

}
