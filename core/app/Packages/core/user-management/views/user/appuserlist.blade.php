@extends('layouts.back_master') @section('title',' List App User')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/datatables/dataTables.bootstrap.css')}}">
<link rel="stylesheet" href="{{asset('assets/dist/bootstrap-switch/switch.css')}}">
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
<style type="text/css">
	.form-horizontal .form-group{
		margin-left:0;
		margin-right:0;
	}

</style>
@stop
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>App User<small>Management</small></h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li><a href="#">App User Management</a></li>
		<li class="active">App User List</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	
	<div class="box">
		<div class="container-fluid">
			<div class="box-header with-border">
				<h3 class="box-title">Search</h3>
				<button type="submit" class="btn btn-info pull-right">
					<i class="fa fa-user"></i><a href="{{ url('user/appuser') }}" style="color:white;"> Add App User </a>
				</button>
			</div>
			<form class="form-horizontal form-validation" role="form"  method="get" id="myForm">
				<div class="box-body">
					<div class="row">
						<div class="col-lg-4">
							<div class="form-group">
								<label class="control-label">Employee Name</label>
								<input type="text" name="name" class="form-control" placeholder="Enter Employee NAme" value="{{$old->name}}">
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<label class="control-label">Username</label>
								<input type="text" name="username" class="form-control" placeholder="Enter Username" value="{{$old->username}}">
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<label class="control-label">Status</label>
								<select name="status" class="form-control chosen">
									<option value="" >-- All Status --</option>
									<option value="1" @if($old->status == 1) selected @endif>Active</option>
									<option value="0" @if($old->status == 0) selected @endif>Inactive</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 text-right">
							<button type="reset" onclick="clear_filters()" class="btn btn-default">
								<i class="fa fa-undo" aria-hidden="true"></i> 
								Clear
							</button>
							<button type="submit" name="action" value="filter" class="btn btn-default">
								<i class="fa fa-filter" aria-hidden="true"></i> 
								Filter
 							</button>
						</div>
					</div>
				</div>
			</form>
		</div><!-- /.box-body -->
	</div><!-- /.box -->

	<!-- Default box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Details</h3>
		</div>
		<div class="container-fluid">
			<div class="box-body">
					<table class="table table-bordered table-hover">
		              	<thead>
			                <tr class="table-header">
			                  	<th class="text-left" width="4%">#</th>
			                  	<th class="text-left" >Full Name</th>
			                  	<th class="text-left" >Code</th>
			                  	<th class="text-left" >Username</th>
			                  	<th class="text-left" >Email</th>
			                  	<th class="text-left" >Contact</th>
			                  	<th class="text-center" >Status</th>
			                  	<th colspan="2" class="text-center" width="20%">Action</th>
			                </tr>
		              	</thead>
		              	<tbody>
		              		@if(sizeof($users) > 0)
								@foreach($users as $key => $_user)
									<tr>
										<td>{{ (($users->currentPage()-1)*$users->perPage())+($key+1) }}</td>
										<td class="text-left">{{ $_user->name }}</td>
										<td class="text-left">{{ $_user->code }}</td>
										<td class="text-left">{{ $_user->username }}</td>
										<td class="text-left">{{ $_user->email }}</td>
										<td class="text-left">{{ $_user->mobile }}</td>
										<td class="text-center">
											@if($_user->status == 1)
												<input 
													data-toggle="toggle" 
													data-on="<span class='fa fa-check'></span>" 
													data-off="<span class='fa fa-times'></span>" 
													data-size="mini" 
													class="siwtch user-activate" 
													type="checkbox" 
													data-onstyle="success" 
													data-offstyle="danger" 
													value="{{ $_user->id }}" checked>
											@else
												<input 
													data-toggle="toggle" 
													data-on="<span class='fa fa-check'></span>" 
													data-off="<span class='fa fa-times'></span>" 
													data-size="mini" 
													class="siwtch user-activate" 
													type="checkbox" 
													data-onstyle="success" 
													data-offstyle="danger" 
													value="{{ $_user->id }}">
											@endif
										</td>
										<td class="text-center">
											@if($user->hasAnyAccess(['admin','user.edit']))
												<a href="{{ url('user/appuseredit', $_user->id) }}" class="btn btn-xs btn-default">
													<span class="fa fa-pencil"></span>
												</a>
											@else
												<a class="btn btn-xs btn-default disabled">
													<span class="fa fa-pencil"></span>
												</a>
											@endif
										</td>
										<td class="text-center">
											@if($user->hasAnyAccess(['admin','user.password_reset']))
												<a href="{{ url('user/appuserpassword/reset', $_user->id) }}" class="btn btn-xs btn-default">
													<span class="fa fa-key"></span>
												</a>
											@else
												<a class="btn btn-xs btn-default disabled">
													<span class="fa fa-key"></span>
												</a>
											@endif
										</td>
									</tr>
								@endforeach
							@else
								<tr>
									<td colspan="9" class="text-center">
										<h4>No Data!</h4>
										<h6>Nothing to display. No data found.</h6>
									</td>
								</tr>
		              		@endif
		              	</tbody>
		            </table>
		            <div class="row">
						<div class="col-md-12 text-right">
							<?php echo $users->render(); ?>
						</div>
					</div>
		</div><!-- /.box-body -->
	</div><!-- /.box -->
</section><!-- /.content -->

@stop
@section('js')

<!-- datatables -->
<script src="{{asset('assets/dist/bootstrap-switch/switch.js')}}"></script>
<script src="{{asset('assets/dist/chosen/chosen.jquery.min.js')}}"></script>

<script type="text/javascript">
	$(document).ready(function() {
	  	$(".chosen").chosen();

	    $('.user-activate').change(function(){
			if($(this).prop('checked') == true)
			{
				change_status($(this).val(), 0);
			}
			else
			{
				change_status($(this).val(), 1);
			}				
	    });
	});

	/* Clear Filters data*/
    function  clear_filters() {
        // document.getElementById("myForm").reset();
        // $('#type_id').prop('selectedIndex',0);
        // $('#type_id'). trigger("chosen:updated");

        window.location.href = '{{url('user/appuserlist')}}';
    }

    function delete_user(id, _url){
	    swal({
		  title: "Are you sure?",
		  text: "you wanna delete this user?",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonColor: "#DD6B55",
		  confirmButtonText: "Yes, delete it!",
		  closeOnConfirm: false
		},
		function(){
		  window.open(_url, '_self');
		});
	}

	$(function(){
	    $('.siwtch').bootstrapToggle({
	     	on: 'ok',
	      	off: 'no'
	    });
	});

    function change_status(id, status)
    {
        var data = new FormData;

        if(status == 0)
        {
            data.append('id', id);
            data.append('status', 1);

            $.ajax({
                url: '{{url('user/appuserstatus')}}',
                method: 'post',
                processData: false,
                contentType: false,
                cache: false,
                data: data,
                success: function(response){
                    if(response.status == "success")
                    {
                        swal("Done!.", "Status has been successfully updated!.", "success")
                    }

                    if(response.status == "error")
                    {
                        swal("Error!.", "Something went wrong!, we couldn't update status.", "error")
                    }

                    if(response.status == "invalid_id")
                    {
                        swal("Error!.", "Invalid Id or user not found!.", "error")
                    }
                },
                error: function(xhr){
                    console.log(xhr);
                }
            });
        }
        else
        {
            data.append('id', id);
            data.append('status', 0);

            $.ajax({
                url: '{{url('user/appuserstatus')}}',
                method: 'post',
                processData: false,
                contentType: false,
                cache: false,
                data: data,
                success: function(response){
                    if(response.status == "success")
                    {
                        swal("Done!.", "Status has been successfully updated!.", "success")
                    }

                    if(response.status == "error")
                    {
                        swal("Error!.", "Something went wrong!, we couldn't update status.", "error")
                    }

                    if(response.status == "invalid_id")
                    {
                        swal("Error!.", "Invalid Id or user not found!.", "error")
                    }
                },
                error: function(xhr){
                    console.log(xhr);
                }
            });
        }
    }
</script>
@stop
