@extends('layouts.back_master') @section('title','Add User')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
<style type="text/css">
.top{
	margin-top: 10px
}
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Web User<small>Management</small></h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li><a href="{{url('user/list')}}">User</a></li>
		<li class="active">Add Web User</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Add Web User</h3>
			<div class="box-tools pull-right"  style="margin-right: 14px;">
				{{--<a href="{{ URL::previous() }}" class="btn btn-info"><span class="fa fa-caret-left"></span></a>--}}
				<!-- <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
				<button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> -->
			</div>
		</div>
		<br>
		<div class="box-body">
			<div class="container-fluid">
				<form action="{{ route('user.add') }}" method="post">
					{!!Form::token()!!}
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							
							<div class="row top">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<label class=" control-label required">Employee</label>
											
									@if($errors->has('employee'))
										<select name="employee" id="employee" class="form-control chosen">
											<option value="">-- Select Employee --</option>
				            				@if(sizeof($employees) > 0)
												@foreach($employees as $employee)
													@if(Input::old('employee') == $employee->id)
														<option value="{{ $employee->id }}" selected>{{ $employee->code." - ".$employee->name }}</option>
													@else
														<option value="{{ $employee->id }}">{{ $employee->code." - ".$employee->name }}</option>
													@endif
												@endforeach

				            				@endif
				            			</select>
			            				<label id="employee-error" class="help-block" for="employee">
			            					{{$errors->first('employee')}}
			            				</label>
			            			@else
			            				<select name="employee" id="employee" class="form-control chosen">
											<option value="">-- Select Employee --</option>
				            				@if(sizeof($employees) > 0)
												@foreach($employees as $employee)
													@if(Input::old('employee') == $employee->id)
														<option value="{{ $employee->id }}" selected>{{ $employee->code." - ".$employee->name }}</option>
													@else
														<option value="{{ $employee->id }}">{{ $employee->code." - ".$employee->name }}</option>
													@endif
												@endforeach
				            				@endif
				            			</select>
				            			<label id="employee-error" class="help-block" for="employee">
			            					{{$errors->first('employee')}}
			            				</label>
			            			@endif
								</div>
							</div>

							<div class="row top">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<label class=" control-label required">User Roles</label>
									@if($errors->has('user_type'))
										<select name="user_type" id="user_type" class="form-control chosen">
											<option value="">-- User Roles --</option>
				            				@if(sizeof($user_types) > 0)
												@foreach($user_types as $user_type)
													@if(Input::old('user_type') == $user_type->id)
														<option value="{{ $user_type->id }}" selected>{{ $user_type->name }}</option>
													@else
														<option value="{{ $user_type->id }}">{{ $user_type->name }}</option>
													@endif
												@endforeach
				            				@endif
				            			</select>
			            				<label id="user_type-error" class="help-block" for="user_type">
			            					{{$errors->first('user_type')}}
			            				</label>
			            			@else
			            				<select name="user_type" id="user_type" class="form-control chosen">
											<option value="">--Select User Roles --</option>
				            				@if(sizeof($user_types) > 0)
												@foreach($user_types as $user_type)
													@if(Input::old('user_type') == $user_type->id)
														<option value="{{ $user_type->id }}" selected>{{ $user_type->name }}</option>
													@else
														<option value="{{ $user_type->id }}">{{ $user_type->name }}</option>
													@endif
												@endforeach
				            				@endif
				            			</select>
				            		@endif
								</div>
								<input type="hidden" id="user_log" name="user_log" value="1">
							</div>

							<div class="row top">
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<label class="control-label required">User Name</label>
									<input type="text" class="form-control " name="user_name" placeholder="User Name" value="{{Input::old('user_name')}}">
			            			@if($errors->has('user_name'))
			            				<label id="label-error" class="help-block" for="label">{{$errors->first('user_name')}}</label>
			            			@endif									
								</div>

								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<label class="control-label required">Password</label>
									<input type="password" class="form-control" name="password" placeholder="Enter Password" value="{{Input::old('password')}}">
			            			@if($errors->has('password'))
			            				<label id="label-error" class="help-block" for="label">{{$errors->first('password')}}</label>
			            			@endif
								</div>

								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<label class="control-label required">Confirm Password</label>									
									<input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" value="{{ Input::old('password_confirmation') }}">
			            			@if($errors->has('password_confirmation'))
			            				<label id="label-error" class="help-block" for="label">{{$errors->first('password_confirmation')}}</label>
			            			@endif
								</div>

							</div>

							<div class="row top" style="margin-bottom: 10px;">
								<div class="col-lg-12">
									<div class="pull-right">
					                	<button type="submit" class="btn btn-default"><i class="fa fa-floppy-o"></i> Save</button>
					                </div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div><!-- /.box-body -->
	</div><!-- /.box -->
</section><!-- /.content -->

@stop
@section('js')

<script src="{{asset('assets/dist/chosen/chosen.jquery.min.js')}}"></script>

<script type="text/javascript">

$(document).ready(function() {
  	$(".chosen").chosen();

  	$('#user_type').change(function(e){
  		if($('#user_type').val()==6){
			document.getElementById('sector_div').style.display = 'block';		
			$("#sector_status").val("1");
  		}else{  			
			document.getElementById('sector_div').style.display = 'none';		
			$("#sector_status").val("0");
  		}
    });

});

</script>
@stop
