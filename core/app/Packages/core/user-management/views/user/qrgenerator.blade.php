@extends('layouts.back_master') @section('title','QR GENERATOR')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/datatables/dataTables.bootstrap.css')}}">
<link rel="stylesheet" href="{{asset('assets/dist/bootstrap-switch/switch.css')}}">
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
<style type="text/css">
	.qr{
		width: 65%;
		margin-left: auto;
		margin-right: auto;
	}

	.qr > div{
		margin-right: auto;
		margin-left: auto;
	}

	.flex {
		min-height: 60pt;
	}

</style>
@stop
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Web User<small>Management</small></h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li><a href="#">User</a></li>
		<li class="active">QR GENERATOR</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	
	<div class="box">
		<div class="container-fluid">
			<div class="box-header with-border">
					<h3 class="box-title">QR GENERATOR</h3>
				</div>
				<div class="box-body">
					<form class="form-horizontal form-validation" role="form"  method="get" id="myForm">
						{!!Form::token()!!}
					<div class="row">
						<div class="col-md-1">
							<label class="control-label">User</label>
						</div>
						<div class="col-md-4">
								<select name="employee" id="employee" class="form-control chosen">
									<option value="">Select User</option>
									@if(sizeof($employees) > 0)
										@foreach($employees as $employee)

											@if($employee->id == $employeer)
												<option value="{{$employeer }}" selected>{{ $employee->code." - ".$employee->name }}</option>
											@elseif(Input::old('employee') == $employee->id)
												<option value="{{Input::old('employee') }}" selected>{{ $employee->code." - ".$employee->name }}</option>
											@else
												<option value="{{ $employee->id }}" >{{ $employee->code." - ".$employee->name }}</option>

											@endif
										@endforeach
									@endif
								</select>
						</div>
						<div class="col-md-4">
							<button type="submit"  class="btn btn-default">
								<i class="fa fa-find" aria-hidden="true"></i>
								Find
							</button>
							<button type="button"  class="btn btn-default" id="print_button" onclick="printdata();">
								<i class="fa fa-find"  aria-hidden="true"></i>
								Print
							</button>
						</div>
					</div>
						<div class="row">
								<div id="ar_div"  class="col-md-5 align-self-center justify-content-center"
									 style="height: 100%;position: relative;padding-top: 30px;">
									<div class="qr"  >
										@if($current_code)
											{!!$current_code->qr!!}
											<div class="text-center" style="margin-top: 30px;">
												<h3 style="margin-bottom: 0;">
													<strong>Code:</strong> {{$current_code->code}}
												</h3>
												<h4 class="text-center" style="margin-bottom: 0;">
													{{$current_code->name}}
												</h4>
											</div>
										@else
											<div>
												<h3>Empty Token..
												</h3>
											</div>

										@endif
									</div>
								</div>
						</div>
			</form>
		</div><!-- /.box-body -->
	</div><!-- /.box -->
</div>
</section><!-- /.content -->

@stop
@section('js')

<!-- datatables -->
<script src="{{asset('assets/dist/bootstrap-switch/switch.js')}}"></script>
<script src="{{asset('assets/dist/chosen/chosen.jquery.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.print/1.5.1/jQuery.print.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
	  	$(".chosen").chosen();

	});

    function printdata() {
        var id = $('#employee').val();
        var url = "{{url('user')}}"+"/printqr?id="+id;
        window.open(url);
    }
</script>
@stop
