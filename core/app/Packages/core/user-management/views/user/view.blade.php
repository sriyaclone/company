@extends('layouts.back_master') @section('title','View User')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
<style type="text/css">
	
</style>
@stop
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Web User<small>Management</small></h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li><a href="{{url('user/list')}}">User Management</a></li>
		<li class="active"><a href="{{ route('user.view', ['id' => 1]) }}">View Web User</a></li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">View Web User Details</h3>
			<div class="box-tools pull-right" style="">
                <a href="{{ URL::previous() }}" class="btn btn-info"><span class="fa fa-caret-left"></span></a>
				<!-- <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
				<button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> -->
			</div>
		</div>
		<div class="box-body">
            <div class="container-fluid">
                <br>
                <form action="{{ url('user/view', ['id' => request()->id]) }}" method="post">
                    {!! Form::token() !!}
                    <div class="row">
                        <div class="col-lg-12">
                          <div class="row" style="margin-bottom: 4px;">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <label class="control-label">First Name</label>
                                            </div>
                                            <div class="col-lg-8">
                                                <input type="text" class="form-control" name="first_name" id="first_name" placeholder="" value="@if($user != null){{ $user->first_name }}@else-@endif" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <label class="control-label">Last Name</label>
                                            </div>
                                            <div class="col-lg-8">
                                                <input type="text" class="form-control" name="last_name" placeholder="Enter Last Name." value="@if($user != null){{ $user->last_name }}@else-@endif" readonly>                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!--/.row -->
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-lg-2">
                                        <label class=" control-label">User Roles</label>
                                    </div>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" name="role" value="@if($role != null){{ $role->name }}@else-@endif" readonly>
                                    </div>
                                </div>
                            </div><!--/.row -->
                            <br>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-2">
                                        <label class="control-label">E-mail</label>                    
                                    </div>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" name="email" placeholder="-" value="@if($user != null){{ $user->email }}@else-@endif" readonly>
                                    </div>
                                </div>
                            </div><!--/.row -->
                            <br>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-lg-2">
                                        <label class="control-label">Username</label>
                                    </div>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" name="username" placeholder="-" value="@if($user != null){{ $user->username }}@else-@endif" readonly>
                                    </div>
                                </div>
                            </div>
                            @if($user == null)
                                <br>
                                <br>
                                <h2 align="center" style="color: rgba(127, 140, 141,1.0)">Data not found!.</h2>
                                <h5 align="center" style="color: rgba(127, 140, 141,1.0)">Maybe ID is invalid.</h5>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
		</div>
	</div>	
</section>	

@stop
@section('js')


<script src="{{asset('assets/dist/chosen/chosen.jquery.min.js')}}"></script>

<script type="text/javascript">
$(document).ready(function() {
  $(".chosen").chosen();
});
	
</script>
@stop
