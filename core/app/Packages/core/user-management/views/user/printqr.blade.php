<style type="text/css">

    td{
        font-weight: 400;
        font-size: 10px;
    }

    .details , .details> th ,  .details> td {
        border-collapse: collapse;
        padding: 5px;
    }

    th {
        text-align: left;
    }

    .border{
        border-width:1px;
        border-style:solid;
    }

    .border-left{
        border-left-style:solid;
        border-width:1px;
    }

    .border-right{
        border-right-style:solid;
        border-width:1px;
    }

    .border-top{
        border-top-style:solid;
        border-width:1px;
    }

    .border-bottom: {
        border-bottom-style:solid;
        border-width:1px;
    }
    .div_border {
        border-style: dotted;
    }
</style>
<div id="ar_div"  class="col-md-5 align-self-center justify-content-center"
     style="height: 100%;position: relative;padding-top: 30px;">
    <div  class="qr">
<table>
    <tr>
        @if($current_code)
        <div class="div_border" style="height: 350px;width: 200px">
            <div style="text-align: center;padding-top: 30px">
                {!!$current_code->qr!!}
            </div>
            <div style="margin-top: 30px;text-align: center" >
                <div style="text-align: center">
                    <strong>{{$current_code->code}} </strong>
                </div>
                <div>
                    <strong>{{$current_code->name}} </strong>
                </div>
            </div>
            @else
                <div>
                    <h3>Empty Token..
                    </h3>
                </div>
        </div>
        @endif
    </tr>
</table>
    </div>
</div>

@section('js')
    <!-- datatables -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.print/1.5.1/jQuery.print.min.js"></script>
