@extends('layouts.back_master') @section('title','User List')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/datatables/dataTables.bootstrap.css')}}">

<style type="text/css">
	

</style>
@stop
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>User Role<small> Management</small></h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li class="active">User Role List</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">User Role List</h3>
			<div class="box-tools pull-right">
				@if($user->hasAnyAccess(['admin','user.role.add']))
					<a href="{{url('user/role/add')}}" class="btn btn-success btn-sm" style=" margin-top: 2px;">Add User Role</a>
				@endif
			</div>
		</div>
		<div class="box-body">
			<div class="table-wrapper">
				<table class="table table-bordered bordered">
	              	<thead>
		                <tr>
		                  	<th class="text-center" width="5%">#</th>
		                  	<th class="text-center" style="font-weight:normal;" width="15%">Role Name</th>
		                  	<th class="text-center" style="font-weight:normal;" width="10%">Discount</th>
		                  	<th style="font-weight:normal;" width="65%">Permissions</th>
		                  	<th colspan="2" class="text-center" width="5%" style="font-weight:normal;">Action</th>
		                </tr>		                
	              	</thead>
	              	<tbody>
	              		@foreach($data as $key=>$item)
							<tr>
								<td>{{$key+1}}</td>
								<td>{{$item->name}}</td>
								<td>{{$item->discount}}</td>
								<td>
									@if($item->groups!=null)
										@foreach($item->groups as $key=>$aa)
											<span class="badge perm">{{$aa->name}}</span>
										@endforeach
									@else
										-
									@endif										
								</td>
								<td>
									@if($user->hasAnyAccess(['user.role.edit','admin']))
										<a href="{{url('user/role/edit/')}}/{{$item->id}}" class="blue"  data-toggle="tooltip" data-placement="top" title="Edit Role">
											<i class="fa fa-pencil"></i>
										</a>
									@else
										<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled">
											<i class="fa fa-pencil"></i>
										</a>
									@endif				
								</td>
								<td>
									{{--@if($user->hasAnyAccess(['user.role.delete','admin']))
										<a href="#" class="red role-delete" data-id="{{$item->id}}" data-toggle="tooltip" data-placement="top" title="Delete Role">
											<i class="fa fa-trash-o"></i>
										</a>
									@else
										<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled">
											<i class="fa fa-trash-o"></i>
										</a>
									@endi--}}

									-			
								</td>
							</tr>
						@endforeach
	              	</tbody>
	            </table>
	            <?php echo $data->appends(Input::except('page'))->render()?>
            </div>
		</div><!-- /.box-body -->
	</div><!-- /.box -->
</section><!-- /.content -->

@stop
@section('js')
<script type="text/javascript">
	var id = 0;
	var table = '';
	$(document).ready(function(){		
		$('.role-delete').click(function(e){
			e.preventDefault();
			id = $(this).data('id');
			sweetAlertConfirm('Delete Role', 'Are you sure?',2, deleteFunc);
		});
	});

	/**
	 * Delete the menu
	 * Call to the ajax request menu/delete.
	 */
	function deleteFunc(){
		ajaxRequest( '{{url('user/role/delete')}}' , { 'id' : id  }, 'post', handleData);
	}

	/**
	 * Delete the menu return function
	 * Return to this function after sending ajax request to the menu/delete
	 */
	function handleData(data){
		if(data.status=='success'){
			sweetAlert('Delete Success','Record Deleted Successfully!',0);
		}else if(data.status=='invalid_id'){
			sweetAlert('Delete Error','Role Id doesn\'t exists.',3);
		}else{
			sweetAlert('Error Occured','Please try again!',3);
		}
	}
</script>
@stop