<?php
namespace App\Modules\Supplier\Requests;

use App\Http\Requests\Request;
use Log;

class SupplierRequest extends Request {

	public function authorize(){
		return true;
	}

	public function rules(){
		$id = $this->id;
		$rules = [
			'name'      => 'required',
			'email'     => 'email|unique:supplier,email,'.$id.',,deleted_at,NULL',
			'contact_1' => 'required|max:10|min:10|regex:/(0)[0-9]{9}/',
			'contact_2' => 'max:10|min:10|regex:/(0)[0-9]{9}/',
		];
		
		return $rules;
	}

}
