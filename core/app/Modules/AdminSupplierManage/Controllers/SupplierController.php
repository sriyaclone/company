<?php

namespace App\Modules\supplier\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\supplier\Models\Suppliers as supplier;
use App\Modules\supplier\Requests\SupplierRequest;
use Illuminate\Http\Request;
use App\Classes\Functions;
use Session;
use DB;

class SupplierController extends Controller
{
    /**
     * Display list of supplier.
     * @param instance of Request
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {   
        $keyword      = trim($request->get('search'));

        if($request->get('status') == null)
        {
            $status  = '';
        }
        else
        {
            $status  = $request->get('status') == 1? 1 : 0;
        }   
        
        $perPage = 25;

        if(!empty($keyword)) 
        {
            $supplier = Supplier::where(function($query) use ($keyword){
                    $query->where('id', 'LIKE', "%".$keyword."%")
                    ->orWhere('code', 'LIKE', "%".$keyword."%")
                    ->orWhere('name', 'LIKE', "%".$keyword."%")
                    ->orWhere('email', 'LIKE', "%".$keyword."%")
                    ->orWhere('contact_first', 'LIKE', "%".$keyword."%")
                    ->orWhere('contact_second', 'LIKE', "%".$keyword."%");
                })
                ->whereNull('deleted_at');
        }else{
            $supplier = Supplier::whereNull('deleted_at');
        }

        if(isset($status) && $status !== ''){
            $supplier = $supplier->where('status', $status)
                ->paginate($perPage);
        }else{
            $supplier = $supplier->paginate($perPage);
        }

        return view('supplier::supplier.index', compact('supplier'));
    }

    /**
     * Show the form for creating a new supplier.
     *
     * @return \Illuminate\View\View
     */

    public function create()
    {
        $common       = new Functions();
        $supplierCode = $common->getNextSupplierCode();

        return view('supplier::supplier.create', compact('supplierCode'));
    }

    /**
     * insert supplier to the database
     *
     * @param instance of request
     *
     * @return redirect to supplier add page
     */

    public function store(SupplierRequest $request)
    {       
        DB::beginTransaction();
        
        $supplier                 = new Supplier;
        $supplier->code           = trim($request->input('supplier_code'));
        $supplier->name           = $request->input('name');
        $supplier->company        = $request->input('company_name');
        $supplier->address        = $request->input('address');
        $supplier->email          = $request->input('email');
        $supplier->contact_first  = $request->input('contact_1');
        $supplier->contact_second = $request->input('contact_2');
        $supplier->status         = $request->input('status') == 'on'? 1 : 0;
        $supplier->created_at     = date('Y-m-d h:i:s');
        $supplier->save();

        if(count($supplier) > 0)
        {
            DB::commit();

            return redirect()->route('admin.supplier.create')->with([
                'success.title'   => 'Done!',
                'success.message' => 'Supplier has been successfully added!.'
            ]);
        }
        else
        {
            DB::rollback();

            return redirect()->route('admin.supplier.create')->with([
                'error.title'   => 'Error!',
                'error.message' => 'Supplier couldn\'t added!.'
            ]);
        }
    }

    /**
     * Display the specified supplier.
     *
     * @param  $id
     * @return view
     */

    public function show($id)
    {
        if(!empty($id))
        {
            $supplierCode = '';
            $supplier     = Supplier::find($id);

            if(sizeof($supplier) > 0)
            {
                return view('supplier::supplier.show', compact('supplier', 'supplierCode'));
            }
            else
            {
                return redirect()->route('admin.supplier.index')->with([
                    'error.title'   => 'Error!.',
                    'error.message' => 'Invalid ID or supplier not found!.',
                ]);   
            }
        }
        else
        {
            return redirect()->route('admin.supplier.index')->with([
                'error.title'   => 'Error!.',
                'error.message' => 'Invalid ID or supplier not found!.',
            ]);
        }
    }

    /**
     * Show the form for editing the specified supplier.
     *
     * @param  int  $id
     * @return view
     */
    public function edit($id)
    {
        if(!empty($id))
        {
            $supplierCode = '';
            $supplier     = Supplier::find($id);

            if(count($supplier) > 0)
            {
                return view('supplier::supplier.edit', compact('supplier', 'supplierCode'));
            }
            else
            {
                return redirect()->route('admin.supplier.index')->with([
                    'error.title'   => 'Error!.',
                    'error.message' => 'Supplier not found!.'
                ]);
            }
        }
        else
        {
            return redirect()->route('admin.supplier.index')->with([
                'error.title'   => 'Error!.',
                'error.message' => 'Invalid ID or Supplier not found!.'
            ]);
        }
    }

    /**
     * Update the Supplier.
     * @param $id
     * @param instance of Request
     * @return redirect to edit page
     */

    public function update($id, SupplierRequest $request)
    {
        DB::beginTransaction();

        $supplier                 = Supplier::find($id);
        $supplier->name           = $request->input('name');
        $supplier->email          = $request->input('email');
        $supplier->contact_first  = $request->input('contact_1');
        $supplier->contact_second = $request->input('contact_2');
        $supplier->company        = $request->input('company_name');
        $supplier->address        = $request->input('address');
        $supplier->status         = $request->input('status') == ''? 0:1;
        $supplier->updated_at     = date('Y-m-d h:i:s');
        $supplier->update();

        if(count($supplier) > 0)
        {
            DB::commit();

            return redirect()->route('admin.supplier.index')->with([
                'success.title'   => 'Done!.',
                'success.message' => 'Supplier has been succeesfully updated!.'
            ]);
        }
        else
        {
            DB::rollback();

            return redirect()->route('admin.supplier.edit', $id)->with([
                'error.title'   => 'Error!.',
                'error.message' => 'Something went wrong!.'
            ]);
        }
    }

    /**
     * delete the specified supplier from database.
     *
     * @param  int  $id
     * @return Redirector to list page
     */
    public function destroy($id)
    {
        DB::beginTransaction();

        if(!empty($id))
        {
            $deleted = Supplier::where('id', $id)->delete();

            if($deleted)
            {
                DB::commit();

                return redirect()->route('admin.supplier.index')->with([
                    'success.title'   => 'Done!.',
                    'success.message' => 'Supplier has been successfully deleted!.'
                ]);
            }
            else
            {
                DB::rollback();

                return redirect()->route('admin.supplier.index')->with([
                    'error.title'   => 'Error!.',
                    'error.message' => 'Something went wrong!.'
                ]);
            }
        }
        else
        {
            DB::rollback();

            return redirect()->route('admin.supplier.destroy')->with([
                'error.title'   => 'Error!.',
                'error.message' => 'Invalid id or supplier not found!.'
            ]);
        }
    }

    /** 
     * This function is used to get all suppliers
     * @param
     * @return $supplier_array 
     */

    public function getSupplier(){
       return $supplier = Supplier::where('status',1)->lists('name','id'); 
    }
}
