<div class="form-group">
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
        <label class="control-label pull-right required" title="name">Supplier ID</label>
    </div>
    <div class="col-lg-9 col-md-9 col-ms-9 col-xs-9">
        <input type="text" name="supplier_code" class="form-control" placeholder="Enter Supplider ID..." value="@if(isset($supplier->code)) {{ $supplier->code }} @elseif(isset($supplierCode)) {{ $supplierCode }} @endif" readonly>
    </div>
</div>
<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
        <label class="control-label pull-right required" title="name">Name</label>
    </div>
    <div class="col-lg-9 col-md-9 col-ms-9 col-xs-9">
        <input type="text" name="name" class="form-control" placeholder="Enter supplier name..." value="{{ empty(old('name'))? isset($supplier->name)? $supplier->name : old('name') : old('name') }}">
        @if($errors->has('name'))
            <span class="help-block">
                {{ $errors->first('name') }}
            </span>
        @endif
    </div>
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
        <label class="control-label pull-right" title="email">Email</label>
    </div>
    <div class="col-lg-9 col-md-9 col-ms-9 col-xs-9">
        <input type="text" name="email" class="form-control" placeholder="Enter Email..." value="{{ empty(old('email'))? isset($supplier->email)? $supplier->email : old('email') : old('email') }}">
        @if($errors->has('email'))
            <span class="help-block">
                {{ $errors->first('email') }}
            </span>
        @endif
    </div>
</div>
<div class="form-group {{ $errors->has('contact_1') ? 'has-error' : ''}}">
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
        <label class="control-label pull-right required" title="contact_1">Contact</label>
    </div>
    <div class="col-lg-9 col-md-9 col-ms-9 col-xs-9">
        <input type="number" name="contact_1" class="form-control" placeholder="Enter contact number..." value="{{ empty(old('contact_1'))? isset($supplier->contact_first)? $supplier->contact_first : old('contact_1') : old('contact_1') }}">
        @if($errors->has('contact_1'))
            <span class="help-block">
                {{ $errors->first('contact_1') }}
            </span>
        @endif
    </div>
</div>
<div class="form-group {{ $errors->has('contact_2') ? 'has-error' : ''}}">
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
        <label class="control-label pull-right" title="contact_2">Other Contact</label>
    </div>
    <div class="col-lg-9 col-md-9 col-ms-9 col-xs-9">
        <input type="text" name="contact_2" class="form-control" placeholder="Enter other contact number..." value="{{ empty(old('contact_2'))? isset($supplier->contact_second)? $supplier->contact_second : old('contact_2') : old('contact_2') }}">
        @if($errors->has('contact_2'))
            <span class="help-block">
                {{ $errors->first('contact_2') }}
            </span>
        @endif
    </div>
</div>
<div class="form-group {{ $errors->has('company_name') ? 'has-error' : ''}}">
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
        <label class="control-label pull-right" title="company_name">Company Name</label>
    </div>
    <div class="col-lg-9 col-md-9 col-ms-9 col-xs-9">
        <input type="text" name="company_name" class="form-control" placeholder="Enter supplier company name..." value="{{ empty(old('company_name'))? isset($supplier->company)? $supplier->company : old('company_name') : old('company_name') }}">
        @if($errors->has('company_name'))
            <span class="help-block">
                {{ $errors->first('company_name') }}
            </span>
        @endif
    </div>
</div>
<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
    <label class="control-label pull-right" title="Status">Status</label>
</div>
<div class="col-lg-9 col-md-9 col-ms-9 col-xs-9">
    <label class="switch" style="margin-left: -10px;margin-bottom: 10px;">
      <input type="checkbox" name="status"  
        @if($page == 'edit')
            @if(empty(old('status')))
                @if(isset($supplier->status) && $supplier->status == 1)
                    checked="checked" value="1"
                @else
                    value="0"
                @endif
            @else
                @if(old('status') == 1)
                    checked="checked" value="1"
                @else
                    value="0"
                @endif
            @endif
        @else
            checked
        @endif >
      <span class="slider"></span>
    </label>
</div>
<div class="form-group">
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
        <label class="control-label pull-right" title="address">Address</label>
    </div>
    <div class="col-lg-9 col-md-9 col-ms-9 col-xs-9">
        <textarea name="address" class="form-control" cols="20" rows="5" placeholder="Enter address...">{{ empty(old('address'))? isset($supplier->address)? $supplier->address : old('address') : old('address') }}</textarea>
        @if($errors->has('address'))
            <span class="help-block">
                {{ $errors->first('address') }}
            </span>
        @endif
    </div>
</div>
<div class="form-group">
    <div class="col-lg-offset-2 col-md-offset-2 col-ms-offset-2 col-xs-offset-2 col-lg-4  col-md-4 col-ms-4 col-xs-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
