<?php
/**
 * SUPPLIER MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-09-25
 */


Route::group(['middleware' => ['auth']], function()
{	
	Route::group(['prefix'=>'admin/supplier/','namespace' => 'App\Modules\AdminSupplierManage\Controllers'],function() {

		/*===================================
					GEt request
    	===================================*/
		Route::get('add', [
			'uses' => 'AdminSupplierController@create',
			'as'   => 'admin.supplier.create'
        ]);

        Route::get('/', [
			'uses' => 'AdminSupplierController@index',
			'as'   => 'admin.supplier.index'
        ]);

        Route::get('show/{id}', [
			'uses' => 'AdminSupplierController@show',
			'as'   => 'admin.supplier.show'
        ]);

        Route::get('edit/{id}', [
			'uses' => 'AdminSupplierController@edit',
			'as'   => 'admin.supplier.edit'
        ]);

        Route::get('delete/{id}', [
			'uses' => 'AdminSupplierController@destroy',
			'as'   => 'admin.supplier.destroy'
        ]);


        /*===================================
					POST request
    	===================================*/
    	Route::post('store', [
    		'uses' => 'AdminSupplierController@store',
    		'as'   => 'admin.supplier.store'
    	]);

    	Route::post('update/{id}', [
    		'uses' => 'AdminSupplierController@update',
    		'as'   => 'admin.supplier.update'
    	]);
	});    
});


