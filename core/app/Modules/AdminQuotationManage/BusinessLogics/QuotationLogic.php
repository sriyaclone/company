<?php namespace App\Modules\AdminQuotationManage\BusinessLogics;


/**
* Business Logics 
* Define all the busines logics in here
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use Illuminate\Database\Eloquent\Model;
use App\Models\Quotation;
use App\Models\QuotationDetails;
use Core\EmployeeManage\Models\Employee;
use App\Models\Inquiry;
use App\Models\User;
use App\Classes\Functions;
use App\Models\Nbt;
use App\Models\Vat;
use App\Models\SVat;
use App\Models\DeliveryTerms;
use App\Modules\AdminProductManage\Models\Product;
use App\Modules\AdminCustomerManage\Models\Customer;
use App\Modules\AdminQuotationManage\Models\CustomerPo;
use App\Modules\AdminQuotationManage\Models\PoAttachments;
use Core\UserRoles\Models\UserRole;

use DB;
use Excel;
use File;
use Response;
use Sentinel;
use PDF;
use QrCode;

class QuotationLogic extends Model {

	private $quotation;
	private $quotationDetail;
	private $functions;

	public function __construct(Quotation $quotation , QuotationDetails $quotationDetail,Functions $functions){
		$this->quotation = $quotation;
		$this->quotationDetail = $quotationDetail;
		$this->functions = $functions;
	}

	public function getQuotationRecords($filters, $perPage = 10)
	{
		$userRole = $this->getUserRole();
		$role_id = ($userRole)?$userRole->id:null;
		$user_id = ($userRole)?$userRole->pivot->user_id:Sentinel::getUser()->id;

		$employee = User::where('status','=',1)->where('id','=',$user_id)->first();
		$employee_id = $employee->employee_id;
		$sector_id = $employee->sector_id;

		$employee_detail = Employee::where('id','=',$employee_id)->first();
		$employee_type_id = $employee_detail->employee_type_id;

		$employeeReps = $this->functions->getEmployeeRepIds($employee_id);
		
		$data = Quotation::with(['customer','createdBy','customerPo', 'ln_reference']);

		if($filters->from!="" && $filters->to!=""){
			$data=$data->whereBetween('quotation.quotation_date', array($filters->from, $filters->to));
		}

		if($filters->reference != ""){
			$data->where('quotation.reference_no','LIKE',$filters->reference);
		}

		if($filters->customer != ""){
			$data->whereIn('customer_id',function($query) use ($filters){
				$query->select('id')
					->from('customer')
					->where('first_name','LIKE','%'.$filters->customer.'%')
				 	->orWhere('last_name','LIKE','%'.$filters->customer.'%');
			});
		}

		if($filters->type != ""){
			$data->where('quotation.quotation_type','=',$filters->type);
		}

		if($filters->status != ""){
			$data->where('quotation.status','=',$filters->status);
		}

		$data = $data->orderBy('quotation.id','desc');
		return $data->paginate($perPage);
	}

	public function getApprovalQuotations($filters, $perPage = 25)
	{
		$userRole 	= $this->getUserRole();
		$role_id 	= ($userRole)?$userRole->id:null;
		$user_id 	= Sentinel::getUser()->id;

		$employee = User::where('status','=',1)->where('id','=',$user_id)->first();
		$employee_id = $employee->employee_id;
		$sector_id = $employee->sector_id;

		$employee_detail = Employee::where('id','=',$employee_id)->first();
		$employee_type_id = $employee_detail->employee_type_id;

		$employeeReps = $this->functions->getEmployeeRepIds($employee_id);

		$data = Quotation::select('*',DB::raw('quotation.id AS quotation_id'),DB::raw('CONCAT(employee.first_name," ",employee.last_name) AS action_emp'),DB::raw('quotation.status AS quotation_status'))
						->join('customer','customer.id','=','quotation.customer_id')
						->join('employee','employee.id','=','quotation.action_by')
						->leftJoin('inquiries','inquiries.id','=','quotation.inquiry_id')
						->orderBy('quotation.id','asc');

		// $data = Quotation::orderBy('quotation.id','asc');

		if($role_id){
			$data = $data->where('quotation.approval_role_id', $role_id);
		}

		if($filters->from!="" && $filters->to!=""){
			$data = $data->whereBetween('quotation.quotation_date', array($filters->from, $filters->to));
		}

		if($filters->reference != ""){
			$data->where('quotation.reference_no','LIKE','%'.$filters->reference.'%');
		}

		if($filters->customer != ""){
			$data->where('customer.first_name','LIKE','%'.$filters->customer.'%')
				 ->orWhere('customer.last_name','LIKE','%'.$filters->customer.'%');
		}

		if($filters->type != ""){
			$data->where('quotation.quotation_type','=',$filters->type);
		}

		//if($filters->status != ""){
		$data->where('quotation.status','=',PENDING);
		//}

		if($filters->action != ""){
			$data->where('quotation.action_by','=',$filters->action);
		}

		if($sector_id != 0){
			$data->where('inquiries.sector','=',$sector_id);
		}

		return $data->paginate($perPage);


	}

	public function getQuotationDetailRecords($id)
	{
		$data = QuotationDetails::with(['product'])->where('quotation_id','=',$id)->get();

        return $data;
	}

	public function getEmployees()
	{
		$user = Sentinel::getUser();
		$user_id = $user->id;
		$userRole = $this->getUserRole();
		$role_id = ($userRole)?$userRole->id:null;
		$user_id = ($userRole)?$userRole->pivot->user_id:$user_id;

		$employee = User::where('status','=',1)->where('id','=',$user_id)->first();
		$employee_id = $employee->employee_id;

		$employee_detail = Employee::where('id','=',$employee_id)->first();
		$employee_type_id = $employee_detail->employee_type_id;

		if($employee_type_id == 5){
			$employeeReps = $this->functions->getEmployeeRepIds($employee_id);
			return $data = Employee::where('status','=','1')->where('id','!=',1)->whereIn('id',$employeeReps)->get();
		}


		return $data = Employee::where('status','=','1')->where('id','!=',1)->get();
	}

	public function getRecordsQuotationId($id)
	{
		return $data = Quotation::with(['customer'])->where('id',$id)->first();
	}

	public function getRecordsQuotationDetailId($id)
	{
		return $data = QuotationDetails::with(['product'])->where('quotation_id',$id)->get();
	}

	public function getInquiryData($id)
	{
		return $data = Inquiry::with(['sectors','source','transaction.users','transaction.events','transaction.inquriyAssign.employee','image'])->find($id);
	}

	public function getUserRole()
	{
		return $userRole = Sentinel::getUser()->roles()->first();
	}

	public function aproveQuotationDiscount($data)
	{
		DB::transaction(function() use($data){
			$quotation_id = $data->quotation_id;

			foreach($data->detail_id as $key => $detail){
				$quotation_detail = QuotationDetails::find($detail);
				$quotation_detail->discount = $data->additional_discount[$key];
				$quotation_detail->discount_price = $data->discount_price[$key];
				$quotation_detail->save();
			}


			$quotation = Quotation::find($quotation_id);
			$quotation->total_discount = $data->total_discount;
			$quotation->total_amount = $data->total_amount;
			$quotation->status = 1;
			$quotation->approved_by = Sentinel::getUser()->employee_id;
			$quotation->approved_at = date('Y-m-d H:i:s');
			$quotation->save();
	    });

	    $quotation = $this->getQuotationById($data->quotation_id);

	    if($quotation && $quotation->status == 1) {
            $vat = Vat::find($quotation->vat_id);
            $nbt = Vat::find($quotation->nbt_id);

            // $qutationFileName = 'quotation/quotation-' . $quotation->id . '-' . date('YmdHis') . '.pdf';
            // $fileName1 = $qutationFileName;
            // $qutationFileName = $this->generateQuotationPdf($quotation, $nbt->value, $vat->value, $qutationFileName, true);

            // $piFileName = 'quotation/pi/pi-' . $quotation->id . '-' . date('YmdHis') . '.pdf';
            // $fileName2  = $piFileName;
            // $piFileName = $this->generateProformaInvoicePdf($quotation, $nbt->value, $vat->value, $piFileName, true);

            // $quotation->quotation_file_path = $fileName1;
            // $quotation->pi_file_path = $fileName2;
            // $quotation->save();
        }
    	return true;
	}

	public function rejectQuotationDiscount($data){
		DB::transaction(function() use($data){
			$quotation_id = $data->quotation_id;

			$quotation = Quotation::find($quotation_id);
			$quotation->status = -1;
			$quotation->approved_by = Sentinel::getUser()->employee_id;
			$quotation->approved_at = date('Y-m-d H:i:s');
			$quotation->save();
	    });

	    return true;
	}

	public function getQuotationById($id){
		try {
			$data = Quotation::select('*', 
					DB::raw('CONCAT(customer.first_name, " ", IFNULL(customer.last_name,"")) as customer_name'), 
					DB::raw('quotation.id AS id'),
					DB::raw('CONCAT(employee.first_name," ",employee.last_name) AS action_emp'),
					DB::raw('CONCAT(approved_employee.first_name," ",approved_employee.last_name) AS approved_emp'),
					DB::raw('IFNULL(employee.mobile,\'\') AS action_mobile'),
					DB::raw('quotation.status AS quotation_status'),
					DB::raw('customer.code AS bpr'),
					DB::raw('customer.address AS customer_address'),
					DB::raw('customer.is_vat AS customer_isvat'),
					DB::raw('customer.vat AS customer_vat_type'),
					DB::raw('IFNULL(delivery_terms.name,\'-\') AS delivery_term'),
					DB::raw('customer.credit_period AS credit_period'),
					DB::raw('quotation.status AS quotation_status'),
					DB::raw('quotation.action_by'))
				->join('customer','customer.id','=','quotation.customer_id')
				->join('employee','employee.id','=','quotation.action_by')
				->leftJoin('employee as approved_employee','approved_employee.id','=','quotation.approved_by')
				->leftJoin('delivery_terms','quotation.delivery_term_id','=','delivery_terms.id')
				->where('quotation.id', $id)
				->orderBy('quotation.id','asc')
				->with(['details.product'])
				->first();

		    return $data;	

		} catch (\Exception $e) {
			return $e->getMessage();
		}
	}



	public function getQuotationByIdForRevise($id){
		try {
			$data = Quotation::select('*', 
					DB::raw('CONCAT(customer.first_name, " ", IFNULL(customer.last_name,"")) as customer_name'), 
					DB::raw('quotation.id AS id'),
					DB::raw('CONCAT(employee.first_name," ",employee.last_name) AS action_emp'),
					DB::raw('CONCAT(approved_employee.first_name," ",approved_employee.last_name) AS approved_emp'),
					DB::raw('IFNULL(employee.mobile,\'\') AS action_mobile'),
					DB::raw('quotation.status AS quotation_status'),
					DB::raw('customer.code AS bpr'),
					DB::raw('customer.address AS customer_address'),
					DB::raw('customer.is_vat AS customer_isvat'),
					DB::raw('customer.vat AS customer_vat_type'),
					DB::raw('IFNULL(delivery_terms.name,\'-\') AS delivery_term'),
					DB::raw('customer.credit_period AS credit_period'),
					DB::raw('quotation.status AS quotation_status'),
					DB::raw('quotation.action_by AS action_by'))
				->join('customer','customer.id','=','quotation.customer_id')
				->join('employee','employee.id','=','quotation.action_by')
				->leftJoin('employee as approved_employee','approved_employee.id','=','quotation.approved_by')
				->leftJoin('delivery_terms','quotation.delivery_term_id','=','delivery_terms.id')
				->where('quotation.id', $id)
				->orderBy('quotation.id','asc')
				->with(['details'=>function($q)
				{
					$q->leftJoin('product','product.id','=','quotation_details.product_id');
				},'details.product'])
				->first();

		    return $data;	

		} catch (\Exception $e) {
			return $e->getMessage();
		}
	}

	public function getCurrentNbt(){
		try {
			$data = Vat::whereNull('ended_at')->where('type_id','=',NBT)->get();
			return $data;
		} catch (\Exception $e) {
			return $e->getMessage();
		}	
	}

	public function getCurrentVat(){
		try {
			$data = Vat::whereNull('ended_at')->where('type_id','=',VAT)->get();
			return $data;
		} catch (\Exception $e) {
			return $e->getMessage();
		}	
	}

	public function getQuotationByNo($no){
		try {
			$data = Quotation::where('reference_no', $no)
						->with(['details.product'])
						->first();

			if(!$data){
				throw new \Exception('boo');
			}

		    return $data;	

		} catch (\Exception $e) {
			return null;
		}
	}
	
	public function getUser($userId)
	{
		return $users = User::where('status','=',1)->where('id','=',$userId)->first();
	}

	/**
	* @param $type strings values would be N-Normal, D-Display Discount, NB-Show NBT
	*/
	public function generateQuotationPdf(
		$quotation,
		$nbt,
		$vat, 
		$fileName, 
		$inServer = false, 
		$type='N', 
		$withHeader=true){
		/*$page = view("AdminQuotationManage::quotation-pdf", compact('quotation','nbt','vat'))->render();
		PDF::reset();

        PDF::AddPage();
        PDF::writeHTML($page);*/

        ////////////BEGIN//
        $nbt = Vat::where('id', $quotation->nbt_id)->first();
        if($quotation->vat_type == 'vat' || $quotation->vat_type == 'svat'){
			$vat = Vat::where('id', $quotation->vat_id)->first();
			$quotation = $this->calculateVatCustomerQuotation($quotation);
		}else{
			$quotation = $this->calculateNonVatCustomerQuotation($quotation);
			$vat = null;
		} 

        $qr = QrCode::format('png')
        		->errorCorrection('L')
        		->size(200)
        		->margin(2)
        		->generate($quotation->reference_no.'|'.$quotation->total_amount); 

        $page = view("AdminQuotationManage::new-quotation-pdf", compact('quotation','nbt','vat','qr','type'))
        		->render();
		PDF::reset();
		//PDF::setFont('myriadpro');
		PDF::setMargins(10,18,10);
		PDF::setAutoPageBreak(TRUE, 32);

		if($withHeader){
			PDF::setHeaderCallback(function($pdf){
				$image_file = url('assets/pi-images/h-img02.png');
	        	$pdf->Image($image_file, 10, 5, 195, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
			});

			PDF::setFooterCallback(function($pdf){
				$image_file = url('assets/pi-images/f-img01.png');
	        	$pdf->Image($image_file, 10, 265, 195, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
			});
		}

        PDF::AddPage();
        PDF::writeHTML($page);
        if (ob_get_contents()) ob_end_clean();
        ////////////END//

        if($inServer){
        	PDF::Output(public_path($fileName),'F');
        }

        return $fileName;
	}

	/**
	* @param $type strings values would be N-Normal, D-Display Discount, NB-Show NBT
	*/
	public function generateProformaInvoicePdf(
		$quotation,
		$nbt,
		$vat, 
		$fileName, 
		$inServer = false, 
		$type='N', 
		$withHeader=true){
		/*$page = view("AdminQuotationManage::new-pi-pdf", compact('quotation','nbt','vat'))->render();
		PDF::reset();

        PDF::AddPage();
        PDF::writeHTML($page);*/

        ////////////BEGIN//
        $nbt = Vat::where('id', $quotation->nbt_id)->first();
        if($quotation->vat_type == 'vat' || $quotation->vat_type == 'svat'){
			$vat = Vat::where('id', $quotation->vat_id)->first();
			$quotation = $this->calculateVatCustomerQuotation($quotation);
		}else{
			$quotation = $this->calculateNonVatCustomerQuotation($quotation);
			$vat = null;
		}

        $qr = QrCode::format('png')
        		->errorCorrection('L')
        		->size(200)
        		->margin(2)
        		->generate($quotation->reference_no.'|'.$quotation->total_amount); 

        $page = view("AdminQuotationManage::new-pi-pdf", compact('quotation','nbt','vat','qr', 'type'))
        		->render();
		PDF::reset();
		//PDF::setFont('myriadpro');
		PDF::setMargins(10,18,10);
		PDF::setAutoPageBreak(TRUE, 32);

		if($withHeader){
			PDF::setHeaderCallback(function($pdf){
				$image_file = url('assets/pi-images/h-img-0.png');
	        	$pdf->Image($image_file, 10, 5, 195, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
			});

			PDF::setFooterCallback(function($pdf){
				$image_file = url('assets/pi-images/f-img01.png');
	        	$pdf->Image($image_file, 10, 265, 195, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
			});
		}
        PDF::AddPage();
        PDF::writeHTML($page);
        if (ob_get_contents()) ob_end_clean();
        ////////////END//

        if($inServer){
        	PDF::Output(public_path($fileName),'F');
        }

        return $fileName;
	}

	public function calculateNbtPrice($unitPrice, $nbtPer){
		return (($unitPrice * (100 + $nbtPer)) / 100);
	}

	public function calculateNbtValue($unitPrice, $nbtPer){
		return (($unitPrice * $nbtPer) / 100);
	}

	public function calculateVatPrice($price, $vatPer){
		return (($price * (100 + $vatPer)) / 100);
	}

	public function calculateVatValue($price, $vatPer){
		return (($price * $vatPer) / 100);
	}

	public function calculateDiscountPrice($price, $per){
		return (($price * (100 - $per)) / 100);
	}

	public function calculateDiscountValue($price, $per){
		return (($price * $per) / 100);
	}

	public function calculateVatCustomerQuotation($quotation){
		$vat = Vat::where('id', $quotation->vat_id)->first();
		$nbt = Vat::where('id', $quotation->nbt_id)->first();

		if(isset($quotation->details) && count($quotation->details) > 0){
			foreach($quotation->details as $productRow){
				
				if($productRow->is_nbt){
					$unitPrice = $this->calculateNbtPrice($productRow->price, $nbt->value);
					$nbtValue = $this->calculateNbtValue($productRow->price, $nbt->value);
				}else{
					$unitPrice = $productRow->price;
					$nbtValue = 0;
				}

				$productRow->udp = (float)$productRow->udp;
				$productRow->discount = (float)$productRow->discount;

				$discount = $productRow->discount+$productRow->udp;

				if($discount > 0){
					$discountAmount = $this->calculateDiscountValue($unitPrice, $discount);
					$discountPrice = $this->calculateDiscountPrice($unitPrice, $discount);
				}else{
					$discountAmount = 0;
					$discountPrice = $unitPrice;
				}

				if($productRow->is_vat){
					$vatAmount = $this->calculateVatValue($discountPrice, $vat->value);
					$vatPrice = $this->calculateVatPrice($discountPrice, $vat->value);
				}else{
					$vatAmount = 0;
					$vatPrice = $discountPrice;
				}

				$productRow->unitPrice 			= $unitPrice;
				$productRow->nbtValue 			= $nbtValue;
				$productRow->discountAmount 	= $discountAmount;
				$productRow->discountPrice 		= $discountPrice;
				$productRow->vatAmount 			= $vatAmount;
				$productRow->vatPrice 			= $vatPrice;
				$productRow->totalAmount 		= $unitPrice * $productRow->qty;
				$productRow->totalAmountWONBT 	= ($unitPrice-$nbtValue) * $productRow->qty;
				$productRow->totalAmountWODis 	= $discountPrice * $productRow->qty;
				$productRow->totalDiscount 		= $discountAmount * $productRow->qty;
				$productRow->totalVat 			= $vatAmount * $productRow->qty;
				$productRow->totalNbt 			= $nbtValue * $productRow->qty;
			}
		}

		return $quotation;

	}

	public function calculateNonVatCustomerQuotation($quotation){
		$vat = Vat::where('id', $quotation->vat_id)->first();
		$nbt = Vat::where('id', $quotation->nbt_id)->first();

		if(isset($quotation->details) && count($quotation->details) > 0){
			foreach($quotation->details as $productRow){
				if($productRow->is_nbt){
					$unitPrice = $this->calculateNbtPrice($productRow->price, $nbt->value);
					$nbtValue = $this->calculateNbtValue($productRow->price, $nbt->value);
				}else{
					$unitPrice = $productRow->price;
					$nbtValue = 0;
				}

				if($productRow->is_vat){
					$vatAmount = $this->calculateVatValue($unitPrice, $vat->value);
					$vatPrice = $this->calculateVatPrice($unitPrice, $vat->value);
					$unitPrice = $vatPrice;
				}else{
					$vatAmount = 0;
					$vatPrice = $unitPrice;
					$unitPrice = $vatPrice;
				}

				$discount = $productRow->discount+$productRow->udp;

				$productRow->udp = (float)$productRow->udp;
				$productRow->discount = (float)$productRow->discount;

				if($discount > 0){
					$discountAmount = $this->calculateDiscountValue($unitPrice, $discount);
					$discountPrice = $this->calculateDiscountPrice($unitPrice, $discount);
				}else{
					$discountAmount = 0;
					$discountPrice = $unitPrice;
				}

				$productRow->unitPrice 			= $unitPrice;
				$productRow->nbtValue 			= $nbtValue;
				$productRow->discountAmount 	= $discountAmount;
				$productRow->discountPrice 		= $discountPrice;
				$productRow->vatAmount 			= 0;
				$productRow->vatPrice 			= 0;
				$productRow->totalAmount 		= $unitPrice * $productRow->qty;
				$productRow->totalAmountWONBT 	= ($unitPrice-$nbtValue) * $productRow->qty;
				$productRow->totalAmountWODis 	= $discountPrice * $productRow->qty;
				$productRow->totalDiscount 		= $discountAmount * $productRow->qty;
				$productRow->totalVat 			= 0;
				$productRow->totalNbt 			= $nbtValue * $productRow->qty;
			}
		}

		return $quotation;
	}


	public function getDeliveryTerms()
	{
		return $data = DeliveryTerms::lists('name','id');
	}

	public function getItems($limit,$search){		

		$data = Product::whereNull('deleted_at');

		if($search != ''){
            $array = explode(" ", $search);
            foreach ($array as $key=>$value) {
	            $data->where(function($query) use ($value){
	                $query->where('description','LIKE', '%' . $value . '%')
	                	->orWhere('code','LIKE', '%' . $value . '%');
	            });
            }
        }

        return $data = $data->paginate($limit);
	}

	public function getCustomers($limit,$search){		

		$data  = Customer::selectRaw('
                        id,
                        first_name,
                        last_name,
                        blocked,
                        ifnull(credit_limit,0) as credit_limit,
                        ifnull(credit_period,0) as credit_period,
                        IFNULL(is_vat,0) as is_vat,
                        IF(IFNULL(is_vat,0) = 0,\'\',vat) as vat_type,
                        code as short_code')
                    ->where('status',1);

		if($search !== ''){
            $data = $data->Where('first_name','LIKE', '%' . $search . '%');   
            $data = $data->orWhere('last_name','LIKE', '%' . $search . '%');   
            $data = $data->orWhere('code','LIKE', '%' . $search . '%');   
        }

        return $data = $data->paginate($limit);
	}

	public function createNewQuotation($data)
	{
		$user = Sentinel::getUser();

		$action_by = ($data->action_by)?$data->action_by:$user->employee_id;
		
		$dis_type = 0;
		
		if($data->dis_type=='credit'){
			$dis_type = 1;
		}else if($data->dis_type=='cash'){
			$dis_type = 2;
		}	
		
		$quotation = Quotation::create([
			'reference_id'        => null,
			'customer_id'         => $data->customer_id,
			'inquiry_id'          => $data->inquiry_id,
			'quotation_date'      => date('Y-m-d H:i:s'),
			'total_amount'        => $data->total_amount,
			'total_discount'      => $data->total_discount,
			'delivery_term_id'    => $data->delivery_term,
			'vat_id'              => $data->vat_id,
			'nbt_id'              => $data->nbt_id,
			'vat_per'             => $data->vat_per,
			'quotation_type'      => 0,
			'discount_type'       => $dis_type,
			'status'              => 0,
			'vat_type'            => $data->vat_type,
			'attn'                => (isset($data->attn))?$data->attn:'',
			'action_by'           => $action_by,
			'action_done_by'      => $user->employee_id,
			'is_revise_quotation' => 0,
			'remarks' 			  => 'WEB QUOTATION'
        ]);

        $neededApproval = false;
        $approvalDiscount = 0;


        foreach ($data->product_id as $key => $details) {
            if($data->additional_discount[$key] > 0){
                $neededApproval = true;
            }

            if($data->additional_discount[$key] > $approvalDiscount){
                $approvalDiscount = $data->additional_discount[$key];
            }

            $dis = 0;

            if($data->dis_type=='credit'){
				$dis = $data->udp[$key];
			}else if($data->dis_type=='cash'){
				$dis = $data->cash_udp[$key];
			}

            $quotation_details = QuotationDetails::create([
				'quotation_id'   		=> $quotation->id,
				'product_id'     		=> $data->product_id[$key],
				'price'          		=> $data->price[$key],
				'discount'       		=> $data->additional_discount[$key],
				'requested_discount'	=> $data->additional_discount[$key],
				'is_vat'         		=> $data->is_vat[$key],
				'is_nbt'         		=> $data->is_nbt[$key],
				'vat'            		=> $data->vat[$key],
				'nbt'            		=> $data->nbt[$key],
				'udp'            		=> $dis,
				'qty'            		=> $data->qty[$key],
				'discount_price' 		=> 0 //FOR NEW REQ APP NOT SENDING TOTALS CALC IT REALTIME
            ]);

	        if(!$neededApproval){
	            $quotation->approved_at = date('Y-m-d H:i:s');
	            $quotation->approved_by = $user->employee_id;
	            $quotation->status = 1;
	            $quotation->save();
	            $quotation_status = 1;
	        }else{
	            $role = UserRole::where('discount','>=', $approvalDiscount)
	                    ->orderBy('discount', 'asc')->first();

	            if($role){
	                $quotation->approval_role_id = $role->id;
	                $quotation->save();

	                $quotation = $this->getQuotationById($quotation->id);

	                // $emails = Employee::whereIn('id',function($query) use ($role){
	                //     $query->select('employee_id')
	                //         ->from('users')
	                //         ->whereIn('id',function($qry) use ($role){
	                //             $qry->select('user_id')
	                //                 ->from('role_users')
	                //                 ->where('role_id',$role->id);
	                //         });
	                // })
	                // ->whereNotNull('email')
	                // ->lists('email')
	                // ->toArray();

	                // $emails = array_filter($emails);
	                

	                // if(count($emails) > 0){
	                //     Mail::send('email.approval',
	                //         ['quotation' => $quotation], 
	                //         function($m) use ($emails){
	                //         $m->from('projectsales@orelcorp.com', 'Project Sales');
	                //         $m->to($emails)->subject('1-Project Sales Quotation Approval NEEDED');
	                //     });
	                // }
	            }else{
	                $role = UserRole::where('discount','<', $approvalDiscount)
	                    ->orderBy('discount', 'desc')->first();

	                if($role && $role->discount > 0){
	                    $quotation->approval_role_id = $role->id;
	                    $quotation->save();

	                    $quotation = $this->getQuotationById($quotation->id);

	                    // $emails = Employee::whereIn('id',function($query) use ($role){
	                    //     $query->select('employee_id')
	                    //         ->from('users')
	                    //         ->whereIn('id',function($qry) use ($role){
	                    //             $qry->select('user_id')
	                    //                 ->from('role_users')
	                    //                 ->where('role_id',$role->id);
	                    //         });
	                    // })
	                    // ->whereNotNull('email')
	                    // ->lists('email')
	                    // ->toArray();

	                    // $emails = array_filter($emails);

	                    // if(count($emails) > 0){
	                    //     Mail::send('email.approval',
	                    //         ['quotation' => $quotation], 
	                    //         function($m) use ($emails){
	                    //         $m->from('projectsales@orelcorp.com', 'Project Sales');
	                    //         $m->to($emails)->subject('2-Project Sales Quotation Approval NEEDED');
	                    //     });
	                    // }
	                }
	            }
	        }
	    }



		$quotation->reference_no = "QU"."_".$user->employee_id."_".$quotation->id;
        $quotation->save();

	    return $quotation;
	}


	public function createNewReviseQuotation($data,$old_quotation)
	{
		$user = Sentinel::getUser();
		
		$dis_type = 0;
		
		if($data->dis_type=='credit'){
			$dis_type = 1;
		}else if($data->dis_type=='cash'){
			$dis_type = 2;
		}	
		
		$quotation = Quotation::create([
			'reference_id'        => null,
			'customer_id'         => $data->customer_id,
			'inquiry_id'          => $data->inquiry_id,
			'quotation_date'      => date('Y-m-d H:i:s'),
			'total_amount'        => $data->total_amount,
			'total_discount'      => $data->total_discount,
			'delivery_term_id'    => $data->delivery_term,
			'vat_id'              => $data->vat_id,
			'nbt_id'              => $data->nbt_id,
			'vat_per'             => $data->vat_per,
			'quotation_type'      => 0,
			'discount_type'       => $dis_type,
			'status'              => 0,
			'vat_type'            => $data->vat_type,
			'attn'                => (isset($data->attn))?$data->attn:'',
			'action_by'           => $old_quotation->action_by,
			'action_done_by'      => $user->employee_id,
			'is_revise_quotation' => 0,
			'remarks' 			  => 'WEB QUOTATION'
        ]);

        $neededApproval = false;
        $approvalDiscount = 0;


        foreach ($data->product_id as $key => $details) {
            if($data->additional_discount[$key] > 0){
                $neededApproval = true;
            }

            if($data->additional_discount[$key] > $approvalDiscount){
                $approvalDiscount = $data->additional_discount[$key];
            }

            $dis = 0;

            if($data->dis_type=='credit'){
				$dis = $data->udp[$key];
			}else if($data->dis_type=='cash'){
				$dis = $data->cash_udp[$key];
			}

            $quotation_details = QuotationDetails::create([
				'quotation_id'   		=> $quotation->id,
				'product_id'     		=> $data->product_id[$key],
				'price'          		=> $data->price[$key],
				'discount'       		=> $data->additional_discount[$key],
				'requested_discount'	=> $data->additional_discount[$key],
				'is_vat'         		=> $data->is_vat[$key],
				'is_nbt'         		=> $data->is_nbt[$key],
				'vat'            		=> $data->vat[$key],
				'nbt'            		=> $data->nbt[$key],
				'udp'            		=> $dis,
				'qty'            		=> $data->qty[$key],
				'discount_price' 		=> 0 //FOR NEW REQ APP NOT SENDING TOTALS CALC IT REALTIME
            ]);

	        if(!$neededApproval){
	            $quotation->approved_at = date('Y-m-d H:i:s');
	            $quotation->approved_by = $user->employee_id;
	            $quotation->status = 1;
	            $quotation->save();
	            $quotation_status = 1;
	        }else{
	            $role = UserRole::where('discount','>=', $approvalDiscount)
	                    ->orderBy('discount', 'asc')->first();

	            if($role){
	                $quotation->approval_role_id = $role->id;
	                $quotation->save();

	                $quotation = $this->getQuotationById($quotation->id);

	            }else{
	                $role = UserRole::where('discount','<', $approvalDiscount)
	                    ->orderBy('discount', 'desc')->first();

	                if($role && $role->discount > 0){
	                    $quotation->approval_role_id = $role->id;
	                    $quotation->save();

	                    $quotation = $this->getQuotationById($quotation->id);
	                }
	            }
	        }
	    }

	   	$old_quot_no = explode("_",$old_quotation->reference_no);
	   	$rev =1;
	   	if(count($old_quot_no)>3) {
            //THIS IS A REVISION increment last digit
            $rev = intval($old_quot_no[3]) + 1;
        }else{
            //THIS IS A NORMAL PO
            $rev =1;
        }

		$quotation->reference_no = $old_quot_no[0]."_".$old_quot_no[1]."_".$old_quot_no[2]."_".$rev;
        $quotation->save();

	    return $quotation;
	}

	public function getQuotationPdfOutput($id, $type='N')
	{
		$quotation = $this->getQuotationById($id);
        if(!$quotation->approved_at){
            return 0;
        }
        /*$nbt_ar = $this->getCurrentNbt();
        $nbt = $nbt_ar[0]->value;
        $vat_ar = $this->getCurrentVat();
        $vat = $vat_ar[0]->value;*/

        if($quotation->vat_type == 'vat' || $quotation->vat_type == 'svat'){
			$vat = Vat::where('id', $quotation->vat_id)->first();
			$quotation = $this->calculateVatCustomerQuotation($quotation);
		}else{
			$quotation = $this->calculateNonVatCustomerQuotation($quotation);
			$vat = null;
		} 

        $qr = QrCode::format('png')
        		->errorCorrection('L')
        		->size(200)
        		->margin(2)
        		->generate($quotation->reference_no.'|'.$quotation->total_amount); 

        $fileName = 'quotation/quotation-'.$quotation->id.'-'.date('YmdHis').'.pdf';
        $page = view("AdminQuotationManage::new-quotation-pdf", compact('quotation','nbt','vat','qr','type'))
        		->render();
		PDF::reset();
		//PDF::setFont('myriadpro');
		PDF::setMargins(10,18,10);
		PDF::setAutoPageBreak(TRUE, 32);
		PDF::setHeaderCallback(function($pdf){
			$image_file = url('assets/pi-images/h-img02.png');
        	$pdf->Image($image_file, 10, 5, 195, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		});

		PDF::setFooterCallback(function($pdf){
			$image_file = url('assets/pi-images/f-img01.png');
        	$pdf->Image($image_file, 10, 265, 195, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		});
        PDF::AddPage();
        PDF::writeHTML($page);
        if (ob_get_contents()) ob_end_clean();
        return PDF::Output(public_path($fileName),'I');
	}

	public function getPerfromaPdfOutput($id, $type='N')
	{
		$quotation = $this->getQuotationById($id);

        if(!$quotation->approved_at){
            return 0;
        }

        /*$nbt_ar = $this->getCurrentNbt();
        $nbt = $nbt_ar[0]->value;

        $vat_ar = $this->getCurrentVat();
        $vat = $vat_ar[0]->value;*/  

        if($quotation->vat_type == 'vat' || $quotation->vat_type == 'svat'){
			$vat = Vat::where('id', $quotation->vat_id)->first();
			$quotation = $this->calculateVatCustomerQuotation($quotation);
		}else{
			$quotation = $this->calculateNonVatCustomerQuotation($quotation);
			$vat = null;
		} 

        $qr = QrCode::format('png')
        		->errorCorrection('L')
        		->size(200)
        		->margin(2)
        		->generate($quotation->reference_no.'|'.$quotation->total_amount);  

        $fileName = 'quotation/pi/pi-'.$quotation->id.'-'.date('YmdHis').'.pdf';
        PDF::setFont('myriadpro');
        $page = view("AdminQuotationManage::new-pi-pdf", compact('quotation','nbt','vat', 'qr', 'type'))->render();
		PDF::reset();
		//PDF::setFont('myriadpro');
		PDF::setMargins(10,18,10);
		PDF::setAutoPageBreak(TRUE, 32);
		PDF::setHeaderCallback(function($pdf){
			$image_file = url('assets/pi-images/h-img-0.png');
        	$pdf->Image($image_file, 10, 5, 195, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		});

		PDF::setFooterCallback(function($pdf){
			$image_file = url('assets/pi-images/f-img01.png');
        	$pdf->Image($image_file, 10, 265, 195, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		});
        PDF::AddPage();
        PDF::writeHTML($page);
        if (ob_get_contents()) ob_end_clean();
        return PDF::Output(public_path($fileName),'I');
	}

	public function getUploadedPOList($quotation_id)
	{
		$po_list = CustomerPo::with(['uploadedby','attachments'])->where('quotation_id',$quotation_id)->get();
        return $po_list;
	}

	public function getPoAttachmets($po_id)
	{
		$attachments = PoAttachments::where('po_id',$po_id)->get();
        return $attachments;
	}

	public function getPoById($po_id)
	{
		$attachments = CustomerPo::with(['uploadedby'])->find($po_id);
        return $attachments;
	}

	public function endAllPOSFor($id)
	{
		return CustomerPo::where('quotation_id',$id)
                ->whereNull('ended_at')
                ->update([
                    'ended_at' => date('Y-m-d H:i:s')
                ]);

	}

	public function createNewPo($data,$quotation,$user)
	{
		return CustomerPo::create([
                'quotation_id'  => $quotation->id,
                'expected_date' => $data->expected_date,
                'reference_no'  => $quotation->reference_no,
                'po_number'     => $data->reference_no,
                'verified'      => $data->verified?$data->verified:0,
                'customer_id'   => $quotation->customer_id,
                'employee_id'   => $user->employee_id
            ]);

	}

	public function createNewPoAttachment($po_id,$file_ext,$name)
	{
		return PoAttachments::create([
                            'po_id'    => $po_id,
                            'file_ext' => $file_ext,
                            'file'     => 'uploads/po/'.$name
                        ]);

	}

	public function doDiscardQuotation($quotation)
	{
		$quotation->status = DISCARD;
		return $quotation->save();

	}
}
