<?php namespace App\Modules\AdminQuotationManage\Controllers;


/**
* Controller class
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Modules\AdminQuotationManage\BusinessLogics\QuotationLogic;
use App\Models\Vat;
use App\Models\User;
use Sentinel;
use PDF;
use App\Models\Quotation;
use App\Models\QuotationDetails;
use DB;
use File;
use App\Modules\AdminCustomerManage\Models\Customer;
use Core\EmployeeManage\Models\Employee;
use App\Modules\AdminCustomerManage\BusinessLogics\CustomerLogic;
use Mail;
use App\Modules\AdminQuotationManage\Requests\QuotationRequest;
use App\Models\AppUser;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use LaravelFCM\Message\Topics;
use FCM;

class AdminQuotationManageController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	protected $quotationLogic;
	protected $CustomerLogic;

	public function __construct(QuotationLogic $quotationLogic,CustomerLogic $customerLogic){
		$this->quotationLogic = $quotationLogic;
		$this->customerLogic = $customerLogic;
	}

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listView(Request $request)
	{
		$old = $request;

		$action_by = $this->quotationLogic->getEmployees();

		$data = $this->quotationLogic->getQuotationRecords($old);

		return view("AdminQuotationManage::list", compact('data','old','action_by'));
	}

	public function approveView(Request $request)
	{
		$old = $request;
		$action_by = $this->quotationLogic->getEmployees();
		$data = $this->quotationLogic->getApprovalQuotations($old);
		return view("AdminQuotationManage::approval", compact('data','old','action_by'));
	}


	public function createView(Request $request)
	{
		$delivery_terms = $this->quotationLogic->getDeliveryTerms();
		$vat = $this->quotationLogic->getCurrentVat()[0];
		$nbt = $this->quotationLogic->getCurrentNbt()[0];
		$user = User::with('emp')->find(Sentinel::getUser()->id);
		$sales_reps = Employee::whereIn('employee_type_id',[9,10])
		->selectRaw('id, CONCAT(code,\'-\',first_name,\' \',last_name) as name')->get();

		$sales_reps->prepend(['id'=>'','name'=>'Select a Sales Person']);

		$is_invoice = ($user->inRole('invoice'))?1:0;

		return view("AdminQuotationManage::create",compact('delivery_terms','vat','nbt','user','is_invoice','sales_reps'));
	}

	public function quotationPdf(Request $request,$id)
	{
		return  $this->quotationLogic->getQuotationPdfOutput($id);
	}

	public function poView(Request $request,$id)
	{
		$list  = $this->quotationLogic->getQuotationById($id);
		$po_list  = $this->quotationLogic->getUploadedPOList($id);
		return view("AdminQuotationManage::po",compact('list','po_list'));
	}

	public function reviseView(Request $request,$id)
	{
		$quotation      = $this->quotationLogic->getQuotationByIdForRevise($id);
		$customer       = $this->customerLogic->getCustomerById($quotation->customer_id);
		$delivery_terms = $this->quotationLogic->getDeliveryTerms();
		$vat            = $this->quotationLogic->getCurrentVat()[0];
		$nbt            = $this->quotationLogic->getCurrentNbt()[0];
		$user           = User::with('emp')->find(Sentinel::getUser()->id);

		return view("AdminQuotationManage::revise",compact('delivery_terms','vat','nbt','user','quotation','customer'));
	}

	public function poAttachmentsView(Request $request,$id)
	{
		$attachments  = $this->quotationLogic->getPoAttachmets($id);
		$list  = $this->quotationLogic->getQuotationById($id);
		$po  = $this->quotationLogic->getPoById($id);
		return view("AdminQuotationManage::attachments",compact('attachments','list','po'));
	}

	public function performaPdf(Request $request,$id)
	{
		return  $this->quotationLogic->getPerfromaPdfOutput($id);
	}

	public function create(Request $request)
	{	

		$validate = $this->validate($request,[
	        'customer_id' => 'required',
	        'delivery_term' => 'required'
	    ]);
		
		$quotation = $this->quotationLogic->createNewQuotation($request);
		$url = url('admin/quotation/quotation_pdf/')."/".$quotation->id;
		return redirect('admin/quotation/create')->with([
			'link'           => true,
			'link.link'      => $url,
			'link.linktitle' => ' Show Quotation',
			'link.message'   => 'New Quotation saved! #('.$quotation->reference_no.')',
			'link.title'     => 'Success..!'
        ]);

	}

	public function createRevision(Request $request,$id)
	{
		try {
            DB::beginTransaction();
			$old_quotation  = $this->quotationLogic->getQuotationById($id);		
			$revision = $this->quotationLogic->createNewReviseQuotation($request,$old_quotation);			
	        DB::commit();
			$url = url('admin/quotation/quotation_pdf/')."/".$revision->id;
			return redirect('admin/quotation/revise/'.$old_quotation->id)->with([
				'link'           => true,
				'link.link'      => $url,
				'link.linktitle' => ' Show Quotation',
				'link.message'   => 'Revise Quotation saved! #('.$revision->reference_no.')',
				'link.title'     => 'Success..!'
	        ]);
	    } catch (Exception $ex) {
            DB::rollback();
            return redirect('admin/quotation/revise/'.$old_quotation->id)->with([
				'error'           => true,
				'error.message'   => 'Error occured while saving',
				'error.title'     => 'Error..!'
	        ]);
        }

	}

	public function listViewDetail($id, Request $request)
	{
		$id        = $request->id;
		$details   = $this->quotationLogic->getQuotationDetailRecords($id);
		$list      = $this->quotationLogic->getQuotationById($id);

		if($list->vat_type == 'vat' || $list->vat_type == 'svat'){
			$vat = Vat::where('id', $list->vat_id)->first();
			$list = $this->quotationLogic->calculateVatCustomerQuotation($list);
		}else{
			$list = $this->quotationLogic->calculateNonVatCustomerQuotation($list);
			$vat = null;
		}

		return view("AdminQuotationManage::details", compact('details', 'list', 'vat'));
	}

	public function show($id, Request $request)
	{
		$user_role = $this->quotationLogic->getUserRole();
		$max_discount = ($user_role)?$user_role->discount:0;
		/*$quotation = $this->quotationLogic->getRecordsQuotationId($id);

		$quotation_detail = $this->quotationLogic->getRecordsQuotationDetailId($id);
		
		$inquiry_id = $quotation->inquiry_id;
		$data = $this->quotationLogic->getInquiryData($inquiry_id);*/

		$id      = $request->id;
		$details = $this->quotationLogic->getQuotationDetailRecords($id);
		$list    = $this->quotationLogic->getQuotationById($id);

		if($list->quotation_status != PENDING || 
			$max_discount < 1 || 
			$list->approval_role_id != $user_role->id){
			return view('errors.404');
		}

		$vat = Vat::where('id', $list->vat_id)->first();
		$nbt = Vat::where('id', $list->nbt_id)->first();

		if($list->vat_type == 'vat' || $list->vat_type == 'svat'){
			$list = $this->quotationLogic->calculateVatCustomerQuotation($list);
		}else{
			$list = $this->quotationLogic->calculateNonVatCustomerQuotation($list);
			//$vat = null;
		}

		//return $list;

		/*return view("AdminQuotationManage::approvaldetail", compact('data','quotation','quotation_detail','max_discount'));*/

		return view("AdminQuotationManage::approve_view", compact('details', 'list', 'vat', 'max_discount', 'nbt'));
	}

	public function edit(Request $request)
	{
		if($request->aprove){
			try {
		        $booh = $this->quotationLogic->aproveQuotationDiscount($request);	

				$quotation = Quotation::find($request->quotation_id);
				$appuser = AppUser::where('employee_id',$quotation->action_by)->first();

		        if($appuser->fcm_token!=null){
                    $optionBuilder                  = new OptionsBuilder();

                    $notificationBuilder = new PayloadNotificationBuilder("");
                    $notificationBuilder->setBody("Your Quotation has been approved, 
                    	(#".$quotation->reference_no.") - Rs".$quotation->total_amount)
                                        ->setTitle('O|Sales - Quotation Approved')
                                        ->setSound('default');


                    $dataBuilder                    = new PayloadDataBuilder();
                    $dataBuilder->addData(['a_data' => 'my_data']);
                    $option                         = $optionBuilder->build();
                    $notification                   = $notificationBuilder->build();
                    $data                           = $dataBuilder->build();
                    $downstreamResponse             = FCM::sendTo($appuser->fcm_token, $option, $notification, $data);
                    $downstreamResponse->numberSuccess();
                    $downstreamResponse->numberFailure();
                    $downstreamResponse->numberModification();
                }	        

		        return redirect('admin/quotation/approval')->with([
	                'success' => true,
	                'success.message' => 'Approval Proceeded!',
	                'success.title'   => 'Success..!'
	            ]);
		    }catch(Exception $e){
	            return redirect('admin/quotation/show',['id' => $request->quotation_id])->with([
	                'error' => true,
	                'error.title' => 'Error..!',
	                'error.message' => $e->getMessage()
	            ])->withInput();
	        }

		}elseif($request->reject) {
			try{
		        $booh = $this->quotationLogic->rejectQuotationDiscount($request);		        
		        return redirect('admin/quotation/approval')->with([
	                'success' => true,
	                'success.message' => 'Approval Rejected!',
	                'success.title'   => 'Success..!'
	            ]);
		    }catch(Exception $e){
	            return redirect('admin/quotation/show',['id' => $request->quotation_id])->with([
	                'error' => true,
	                'error.title' => 'Error..!',
	                'error.message' => $e->getMessage()
	            ])->withInput();
	        }
		}
	}


	public function quotationView($id, Request $request){
		$old = $request;

		$quotation = $this->quotationLogic->getQuotationById($id);

		$page = view("AdminQuotationManage::quotation-pdf", compact('quotation'))->render();

		PDF::AddPage();
        PDF::writeHTML($page);
        PDF::Output(public_path('quotation/quotation-'.$id.'.pdf'),'F');
	}

	public function getItems(Request $request){
		return $this->quotationLogic->getItems(50,$request->search);
	}

	public function getCustomers(Request $request){
		return $this->quotationLogic->getCustomers(50,$request->search);
	}

	public function poUpload(Request $request,$id)
	{
		$quotation  = $this->quotationLogic->getQuotationById($id);
		return view("AdminQuotationManage::po_upload",compact('quotation'));
	}

	public function poUploadSave(Request $request,$id)
	{

		$this->validate($request, [
			'file'          => 'required',
			'warehouse'     => 'required',
			'expected_date' => 'required',
			'reference_no'  => 'required',
			'verified'      => 'required'
		]);

			

		try {
            DB::beginTransaction();
            $user              = Sentinel::getUser();
			$quotation         = $this->quotationLogic->getQuotationById($id);
			$ended             = $this->quotationLogic->endAllPOSFor($id);
			$po                = $this->quotationLogic->createNewPo($request,$quotation,$user);
			

            if($request->hasfile('file')){
	            foreach($request->file('file') as $key=>$file){
                    $file_ext = $file->getClientOriginalExtension();
                    $destinationPath = storage_path('uploads/po/');
                    if (!file_exists($destinationPath)) {
                        mkdir($destinationPath, 0777, true);
                    }

                    $name = date("YmdHis").'_'.$quotation->id.'_'.$key.'.'.$file_ext;
                    if($file->move(storage_path().'/uploads/po/', $name)){
                        $poAttachment = $this->quotationLogic->createNewPoAttachment($po->id,$file_ext,$name);                             
                    }
                }
            }

            DB::commit();

            try{
                $customer = Customer::find($quotation->customer_id);
                $employee = Employee::find($user->employee_id);

                Mail::send('email.po_upload',[
                    'employee' => $employee,
                    'po' => $po,
                    'reference' => $po->reference_no
                ],function($m) use ($imag){
                    $m->from('oits.webteam@gmail.com', 'Orel Project Sales');
                    $m->bcc('yasith@orelit.com', 'Yasith');
                    $m->to('tharindup@orelit.com', 'Tharindu');
                    $m->to('isuruf@orelit.com', 'Isuru')
                      ->subject('New PO #'.$po->reference_no.' Uploaded');
                });

                // if($customer->blocked && $employee->email && $employee->email != ''){
                //     Mail::send('email.customer_block',[
                //         'customer' => $customer
                //     ],function($m) use ($employees, $imag){
                //         $m->from('oits.webteam@gmail.com', 'Orel Project Sales');
                //         $m->bcc('yasith@orelit.com', 'Yasith Sam');
                //         $m->to($employee->email, $employee->first_name)
                //           ->subject('Customer Blocked for PO #'.$imag->po_number);
                //     });
                // }
            }catch(\Exception $e){

            }

			return response()->json(['status'=>1]);


        } catch (Exception $ex) {
            DB::rollback();
            return response()->json(['status'=>0]);
        }
	}


	public function verifyCustomer(Request $request){
		$response['status'] = 0;
		$response['data']   = null;

		try {
            $quotation       = $this->quotationLogic->getQuotationById($request->id);
			$customer        = Customer::find($quotation->customer_id);
			$customer_status = $this->customerUnBlockLogic->verifyCustomer($customer->code,$quotation);
			$response['status'] = 1;
			$response['data']   = $customer_status;
			return response()->json($response);
        } catch (Exception $ex) {
            return response()->json($response);
        }
	}

	public function sendRequest(Request $request){
		$response['status'] = 0;
		$response['data']   = null;

		try {
            DB::beginTransaction();
			$user      = Sentinel::getUser();
			$quotation = $this->quotationLogic->getQuotationById($request->quotation_id);
			$customer  = Customer::find($quotation->customer_id);			
			$req       = $this->customerUnBlockLogic->newRequest($request->type,$quotation,$customer->id,$request->note,$user);
			DB::commit();
			return response()->json($req);
        } catch (Exception $ex) {
            DB::rollback();
            return response()->json($response);
        }
	}

	public function getQuotationWithDetails(Request $request){
		$response['status'] = 0;
		$response['data']   = null;
		try {
			$quotation = $this->quotationLogic->getQuotationByIdForRevise($request->quotation_id);
			$response['data'] = $quotation;
			return response()->json($response);
        } catch (Exception $ex) {
            DB::rollback();
            return response()->json($response);
        }
	}

	public function doDiscardQuotation(Request $request){
		$response['status'] = 0;
		try {
			DB::beginTransaction();
			$quotation = $this->quotationLogic->getQuotationById($request->id);
			$canceled = $this->quotationLogic->doDiscardQuotation($quotation);
			$response['status'] = 1;
			DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            return redirect('admin/quotation/list')->with([
                'error' => true,
                'error.title' => 'Error..!',
                'error.message' => $e->getMessage()
            ])->withInput();
        }
        return redirect('admin/quotation/list')->with([
            'success' => true,
            'success.title' => 'Success..!',
            'success.message' => "Quotation discarded"
        ])->withInput();
	}
}
