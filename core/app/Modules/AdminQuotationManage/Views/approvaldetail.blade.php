@extends('layouts.back_master') @section('title','List Quotation Details')
@section('current_title','Approval List')

@section('css')
<style type="text/css">
    .port-image
    {
        width: 100%;
    }

    .gallery_product
    {
        margin-bottom: 30px;
    }

    .mediam
    {
        font-size: medium;
    }

    .timeline{position:relative;margin:0 0 30px 0;padding:0;list-style:none}

    .timeline:before{content:'';position:absolute;top:0;bottom:0;width:4px;background:#ddd;left:31px;margin:0;border-radius:2px}

    .timeline>li{position:relative;margin-right:10px;margin-bottom:15px}

    .timeline>li:before,.timeline>li:after{content:" ";display:table}

    .timeline>li:after{clear:both}

    .timeline>li>.timeline-item{-webkit-box-shadow:0 1px 1px rgba(0,0,0,0.1);box-shadow:0 1px 1px rgba(0,0,0,0.1);border-radius:3px;margin-top:0;background:#fff;color:#444;margin-left:60px;margin-right:15px;padding:0;position:relative}

    .timeline>li>.timeline-item>.time{color:#999;float:right;padding:10px;font-size:12px}

    .timeline>li>.timeline-item>.timeline-header{margin:0;color:#555;border-bottom:1px solid #f4f4f4;padding:10px;font-size:16px;line-height:1.1}

    .timeline>li>.timeline-item>.timeline-header>a{font-weight:600}

    .timeline>li>.timeline-item>.timeline-body,.timeline>li>.timeline-item>.timeline-footer{padding:10px}

    .timeline>li>.fa,.timeline>li>.glyphicon,.timeline>li>.ion{width:30px;height:30px;font-size:15px;line-height:30px;position:absolute;color:#666;background:#d2d6de;border-radius:50%;text-align:center;left:18px;top:0}

    .timeline>.time-label>span{font-weight:600;padding:5px;display:inline-block;background-color:#fff;border-radius:4px}

    .timeline-inverse>li>.timeline-item{background:#f0f0f0;border:1px solid #ddd;-webkit-box-shadow:none;box-shadow:none}

    .timeline-inverse>li>.timeline-item>.timeline-header{border-bottom-color:#ddd}

    .profile-user-img {
        margin: 0px auto;
        width: 100px;
        height: 100px;
        padding: 3px;
        border: 3px solid #D2D6DE;
    }

    .img-circle {
        border-radius: 50%;
    }

    .inquiry-details span{
        font-size: 12px;
    }

    .inquiry-status{
        font-size: 12px
    }

    .mailbox-attachment img {
        max-width: 100%;
        max-height: 100%;
    }


    .square {
        height: 75px;
        width: 75px;
    }
    .mailbox-attachment-name{
        font-size: 12px;
    }
</style>  
@stop

@section('content')
<!-- Content-->
<section>
  <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Quotation<small>Management</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
            <li class="active">Quotation Management</li>
        </ol>
    </section>

    <!-- !!Content Header (Page header) -->
     <section class="content"> 
        <form action="{{url('admin/quotation/edit')}}" class="form-horizontal" method="post">
            {!!Form::token()!!}    
            <div class="box box-default">
                <div class="box-body">    
                   <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="pull-left" style="margin-left:20px;margin-top:10px">
                                <span style="margin-top:-8px">
                                    <p class="text-muted pull-left">Inquiry No: 
                                        @if($data != '')
                                        #{{$data->inquiry_code?$data->inquiry_code:"n/a"}}
                                        @endif
                                    </p>
                                    <p class="text-muted pull-left" style="margin-left:20px">Project : 
                                        @if($data != '')
                                        {{$data->title?$data->title:"no project name"}}
                                        @endif
                                    </p>
                                </span>
                                <br>               
                                <span class="inquiry-details">                              
                                    <span class="label label-primary pull-left" >Sector - 
                                        @if($data != '')
                                        {{$data['sectors']?$data['sectors']->name:"no sector"}}
                                        @endif
                                    </span>
                                    <span class="label label-primary pull-left" style="margin-left:10px">Source - 
                                        @if($data != '')
                                        {{$data['source']?$data['source']->name:"no source"}}
                                        @endif
                                    </span>
                                    @if($data != '')
                                    <span class="label label-primary pull-left" style="margin-left:10px">
                                        {{ $data->created_source}}
                                    </span>
                                    @endif
                                </span>
                            </div>
                            <div class="box-tools pull-right">
                                <input type="submit" name="aprove" id="aprove" class="btn btn-success" value="Approve">
                                <input type="submit" name="reject" id="reject" class="btn btn-danger" value="Reject">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="pull-left" style="margin-left:20px;margin-top:10px">
                                <span style="margin-top:-8px">
                                    <p class="text-muted pull-left">Quotation No: #{{$quotation->reference_no?$quotation->reference_no:"n/a"}}</p>
                                    <p class="text-muted pull-left" style="margin-left:20px">Date : {{$quotation->quotation_date?$quotation->quotation_date:"no date"}}</p>
                                    <p class="text-muted pull-left" style="margin-left:20px">Customer Name : {{$quotation->customer?$quotation->customer->first_name.' ('.$quotation->customer->code.')':"no date"}}</p>
                                </span>
                                <input type="hidden" name="quotation_id" id="quotation_id" value="{{$quotation->id}}">
                                <br>               
                                
                            </div>
                            <div class="box-tools pull-right">
                                <span class="inquiry-details">                              
                                    @if($quotation->quotation_type==1)
                                        <span class="label label-warning" style="margin-top:15px;">
                                            Special
                                        </span>
                                    @elseif($quotation->quotation_type==0)
                                        <span class="label label-primary" style="margin-top:15px;">
                                            Normal
                                        </span>
                                    @endif
                                </span>
                            </div>
                        </div>
                   </div>
                   <table class="table table-bordered">
                            <thead>
                              <tr>
                                <th class="text-center">#</th>
                                <th class="text-center" width="15%">Product Code</th>
                                <th class="text-center">Product</th>
                                <th class="text-center">Unit Price (Rs)</th>
                                <th class="text-center">Qty</th>
                                <th class="text-center">Discount (%)</th>
                                <th class="text-center">Discount Price (Rs)</th>
                                <th class="text-center">Amount (Rs)</th>
                              </tr>
                            </thead>
                            <tbody>
                                @if(count($quotation_detail) > 0)
                                <?php $total = 0; ?>
                                <?php $total_discount = 0; ?>
                                <?php $i=0; ?>
                                @foreach($quotation_detail as $key => $val)
                                    <?php $discount_price = ((float)$val->price * (100 - (float)$val->discount))/100 ; ?>
                                    <?php $amount = (float)$discount_price * (int)$val->qty ; ?>
                                    <?php $total += ((float)$val->price * (int)$val->qty) ?>
                                    <?php $total_discount +=  (((float)$val->price * (float)$val->discount)/100) * (int)$val->qty ;?>

                                    <tr id="tr_{{$key+1}}">
                                        <td align="center" style="width: 5%">{{$key+1}}<input type="hidden" name="detail_id_{{$key+1}}" id="detail_id_{{$key+1}}" value="{{$val->id}}"></td>    
                                        <td align="right" style="width: 15%">{{$val->product->code}}</td>    
                                        <td align="right" style="width: 20%">{{$val->product->name}}</td>    
                                        <td align="right" style="width: 10%">
                                            {{$val->price}}<input type="hidden" name="price_{{$key+1}}" id="price_{{$key+1}}" value="{{$val->price}}">
                                        </td>    
                                        <td align="right" style="width: 10%">
                                            {{$val->qty}}<input type="hidden" name="qty_{{$key+1}}" id="qty_{{$key+1}}" value="{{$val->qty}}">
                                        </td>    
                                        <td align="right" style="width: 10%">
                                            <input class="form-control text-center" onkeyup="setPrice({{$key+1}})" type="number" name="discount_{{$key+1}}" id="discount_{{$key+1}}" value="{{$val->discount}}">
                                            <input type="hidden" name="max_discount_{{$key+1}}" id="max_discount_{{$key+1}}" value="{{$max_discount}}">                                        
                                            <input type="hidden" name="previous_discount_{{$key+1}}" id="previous_discount_{{$key+1}}" value="{{$val->discount}}">                                        
                                        </td>    
                                        <td align="right" style="width: 15%">
                                            <input class="form-control text-center" readonly="true" type="text" name="discount_price_{{$key+1}}" id="discount_price_{{$key+1}}" value="{{number_format($discount_price,2)}}">
                                        </td>    
                                        <td align="right" style="width: 15%">
                                            <input class="form-control text-center" readonly="true" type="text" name="amount_{{$key+1}}" id="amount_{{$key+1}}" value="{{number_format($amount,2)}}">
                                        </td>
                                        <?php $i++; ?>
                                    </tr>
                                @endforeach 
                                <tr>
                                  <td colspan="7" align="right">Amount</td>
                                  <td align="right">
                                    <input type="hidden" name="row_count" id="row_count" value="{{$i}}">
                                    <span id="total">{{number_format($total, 2)?:'-' }}</span>
                                  </td>
                                </tr>
                                <tr>
                                  <td colspan="7" align="right">Discount</td>
                                  <td align="right">
                                    <span id="discount">{{number_format($total_discount, 2)?:'-' }}</span>
                                  </td>
                                </tr>
                                
                                <tr>
                                  <td colspan="7" align="right"><strong>Total Amount</strong></td>
                                  <td align="right"><strong>
                                    <span id="amount">{{ number_format(($total - $total_discount), 2)?:'-' }}</span>
                                  </strong></td>
                                </tr>
                              @else
                                <tr>
                                  <td colspan="9" align="center"> - No Data to Display - </td>
                                </tr>
                              @endif  
                            </tbody>
                          </table>
                </div>
            </div>

            <!-- <div class="box box-default">
                <div class="box-body">
                    <div class="form-group">    
                        <p>Quotation Details </p>
                        <hr style="margin-top:0">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">Product Code</th>
                                    <th class="text-center">Product</th>
                                    <th class="text-center">Price(Rs.)</th>
                                    <th class="text-center">Qty</th>
                                    <th class="text-center">Discount(%)</th>
                                    <th class="text-center">Discount Price(Rs.)</th>
                                    <th class="text-center">Amount(Rs.)</th>
                                    </tr>
                            </thead>
                            <tbody>
                                <?php $total = 0; ?>
                                <?php $total_discount = 0; ?>
                                <?php $i=0; ?>
                                @foreach($quotation_detail as $key => $val)
                                    <?php $discount_price = ((float)$val->price * (100 - (float)$val->discount))/100 ; ?>
                                    <?php $amount = (float)$discount_price * (int)$val->qty ; ?>
                                    <?php $total += ((float)$val->price * (int)$val->qty) ?>
                                    <?php $total_discount +=  (((float)$val->price * (float)$val->discount)/100) * (int)$val->qty ;?>

                                    <tr id="tr_{{$key+1}}">
                                        <td align="center" style="width: 5%">{{$key+1}}<input type="hidden" name="detail_id_{{$key+1}}" id="detail_id_{{$key+1}}" value="{{$val->id}}"></td>    
                                        <td align="right" style="width: 15%">{{$val->product->code}}</td>    
                                        <td align="right" style="width: 20%">{{$val->product->name}}</td>    
                                        <td align="right" style="width: 10%">
                                            {{$val->price}}<input type="hidden" name="price_{{$key+1}}" id="price_{{$key+1}}" value="{{$val->price}}">
                                        </td>    
                                        <td align="right" style="width: 10%">
                                            {{$val->qty}}<input type="hidden" name="qty_{{$key+1}}" id="qty_{{$key+1}}" value="{{$val->qty}}">
                                        </td>    
                                        <td align="right" style="width: 10%">
                                            <input class="form-control text-center" onkeyup="setPrice({{$key+1}})" type="number" name="discount_{{$key+1}}" id="discount_{{$key+1}}" value="{{$val->discount}}">
                                            <input type="hidden" name="max_discount_{{$key+1}}" id="max_discount_{{$key+1}}" value="{{$max_discount}}">                                        
                                            <input type="hidden" name="previous_discount_{{$key+1}}" id="previous_discount_{{$key+1}}" value="{{$val->discount}}">                                        
                                        </td>    
                                        <td align="right" style="width: 15%">
                                            <input class="form-control text-center" readonly="true" type="text" name="discount_price_{{$key+1}}" id="discount_price_{{$key+1}}" value="{{number_format($discount_price,2)}}">
                                        </td>    
                                        <td align="right" style="width: 15%">
                                            <input class="form-control text-center" readonly="true" type="text" name="amount_{{$key+1}}" id="amount_{{$key+1}}" value="{{number_format($amount,2)}}">
                                        </td>
                                        <?php $i++; ?>
                                    </tr>
                                @endforeach  
                            </tbody>
                        </table>
                    </div>
                    <input type="hidden" name="row_count" id="row_count" value="{{$i}}">
                    <div class="form-group">
                        <div class="col-md-8"></div>
                        <div class="col-md-1">Total(Rs.) : </div>
                        <div class="col-md-3">
                            <input type="text" readonly id="total" name="total" value="{{number_format($total,2)}}" class='form-control'/>
                        </div>
                        <br>
                    </div>
                    <div class="form-group">
                        <div class="col-md-8"></div>
                        <div class="col-md-1">Discount(Rs.) : </div>
                        <div class="col-md-3">
                            <input type="text" readonly id="discount" name="discount" value="{{number_format($total_discount,2)}}" class='form-control'/>
                        </div>
                        <br>
                    </div>
                    <div class="form-group">
                        <div class="col-md-8"></div>
                        <div class="col-md-1">Amount(Rs.) : </div>
                        <div class="col-md-3">
                            <input type="text" readonly id="amount" name="amount" value="{{number_format(($total - $total_discount),2)}}" class='form-control'/>
                        </div>
                        <br>
                    </div>
                </div>
            </div> -->   
        </form> 
    </section>        


    <!-- Main content -->

</section>
<!-- !!!Content -->


@stop
@section('js')
  <!-- CORE JS -->
<script type="text/javascript">
    $(document).ready(function(){
        // $('body').addClass('sidebar-collapse');
        // $('#time_container').perfectScrollbar();
    });

    function setPrice(x){
        if($("#discount_"+x).val() == ""){
            $("#discount_"+x).val(0);
        }

        var price = parseFloat($("#price_"+x).val());
        var discount = parseFloat($("#discount_"+x).val());
        var qty = parseInt($("#qty_"+x).val());
        var previous_discount = parseFloat($("#previous_discount_"+x).val());
        var max_discount = parseFloat($("#max_discount_"+x).val());

        if(discount > max_discount){
            $("#discount_"+x).val(previous_discount);
            discount = previous_discount;
            $('#discount_'+x).css("border", "1px solid red");
        }else{
            $('#discount_'+x).css("border", "1px solid black");
        }

        var discount_price = (price * (100 - discount))/100;
        $("#discount_price_"+x).val(discount_price.toLocaleString(undefined,{ minimumFractionDigits: 2 }));

        $("#amount_"+x).val((discount_price * qty).toLocaleString(undefined,{ minimumFractionDigits: 2 })); 

        var row_count = $("#row_count").val();
        var total_all = 0;
        var discount_all = 0;
        for(var i=1; i<=row_count ; i++){
            var price_line = parseFloat($("#price_"+i).val());
            var qty_line = parseInt($("#qty_"+i).val());
            var discount_line = parseFloat($("#discount_"+i).val());

            discount_all +=  (price_line*discount_line/100) * qty_line;

            total_all += (price_line - (price_line * discount_line/100)) * qty_line;
        }       
        $("#discount").text(discount_all.toLocaleString(undefined,{ minimumFractionDigits: 2 }));
        $("#amount").text(total_all.toLocaleString(undefined,{ minimumFractionDigits: 2 }));
    }
</script>
  <!-- //CORE JS -->
@stop
