@extends('layouts.back_master') @section('title','List Quotation')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
<link rel="stylesheet" type="text/css" src="{{asset('assets/dist/bootstrap-datepicker/css/bootstrap-datepicker.css')}}"/>

<style type="text/css">
  table .btn{
    padding: 2px 6px;
  }

  .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
    padding-top:5px; 
    padding-bottom:5px; 
  }
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Quotation<small>Management</small></h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li class="active">Quotation Management</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Approval Quotations</h3>
		</div>
		<br>
		<div class="box-body">

      <form method="get">
        <div class="row form-group">
          <div class="col-sm-4">
            <label for="" class="control-label">Reference No.</label>
            <input type="text" class="form-control" name="reference" id="reference" value="{{$old->reference}}">
          </div>
          <div class="col-sm-4">
            <label for="" class="control-label">Customer</label>
            <input type="text" class="form-control" name="customer" id="customer" value="{{$old->customer}}">
          </div>
          <div class="col-sm-4">
            <label for="" class="control-label">Type</label>
            <select name="type" id="type" class="form-control chosen">
              <option value="" @if($old->type == "") selected @endif >Select an Option</option>  
              <option value="1" @if($old->type == "1") selected @endif >Special</option>  
              <option value="0" @if($old->type == "2") selected @endif >Normal</option>  
            </select>
          </div>
        </div>
        <div class="row form-group">
          <div class="col-sm-4">
            <label for="" class="control-label">From</label>
            <input id="from" name="from" type="text" class="form-control datepick" value="@if(isset($old->from)){{$old->from}}@else{{'2017-01-01'}}@endif">
          </div>
          <div class="col-sm-4">
            <label for="" class="control-label">To</label>
            <input type="text" class="form-control datepick" name="to" id="to" value="@if(isset($old->to)){{$old->to}}@else{{date('Y-m-d')}}@endif">
          </div>
          <div class="col-sm-4">
            <label for="" class="control-label">Status</label>
            <select name="status" id="status" class="form-control chosen">
              <option value="" @if($old->status == "") selected @endif >Select an Option</option>  
              <option value="0" @if($old->status == "0") selected @endif >Pending</option>  
              <option value="1" @if($old->status == "1") selected @endif >Aproved</option>  
              <option value="-1" @if($old->status == "-1") selected @endif >Rejected</option>  
              <option value="2" @if($old->status == "2") selected @endif >Revised</option>  
            </select>
          </div>
        </div>
        <div class="row form-group">
          <div class="col-sm-4">
            <label for="" class="control-label">Action By</label>
            <select name="action" id="action" class="form-control chosen">
              <option value="" @if($old->type == "") selected @endif >Select an Option</option>  
              @foreach($action_by as $val)
                <option value="{{$val->id}}" @if($val->id == $old->action) selected @endif >{{$val->first_name}} {{$val->last_name}}</option>  
              @endforeach
            </select>
          </div>
        </div>

        <div class="row form-group">
          <div class="col-md-12">
            <button class="btn btn-default pull-right" type="submit"><i class="fa fa-search"></i> Filter</button>
          </div>
        </div>
      </form>

		  <table class="table table-bordered">
        <thead>
          <tr>
            <th class="text-center">#</th>
            <th class="text-center">Reference No.</th>
            <th class="text-center">Customer</th>
            <th class="text-center">Date</th>
            <th class="text-center">Amount</th>
            <th class="text-center">Action By</th>
            <th class="text-center">Type</th>
            <th class="text-center">Status</th>
            <th class="text-center">Actions</th>
          </tr>
        </thead>
        <tbody>
          @if(count($data) > 0)
            @foreach($data as $key => $val)
              <tr>
                <td align="center">{{ (($data->currentPage()-1)*$data->perPage())+($key+1) }}</td>
                <td align="left">{{$val->reference_no}}</td>
                <td align="left">{{$val->first_name}} {{$val->last_name}}</td>
                <td align="left">{{explode(" ",$val->quotation_date)[0]}}</td>
                <td align="left">{{number_format($val->total_amount,2)}}</td>
                <td align="left">{{$val->action_emp}}</td>
                <td align="left">@if($val->quotation_type == "1") Special @else Normal @endif </td>
                <td align="left">@if($val->quotation_status == "0") Pending 
                                 @elseif($val->quotation_status == "1") Aproved 
                                 @elseif($val->quotation_status == "-1") Rejected
                                 @else Revised 
                                 @endif 
                </td>
                <td align="center"><i class="fa fa-th" aria-hidden="true" onclick="show('{{$val->quotation_id}}')"></i></td>
              </tr>
            @endforeach
          @else
            <tr>
              <td colspan="9" align="center"> - No Data to Display - </td>
            </tr>
          @endif  
        </tbody>
      </table>
      <div class="row">
        <div class="col-sm-12 text-right">
          {!! $data->appends($old->except('page'))->render() !!}
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12 text-right">
          
        </div>
      </div>
		</div><!-- /.box-body -->
    <div class="overlay" style="display: none;">
      <i class="fa fa-spin fa-refresh"></i>
    </div>
	</div><!-- /.box -->
</section><!-- /.content -->




@stop
@section('js')

<script src="{{asset('assets/dist/chosen/chosen.jquery.min.js')}}"></script>
<script src="{{asset('assets/dist/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>


<script type="text/javascript">
$(document).ready(function() {
  $(".chosen").chosen();

  $('.datepick').datepicker({
      keyboardNavigation: false,
      forceParse: false,
      format: 'yyyy-mm-dd'
  });
});

function show(id){
  window.location.href = '{{url('admin/quotation/show')}}/'+id;
}

</script>
@stop
