@extends('layouts.back_master') @section('title','List Quotation')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
<link rel="stylesheet" href="{{asset('assets/dist/bootstrap-fileinput/css/fileinput.min.css')}}">
<link rel="stylesheet" type="text/css" src="{{asset('assets/dist/bootstrap-datepicker/css/bootstrap-datepicker.css')}}"/>

<style type="text/css">
  
.form-control{
  height: 34px;
}


input{
  height: 28px;
}

.help-block{
  color: red;
}
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Quotation<small>Management</small></h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
    <li><a href="{{ route('quotation.list') }}">Quotation List</a></li>
		<li class="active">PO Upload</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <!-- Default box -->
      <div class="box">
        <div class="box-body">
          @if(isset($quotation) && !empty($quotation))
          <div class="row">
            <div class="col-md-6">
              <h3 style="margin-top: 2px;margin-bottom: 0px;">{{ $quotation->reference_no?:'-' }}</h3>
              <h4 style="margin-bottom: 0;margin-top: 8px;">{{ $quotation->customer_name?:'-' }}</h4>
              <h5 style="margin-top: 5px;">{{ $quotation->customer_address?:'-' }}</h5>
              @if($quotation->approved_emp != $quotation->action_emp)
              <span>Approved By: {{$quotation->approved_emp}}</span>
              @endif
            </div>
            <div class="col-md-6">
              <h4 class="text-right" style="margin-bottom: 0">
                {{ ($quotation->quotation_date)?date('jS M, Y',strtotime($quotation->quotation_date)):'-' }}
              </h4>
              <h5 class="text-right" style="margin-top:3px;margin-bottom: 0;">
                {{ $quotation->action_emp?:'-' }}
              </h5>
              <h5 class="text-right" style="margin-top: 8px;">
                @if(PENDING == $quotation->quotation_status)
                  <span class="text-warning">Pending</span>
                @elseif(APPROVED == $quotation->quotation_status)
                  <span class="text-success">Approved</span>
                @elseif(REJECTED == $quotation->quotation_status)
                  <span class="text-danger">Rejected</span>
                @elseif(REVISED == $quotation->quotation_status)
                  <span class="text-primary">Revised</span>
                @elseif(CLOSED == $quotation->quotation_status)
                  <span class="text-success">Closed</span>
                @elseif(INVOICED == $quotation->quotation_status)
                  <span class="text-success">Invoiced</span>
                @else
                  -
                @endif
              </h5>
              @if($quotation->quotation_status != PENDING && $quotation->quotation_status != REJECTED)
              <div class="text-right">
                <a href="{{url('admin/quotation/quotation_pdf/')}}/{{$quotation->id}}" target="_blank">Quotation PDF</a> | 
                <a href="{{url('admin/quotation/performa_pdf/')}}/{{$quotation->id}}" target="_blank">Proforma Invoice PDF</a>
              </div>
              @endif
            </div>
          </div>
        @endif
        </div><!-- /.box -->
      </div> 
    </div>
  </div>


  <div class="row">
   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h5>Upload PO</h5>           
        </div><!-- /.box -->
        <div class="box-body">
          <form action="" method="post" enctype="multipart/form-data" id="example-form" data-toggle="validator" onsubmit="false">
            {!!Form::token()!!} 
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-group">
              <label>Reference No</label>
              <input type="text" name="reference_no" class="form-control" value="{{Input::old('reference_no')}}" required>
              <div class="help-block with-errors"></div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-group">
              <label>Expected Delivery Date</label>
              <input type="text" name="expected_date" class="form-control datepick" value="{{Input::old('expected_date')}}" required>
              <div class="help-block with-errors"></div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-group">
              <label>Warehouse</label>
              <select name="warehouse" id="input" class="form-control" required>
                <option value="" @if(Input::old('warehouse')==null) selected @endif>Select Warehouse</option>
                <option value="555SP2" @if(Input::old('warehouse')=='555SP2') selected @endif>Meegoda</option>
                <option value="555PS4" @if(Input::old('warehouse')=='555PS4') selected @endif>Gangarame</option>
              </select>
              <div class="help-block with-errors"></div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-group" style="margin-top: 10px;">
              <div class="checkbox">
                <label>
                  <input type="checkbox" name="verified" value="1" required>
                  <h6>Verified</h6>
                </label>
                <div class="help-block with-errors"></div>
              </div>              
            </div>      

              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                <input id="input-b1" name="file[]" type="file" class="file" multiple 
                value="{{Input::old('file')}}">
                <div class="help-block with-errors"></div>
              </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right form-group" style="margin-top: 10px">
              <button type="submit" class="btn btn-primary">Upload</button>
            </div>

          </form>
        </div><!-- /.box -->

        <div class="overlay" style="display: none;">
          <i class="fa fa-spin fa-refresh"></i>
        </div>

      </div> 
    </div>
  </div>
</section><!-- /.content -->


<div class="modal fade" id="modal-customerblock">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Customer Blocked!</h4>
      </div>
      <div class="modal-body">
          <h5 class="message"></h5>
          <label>Note</label>
          <textarea rows="5" class="form-control" id="note"></textarea>
      </div>          
      <div class="overlay" style="display: none;">
        <i class="fa fa-spin fa-refresh"></i>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btnRequest" data-type="">Send Request</button>
      </div>
    </div>
  </div>
</div>


@stop
@section('js')
<script src="{{asset('assets/dist/chosen/chosen.jquery.min.js')}}"></script>
<script src="{{asset('assets/dist/bootstrap-fileinput/js/fileinput.min.js')}}"></script>
<script src="{{asset('assets/dist/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('assets/dist/bs-validator/validator.js')}}"></script>

<script type="text/javascript">
$(document).ready(function() {
      $('#example-form').validator();

      $('.datepick').datepicker({
          keyboardNavigation: false,
          forceParse: false,
          format: 'yyyy-mm-dd'
      });

      $('#example-form').on('submit', function (e) {  
        if(e.isDefaultPrevented()) {
           
        }else{
           e.preventDefault();
           $('.overlay').show();
           $.ajax({
              url: "{{url('admin/quotation/verifyCustomer/')}}",
              type: 'GET',
              data: {id:{{$quotation->id}}},
              success: function (data) {
                 $('.overlay').hide();
                  if(data.data.is_inactive>0){   
                      $('#modal-customerblock').modal('show');  
                      $('#modal-customerblock .message').html('');
                      $('#modal-customerblock .message').html(data.data.message);   
                      $('#modal-customerblock #btnRequest').data('type',1);    
                      //UPLOAD WITHOUT PO
                  }else if(data.data.is_credit_limit>0 || data.data.is_credit_period>0){
                      $('#modal-customerblock').modal('show');  
                      $('#modal-customerblock .message').html('');
                      $('#modal-customerblock .message').html(data.data.message);   
                      //UPLOAD PO AND REQUEST
                      $('#modal-customerblock #btnRequest').data('type',4);

                  }else if(data.data.is_already_request){
                    //ALREADY REQ
                    sweetAlert('Request Pending!', data.data.already_request_reason,2);
                  }else{
                    //OK
                     $('.overlay').show();
                    var formData = new FormData($('#example-form')[0]);
                    $.ajax({            
                         url: "{{url('admin/quotation/po/upload')}}/{{$quotation->id}}",
                         type: 'POST',
                         data: formData,
                         async: false,             
                         cache: false,
                         contentType: false,
                         enctype: 'multipart/form-data',
                         processData: false,
                         success: function (response) {
                             $('.overlay').hide();
                             if (response.status=1) {
                               window.location.href = "{{url('admin/quotation/po/')}}/{{$quotation->id}}";
                             }
                         }
                    });
                  }
              }
          });
          
        }



       
        return false;

      });

      $("#btnRequest").click(function(){
           var type = $(this).data('type');
           if(type==4){
              $.ajax({
                  url: "{{url('admin/quotation/sendRequest/')}}",
                  type: 'GET',
                  data: {quotation_id:{{$quotation->id}},note:$('#note').text(),type:type},
                  success: function (data) {
                      $('#modal-customerblock').modal('hide'); 
                      if(data.status=1){
                          $('.overlay').show();
                          sweetAlert('Request Sent', 'Your request is made and po will be upload please wait...',0);
                          var formData = new FormData($('#example-form')[0]);
                          $.ajax({            
                               url: "{{url('admin/quotation/po/upload')}}/{{$quotation->id}}",
                               type: 'POST',
                               data: formData,
                               async: false,             
                               cache: false,
                               contentType: false,
                               enctype: 'multipart/form-data',
                               processData: false,
                               success: function (response) {
                                   $('.overlay').hide();
                                   if (response.status=1) {
                                     window.location.href = "{{url('admin/quotation/po/')}}/{{$quotation->id}}";
                                   }
                               }
                          });

                      }
                  }
              });
           }else{
              $.ajax({
                  url: "{{url('admin/quotation/sendRequest/')}}",
                  type: 'GET',
                  data: {quotation_id:{{$quotation->id}},note:$('#note').text(),type:type},
                  success: function (data) {
                      $('#modal-customerblock').modal('hide'); 
                      if(data.status=1){
                          sweetAlert('Request Sent', 'Your request is made and please upload the po after the aproval...',0);
                      }
                  }
              });
           }
           
      });

  $("#input-b1").fileinput({'showUpload':false, 'previewFileType':'any'});
});
</script>
@stop
