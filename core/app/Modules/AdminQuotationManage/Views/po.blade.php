@extends('layouts.back_master') @section('title','List Quotation')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">


<style type="text/css">
  

</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Quotation<small>Management</small></h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
    <li><a href="{{ route('quotation.list') }}">Quotation List</a></li>
		<li class="active">PO Details</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <!-- Default box -->
      <div class="box">
        <div class="box-body">
          @if(isset($list) && !empty($list))
          <div class="row">
            <div class="col-md-6">
              <h3 style="margin-top: 2px;margin-bottom: 0px;">{{ $list->reference_no?:'-' }}</h3>
              <h4 style="margin-bottom: 0;margin-top: 8px;">{{ $list->customer_name?:'-' }}</h4>
              <h5 style="margin-top: 5px;">{{ $list->customer_address?:'-' }}</h5>
              @if($list->approved_emp != $list->action_emp)
              <span>Approved By: {{$list->approved_emp}}</span>
              @endif
            </div>
            <div class="col-md-6">
              <h4 class="text-right" style="margin-bottom: 0">
                {{ ($list->quotation_date)?date('jS M, Y',strtotime($list->quotation_date)):'-' }}
              </h4>
              <h5 class="text-right" style="margin-top:3px;margin-bottom: 0;">
                {{ $list->action_emp?:'-' }}
              </h5>
              <h5 class="text-right" style="margin-top: 8px;">
                @if(PENDING == $list->quotation_status)
                  <span class="text-warning">Pending</span>
                @elseif(APPROVED == $list->quotation_status)
                  <span class="text-success">Approved</span>
                @elseif(REJECTED == $list->quotation_status)
                  <span class="text-danger">Rejected</span>
                @elseif(REVISED == $list->quotation_status)
                  <span class="text-primary">Revised</span>
                @elseif(CLOSED == $list->quotation_status)
                  <span class="text-success">Closed</span>
                @elseif(INVOICED == $list->quotation_status)
                  <span class="text-success">Invoiced</span>
                @else
                  -
                @endif
              </h5>
              @if($list->quotation_status != PENDING && $list->quotation_status != REJECTED)
              <div class="text-right">
                <a href="{{url('admin/quotation/quotation_pdf/')}}/{{$list->id}}" target="_blank">Quotation PDF</a> | 
                <a href="{{url('admin/quotation/performa_pdf/')}}/{{$list->id}}" target="_blank">Proforma Invoice PDF</a>
              </div>
              @endif
            </div>
          </div>
        @endif
        </div><!-- /.box -->
      </div> 
    </div>
  </div>

  <div class="row">
   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h5>PO Uploads</h5>           
        </div><!-- /.box -->
        <div class="box-body">
          <table class="table table-bordered">
        <thead>
          <tr>
            <th class="text-center">#</th>
            <th class="text-center">PO No.</th>
            <th class="text-center">Added On</th>
            <th class="text-center">Expected Delivery Date</th>
            <th class="text-center">Attachment(s)</th>
            <th class="text-center">Uploaded By</th>
            <th class="text-center">Status</th>
          </tr>
        </thead>
          <tbody>
            @foreach($po_list as $key=>$po)
              <tr>
                <td>{{$key+1}}</td>
                <td>#{{$po->po_number}}</td>
                <td>{{date('Y-m-d',strtotime($po->created_at))}}</td>
                <td>{{$po->expected_date}}</td>
                <td><a data-toggle="modal" href='{{url("admin/quotation/po/attachments")}}/{{$po->id}}'>{{count($po->attachments)}} Attachment(s)</a></td>
                <td>{{$po->uploadedby->first_name}} {{$po->uploadedby->last_name}}</td>
                <td>
                  @if($po->ended_at==null)
                    <span class="label label-info">Active</span>
                  @else
                    <span class="label label-danger">Discarded</span>
                  @endif
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
        </div><!-- /.box -->
      </div> 
    </div>
  </div>



</section><!-- /.content -->




@stop
@section('js')
<script src="{{asset('assets/dist/chosen/chosen.jquery.min.js')}}"></script>

<script type="text/javascript">
$(document).ready(function() {


});
</script>
@stop
