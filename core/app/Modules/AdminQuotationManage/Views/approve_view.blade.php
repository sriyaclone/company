@extends('layouts.back_master') @section('title','List Quotation')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
<link rel="stylesheet" type="text/css" src="{{asset('assets/dist/bootstrap-datepicker/css/bootstrap-datepicker.css')}}"/>

<style type="text/css">
  table .btn{
    padding: 2px 6px;
  }

  .table>tbody>tr>td, 
  .table>tbody>tr>th, 
  .table>tfoot>tr>td, 
  .table>tfoot>tr>th, 
  .table>thead>tr>td, 
  .table>thead>tr>th{
    padding-top:5px; 
    padding-bottom:5px; 
  }
</style>
<script src="{{asset('assets/dist/angular/angular/angular.min.js')}}"></script>
<script src="{{asset('assets/logics.js')}}"></script>
<script type="text/javascript">
  var app = angular.module('app', []);

  app.filter('total', function(){
    return function(array, type){
      var total = 0;
      angular.forEach(array, function(value, index){
        if(type.indexOf("allTotal") === -1){
          if(!isNaN(value[type])){
            total = total + (parseFloat(value[type]));
          }
        }else{
          total = total + (parseFloat(value['totalAmount']-value['totalDiscount']+value['totalVat']));
        }
      })
      return total;
    }
  });

  app.controller('QuotationController', function QuotationController($scope) {
    $scope.quotation_details = {!!json_encode($list->details)!!};
    $scope.quotation = {!!json_encode($list)!!};
    $scope.max_discount = {{$max_discount}};
    $scope.vat = {!!json_encode($vat)!!};
    $scope.nbt = {!!json_encode($nbt)!!};

    $scope.calculateQuotation = function(){
      if( $scope.quotation.vat_type === 'vat' || 
          $scope.quotation.vat_type === 'svat'){
        $scope.quotation_details = calculateVatCustomerQuotation($scope.quotation_details,$scope.vat.value,$scope.nbt.value);
      }else{
        console.log('dads');
        $scope.quotation_details = calculateNonVatCustomerQuotation($scope.quotation_details,$scope.vat.value,$scope.nbt.value);
      }
    };
  });

  app.controller('fooCtrl', function fooCtrl($scope) {
    $scope.$watch('detail.discount', function (newValue, oldValue) {
      if(isNaN(newValue)){
        $scope.$parent.detail.discount = 0;
      }
    });
  });
</script>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Quotation<small>Management</small></h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
    <li><a href="{{ url('admin/quotation/approval') }}">Quotation List</a></li>
		<li class="active">Details</li>
	</ol>
</section>

<!-- Main content -->
<section class="content" ng-app="app" ng-cloak>
	<!-- Default box -->
	<div class="box" ng-controller="QuotationController">
		<div class="box-body">
      <form action="{{url('admin/quotation/edit')}}" method="post">
        {!!Form::token()!!} 
        @if(isset($list) && !empty($list))
        <div class="row">
          <div class="col-md-12">
            <strong>
              <h3 class="text-center">
                {{($list->vat_type != 'novat')?strtoupper($list->vat_type):''}} QUOTATION
              </h3>
            </strong>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <h3 style="margin-top: 2px;margin-bottom: 0px;">{{ $list->reference_no?:'-' }}</h3>
            <h4 style="margin-bottom: 0;margin-top: 8px;">{{ $list->customer_name?:'-' }}</h4>
            <h5 style="margin-top: 5px;">{{ $list->customer_address?:'-' }}</h5>
          </div>
          <div class="col-md-6">
            <h4 class="text-right" style="margin-bottom: 0">
              {{ ($list->quotation_date)?date('jS M, Y',strtotime($list->quotation_date)):'-' }}
            </h4>
            <h5 class="text-right" style="margin-top:3px;margin-bottom: 0;">
              {{ $list->action_emp?:'-' }}
            </h5>
            <h5 class="text-right" style="margin-top: 8px;">
              @if(PENDING == $list->quotation_status)
                <span class="text-warning">Pending</span>
              @elseif(APPROVED == $list->quotation_status)
                <span class="text-success">Approved</span>
              @elseif(REJECTED == $list->quotation_status)
                <span class="text-danger">Rejected</span>
              @elseif(REVISED == $list->quotation_status)
                <span class="text-primary">Revised</span>
              @elseif(CLOSED == $list->quotation_status)
                <span class="text-success">Closed</span>
              @elseif(INVOICED == $list->quotation_status)
                <span class="text-success">Invoiced</span>
              @else
                -
              @endif
            </h5>
            @if($list->quotation_status != PENDING && $list->quotation_status != REJECTED)
            <div class="text-right"><a href="{{url($list->quotation_file_path)}}" target="_blank">Quotation PDF</a> | 
            <a href="{{url($list->pi_file_path)}}" target="_blank">Proforma Invoice PDF</a></div>
            @endif
          </div>
        </div>
        @if($max_discount > 0)
        <div class="row">
          <div class="col-md-12 text-right">
            <input type="submit" name="aprove" id="aprove" class="btn btn-success" value="Approve">
            <input type="submit" name="reject" id="reject" class="btn btn-danger" value="Reject">
          </div>
        </div>
        @endif
        <input type="hidden" name="quotation_id" value="{{$list->id}}">
      @endif
      <br>
      <br>
  		  <table class="table table-bordered">
          <thead>
            <tr>
              <th class="text-center">#</th>
              <th class="text-center" width="15%">Product Code</th>
              <th class="text-center">Product</th>
              <th class="text-center">Unit Price (Rs)</th>
              <th class="text-center">Qty</th>
              <th class="text-center">Max (%)</th>
              <th class="text-center">UDP (%)</th>
              <th class="text-center">Additional Discount (%)</th>
              <th class="text-center">Discount (Rs)</th>
              <th class="text-center">Amount (Rs)</th>
            </tr>
          </thead>
          <tbody>
                <tr ng-hide="quotation_details.length < 1" ng-repeat="(key, detail) in quotation_details" ng-controller="fooCtrl">
                  <td>@{{key+1}}</td>
                  <td>@{{detail.product.code}}</td>
                  <td>@{{detail.product.name}}</td>
                  <td align="right">
                    @{{detail.unitPrice|number:2}}
                    <input type="hidden" name="detail_id[]" value="@{{detail.id}}">
                    <input type="hidden" name="unit[]" value="@{{detail.unitPrice}}">
                  </td>
                  <td align="right">
                    @{{detail.qty}}
                    <input type="hidden" name="qty[]" value="@{{detail.qty}}">
                  </td>
                  <td align="right">
                    @{{detail.product.max_discount}}
                    <input type="hidden" name="max_discount[]" value="@{{detail.product.max_discount}}">
                  </td>
                  <td align="right">
                    @{{detail.udp}}
                    <input type="hidden" name="udp[]" value="@{{detail.udp}}">
                  </td>
                  <td align="right">
                    <input type="number" step="0.01" class="form-control" ng-change="calculateQuotation()" ng-model="detail.discount" name="additional_discount[]" ng-min="0" ng-max="@{{((detail.product.max_discount - detail.udp) <= max_discount)?(detail.product.max_discount-detail.udp):max_discount}}">
                  </td>
                  <td align="right">
                    @{{detail.totalDiscount|number:2}}
                    <input type="hidden" name="discount_price[]" value="@{{detail.totalDiscount}}">
                  </td>
                  <td align="right">
                    <span class="row_amount">
                    @{{detail.totalAmount|number:2}}
                  </span>
                  </td>
                </tr>
              <tr ng-hide="quotation_details.length < 1">
                <td colspan="9" align="right">Amount</td>
                <td align="right">
                  <span class="amount">
                    @{{ (quotation_details|total:'totalAmount')|number:2 }}
                  </span>
                </td>
              </tr>
              <tr ng-hide="quotation_details.length < 1">
                <td colspan="9" align="right">Discount</td>
                <td align="right">
                  <span class="discount">
                    <input type="hidden" name="total_discount" value="@{{ (quotation_details|total:'totalDiscount') }}">
                    @{{ (quotation_details|total:'totalDiscount')|number:2 }}
                  </span>
                </td>
              </tr>
              @if($list->vat_type=='vat')
              <tr ng-hide="quotation_details.length < 1">
                <td colspan="9" align="right">VAT @{{vat.value}}%</td>
                <td align="right">
                  <span class="vat">
                    @{{ (quotation_details|total:'totalVat')|number:2 }}
                  </span>
                </td>
              </tr>
              @elseif($list->vat_type=='svat')
              <tr ng-hide="quotation_details.length < 1">
                <td colspan="9" align="right">SVAT @{{vat.value}}%</td>
                <td align="right">
                  <span class="vat">
                    @{{ (quotation_details|total:'totalVat')|number:2 }}
                  </span>
                </td>
              </tr>
              @endif
              <tr ng-hide="quotation_details.length < 1">
                <td colspan="9" align="right"><strong>Total Amount</strong></td>
                <td align="right">
                  <strong>
                    <span class="total">
                      <input type="hidden" name="total_amount" value="@{{ (quotation_details|total:'allTotal') }}">
                      @{{ (quotation_details|total:'allTotal')|number:2 }}
                    </span>
                  </strong>
                </td>
              </tr>
              <tr ng-show="quotation_details.length < 1">
                <td colspan="10" align="center"> - No Data to Display - </td>
              </tr> 
          </tbody>
        </table>
      </form>
		</div><!-- /.box-body -->
    <div class="overlay" style="display: none;">
      <i class="fa fa-spin fa-refresh"></i>
    </div>
	</div><!-- /.box -->
</section><!-- /.content -->


@stop
@section('js')
<script src="{{asset('assets/dist/chosen/chosen.jquery.min.js')}}"></script>
<script src="{{asset('assets/dist/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript">
$(document).ready(function() {
  $(".chosen").chosen();

  $('.datepick').datepicker({
      keyboardNavigation: false,
      forceParse: false,
      format: 'yyyy-mm-dd'
  });
});
</script>
@stop
