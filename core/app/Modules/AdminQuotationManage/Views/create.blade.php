@extends('layouts.back_master') @section('title','List Quotation')
@section('css')
<style type="text/css">
.pagination {
    display: inline-block;
    padding-left: 0;
     margin: 0 0; 
    border-radius: 4px;
}

.modal .overlay, .overlay-wrapper .overlay {
    z-index: 50;
    background: rgba(255,255,255,0.7);
    border-radius: 3px;
}

.modal .overlay, .overlay-wrapper>.overlay, .box>.loading-img, .overlay-wrapper>.loading-img {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
}

.modal  .overlay>.fa, .overlay-wrapper .overlay>.fa {
    position: absolute;
    top: 50%;
    left: 50%;
    margin-left: -15px;
    margin-top: -15px;
    color: #000;
    font-size: 30px;
}

.add-item{
  color: #6aa8e8;
}

.rem-item{
  color: #e45656;
}

[ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
  display: none !important;
}

.item-selected{
  background: #b0d3ff;
}

.error{
  color: red;
}

.btn-active {
    color: #333;
    background-color: #e6e6e6;
    border-color: #adadad;
    z-index: 2;
    background-image: none;
    outline: 0; 
    -webkit-box-shadow: inset 0 3px 5px rgba(0, 0, 0, .125);
    box-shadow: inset 0 3px 5px rgba(0, 0, 0, .125);
}


</style>

@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Quotation<small>Management</small></h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
    <li><a href="{{ route('quotation.list') }}">Quotation List</a></li>
		<li class="active">create</li>
	</ol>
</section>

<!-- Main content -->
<section class="content" ng-app="app" ng-cloak ng-controller="QuotationController" >
  <form onsubmit="return formSubmit();" action="{{url('admin/quotation/create')}}" method="post" name="form" >
  {!!Form::token()!!} 
  <!-- Default box -->
  <div class="box">
    <div class="box-body">      
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <strong>
            <h3 class="text-left hidden-xs hidden-sm" style="margin-top:3px;margin-bottom: 20px;">
              NEW QUOTATION
            </h3>
            <h3 class="text-center hidden-md hidden-lg" style="margin-top:3px;margin-bottom: 20px;">
              NEW QUOTATION
            </h3>
          </strong>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 text-right hidden-xs hidden-sm" >
          <div class="btn-group" ng-show="customer!=null">
            <button type="button" class="btn btn-default btn-md" ng-class="dis_type==='cash'?'btn-active':''" ng-click="dis_type='cash';">CASH</button>
            <button type="button" class="btn btn-default btn-md" ng-class="dis_type==='credit'?'btn-active':''" ng-click="dis_type='credit'">CREDIT</button>
          </div>

          <div class="btn-group" ng-show="customer!=null">
            <button type="button" class="btn btn-default btn-md" ng-class="vat_type==='vat'?'btn-active':''" ng-click="vat_type='vat';">VAT</button>
            <button type="button" class="btn btn-default btn-md" ng-class="vat_type===''?'btn-active':''" 
            ng-click="vat_type='notax'">NON-VAT</button>
            <button type="button" class="btn btn-default btn-md" ng-class="vat_type==='svat'?'btn-active':''" ng-click="vat_type='svat'">SVAT</button>
          </div>

          <button type="button" class="btn btn-md btn-default" ng-click="clearAll()" ng-show="quotDetails.length>0">Clear All</button>
          <button type="button" class="btn btn-md btn-success" data-toggle="modal"
          href='#modal-customer' ng-click="getCustomers(1,'')">+ Customer</button>
          <button type="button" class="btn btn-md btn-primary" data-toggle="modal" 
          href='#modal-items'          
          ng-click="getItems(1,'')" ng-show="customer!=null">Add Item</button>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 text-center hidden-md hidden-lg">
          <div class="btn-group" ng-show="customer!=null">
            <button type="button" class="btn btn-default btn-md" ng-class="dis_type==='cash'?'active':''" ng-click="dis_type='cash';">CASH</button>
            <button type="button" class="btn btn-default btn-md" ng-class="dis_type==='credit'?'active':''" ng-click="dis_type='credit'">CREDIT</button>
          </div>

          <div class="btn-group" ng-show="customer!=null">
            <button type="button" class="btn btn-default btn-md" ng-class="customer.vat_type==='vat'?'active':''" ng-click="vat_type='vat';">VAT</button>
            <button type="button" class="btn btn-default btn-md" ng-class="customer.vat_type===''?'active':''" ng-click="vat_type='notax'">NON-VAT</button>
            <button type="button" class="btn btn-default btn-md" ng-class="customer.vat_type==='svat'?'active':''" ng-click="vat_type='svat'">SVAT</button>
          </div>

          
          <div class="row" style="margin-top: 10px;margin-bottom: 10px">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <button type="button" class="btn btn-md btn-default" ng-click="clearAll()" ng-show="quotDetails.length>0">Clear All</button>
              <button type="button" class="btn btn-md btn-success" data-toggle="modal"
              href='#modal-customer' ng-click="getCustomers(1,'')">+ Customer</button>
              <button type="button" class="btn btn-md btn-primary" data-toggle="modal" 
              href='#modal-items'          
              ng-click="getItems(1,'')" ng-show="customer!=null">Add Item</button>
            </div>
          </div>
        </div>


      </div>
      
      <div class="row">
        <hr style="margin-top:0;margin-bottom: 15px;">
      </div>

      <div class="row hidden-sm hidden-xs">
        <div class="col-md-6">
          <input type="hidden" name="customer_id" value="@{{customer.id}}">
          <input type="hidden" name="vat_type" value="@{{vat_type}}">
          <input type="hidden" name="dis_type" value="@{{dis_type}}">
          <input type="hidden" name="vat_id" value="{{$vat['id']}}">
          <input type="hidden" name="nbt_id" value="{{$nbt['id']}}">
          <input type="hidden" name="vat_per" value="{{$vat['value']}}">
          <input type="hidden" name="action_done_by" value="{{$user->employee_id}}">
          <input type="hidden" name="action_by" value="@{{action_by}}">
          <input type="hidden" name="inquiry_id" value="0">

          <!-- ng-show="is_invoice==1" -->
          <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" ng-show="is_invoice==1">
              <h5 style="margin-top: 0px;margin-bottom: 20px;">
                <span class="">Sales Person</span>
                <select class="form-control chosen" style="width:50%" ng-model="action_by"
                 ng-options="val.id as val.name for (rep , val) in sales_reps"></select>
              </h5>
            </div>
          </div>
          
           
          <h4 style="margin-top: 2px;margin-bottom: 0px;" ng-show="customer!=null">
            @{{customer.first_name}} @{{customer.last_name}}<br>
            <small>@{{customer.short_code}}</small>
          </h4>

          <h4 style="margin-top: 2px;margin-bottom: 0px;" ng-show="customer==null">
            <a data-toggle="modal" href='#modal-customer' ng-click="getCustomers(1,'')">Choose Customer</a>
          </h4>

          @if($errors->has('customer_id'))
            <label id="label-error" class="error" for="label">{{$errors->first('customer_id')}}</label>
          @endif


          <p style="margin-bottom: 0;margin-top: 0px;">@{{customer.address}}</p>
          <div class="row" style="margin-top: 10px">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
              <input type="text" name="attn" placeholder="attention" class="form-control">
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <h4 class="text-right" style="margin-top:3px;margin-bottom: 0;">
            {{$user->emp->first_name}} {{$user->emp->last_name}}
          </h4>          
          <p class="text-right" style="margin-bottom: 0">
            {{date('Y-m-d')}}
          </p>

          <div class="row" style="margin-top: 10px">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pull-right text-right">
              Delivery Term
              {!! Form::select('delivery_term', $delivery_terms,Input::old('delivery_term'),['class'=>'chosen','style'=>'width:100%;','required','data-placeholder'=>'Choose Parent']) !!}
            </div>
          </div>
        </div>
      </div>

      <div class="row hidden-md hidden-lg" >
        <div class="col-md-12">
          <input type="hidden" name="customer_id" value="@{{customer.id}}" id="customer_id">
          <input type="hidden" name="vat_type" value="@{{vat_type}}">
          <input type="hidden" name="dis_type" value="@{{dis_type}}">
          <input type="hidden" name="vat_id" value="{{$vat['id']}}">
          <input type="hidden" name="nbt_id" value="{{$nbt['id']}}">
          <input type="hidden" name="vat_per" value="{{$vat['value']}}">
          <input type="hidden" name="action_done_by" value="{{$user->employee_id}}">
          <input type="hidden" name="action_by" value="@{{action_by}}">
          <input type="hidden" name="inquiry_id" value="0">

          <h5 style="margin-top: 0px;margin-bottom: 20px;" ng-show="is_invoice==1">
            <span class="">Sales Person</span>
            <select class="form-control" style="width:50%" ng-model="action_by"
             ng-options="val.id as val.name for (rep , val) in sales_reps"></select>
          </h5>
          
          <h4 style="margin-top: 2px;margin-bottom: 0px;" ng-show="customer!=null" class="text-center">
            @{{customer.first_name}} @{{customer.last_name}}<br>
            <small>@{{customer.short_code}}</small>
          </h4>

          @if($errors->has('customer_id'))
            <label id="label-error" class="error" for="label">{{$errors->first('customer_id')}}</label>
          @endif
          
          <p style="margin-bottom: 0;margin-top: 0px;">@{{customer.address}}</p>
          <div class="row" style="margin-top: 10px">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
              <input type="text" name="attn" placeholder="attention" class="form-control">
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <h4 class="text-right  hidden-sm hidden-xs" style="margin-top:3px;margin-bottom: 0;">
            {{$user->emp->first_name}} {{$user->emp->last_name}}
          </h4>          
          <p class="text-right  hidden-sm hidden-xs" style="margin-bottom: 0">
            {{date('Y-m-d')}}
          </p>

          <div class="row" style="margin-top: 10px">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pull-right text-right">
              Delivery Term
              {!! Form::select('delivery_term', $delivery_terms,Input::old('delivery_term'),['class'=>'chosen','style'=>'width:100%;','required','data-placeholder'=>'Choose Parent']) !!}
            </div>
            @if($errors->has('delivery_term'))
              <label id="label-error" class="error" for="label">{{$errors->first('delivery_term')}}</label>
            @endif
          </div>
        </div>
      </div>
    
      <div class="container-fluid table-responsive" style="margin-top: 30px;">
        <table class="table table-bordered items-table" width="100%">
          <thead>
            <tr>
              <th class="text-center">#</th>
                <th class="text-center" width="25%">Product / Code</th>
                <th class="text-center" width="10%">Price (Rs)</th>
                <th class="text-center" width="10%">Max (%)</th>
                <th class="text-center" width="10%">
                  <span ng-show="dis_type==='cash'">CASH (%)</span>
                  <span ng-show="dis_type==='credit'">UDP (%)</span>
                </th>
                <th class="text-center" width="12%">Qty</th>
                <th class="text-center" width="12%">Additional (%)</th>
                <th class="text-center" width="15%">Discount (Rs)</th>
                <th class="text-center" width="15%">Amount (Rs)</th>
                <th class="text-center">Actions</th>
            </tr>
          </thead>
          <tbody>
              <tr ng-repeat="quotDetail in quotDetails" class="item-rows">
                    <td>
                      @{{$index+1}}
                      <input type="hidden" name="product_id[]" value="@{{quotDetail.id}}">
                      <input type="hidden" name="is_vat[]" value="@{{quotDetail.is_vat}}">
                      <input type="hidden" name="is_nbt[]" value="@{{quotDetail.is_nbt}}">
                      <input type="hidden" name="vat[]" value="{{$vat['value']}}">
                      <input type="hidden" name="nbt[]" value="{{$nbt['value']}}">
                    </td>
                    <td>
                      <strong>@{{quotDetail.code}}</strong><br>
                      <small>@{{quotDetail.description}}</small>
                    </td>
                    <td align="right">
                      @{{quotDetail.unitPrice|number:2}}
                      <input type="hidden" name="price[]" value="@{{quotDetail.price}}">
                    </td>
                    <td align="right">
                      @{{quotDetail.max_discount}}
                      <input type="hidden" name="max_discount[]" value="@{{quotDetail.max_discount}}">
                    </td>
                    <td align="right" ng-show="dis_type==='credit'">
                      @{{quotDetail.udp}}
                      <input type="hidden" name="udp[]" value="@{{quotDetail.udp}}">
                    </td> 
                    <td align="right" ng-show="dis_type==='cash'">
                      @{{quotDetail.cash_udp}}
                      <input type="hidden" name="cash_udp[]" value="@{{quotDetail.cash_udp}}">
                    </td>                  
                    <td align="right">
                      <input type="number"
                       ng-min="0"
                       ng-model="quotDetail.qty"
                       name="qty[]" 
                       step="1" class="form-control qty" ng-change="calculateQuotation()">
                    </td>
                    <td align="right">
                      <input type="number" step="0.01" class="form-control" 
                        ng-model="quotDetail.discount" 
                        name="additional_discount[]" 
                        ng-min="0"
                        ng-change="calculateQuotation()" 
                        ng-max="@{{(quotDetail.max_discount - quotDetail.udp)}}" ng-show="dis_type==='credit'">

                      <input type="number" step="0.01" class="form-control" 
                        ng-model="quotDetail.discount" 
                        name="additional_discount[]" 
                        ng-min="0"
                        ng-change="calculateQuotation()" 
                        ng-max="@{{(quotDetail.max_discount - quotDetail.cash_udp)}}" ng-show="dis_type==='cash'">
                    </td>
                    <td align="right">
                       @{{quotDetail.totalDiscount|number:2}}
                      <input type="hidden" name="line_discount_price[]" value="@{{quotDetail.totalDiscount}}">
                    </td>
                    <td align="right">
                      <span class="row_amount">
                        @{{quotDetail.totalAmount|number:2}}
                        <input type="hidden" name="line_total_amount[]" value="@{{quotDetail.totalAmount}}">
                      </span>
                    </td>
                    <td class="text-center">
                      <a href="" ng-click="removeItem($index)" class="rem-item"><i class="fa fa-minus-circle fa-lg" aria-hidden="true"></i></a>
                      <a  data-toggle="modal" href='#modal-items' ng-click="getItems(1,'')" class="add-item"><i class="fa fa-plus-circle fa-lg" aria-hidden="true"></i></a>
                    </td>
              </tr>
              <tr>
                <td colspan="8" align="right">Amount</td>
                <td align="right">@{{ (quotation_details|total:'totalAmount')|number:2 }}</td>
                <td></td>
              </tr>
              <tr ng-hide="quotation_details.length < 1">
                <td colspan="8" align="right">Discount </td>
                <td align="right">@{{ (quotation_details|total:'totalDiscount')|number:2 }}</td>
                <input type="hidden" name="total_discount" value="@{{(quotation_details|total:'totalDiscount')}}">
                <td></td>
              </tr>
              <tr  ng-show="vat_type=='vat'">
                <td colspan="8" align="right">VAT @{{vat}}%</td>
                <td align="right"> @{{ (quotation_details|total:'totalVat')|number:2 }}</td>
                <td></td>
              </tr>
              <tr  ng-show="vat_type=='svat'">
                <td colspan="8" align="right">SVAT @{{vat}}%</td>
                <td align="right">@{{ (quotation_details|total:'totalVat')|number:2 }}</td>
                <td></td>
              </tr>
              <tr>
                <td colspan="8" align="right"><strong>Total Amount</strong></td>
                <td align="right"><strong>@{{ (quotation_details|total:'allTotal')|number:2 }}</strong></td>
                 <input type="hidden" name="total_amount" value="@{{(quotation_details|total:'allTotal')}}" >
                 <td></td>
              </tr>
          </tbody>
          @if($errors->has('line_total_amount'))
            <label id="label-error" class="error" for="label">{{$errors->first('line_total_amount')}}</label>
          @endif
        </table>
      </div>

      <div class="alert alert-error" style="display: none;" id="alert">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Error!</strong> <h6 id="message"></h6>
      </div>

      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right" style="margin-top: 10px;">
          <button type="submit" class="btn btn-primary" ng-disabled="form.$invalid">Save Quotation</button>
        </div>
      </div>
    </div><!-- /.box-body -->


    <div class="overlay" style="display: none;">
      <i class="fa fa-spin fa-refresh"></i>
    </div>
  </div><!-- /.box -->

  </form>

  <div class="modal fade" id="modal-customer">
    <div class="modal-dialog">
      <div class="overlay" id="modal-overlay-1">
        <i class="fa fa-refresh fa-spin"></i>
      </div>
      <div class="modal-content">
        <div class="modal-header">
         <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <h4 style="margin-top: 5px">Select Customer</h4>
          </div>
          <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
            <div class="input-group">
              <input type="text" name="customerSearch" my-enter="getCustomers(1,customerSearch)" ng-model="customerSearch" placeholder="Type Message ..." class="form-control" style="height: 30px">
              <span class="input-group-btn">
                <button type="submit" class="btn btn-defaukt btn-flat btn-sm" ng-click="getCustomers(customers.current_page,customerSearch)">Find</button>
                <button type="button" class="btn btn-defaukt btn-flat btn-sm" data-dismiss="modal">Close</button>
              </span>
            </div>
          </div>
         </div>
        </div>
        <div class="modal-body ">
          <table class="table table-bordered " id="table-customer">
            <thead>
              <tr>
                <th width="10%">#</th>
                <th width="23%">Code</th>
                <th width="50%">Name</th>
                <th width="10%">Action</th>
              </tr>
            </thead>
            <tbody>
              <tr ng-repeat="customer in customers.data">
                <td>@{{$index+1}}</td>
                <td>@{{customer.short_code}} 
                  <span ng-hide='customer.vat_type==""'>(@{{customer.vat_type}})</span>
                  <span ng-hide='customer.vat_type!=""'>(nonvat)</span>
                </td>
                <td>@{{customer.first_name}} @{{customer.last_name}}</td>
                <td>
                  <a class="btn btn-sm btn-default" href="" data-dismiss="modal" ng-click="selectCustomer(customer)" ng-show="customer.blocked==0">Select</a>
                  <span ng-show="customer.blocked==1">blocked</span>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="modal-footer">
           <paging
              page="customers.current_page" 
              page-size="customers.per_page" 
              total="customers.total"
              show-prev-next="true"
              show-first-last="true"
              paging-action="getCustomers(page,search)">
          </paging> 
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modal-items">
    <div class="modal-dialog modal-lg">
      <div class="overlay" id="modal-overlay-2">
        <i class="fa fa-refresh fa-spin"></i>
      </div>
      <div class="modal-content">
        <div class="modal-header">
          <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
              <h4 style="margin-top: 5px">Items</h4>
            </div>
            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
              <div class="input-group">
                <input type="text" my-enter="getItems(1,search)" name="search" autofocus ng-model="search" placeholder="Type Message ..." class="form-control" style="height: 30px">
                <span class="input-group-btn">
                  <button type="submit" class="btn btn-defaukt btn-flat btn-sm" ng-click="getItems(items.current_page,search)">Find</button>
                </span>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-body">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th width="40%">Code</th>
                <th width="18%">Price</th>
                <th width="11%">UDP (%)</th>
                <th width="11%">CASH (%)</th>
                <th width="11%">MAX (%)</th>
                <th width="5%">Action</th>
              </tr>
            </thead>
            <tbody>
              <tr ng-repeat="item in items.data" ng-class="isItemInList(item)>0?'item-selected':''">
                <td>
                  <div>
                    <img class="pull-left" src="@{{item.image}}" width="20%" style="    border-radius: 50%;">
                    <div class="pull-left" style="margin-top: 20px;margin-left: 10px;">
                      <strong class="pull-left">@{{item.code}}</strong>
                      <br>
                      <small class="pull-left">@{{item.description}}</small>
                    </div>
                  </div>
                </td>
                <td>@{{item.price}}</td>
                <td>@{{item.udp}}</td>
                <td>@{{item.cash_udp}}</td>
                <td>@{{item.max_discount}}</td>
                <td><a class="btn btn-sm btn-default"  data-dismiss="modal" href="" ng-click="addItem(item)">add</a></td>
              </tr>
            </tbody>
          </table> 
        </div>
        <div class="modal-footer">
          <paging
            page="items.current_page" 
            page-size="items.per_page" 
            total="items.total"
            show-prev-next="true"
            show-first-last="true"
            paging-action="getItems(page,search)">
          </paging> 
        </div>
      </div>
    </div>
  </div>


</section><!-- /.content -->


@stop
@section('js')
<script src="{{asset('assets/dist/chosen/chosen.jquery.min.js')}}"></script>
<script src="{{asset('assets/dist/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('assets/dist/angular/angular/angular.min.js')}}"></script>
<script src="{{asset('assets/dist/ng-pagination/dist/paging.min.js')}}"></script>
<script src="{{asset('assets/logics.js')}}"></script>
<script src="{{asset('assets/dist/shortcut/shortcut.js')}}"></script>


<script type="text/javascript">


  function formSubmit() {
    // Validate URL
    var customer_id = $("#customer_id").val();
    $('#alert').hide();
    console.log(isQtyEmpty()); 
    if(customer_id==null || customer_id==""){
      $('#alert').show();
      $('.alert-error #message').text('Please select customer');
      return false;
    }

    if(numberOfItems()==0){
      $('#alert').show();
      $('.alert-error #message').text('You cannot create quotation without items');
      return false;
    }; 

    if(isQtyEmpty()){
      return false;
    };    

    return true;

  }

  function isQtyEmpty() {
    var result = false;
    $('.items-table tbody .item-rows td .qty').each(function (index) {
       var qty = $(this).val();       
       $(this).parent().parent().css( "background-color", "transparent" );
       if(qty==null || qty=="" || qty==0){
          $(this).parent().parent().css( "background-color", "#ffc6c6" );
          result = true;
          return true; 
       }
    });
    return result;
  }

  function numberOfItems() {
    return $('.items-table tbody tr td .qty').length;
  }

  var app = angular.module('app', ['bw.paging']);


  app.filter('total', function(){
    return function(array, type){
      var total = 0;
      angular.forEach(array, function(value, index){
        if(type.indexOf("allTotal") === -1){
          if(!isNaN(value[type])){
            total = total + (parseFloat(value[type]));
          }
        }else{
          total = total + (parseFloat(value['totalAmount']-value['totalDiscount']+value['totalVat']));
        }
      })
      return total;
    }
  });

  app.directive('myEnter', function () {
      return function (scope, element, attrs) {
          element.bind("keydown keypress", function (event) {
              if(event.which === 13) {
                  scope.$apply(function (){
                      scope.$eval(attrs.myEnter);
                  });

                  event.preventDefault();
              }
          });
      };
  });





 app.controller('QuotationController', function QuotationController($scope,$filter) {

    $scope.items            = [];
    $scope.quotDetails      = [];
    $scope.vat_type         = null;
    $scope.dis_type         = 'credit';
    $scope.vat              = '{{$vat["value"]}}';
    $scope.customer         = null;
    $scope.customers        = [];
    $scope.is_invoice       = '{{$is_invoice}}';
    $scope.action_by        = '{{(!$is_invoice)?$user->employee_id:''}}';
    $scope.nbt              = '{{$nbt["value"]}}';
    $scope.sales_reps       = {!!json_encode($sales_reps)!!};



    $scope.calculateQuotation = function(){
      if($scope.vat_type === 'vat' || $scope.vat_type === 'svat'){
        $scope.quotation_details = calculateVatCustomerQuotation($scope.dis_type,$scope.quotDetails,$scope.vat,$scope.nbt);
      }else if($scope.vat_type === '' || $scope.vat_type === 'notax'){
        $scope.quotation_details = calculateNonVatCustomerQuotation($scope.dis_type,$scope.quotDetails,$scope.vat,$scope.nbt);
      }
    };

    $scope.$watch('vat_type', function (newValue, oldValue) {
      $scope.calculateQuotation();
    });

    $scope.$watch('dis_type', function (newValue, oldValue) {
      $scope.clearDis();
      $scope.calculateQuotation();
    });


    $scope.getItems = function(page,search) {
      $("#modal-overlay-2").show();
      $.ajax({
          url: "{{URL::to('admin/quotation/getItems')}}",
          method: 'GET',
          data: {'page': page,'search':search},
          async: true,
          success: function (data) {
            $scope.items = data;
            $scope.$apply();
            $("#modal-overlay-2").hide();
          },
          error: function () {
              
          }
      });
    }

    $scope.getCustomers = function(page,search) {
      $("#modal-overlay-1").show();
      $.ajax({
          url: "{{URL::to('admin/quotation/getCustomers')}}",
          method: 'GET',
          data: {'page': page,'search':search},
          async: true,
          success: function (data) {
            $scope.customers = data;
            $scope.$apply();
            $("#modal-overlay-1").hide();
          },
          error: function () {
              
          }
      });
    }

    $scope.addItem = function(item) {
      var isItem = $scope.isItemInList(item);
      if(isItem<0){
        $scope.quotDetails.push(item);
        $scope.calculateQuotation();
      }else{
        //HIGELIGHT RAW

      }      
    }

    $scope.clearAll = function(item) {
      sweetAlertConfirm("Are you sure ?","This will clear all the items are you sure you want to do this ?",3,function() {
        $scope.quotDetails = [];  
        $scope.$apply(); 
      });         
    }

    $scope.removeItem = function(index) {
        $scope.quotDetails.splice(index,1);    
    }

    $scope.selectCustomer = function(customer) {
      $scope.customer = customer;
      $scope.vat_type = customer.vat_type;
    }

    $scope.isItemInList = function (pro) {
      for($i=0;$i<$scope.quotDetails.length;$i++){
          if ($scope.quotDetails[$i].id == pro.id) {
              return 1;
          } 
      };
      return -1;
    }

    $scope.clearDis = function () {
      $.each($scope.quotDetails, function(index,value) {
        value.discount = 0;
      });    
    }

    $scope.submit = function(){
    // Set the 'submitted' flag to true
      $scope.submitted = true;
      // Send the form to server
      // $http.post ...
    } 


    shortcut.add("alt+a",function() {
        if($scope.customer!=null){
          $('#modal-items').modal('show');
          $scope.getItems(1,'');
        }else{
          sweetAlertConfirm("No Customer!","Before Adding items you have to Select a Customer.  Would you like to select a Customer?",3,function() {
              $('#modal-customer').modal('show');
              $scope.getCustomers(1,'');
          });         
        }
    });

    shortcut.add("alt+q",function() {
        $('#modal-customer').modal('show');
        $scope.getCustomers(1,'');
    });



 });


$(document).ready(function() { 
  $(".chosen").chosen();

  $('.datepick').datepicker({
      keyboardNavigation: false,
      forceParse: false,
      format: 'yyyy-mm-dd'
  });

  $(".btn-group > .btn").click(function(){
      $(".btn-group > .btn").removeClass("active");
      $(this).addClass("active");
  });

  $('.modal-items').on('shown.bs.modal', function() {
    $(this).find('[autofocus]').focus();
  });

 

});
</script>
@stop
