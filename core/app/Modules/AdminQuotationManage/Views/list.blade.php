@extends('layouts.back_master') @section('title','List Quotation')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
<link rel="stylesheet" type="text/css" src="{{asset('assets/dist/bootstrap-datepicker/css/bootstrap-datepicker.css')}}"/>

<style type="text/css">
  table .btn{
    padding: 2px 6px;
  }

  .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
    padding-top:5px; 
    padding-bottom:5px; 
  }
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Quotation<small>Management</small></h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li class="active">Quotation Management</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
		<div class="box-header with-border">
			<div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <h3 class="box-title" style="margin-top: 5px">List Quotaton</h3>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">
            <a type="button" class="btn btn-sm btn-default" href="{{url('admin/quotation')}}">Create Quotation</a>  
        </div>   
      </div>
		</div>
		<br>
		<div class="box-body">

      <form method="get">
        <div class="row form-group">
          <div class="col-sm-4">
            <label for="" class="control-label">Reference No. (wildcard % supported)</label>
            <input type="text" class="form-control" name="reference" id="reference" value="{{$old->reference}}">
          </div>
          <div class="col-sm-4">
            <label for="" class="control-label">Customer</label>
            <input type="text" class="form-control" name="customer" id="customer" value="{{$old->customer}}">
          </div>
          <div class="col-sm-4">
            <label for="" class="control-label">Type</label>
            <select name="type" id="type" class="form-control chosen">
              <option value="" @if($old->type == "") selected @endif >Select an Option</option>  
              <option value="1" @if($old->type == "1") selected @endif >Special</option>  
              <option value="0" @if($old->type == "2") selected @endif >Normal</option>  
            </select>
          </div>
        </div>
        <div class="row form-group">
          <div class="col-sm-4">
            <label for="" class="control-label">From</label>
            <input id="from" name="from" type="text" class="form-control datepick" value="@if(isset($old->from)){{$old->from}}@else{{''}}@endif">
          </div>
          <div class="col-sm-4">
            <label for="" class="control-label">To</label>
            <input type="text" class="form-control datepick" name="to" id="to" value="@if(isset($old->to)){{$old->to}}@else{{date('Y-m-d')}}@endif">
          </div>
          <div class="col-sm-4">
            <label for="" class="control-label">Status</label>
            <select name="status" id="status" class="form-control chosen">
              <option value="" @if($old->status == "") selected @endif >Select an Option</option>  
              <option value="0" @if($old->status == PENDING && $old->status != "") selected @endif >Pending</option>  
              <option value="1" @if($old->status == APPROVED) selected @endif >Approved</option>  
              <option value="-1" @if($old->status == REJECTED) selected @endif >Rejected</option>  
              <option value="2" @if($old->status == REVISED) selected @endif >Revised</option>  
              <option value="3" @if($old->status == CLOSED) selected @endif >Closed</option>  
              <option value="4" @if($old->status == INVOICED) selected @endif >Invoiced</option>  
              <option value="5" @if($old->status == PO_PROCESSING) selected @endif >PO Processing</option>  
              <option value="-2" @if($old->status == DISCARDED) selected @endif >Discarded</option>  
              <option value="-3" @if($old->status == HOLD) selected @endif >HOLD</option>  
            </select>
          </div>
        </div>
        @if(!Sentinel::inRole('sales-team-head') && !Sentinel::inRole('sales-person'))
        <div class="row form-group">
          <div class="col-sm-4">
            <label for="" class="control-label">Action By</label>
            <select name="action" id="action" class="form-control chosen">
              <option value="" @if($old->type == "") selected @endif >Select an Option</option>  
              @foreach($action_by as $val)
                <option value="{{$val->id}}" @if($val->id == $old->action) selected @endif >{{$val->first_name}} {{$val->last_name}}</option>  
              @endforeach
            </select>
          </div>
        </div>
        @endif
        <div class="row form-group">
          <div class="col-md-12">
            <button class="btn btn-default pull-right" type="submit"><i class="fa fa-search"></i> Filter</button>
          </div>
        </div>
      </form>

		  <table class="table table-bordered">
        <thead>
          <tr>
            <th class="text-center">#</th>
            <th class="text-center">Reference No.</th>
            <th class="text-center">Customer</th>
            <th class="text-center">Date</th>
            <th class="text-center">Amount (Rs)</th>
            <th class="text-center">Action By</th>
            <th class="text-center">Type</th>
            <th class="text-center">Status</th>
            <th class="text-center">PO</th>
            <th class="text-center">Actions</th>
          </tr>
        </thead>
        <tbody>
          @if(count($data) > 0)
            @foreach($data as $key => $val)
              <tr>
                <td align="center">{{ (($data->currentPage()-1)*$data->perPage())+($key+1) }}</td>
                <td align="left">{{$val->reference_no}}<br/>{{($val->ln_reference)? '('.($val->ln_reference->reference_no).')':(($val->customerPo)?'(PSQ'.STR_PAD($val->id,6,'0',STR_PAD_LEFT).')':'-')}}</td>
                <td align="left">{{$val['customer']->first_name}} {{$val['customer']->last_name}}</td>
                <td align="left">{{explode(" ",$val->quotation_date)[0]}}</td>
                <td align="right">{{number_format($val->total_amount,2)}}</td>
                <td align="left">{{$val['createdBy']->first_name}} {{$val['createdBy']->last_name}}</td>
                <td align="left">@if($val->quotation_type == "1") Special @else Normal @endif </td>
                <td align="left">@if($val->status == PENDING) Pending 
                                 @elseif($val->status == APPROVED) Approved 
                                 @elseif($val->status == REJECTED) Rejected
                                 @elseif($val->status == REVISED) Revised
                                 @elseif($val->status == CLOSED) Closed
                                 @elseif($val->status == INVOICED) Invoiced 
                                 @elseif($val->status == PO_PROCESSING) PO Processing 
                                 @elseif($val->status == DISCARDED) Discarded 
                                 @elseif($val->status == HOLD) Hold 
                                 @endif 
                </td>
                <td align="center">
                   @if(!$val->customerPo) -
                   @else<a href="{{url('admin/quotation/po')}}/{{$val->id}}" target=_blank>{{$val->customerPo->po_number}}</a><br>Verified
                   @endif
                </td>
                <td align="center">
                  <div class="btn-group">
                    <a  class="btn btn-sm btn-default" href="{{ route('quotation.list.detail', $val->id) }}"><i class="fa fa-eye" aria-hidden="true"></i></a>
                    @if($val->status != DISCARDED)
                    <a  class="btn btn-sm btn-default" href="{{ route('quotation.po.upload', $val->id) }}"><i class="fa fa-file" aria-hidden="true"></i></a>

                    <a  class="btn btn-sm btn-default" href="{{url('admin/quotation/doDiscardQuotation')}}?id={{$val->id}}"><i class="fa fa-times" aria-hidden="true"></i></a>
                    @if($val->status != PENDING)
                    <a  class="btn btn-sm btn-default" href="{{ route('quotation.revise', $val->id) }}"><i class="fa fa-exchange" aria-hidden="true"></i></a>
                    @endif
                    @endif
                  </div>
                </td>
              </tr>
            @endforeach
          @else
            <tr>
              <td colspan="9" align="center"> - No Data to Display - </td>
            </tr>
          @endif  
        </tbody>
      </table>
      <div class="row">
        <div class="col-sm-12 text-left">
            Row {{$data->count()}} of {{$data->total()}} 
        </div>
        <div class="col-sm-12 text-right">
            {!! $data->appends($old->except('page'))->render() !!}
        </div>
      </div>
		</div><!-- /.box-body -->
    <div class="overlay" style="display: none;">
      <i class="fa fa-spin fa-refresh"></i>
    </div>
	</div><!-- /.box -->
</section><!-- /.content -->
@stop
@section('js')

<script src="{{asset('assets/dist/chosen/chosen.jquery.min.js')}}"></script>
<script src="{{asset('assets/dist/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>


<script type="text/javascript">
$(document).ready(function() {
  $(".chosen").chosen();

  $('.datepick').datepicker({
      keyboardNavigation: false,
      forceParse: false,
      format: 'yyyy-mm-dd'
  });
});
</script>
@stop
