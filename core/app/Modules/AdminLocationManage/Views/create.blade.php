@extends('layouts.back_master') @section('title','Admin - Location Management')
@section('css')
<style type="text/css">
    .row{
        margin-bottom: 10px; 
    }
    
    .default-error{
        padding: 0px !important;
        margin-bottom: 0px !important;
        margin-top: 5px;
        width: 50%;
        text-align: left !important;
        padding-left: 10px !important;
    }

</style>
@stop
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Location Management</h2>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{url('admin/location/index')}}">Location</a></li>
            <li class="active">List Location</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a href="{{url('admin/location/index')}}" class="btn btn-primary"><i class="fa fa-th"></i> List Location</a>
        </div>
    </div>
</div>

<!-- Content-->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Add Location</h5>
            <div class="ibox-tools">
                
            </div>
        </div>

        <div class="ibox-content" style="display: block;">
            <div class="row">
                <div class="form-group">
                    <label class="col-sm-2 control-label required">Location Type</label>
                    <div class="col-sm-8">                      
                        {!! Form::select('loc_type', $location_types, [],['class'=>'form-control chosen loc_type','style'=>'width:100%;','ng-model'=>'loc_type','required','data-placeholder'=>'Choose Location Type','id'=>'loc_type']) !!}
                        @if($errors->has('loc_type'))
                            <span class="help-block">{{$errors->first('loc_type')}}</span>
                        @endif
                        <div class="alert alert-danger default-error" style="display: none;" id="alert1" align="right">
                            <button type="button" class="close" data-dismiss="alert1" aria-hidden="true">&times;</button>
                            <h6 id="error-type"></h6>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    <label class="col-sm-2 control-label required">Parent Location</label>
                    <div class="col-sm-8"> 
                        <select class="form-control chosen parent_location" id="parent_location" name="parent_location" ng-model="parent_location" required>
                            <option value='0'>Select Parent </option>
                            @foreach($parent_locations as $parent_location)
                                <option value="{{$parent_location->id}}" @if(old('parent_location')==$parent_location->id) selected @endif>
                                    {{$parent_location->parent}}
                                </option>
                            @endforeach
                        </select>
                        @if($errors->has('parent_location'))
                            <span class="help-block">
                                {{ $errors->first('parent_location') }}
                            </span>
                        @endif
                        <div class="alert alert-danger default-error" style="display: none;" id="alert2" align="right">
                            <button type="button" class="close" data-dismiss="alert2" aria-hidden="true">&times;</button>
                            <h6 id="error-parent"></h6>
                        </div>
                    </div>    
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    <label class="col-sm-2 control-label required">Code</label>
                    <div class="col-sm-8 @if($errors->has('code')) has-error @endif">
                        <input type="text" class="form-control" name="code" id="code" placeholder="Code" value="{{Input::old('code')}}">
                        @if($errors->has('code'))
                            <span class="help-block">{{$errors->first('code')}}</span>
                        @endif
                    </div>
                </div>    
            </div>

            <div class="row">
                <div class="form-group">
                    <label class="col-sm-2 control-label required">Name</label>
                    <div class="col-sm-8 @if($errors->has('name')) has-error @endif">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{Input::old('name')}}">
                        @if($errors->has('name'))
                            <span class="help-block">{{$errors->first('name')}}</span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-8 @if($errors->has('description')) has-error @endif">
                       <textarea name="description" id="description" class="form-control" rows="4">{{old('description')}}</textarea>
                        @if($errors->has('description'))
                            <span class="help-block">{{$errors->first('description')}}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <div class="col-sm-10">
                        <button type="button" class="btn btn-success pull-right save"><i class="fa fa-floppy-o"></i> Save</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


@stop

@section('js')
	<!-- CORE JS -->
	<script type="text/javascript">
    	$(document).ready(function() {
	  		$(".chosen").chosen();

            $('.loc_type').change(function(){
                $('.overlay').show();
                ajaxRequest( '{{url('admin/location/json/parent/list')}}' , { 'id' : $(this).val() }, 'get', resultFunc);
            });

            $('.save').click(function(e){
                e.preventDefault();

                var txt = $('#name').val();                
                $('#name').val(txt.trim());

                loc_type = $('#loc_type').val();
                parent_location = $('#parent_location').val();
                code = $('#code').val();
                name = txt.trim();
                description = $('#description').text();

                $.ajax({
                    url: '{{url('admin/location/create')}}',
                    method: 'post',
                    cache: false,
                    data: {
                            'loc_type':loc_type,
                            'parent_location' : parent_location,
                            'code' : code,
                            'name' : name,
                            'description' : description
                        },
                    success: function(response){
                        if(response.status == 'error'){

                            _loadParentLocation(response.old.loc_type,response.old.parent_location,response.old.code,response.old.name);

                            if(response.old.loc_type==0 ){
                                $('#alert1').show();
                                $('#error-type').text('Please select a location type');
                            }
                            if(response.old.parent_location==0){
                                $('#alert2').show();
                                $('#error-parent').text('Please select a parent location');
                            }
                            if(response.old.code=='' ){
                                $('#code').addClass('error');
                            }
                            if(response.old.name=='' ){
                                $('#name').addClass('error');
                            }

                        }else if(response.status='success'){
                            swal("Done!", "Location successfully created!.", "success");
                            location.reload();
                        }
                    },
                    error: function(xhr){
                        console.log(xhr);
                        swal("Error!", 'Error occurred. Please try again', "error");
                    } 
                });

            });

        });

        function resultFunc(data){
            $('#parent_location').html("");
            $('#parent_location').append('<option value="0" selected>--Select parent location--</option>');   
            $.each(data,function(key,value){
                $('#parent_location').append('<option value="'+key+'">'+value+'</option>');            
            });
            $('#parent_location').trigger("chosen:updated");
            $('.overlay').hide();
        }

        function _loadParentLocation(old_type,old_pl){
            ajaxRequest( '{{url('admin/location/json/parent/list')}}' , { 'id' : old_type }, 'get', function(res){
                $('#parent_location').html("");
                if(old_pl=='0'){
                    $('#parent_location').append('<option value="0" selected>--Select parent location--</option>');
                }else{
                    $('#parent_location').append('<option value="0">--Select parent location--</option>');                    
                }

                $.each(res,function(key,value){
                    if(old_pl==key){
                        $('#parent_location').append('<option value="'+key+'" selected>'+value+'</option>');            
                    }else{
                        $('#parent_location').append('<option value="'+key+'">'+value+'</option>');            
                    }
                });
                $('#parent_location').trigger("chosen:updated");
                $('.overlay').hide();
            });
        }

	</script>
  	<!-- //CORE JS -->
@stop
