<tr id="{{$i}}">
     <td class="">{{$i}}</td>
     <td>{{$result_val->code}}</td>
     <td>{{$result_val->name}}</td>
     <td>{{$result_val->description}}</td>
     <td>{{($result_val->type)? $result_val->type->name : '-'}}</td>
     <td>{{$result_val->created_at}}</td>
     <td class="text-center">
        <div class="btn-group">
            <a href="{{'edit/'.$result_val['id']}}" class="btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="Edit Location"><i class="fa fa-pencil"></i></a>
        </div>
     </td>
</tr>

