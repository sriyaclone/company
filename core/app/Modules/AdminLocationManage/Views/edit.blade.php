@extends('layouts.back_master') @section('title','Admin - Location Management')
@section('current_title','Location Edit')

@section('css')
<style type="text/css">
    .row{
        margin-bottom: 10px; 
    }
    
    .default-error{
        padding: 0px !important;
        margin-bottom: 0px !important;
        margin-top: 5px;
        width: 50%;
        text-align: left !important;
        padding-left: 10px !important;
    }

</style>
@stop

@section('content')
    <!-- Content Header (Page header) -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Location Management</h2>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{url('admin/location/index')}}">Location</a></li>
            <li class="active">Edit Location</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a href="{{url('admin/location/index')}}" class="btn btn-primary"><i class="fa fa-th"></i> List Location</a>
            <a href="{{url('admin/location/create')}}" class="btn btn-warning"><i class="fa fa-plus"></i> Add Location</a>
        </div>
    </div>
</div>
<!-- !!Content Header (Page header) -->

<!-- Main content -->
<div class="wrapper wrapper-content animated fadenRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Edit Location</h5>
            <div class="ibox-tools">
                
            </div>
        </div>

        <div class="ibox-content" style="display: block;">
            <form role="form" class="form-horizontal form-validation" method="post">
                {!!Form::token()!!}
                <div class="form-group">
                    <div class=" @if($errors->has('name')) has-error @endif">
                        <label class="col-sm-2 control-label required">Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @if($errors->has('name')) error @endif" name="name" placeholder="Name" value="{{$location_details[0]->name}}">
                            @if($errors->has('name'))
                                <span class="help-block">{{$errors->first('name')}}</span>
                            @endif
                        </div>
                    </div>
                </div>
                    
                <div class="form-group">
                    <div class="@if($errors->has('code')) has-error @endif">
                        <label class="col-sm-2 control-label required">Code</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @if($errors->has('code')) error @endif" name="code" placeholder="Code" value="{{$location_details[0]->code}}">
                            @if($errors->has('code'))
                                <span class="help-block">{{$errors->first('code')}}</span>
                            @endif
                        </div>
                    </div>
                </div>
        
                <div class="form-group @if($errors->has('description')) has-error @endif">
                    <label for="" class="col-sm-2 control-label">Location Description</label>
                    <div class="col-sm-10">
                        <textarea name="description" class="form-control" rows="3">{{$location_details[0]->description}}</textarea>
                        @if($errors->has('description'))
                            <span class="help-block">{{$errors->first('description')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label required">Location Type</label>
                    <div class="col-sm-10 @if($errors->has('loc_type')) has-error @endif">
                        {!! Form::select('loc_type', $location_types, $location_details[0]->type_id,['class'=>'form-control chosen loc_type','style'=>'width:100%;','required','data-placeholder'=>'Choose Location Type']) !!}
                        @if($errors->has('loc_type'))
                            <span class="help-block">{{$errors->first('loc_type')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label required">Parent Location</label>
                    <div class="col-sm-10 @if($errors->has('parent_location')) has-error @endif">
                        {!! Form::select('parent_location',$parent,$location_details[0]->parent,['class'=>'form-control chosen','style'=>'width:100%;','required','data-placeholder'=>'Choose Parent Location','id'=>'parent_location']) !!}
                        @if ($errors->has('parent_location'))
                            <span class="help-block">
                                {{ $errors->first('parent_location') }}
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-success pull-right"><i class="fa fa-floppy-o"></i> Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div> 
<!-- !!!Content -->

@stop
@section('js')
    <!-- CORE JS -->
  <script type="text/javascript">
        $(document).ready(function() {
            $(".chosen").chosen();

            $('.loc_type').change(function(){
                $('.overlay').show();
                ajaxRequest( '{{url('admin/location/json/parent/list')}}' , { 'id' : $(this).val() }, 'get', resultFunc);
            });

            $('.save').click(function(e){
                e.preventDefault();

                var txt = $('#name').val();                
                $('#name').val(txt.trim());

                loc_type = $('#loc_type').val();
                parent_location = $('#parent_location').val();
                code = $('#code').val();
                name = txt.trim();
                description = $('#description').text();

                $.ajax({
                    url: '{{url('admin/location/edit')}}',
                    method: 'post',
                    cache: false,
                    data: {
                            'loc_type':loc_type,
                            'parent_location' : parent_location,
                            'code' : code,
                            'name' : name,
                            'description' : description
                        },
                    success: function(response){
                        if(response.status == 'error'){

                            _loadParentLocation(response.old.loc_type,response.old.parent_location,response.old.code,response.old.name);

                            if(response.old.loc_type==0 ){
                                $('#alert1').show();
                                $('#error-type').text('Please select a location type');
                            }
                            if(response.old.parent_location==0){
                                $('#alert2').show();
                                $('#error-parent').text('Please select a parent location');
                            }
                            if(response.old.code=='' ){
                                $('#code').addClass('error');
                            }
                            if(response.old.name=='' ){
                                $('#name').addClass('error');
                            }

                        }else if(response.status='success'){
                            swal("Done!", "Location successfully updated!.", "success");
                            location.reload();
                        }
                    },
                    error: function(xhr){
                        console.log(xhr);
                        swal("Error!", 'Error occurred. Please try again', "error");
                    } 
                });

            });

            $('#loc_type').trigger('change');

        });

        function resultFunc(data){
            $('#parent_location').html("");
            $('#parent_location').append('<option value="0" selected>--Select parent location--</option>');   
            $.each(data,function(key,value){
                $('#parent_location').append('<option value="'+key+'">'+value+'</option>');            
            });
            $('#parent_location').trigger("chosen:updated");
            $('.overlay').hide();
        }

        function _loadParentLocation(old_type,old_pl){
            ajaxRequest( '{{url('admin/location/json/parent/list')}}' , { 'id' : old_type }, 'get', function(res){
                $('#parent_location').html("");
                if(old_pl=='0'){
                    $('#parent_location').append('<option value="0" selected>--Select parent location--</option>');
                }else{
                    $('#parent_location').append('<option value="0">--Select parent location--</option>');                    
                }

                $.each(res,function(key,value){
                    if(old_pl==key){
                        $('#parent_location').append('<option value="'+key+'" selected>'+value+'</option>');            
                    }else{
                        $('#parent_location').append('<option value="'+key+'">'+value+'</option>');            
                    }
                });
                $('#parent_location').trigger("chosen:updated");
                $('.overlay').hide();
            });
        }

        

    </script>
    <!-- //CORE JS -->
@stop
