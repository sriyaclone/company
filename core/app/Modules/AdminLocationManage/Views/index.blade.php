@extends('layouts.back_master') @section('title','Admin - Location Management') 
@section('css')
<style type="text/css">
.chosen-single{
    height: 25px !important;
    border : 0px !important;
}  
</style>
@stop

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Location Management</h2>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{url('admin/location/index')}}">Location</a></li>
            <li class="active">List Location</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a href="{{url('admin/location/create')}}" class="btn btn-warning"><i class="fa fa-plus"></i> Add Location</a>
        </div>
    </div>
</div>

<!-- Content-->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>List Location</h5>
            <div class="ibox-tools">
                
            </div>
        </div>

        <div class="ibox-content" style="display: block;">
                <table class="table table-striped table-bordered table-hover dataTables-example">
                    <thead>
                        <form id="form" role="form" method="get" action="{{url('admin/location/search')}}">
                            <tr>
                                <th width="5%">#</th>
                                <th>Location Code </br>
                                    <input type="text" class="form-control" name="code" placeholder="Enter Code" value="{{$code}}">
                                </th>
                                <th> Location Name</br>
                                    <input type="text" class="form-control" name="name" placeholder="Enter Name" value="{{$name}}">
                                </th>
                                <th>Location Type </br>
                                    {!! Form::select('parent', $location_types, $parent,['class'=> 'chosen form-control input-sm','style'=>'width:100%;','required','data-placeholder'=>'Choose Location Type','id'=>'parent']) !!}
                                </th>
                                <th>Parent Location</th>
                                <th>Created At</th>
                                <th width="10%">Action <br>
                                    <button type="submit" class="btn btn-default btn-sm" id="plan"><i class="fa fa-search" style="padding-right: 16px;width: 28px;"></i>Find</button>
                                </th>
                            </tr>
                        </form>
                    </thead>
                    <tbody>
                    @if(count($location_details) > 0)
                        @foreach($location_details as $key => $row)
                            <tr>                           
                                <td class="text-center">
                                    {{ (($location_details->currentPage()-1)*$location_details->perPage())+($key+1) }}</td>
                                <td> 
                                    {{ ($row->code != '') ? $row->code : '-' }}
                                </td>
                                <td class="text-center">
                                    {{ ($row->name !='') ? $row->name : '-' }}
                                </td>
                                <td class="text-center">
                                    {{ ($row->type != '') ? $row->type->type : '-' }}
                                </td>
                                <td class="text-center">
                                    {{ ($row['parentDetail'] != '') ? $row['parentDetail']->name : '-' }}
                                </td>
                                <td>
                                    {{ ($row->created_at != '') ? $row->created_at : '-' }}
                                </td>
                                <td class="text-center">
                                    <div class="btn-group">
                                    @if($user->hasAnyAccess(['location.edit', 'admin']))
                                        <button class="btn btn-default" onclick="window.location.href='{{url('admin/location/edit/'.$row->id)}}'"><i class="fa fa-pencil"></i></button>
                                    @else
                                        <button class="btn btn-default" disabled><i class="fa fa-pencil"></i></button>
                                    @endif

                                    @if($user->hasAnyAccess(['location.delete', 'admin']))
                                        <button class="btn btn-default btn-delete" data-id="{{$row->id}}" onclick="deleteData('{{url('admin/location/delete?id='.$row->id)}}');"><i class="fa fa-trash-o"></i></button>
                                    @else
                                        <button class="btn btn-default" disabled><i class="fa fa-trash-o"></i></button>
                                    @endif
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9" align="center"> - No Data to Display - </td>
                        </tr>
                    @endif
                    </tbody>
                </table>
                @if($location_details != null)
                <div class="row">
                    <div class="col-sm-12 text-left">
                         Row {{$location_details->count()}} of {{$location_details->total()}} 
                    </div>
                    <div class="col-sm-12 text-right">
                         {!! $location_details->render() !!}
                    </div>
                </div>
                @endif
        </div>
    </div>
</div>

@stop

@section('js')
<script type="text/javascript">
    $(document).ready(function() {
        $(".chosen").chosen();

        $('#parent').change(function(){
            $("form").submit();
        });
    });

      function deleteData(_url){
        swal({
            title: "Are you sure?",
            text: "you wanna delete this location?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        },
        function(){
            $.ajax({
                url: _url,
                method: 'get',
                cache: false,
                data: [],
                success: function(response){
                    if(response.status == 1){
                        swal("Done!", "location has been deleted!.", "success");
                        location.reload();
                    }else{
                        swal("Error!", response.message, "error");
                    }
                },
                error: function(xhr){
                    console.log(xhr);
                    swal("Error!", 'Error occurred. Please try again', "error");
                } 
            });
      
        });
    }
</script>
@stop
