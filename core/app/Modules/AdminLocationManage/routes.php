<?php


Route::group(['middleware' => ['auth']], function()
{	
	Route::group(['prefix'=>'admin/location/','namespace' => 'App\Modules\AdminLocationManage\Controllers'],function() {

	    Route::get('create', [
	      'as' => 'location.create', 'uses' => 'AdminLocationManageController@create'
	    ]);

	    Route::get('edit/{id}', [
	      'as' => 'location.edit', 'uses' => 'AdminLocationManageController@edit'
	    ]);

	    Route::post('edit/{id}', [
            'as' => 'location.edit','uses' => 'AdminLocationManageController@update'
        ]);

        Route::get('json/parent/list', [
            'as' => 'location.list','uses' => 'AdminLocationManageController@getLocationJsonList'
        ]);

        Route::get('index', [
            'as' => 'location.index', 'uses' => 'AdminLocationManageController@index'
        ]);

        Route::get('search', [
            'as' => 'location.search', 'uses' => 'AdminLocationManageController@searchLocation'
        ]);

	    Route::post('create', [
	      'as' => 'location.create', 'uses' => 'AdminLocationManageController@store'
	    ]);

	    Route::get('delete', [ 
            'as' => 'location.delete', 'uses' => 'AdminLocationManageController@delete'
        ]);
	});    
});