<?php
namespace App\Modules\AdminLocationManage\Requests;

use App\Http\Requests\Request;

class LocationRequest extends Request {

	public function authorize(){
		return true;
	}

	public function rules(){
		$id = $this->id;

		if($this->is('admin/location/create')){
			
			$rules = [
				'name'	    	 => 'required|unique:location,name,'.$this->name,
				'code'	    	 => 'required|alpha_dash|unique:location,code,'.$this->code,
				'loc_type'		 => 'required|numeric|min:1',
				'parent_location'=>	'required'
				
			];
		}else if($this->is('admin/location/edit/'.$id)){

			$rules = [
				'name'	     	 => 'required|unique:location,name,'.$id,
				'code'	     	 => 'required|unique:location,code,'.$id,
				'loc_type'	 		 => 'required|numeric|min:1,type_id',
				'parent_location'=>	'required'
				
			];
		}

		return $rules;
	}

	public function messages(){

        return [
            'parent.min' => 'The Location Type can not be blank'
        ];

    }

}
