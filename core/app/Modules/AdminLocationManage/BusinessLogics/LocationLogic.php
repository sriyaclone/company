<?php namespace App\Modules\AdminLocationManage\BusinessLogics;


/**
* Business Logics 
* Define all the busines logics in here
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use App\Modules\AdminLocationManage\Models\Location;
use App\Models\LocationType;

use App\Models\User;
use App\Exceptions\TransactionException;

use Exception;
use Sentinel;

class LocationLogic{

    public function add($data){

        $loc = null;
        DB::transaction(function() use($data, &$loc){
            $user = Sentinel::getUser();

            $parent = Location::find($data->parent_location);

            $location = Location::create([
                'name'                =>  $data->name, 
                'code'                =>  $data->code,
                'parent'              =>  $data->parent_location,
                'description'         =>  $data->description,
                'type_id'    		  =>  $data->loc_type,
            ]);

            if($location) {
                $loc = $location;
            }else{
                throw new TransactionException('Record wasn\'t updated', 101);
            }
        });

        return $loc;
    }

    /**
     * This function is used to get all location details
     * @parm  integer $id that need to get locationdetails
     * @return type
     */
    public function getLocationDetails($id){
        $location_details = Location::where('id','=',$id)->get();
        if(count($location_details) > 0){
            return $location_details;
        }else{
            return false;
        }
    }

    /**
     * This function is used to update location
     * @param integer id and array $data
     * @response is boolean
     */
    public function updateLocation($id, $data){
         $loc = null;
        DB::transaction(function() use($id, $data,&$loc){
            
            $location       = Location::find($id);

            $location->name = $data->name;
            $location->code = $data->code;
            $location->description= $data->description;
            $location->type_id = $data->loc_type;
            $location->parent=$data->parent_location;
            $location->save();

            if($location){
                $loc = $location;
            }else{
                throw new Exception("Error occurred while updating location.");
            }
        });
        return $loc;
    }

    /**
     * This function is used to get all location details
     * @parm -
     * @return location object
     */
    public function getAllLocations(){
        return Location::with(['type','parentDetail'])->paginate(10);
    }

    /**
     * This function is used to search location
     * @param integer $name, $code
     * @response location object
     */
    public function searchLocation($name, $code, $parent) {
        $search = Location::with(['type']);
        if($name != ''){
            $search = $search->where('name','LIKE', '%' . $name . '%');
        }
        if($code != ''){
            $search = $search->where('code','LIKE', '%' . $code . '%');
        }
        if($parent != ''){
            if($parent!='0'){
                $search = $search->where('type_id', $parent);
            }
        }
        $search->orderBy('created_at','desc');
        return $search->paginate(10);
    }

    public function getType($id){
        
        $type_id= Location::select('type_id')->where('id', $id)->first()->type_id;

        return $type_id;
    }

    public function getLocationById($id){

        $location = Location::find($id);

        if(!$location){
            throw new \Exception('P2-Location not found for ID:'.$id);
        }

        return $location;
    }

    public function deleteLocation($id){
        $location = $this->getLocationById($id);

        if(!$location->delete()){
            throw new \Exception('P3-Could not delete Location ID:'.$id);
        }

        return 1;
    }

    public function getLocationJsonList($loc_type){

        return $location= Location::whereIn('id',function($query) use($loc_type){
                $query->select('id')
                      ->from('location')
                      ->where('type_id',($loc_type-1));
        })->select(DB::raw('CONCAT(code," ",name) as name'),'id')->lists('name','id');

     }
 }