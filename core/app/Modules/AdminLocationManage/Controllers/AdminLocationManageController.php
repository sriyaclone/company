<?php namespace App\Modules\AdminLocationManage\Controllers;


/**
* Controller class
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Modules\AdminLocationManage\Models\Location;
use App\Modules\AdminLocationManage\BusinessLogics\LocationLogic;
use App\Modules\AdminLocationManage\Requests\LocationRequest;

use App\Models\LocationType;

use DB;

class AdminLocationManageController extends Controller {

	protected $location;

    public function __construct(LocationLogic $locationLogic){
        $this->location = $locationLogic;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$location_details = $this->location->getAllLocations();
		
		$location_types = LocationType::all()->lists('type', 'id')->prepend('All','0');

		$parent_locations=Location::all()->lists('name','id')->prepend('All','0');

		return view("AdminLocationManage::index")->with([
			'location_details' 	        => $location_details,
			'location_types' 			=> $location_types,
			'parent'					=> 0,
			'parent_location'			=> 0,
			'name' 						=> '',
			'code' 						=> ''
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{	

		$location_types = LocationType::all()->lists('type', 'id')->prepend('Select Location Type','0');
		$parent_locations=[];
		
		return view("AdminLocationManage::create",compact('location_types','parent_locations'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{

		try {
	        
	        if($request->loc_type == 0 || $request->parent_location == 0 || $request->code == "" || $request->name == ""){

	        	$errors = [];

	        	if($request->loc_type == 0){
	        		$errors['type'] = 'Please select type';
	        	}

	        	if($request->parent_location == 0){
	        		$errors['parent'] = 'Please select parent location';
	        	}

	        	if($request->code == ''){
	        		$errors['code'] = 'Code cannot be empty';
	        	}

	        	if($request->name == ''){
	        		$errors['name'] = 'Name cannot be empty';
	        	}
	        	
				return response()->json([
	                'status' 	=> 'error',
	                'data' 		=> $errors,
	                'old' 		=> [
	                	'loc_type'=>$request->loc_type,
	                	'parent_location'=>$request->parent_location,
	                	'code'=>$request->code,
	                	'name'=>$request->name]
	            ]);

	        }else{
	        	
		        $bool = $this->location->add($request);

		        return response()->json([
	                'status' => 'success'            
	            ]);

	        }

	    }
	    catch(Exception $e){
            return redirect('admin/location/index')->with([
                'error' => true,
                'error.title' => 'Error..!',
                'error.message' => $e->getMessage()
            ])->withInput();
        }
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
       	$location_details = $this->location->getLocationDetails($id);

     	$type_id= $this->location->getType($id);

     	$parent =Location::whereIn('id',function($query) use($type_id){
                $query->select('id')
                      ->from('location')
                      ->where('type_id',($type_id-1));
        })->select(DB::raw('CONCAT(code," ",name) as name'),'id')->lists('name','id');	
    
        $location_types = LocationType::all()->lists('type','id')->prepend('Select Location Type',0);

        $parent_locations=[];

        if($location_details) {
            return view("AdminLocationManage::edit")->with([
            	'location_details' 			=> $location_details,
            	'parent'					=> $parent,
            	'location_types' 			=> $location_types,
            	'parent_locations'			=> $parent_locations,
           	]);
        }else{
            return response()->view('errors.404');
        }
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	        
	public function update(LocationRequest $request,$id)
	{ 
		try {
	        
	        if($request->loc_type == 0 || $request->parent_location == 0 || $request->code == "" || $request->name == ""){

	        	$errors = [];

	        	if($request->loc_type == 0){
	        		$errors['type'] = 'Please select type';
	        	}

	        	if($request->parent_location == 0){
	        		$errors['parent'] = 'Please select parent location';
	        	}

	        	if($request->code == ''){
	        		$errors['code'] = 'Code cannot be empty';
	        	}

	        	if($request->name == ''){
	        		$errors['name'] = 'Name cannot be empty';
	        	}
	        	
				return response()->json([
	                'status' 	=> 'error',
	                'data' 		=> $errors,
	                'old' 		=> [
	                	'loc_type'=>$request->loc_type,
	                	'parent_location'=>$request->parent_location,
	                	'code'=>$request->code,
	                	'name'=>$request->name]
	            ]);

	        }else{
	        	
		       $bool = $this->location->updateLocation($id, $request);

		            return redirect('admin/location/index')->with([
                'success' => true,
                'success.message' => 'Location updated successfully!',
                'success.title'   => 'Success..!'            
	            ]);

	        }

	    }
	    catch(Exception $e){
            return redirect('admin/location/index')->with([
                'error' => true,
                'error.title' => 'Error..!',
                'error.message' => $e->getMessage()
            ])->withInput();
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function delete(Request $request)
	{
		try{
			$this->location->deleteLocation($request->id);
			return response()->json([
				'status' => 1
			]);
		}catch(\Exception $e){
			return response()->json([
				'status' => 0,
				'message' => $e->getMessage()
			]);
		}
	}

	/**
     * This function is used to search location type
     * @return Response
     */
    public function searchLocation(Request $request){
        $location_details = $this->location->searchLocation($request->get('name'), $request->get('code'), $request->get('parent'));

        $location_types = LocationType::all()->lists('type', 'id')->prepend('All','0');
        
        return view("AdminLocationManage::index")->with([
        	'location_details' 			=> $location_details,
        	'location_types' 			=> $location_types,
        	'parent'					=> $request->get('parent'),
        	'name' 						=> $request->get('name'),
        	'code' 						=> $request->get('code')
        ]);
    }

    public function getLocationJsonList(Request $request)
    {
    	return $parent_location=$this->location->getLocationJsonList($request->id);
    }

}
