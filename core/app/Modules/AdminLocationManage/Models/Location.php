<?php namespace App\Modules\AdminLocationManage\Models;

/**
*
* Model
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Location extends Model {

	use SoftDeletes;

	protected $table = 'location';

	protected $dates = ['deleted_at'];

	protected $guarded = array('id');

	public function type(){
		
		return $this->belongsTo('App\Models\LocationType','type_id', 'id');
	}

	public function parentDetail()
	{
		return $this->belongsTo($this,'parent','id');
	}
	
}