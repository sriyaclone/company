<?php namespace App\Modules\AdminVehicleManage\Models;

/**
*
* Model
* @author Sriya <csriyarathne@gmail.com>
* @version 1.0.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vehicle extends Model{

	use SoftDeletes;

	protected $table = 'vehicle';

	protected $dates = ['deleted_at'];

	protected $guarded = [
		'id'
	];

	public function vehicleType(){
		return $this->belongsTo('App\Modules\AdminVehicleManage\Models\VehicleType','vehicle_type_id','id');
	}

	// public function vehicle_warehouse(){
	// 	return $this->belongsTo('App\Models\WarehouseVehicle','id','vehicle_id')->whereNull('deleted_at')->with(['warehouse']);
	// }

}
