<?php namespace App\Modules\AdminVehicleManage\Models;

/**
*
* Model
* @author Sriya <csriyarathne@gmail.com>
* @version 1.0.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VehicleType extends Model{

	use SoftDeletes;

	protected $table = 'vehicle_type';

	protected $dates = ['deleted_at'];

	protected $fillable = [
		'name',
		'description'
	];

}
