<tr id="{{$i}}">
     <td class="">{{$i}}</td>
     <td>{{$result_val->vehicleType->name}}</td>
     <td>{{$result_val->code}}</td>
     <td>{{$result_val->plate_no}}</td>
     <td>{{$result_val->chassis_no}}</td>
     <td>{{$result_val->color}}</td>
     <td>{{$result_val->manufacturer}}</td>
     <td>{{$result_val->year}}</td>
     <td>{{$result_val->modal}}</td>
     <td>@if(sizeof($result_val->vehicle_warehouse) > 0) {{ $result_val->vehicle_warehouse->warehouse->name }} @endif</td>
     <td>@if($result_val->status == 1) Active @else Deactive @endif</td>
     <td>{{$result_val->created_at}}</td>
     <td class="text-center">
        <div class="btn-group">
            <a href="{{'edit/'.$result_val['id']}}" class="btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="Edit Vehicle"><i class="fa fa-pencil"></i></a>
            <a href="javascript:void(0);" class="btn btn-xs btn-default delete" data-toggle="tooltip" data-placement="top" title="Delete Customer" data-id="{{$result_val['id']}}"><i class="fa fa-trash-o"></i></a>
        </div>
     </td>
</tr>

