@extends('layouts.back_master') @section('title','Add Vehicle')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
<link rel="stylesheet" href="{{asset('assets/selectize/css/selectize.bootstrap3.css')}}">
<style type="text/css">
	.has-error .help-block, .has-error .control-label{
		color:#e41212;
	}
	.has-error .chosen-container{
		border:1px solid #e41212;
	}
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Vehicle<small>Management</small></h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li><a href="{{url('admin/vehicle/index')}}">Vehicle Management</a></li>
		<li class="active">Add Vehicle</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Add Vehicle</h3>
		</div>
		<br>
		<form action="" class="form-horizontal" method="post">
			<div class="box-body">
				{!!Form::token()!!}
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
						<label for="" class="control-label required">Code</label>
	            		<input type="text" class="form-control" name="code" value="{{old('code')}}">
	            		@if($errors->has('code'))
	            		<span class="help-block">{{$errors->first('code')}}</span>
	            		@endif							
					</div>

					<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
						<label for="" class="control-label required">Plate No</label>
	            		<input type="text" class="form-control" name="plate_no" value="{{old('plate_no')}}">
	            		@if($errors->has('plate_no'))
	            		<span class="help-block">{{$errors->first('plate_no')}}</span>
	            		@endif
					</div>
					<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
						<label for="" class="control-label required">Vehicle Class</label>
	            		{!! Form::select('vehicle_type',$vehicle_types, old('vehicle_type'), ['class'=>'form-control selectize','style'=>'width:100%;','required','data-placeholder'=>'Set Vehicle Class','id'=>'vehicle_type']) !!}
					</div>
					<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
						<label for="" class="control-label">Chassis No</label>
	                	<input type="text" class="form-control" name="chassis_no" value="{{old('chassis_no')}}">
	            		@if($errors->has('chassis_no'))
	            		<span class="help-block">{{$errors->first('chassis_no')}}</span>
	            		@endif
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
						<label for="" class="control-label">Engine No</label>
	                	<input type="text" class="form-control" name="engine_no" value="{{old('engine_no')}}">
	            		@if($errors->has('engine_no'))
	            		<span class="help-block">{{$errors->first('engine_no')}}</span>
	            		@endif
					</div>
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<label for="" class="control-label">Body Type</label>
	            		{!! Form::select('body_type',$bodyType, old('body_type'),['class'=>'form-control selectize','style'=>'width:100%;','data-placeholder'=>'Set Body Type','id'=>'body_type']) !!}
					</div>
					<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
						<label for="" class="control-label">Fuel Type</label>
	            		{!! Form::select('fuel_type',$fuel, old('fuel_type'), ['class'=>'form-control selectize','style'=>'width:100%;','data-placeholder'=>'Set Fuel Type','id'=>'fuel_type']) !!}
					</div>
					<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
						<label for="" class="control-label">Cylinder Capacity (CC)</label>
	                	<input type="text" class="form-control" name="cylinder_capacity" value="{{old('cylinder_capacity')}}">
	            		@if($errors->has('cylinder_capacity'))
	            		<span class="help-block">{{$errors->first('cylinder_capacity')}}</span>
	            		@endif
					</div>
				</div>

				<div class="row">
					
				
					
				</div>

				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<label for="" class="control-label">Make</label>
						{!! Form::select('manufacturer',$make, old('manufacturer'),['class'=>'form-control selectize','style'=>'width:100%;','data-placeholder'=>'Set Make Type','id'=>'manufacturer']) !!}
	            		@if($errors->has('manufacturer'))
	            		<span class="help-block">{{$errors->first('manufacturer')}}</span>
	            		@endif
					</div>
				
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<label for="" class="control-label">Modal</label>
	                	{!! Form::select('modal',$modal, old('modal'),['class'=>'form-control selectize','style'=>'width:100%;','data-placeholder'=>'Set Modal Type','id'=>'modal']) !!}
	            		@if($errors->has('modal'))
	            		<span class="help-block">{{$errors->first('modal')}}</span>
	            		@endif
					</div>

					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<label for="" class="control-label">Country</label>
	                	{!! Form::select('country',$country, old('country'),['class'=>'form-control selectize','style'=>'width:100%;','data-placeholder'=>'Set Country Type','id'=>'country']) !!}
	            		@if($errors->has('country'))
	            		<span class="help-block">{{$errors->first('country')}}</span>
	            		@endif
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<label for="" class="control-label">Color</label>
	                	<input type="text" class="form-control" name="color" value="{{old('color')}}">
	            		@if($errors->has('color'))
	            		<span class="help-block">{{$errors->first('color')}}</span>
	            		@endif
					</div>

					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<label for="" class="control-label">Year</label>
	                	<input type="text" class="form-control" name="year" value="{{old('year')}}">
	            		@if($errors->has('year'))
	            		<span class="help-block">{{$errors->first('year')}}</span>
	            		@endif
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<label for="" class="control-label">Front Tyre Size (cm)</label>
	                	<input type="text" class="form-control" name="front_tyre" value="{{old('front_tyre')}}">
	            		@if($errors->has('front_tyre'))
	            		<span class="help-block">{{$errors->first('front_tyre')}}</span>
	            		@endif
					</div>
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<label for="" class="control-label">Rear Tyre Size (cm)</label>
	                	<input type="text" class="form-control" name="rear_tyre" value="{{old('rear_tyre')}}">
	            		@if($errors->has('rear_tyre'))
	            		<span class="help-block">{{$errors->first('rear_tyre')}}</span>
	            		@endif
					</div>

					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<label for="" class="control-label">Dual/Single Tyre</label>
	                	{!! Form::select('dual_tyre',$dual, old('dual_tyre'),['class'=>'form-control','style'=>'width:100%;','data-placeholder'=>'','id'=>'dual_tyre']) !!}
	            		@if($errors->has('dual_tyre'))
	            		<span class="help-block">{{$errors->first('dual_tyre')}}</span>
	            		@endif
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<label for="" class="control-label required">Division</label>
	            		{!! Form::select('department',$departments, old('department'),['class'=>'form-control selectize','style'=>'width:100%;','required','data-placeholder'=>'Set Division','id'=>'department']) !!}
	            		@if($errors->has('department'))
	            		<span class="help-block">{{$errors->first('department')}}</span>
	            		@endif
					</div>

					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<label for="" class="control-label required">Warehouse</label>
	                	{!! Form::select('warehouse',$warehouses, old('warehouse'),['class'=>'form-control chosen','style'=>'width:100%;','data-placeholder'=>'Choose Warehouse','id'=>'warehouse']) !!}
	            		@if($errors->has('warehouse'))
	            		<span class="help-block">{{$errors->first('warehouse')}}</span>
	            		@endif
					</div>
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<label for="" class="control-label required">Driver</label>
	                	{!! Form::select('driver',$driver, old('driver'),['class'=>'form-control chosen','style'=>'width:100%;','data-placeholder'=>'Choose Driver','id'=>'driver']) !!}
	            		@if($errors->has('driver'))
	            		<span class="help-block">{{$errors->first('driver')}}</span>
	            		@endif
					</div>
				</div>		        
			</div><!-- /.box-body -->
			<div class="box-footer">
				<button type="submit" class="btn btn-default pull-right">Save</button>
			</div>
		</form>
	</div><!-- /.box -->
</section><!-- /.content -->

@stop
@section('js')

<script src="{{asset('assets/dist/chosen/chosen.jquery.min.js')}}"></script>
<script src="{{asset('assets/selectize/js/standalone/selectize.min.js')}}"></script>

<script type="text/javascript">
$(document).ready(function() {
	$('.selectize').selectize({
		 create: true
	});

  	$(".chosen").chosen();
});
</script>
@stop
