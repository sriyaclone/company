@extends('layouts.back_master') @section('title','Admin - Vehicle Management')
@section('current_title','Customer List')

@section('css')
    <link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
    <style type="text/css">
        .box-header, .box-body {
            padding: 20px;
        }
    
        .panel.panel-bordered {
            border: 1px solid #ccc;
        }

        .btn-primary {
            color: white;
            background-color: #C51C6A;
            border-color: #C51C6A;
        }

        .chosen-container {
            font-family: 'FontAwesome', 'Open Sans', sans-serif;
            width: 100% !important;
        }

        b, strong {
            font-weight: bold;
        }

        .top{
            margin-top: 10px;
        }
    </style>
@stop

@section('content')
<!-- Content-->
<section>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Vehicle<small>Management</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
            <li><a href="{{url('admin/vehicle/index')}}">Vehicle Management</a></li>
            <li class="active">List Vehicle</li>
        </ol>
    </section>
    <!-- !!Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">  
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">List Vehicle</h3>
                <button type="button" class="btn btn-primary btn-sm pull-right" onclick="window.location.href='{{url('admin/vehicle/create')}}'"><i class="fa fa-plus" style="padding-right: 5px;"></i>Vehicle</button>
            </div>
            <div class="box-body">  
                <form role="form" class="form-validation" method="get">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <div class="form-group">
                                    <label>Vehicle Type</label>
                                    @if($args['vehicle_type']!="")
                                        {!! Form::select('vehicle_type',$vehicle_types, $args['vehicle_type'],['class'=>'form-control chosen error','style'=>'width:100%;','required','data-placeholder'=>'Choose Employee Designation','id'=>'vehicle_type']) !!}
                                        <label id="label-error" class="error" for="label">{{$errors->first('vehicle_type')}}</label>
                                    @else
                                        {!! Form::select('vehicle_type',$vehicle_types, [],['class'=>'form-control chosen','style'=>'width:100%;','required','data-placeholder'=>'Choose Employee Designation','id'=>'vehicle_type']) !!}
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <div class="form-group">
                                    <label>Department</label>
                                    @if($args['department_id']!="")
                                        {!! Form::select('department',$departments, $args['department_id'],['class'=>'form-control chosen error','style'=>'width:100%;','id'=>'department']) !!}
                                        <label id="label-error" class="error" for="label">{{$errors->first('department')}}</label>
                                    @else
                                        {!! Form::select('department',$departments, [],['class'=>'form-control chosen','style'=>'width:100%;','id'=>'department']) !!}
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <div class="form-group">
                                    <label>Code</label>
                                    <input type="text" class="form-control input-sm" name="code" placeholder="" value="{{$args['code']}}">
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <div class="form-group">
                                    <label>Plate No</label>
                                    <input type="text" class="form-control input-sm" name="plate_no" placeholder="" value="{{$args['plate_no']}}">
                                </div>
                            </div>
                        </div>

                        <div class="row">                            
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="form-group">
                                    <label>Chassis No</label>
                                    <input type="text" class="form-control input-sm" name="chassis_no" placeholder="" value="{{$args['chassis_no']}}">
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="form-group">
                                    <label>Manufacturer</label>
                                    <input type="text" class="form-control input-sm" name="manufacturer" placeholder="" value="{{$args['manufacturer']}}">
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="form-group">
                                    <label>Modal</label>
                                    <input type="text" class="form-control input-sm" name="modal" placeholder="" value="{{$args['modal']}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-md-offset-8">
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-default btn-sm" id="plan"><i class="fa fa-search" style="padding-right: 16px;width: 28px;"></i>Find</button>
                                    <a href="index" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top"><i class="fa fa-refresh" style="padding-right: 16px;width: 20px;"></i>Refresh</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <table class="table table-bordered bordered table-striped table-condensed" id="orderTable">
                    <thead>
                        <tr>    
                            <th width="5%">#</th>
                            <th>Vehicle Type</th>
                            <th>Code</th>
                            <th>Plate No</th>
                            <th>Chassis No</th>
                            <th>Color</th>
                            <th>Manufacturer</th>
                            <th>Year</th>
                            <th>Modal</th>
                            <th>Warehouse</th>
                            <th>Status</th>
                            <th>Created At</th>
                            <th width="10%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1;?>
                    @if(count($vehicle_details) > 0)
                        @foreach($vehicle_details as $result_val)
                            @include('AdminVehicleManage::vehicle.template.vehicle')
                            <?php $i++;?>
                        @endforeach
                    @else
                        <tr><td colspan="12" class="text-center">No data found.</td></tr>
                    @endif
                    </tbody>
                </table>

                @if($vehicle_details != null)
                  <div style="float: right;">{!! $vehicle_details->render() !!}</div>
                @endif
            </div>
        </div>

    </section>
    <!-- !!Main content -->

</section>
<!-- !!!Content -->
@stop

@section('js')
<script src="{{asset('assets/dist/chosen/chosen.jquery.min.js')}}"></script>

<script type="text/javascript">    
    $(document).ready(function () {
        $(".chosen").chosen();
    });
@stop
