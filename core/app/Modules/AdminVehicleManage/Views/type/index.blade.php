@extends('layouts.back_master') @section('title','Admin - Vehicle Type Management')
@section('current_title','Vehicle Type List')

@section('css')
    <link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
    <style type="text/css">
        .box-header, .box-body {
            padding: 20px;
        }
    
        .panel.panel-bordered {
            border: 1px solid #ccc;
        }

        .btn-primary {
            color: white;
            background-color: #C51C6A;
            border-color: #C51C6A;
        }

        .chosen-container {
            font-family: 'FontAwesome', 'Open Sans', sans-serif;
            width: 100% !important;
        }

        b, strong {
            font-weight: bold;
        }

        .top{
            margin-top: 10px;
        }
    </style>
@stop

@section('content')
<!-- Content-->
<section>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Vehicle<small>Management</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
            <li><a href="{{url('admin/vehicle/type/index')}}">Vehicle Type Management</a></li>
            <li class="active">List Vehicle Type</li>
        </ol>
    </section>
    <!-- !!Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">  
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">List Vehicle</h3>
                <button type="button" class="btn btn-primary btn-sm pull-right" onclick="window.location.href='{{url('admin/vehicle/type/create')}}'"><i class="fa fa-plus" style="padding-right: 5px;"></i>Vehicle Type</button>
            </div>
            <div class="box-body">  
                <form role="form" class="form-validation" method="get">
                {!!Form::token()!!}
                    <div class="form-group">
                        <div class="row">                            
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" class="form-control input-sm" name="name" placeholder="" value="{{$args['name']}}">
                                </div>
                            </div>
                            <div class="col-md-4 col-md-offset-8">
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-default btn-sm" id="plan"><i class="fa fa-search" style="padding-right: 16px;width: 28px;"></i>Find</button>
                                    <a href="list" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top"><i class="fa fa-refresh" style="padding-right: 16px;width: 20px;"></i>Refresh</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <table class="table table-bordered bordered table-striped table-condensed" id="orderTable">
                    <thead>
                        <tr>    
                            <th width="5%">#</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Created At</th>
                            <th width="10%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1;?>
                    @if(count($vehicle_type_details) > 0)
                        @foreach($vehicle_type_details as $result_val)
                            @include('AdminVehicleManage::type.template.vehicleType')
                            <?php $i++;?>
                        @endforeach
                    @else
                        <tr><td colspan="12" class="text-center">No data found.</td></tr>
                    @endif
                    </tbody>
                </table>

                @if($vehicle_type_details != null)
                  <div style="float: right;">{!! $vehicle_type_details->render() !!}</div>
                @endif
            </div>
        </div>

    </section>
    <!-- !!Main content -->

</section>
<!-- !!!Content -->
@stop

@section('js')
<script src="{{asset('assets/dist/chosen/chosen.jquery.min.js')}}"></script>

<script type="text/javascript">    
    $(document).ready(function () {
        $(".chosen").chosen();
    });
@stop
