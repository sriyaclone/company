<tr id="{{$i}}">
     <td class="">{{$i}}</td>
     <td>{{$result_val->name}}</td>
     <td>{{$result_val->description}}</td>
     <td>{{$result_val->created_at}}</td>
     <td class="text-center">
        <div class="btn-group">
            <a href="javascript:void(0);" class="btn btn-xs btn-default delete" data-toggle="tooltip" data-placement="top" title="Delete Customer" data-id="{{$result_val['id']}}"><i class="fa fa-trash-o"></i></a>
            <!-- <a href="{{'edit/'.$result_val['id']}}" class="btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="Edit Customer"><i class="fa fa-pencil"></i></a> -->
        </div>
     </td>
</tr>

