@extends('layouts.back_master') @section('title','Add Vehicle Type')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
<style type="text/css">
	.has-error .help-block, .has-error .control-label{
		color:#e41212;
	}
	.has-error .chosen-container{
		border:1px solid #e41212;
	}

	.bottom{
		margin-bottom: 5px;
	}
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Vehicle Type<small>Management</small></h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li><a href="{{url('admin/vehicle/type/index')}}">Vehicle Type Management</a></li>
		<li class="active">Add Vehicle Type</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Add Vehicle Type</h3>
			<button type="button" class="btn btn-primary btn-sm pull-right" onclick="window.location.href='{{url('admin/vehicle/type/index')}}'"><i class="fa fa-bars" style="padding-right: 5px;"></i>List</button>
		</div>
		<br>
		<form action="" class="form-horizontal" method="post">
			<div class="box-body">
				{!!Form::token()!!}
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="row">
						
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<label for="" class="control-label required">Name</label>
		            		<input type="text" class="form-control" name="name" value="{{old('name')}}">
		            		@if($errors->has('name'))
		            		<span class="help-block">{{$errors->first('name')}}</span>
		            		@endif							
						</div>
					</div>

					<div class="row bottom">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<label for="" class="control-label required">Description</label>
		            		<textarea class="form-control" name="description">{{old('description')}}</textarea>
		            		@if($errors->has('description'))
		            		<span class="help-block">{{$errors->first('description')}}</span>
		            		@endif
		            	</div>
					</div>					

				</div>  
		        
			</div><!-- /.box-body -->
			<div class="box-footer">
				<button type="submit" class="btn btn-default pull-right">Save</button>
			</div>
		</form>
	</div><!-- /.box -->
</section><!-- /.content -->

@stop
@section('js')

<script src="{{asset('assets/dist/chosen/chosen.jquery.min.js')}}"></script>

<script type="text/javascript">
$(document).ready(function() {
  $(".chosen").chosen();
});
</script>
@stop
