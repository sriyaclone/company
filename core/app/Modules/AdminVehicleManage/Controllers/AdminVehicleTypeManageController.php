<?php namespace App\Modules\AdminVehicleManage\Controllers;


/**
* Controller class
* @author Sriya <csriyarathne@gmail.com>
* @version 1.0.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Modules\AdminVehicleManage\BusinessLogics\VehicleTypeLogic;
use App\Modules\AdminVehicleManage\Models\VehicleType;

class AdminVehicleTypeManageController extends Controller {

	protected $vehicle_type;

    public function __construct(VehicleTypeLogic $vehicle_type){
        $this->vehicle_type = $vehicle_type;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$args = array(
			'name' 	=> $request->vehicle_type
		);

		$vehicle_type_details = $this->vehicle_type->getAllVehicleType($args);

		return view("AdminVehicleManage::type.index")->with([
			'vehicle_type_details' 	=> $vehicle_type_details,
			'args' 				=> $args
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view("AdminVehicleManage::type.create");
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$args = array(
			'name' 			=> $request->name,
			'description' 	=> $request->description
		);

		$result = $this->vehicle_type->storeVehicleType($args);

		if($result){
			return redirect('admin/vehicle/type/create')->with(['success' => true,
                'success.message' => 'Vehicle type added successfully!',
                'success.title' => 'Well Done!']);
		}else{
			return redirect('admin/vehicle/type/create')->with(['error' => true,
                'success.message' => 'Vehicle type added faild!',
                'success.title' => 'Error!']);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	/**
     * This function is used to search customer
     * @return Response
     */
    public function searchCustomer(Request $request){
        $vehicle_details = $this->customer->searchCustomer($request->get('name'), $request->get('credit_limit'), $request->get('credit_period'));
        
        return view("AdminVehicleManage::index")->with([
        	'vehicle_details' 	=> $vehicle_details,
        	'name' 				=> $request->get('name'),
        	'credit_limit' 		=> $request->get('credit_limit'),
			'credit_period' 	=> $request->get('credit_period')
        ]);
    }

}
