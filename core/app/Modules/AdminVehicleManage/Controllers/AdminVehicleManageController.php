<?php namespace App\Modules\AdminVehicleManage\Controllers;


/**
* Controller class
* @author Sriya <csriyarathne@gmail.com>
* @version 1.0.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Modules\AdminVehicleManage\BusinessLogics\VehicleLogic;
use App\Modules\AdminVehicleManage\Models\VehicleType;
use App\Modules\AdminVehicleManage\Models\Vehicle;
use App\Models\Warehouse;
use App\Models\Department;
use App\Models\FuelType;
use App\Models\Make;
use App\Models\Modal;
use App\Models\Country;
use App\Models\BodyType;
use Core\EmployeeManage\Models\Employee;
use DB;

class AdminVehicleManageController extends Controller {

	protected $vehicle;

    public function __construct(VehicleLogic $vehicle){
        $this->vehicle = $vehicle;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$vehicle_types=VehicleType::all()->lists('name','id')->prepend('-- All --',0);
		$departments=Department::all()->lists('name','id')->prepend('-- All --','');

		$args = array(
			'vehicle_type' 	=> $request->vehicle_type,
			'department_id'	=> $request->department,
			'code' 			=> $request->code,
			'plate_no' 		=> $request->plate_no,
			'chassis_no' 	=> $request->chassis_no,
			'manufacturer' 	=> $request->manufacturer,
			'modal' 		=> $request->modal
		);

		$vehicle_details = $this->vehicle->getAllVehicle($args);

		return view("AdminVehicleManage::vehicle.index")->with([
			'vehicle_details' 	=> $vehicle_details,
			'vehicle_types' 	=> $vehicle_types,
			'args' 				=> $args,
			'departments'		=> $departments
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$vehicle_types=VehicleType::all()->lists('name','name')->prepend('No Types', '');
		$warehouse = Warehouse::all()->lists('name','id')->prepend('No Warehouse', '');
		$departments = Department::all()->lists('name','name')->prepend('No Divisions', '');
		$dual = [
			'0' => 'Single',
			'1' => 'Dual'
		];

		$fuel = FuelType::all()->lists('name','name')->prepend('No Type', '');
		$make = Make::all()->lists('name','name')->prepend('No Make', '');
		$modal = Modal::all()->lists('name','name')->prepend('No Modal', '');
		$country = Country::all()->lists('name','name')->prepend('No Country', '');
		$bodyType = BodyType::all()->lists('name','name')->prepend('No Body', '');
		$driver = Employee::where('employee_type_id',4)
			->select(DB::raw('CONCAT(\'(\',short_code,\') \',first_name,\' \',last_name) as name'),'id')
			->get()
			->lists('name','id')->prepend('No Driver', '');

		return view("AdminVehicleManage::vehicle.create")->with([
			'vehicle_types' 	=> $vehicle_types,
			'warehouses' 		=> $warehouse,	
			'departments' 		=> $departments,
			'dual'				=> $dual,
			'driver'			=> $driver,
			'fuel'				=> $fuel,
			'make'				=> $make,
			'modal'				=> $modal,
			'country'			=> $country,
			'bodyType'			=> $bodyType
		]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{

		$type = null;
		$body = null;
		$fuel = null;
		$make = null;
		$modal = null;
		$country = null;
		$division = null;

		if($request->vehicle_type && $request->vehicle_type != ""){
			$type = VehicleType::firstOrCreate([ 'name' => $request->vehicle_type ]);
		}

		if($request->body_type && $request->body_type != ""){
			$body = BodyType::firstOrCreate([ 'name' => $request->body_type ]);
		}

		if($request->fuel_type && $request->fuel_type != ""){
			$fuel = FuelType::firstOrCreate([ 'name' => $request->fuel_type ]);
		}

		if($request->manufacturer && $request->manufacturer != ""){
			$make = Make::firstOrCreate([ 'name' => $request->manufacturer ]);
		}

		if($request->modal && $request->modal != ""){
			$modal = Modal::firstOrCreate([ 'name' => $request->modal ]);
		}

		if($request->country && $request->country != ""){
			$country = Country::firstOrCreate([ 'name' => $request->country ]);
		}

		if($request->department && $request->department != ""){
			$division = Department::firstOrCreate([ 'name' => $request->department ]);
		}

		$args = array(
			'vehicle_type_id' 	=> (($type) ? $type->id : null),
			'code' 				=> $request->code,
			'plate_no' 			=> $request->plate_no,
			'chassis_no' 		=> $request->chassis_no,
			'manufacturer' 		=> (($make) ? $make->name : null),
			'modal' 			=> (($modal) ? $modal->name : null),
			'color' 			=> $request->color,
			'country' 			=> (($country) ? $country->name : null),
			'fuel_type' 		=> (($fuel) ? $fuel->name : null),
			'body_type' 		=> (($body) ? $body->name : null),
			'department_id' 	=> (($division) ? $division->id : null ),
			'year' 				=> $request->year,
			'is_dual' 			=> $request->dual_tyre,
			'engine_no' 		=> $request->engine_no,
			'cylinder_capacity' => $request->cylinder_capacity,
			'front_tyre_size'	=> $request->front_tyre,
			'rear_tyre_size' 	=> $request->rear_tyre,
			'driver_id' 		=> $request->driver		
		);

		$vehicle = $this->vehicle->storeVehicle($args);

		if($request->warehouse != ''){
			$warehaouse_vehicle = array(
				'warehouse_id'   => $request->warehouse,
				'vehicle_id'     => $vehicle->id,
				'vehicle_type_id'=> $request->vehicle_type
			);

			$result = $this->vehicle->storeWarehouseVehicle($warehaouse_vehicle);
		}

		if($vehicle){
			return redirect('admin/vehicle/create')->with(['success' => true,
                'success.message' => 'Vehicle added successfully!',
                'success.title' => 'Well Done!']);
		}else{
			return redirect('admin/vehicle/create')->with(['error' => true,
                'success.message' => 'Vehicle added faild!',
                'success.title' => 'Error!']);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$vehicle_types=VehicleType::all()->lists('name','name')->prepend('No Types', '');
		$warehouse = Warehouse::all()->lists('name','id')->prepend('No Warehouse', '');
		$departments = Department::all()->lists('name','name')->prepend('No Divisions', '');
		$dual = [
			'0' => 'Single',
			'1' => 'Dual'
		];

		$fuel = FuelType::all()->lists('name','name')->prepend('No Type', '');
		$make = Make::all()->lists('name','name')->prepend('No Make', '');
		$modal = Modal::all()->lists('name','name')->prepend('No Modal', '');
		$country = Country::all()->lists('name','name')->prepend('No Country', '');
		$bodyType = BodyType::all()->lists('name','name')->prepend('No Body', '');
		$driver = Employee::where('employee_type_id',4)
			->select(DB::raw('CONCAT(\'(\',short_code,\') \',first_name,\' \',last_name) as name'),'id')
			->get()
			->lists('name','id')->prepend('No Driver', '');

		$veh = Vehicle::find($id);
		$vehicle_type = VehicleType::find($veh->vehicle_type_id);
		$department = Department::find($veh->department_id);

		return view("AdminVehicleManage::vehicle.edit")->with([
			'vehicle_types' 	=> $vehicle_types,
			'warehouses' 		=> $warehouse,	
			'departments' 		=> $departments,
			'dual'				=> $dual,
			'driver'			=> $driver,
			'fuel'				=> $fuel,
			'make'				=> $make,
			'modal'				=> $modal,
			'country'			=> $country,
			'bodyType'			=> $bodyType,
			'vehicle'			=> $veh,
			'vehicle_type'		=> $vehicle_type,
			'department'		=> $department
		]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$type = null;
		$body = null;
		$fuel = null;
		$make = null;
		$modal = null;
		$country = null;
		$division = null;

		if($request->vehicle_type && $request->vehicle_type != ""){
			$type = VehicleType::firstOrCreate([ 'name' => $request->vehicle_type ]);
		}

		if($request->body_type && $request->body_type != ""){
			$body = BodyType::firstOrCreate([ 'name' => $request->body_type ]);
		}

		if($request->fuel_type && $request->fuel_type != ""){
			$fuel = FuelType::firstOrCreate([ 'name' => $request->fuel_type ]);
		}

		if($request->manufacturer && $request->manufacturer != ""){
			$make = Make::firstOrCreate([ 'name' => $request->manufacturer ]);
		}

		if($request->modal && $request->modal != ""){
			$modal = Modal::firstOrCreate([ 'name' => $request->modal ]);
		}

		if($request->country && $request->country != ""){
			$country = Country::firstOrCreate([ 'name' => $request->country ]);
		}

		if($request->department && $request->department != ""){
			$division = Department::firstOrCreate([ 'name' => $request->department ]);
		}

		$vehicle = Vehicle::find($id);

		$vehicle->vehicle_type_id 	= (($type) ? $type->id : null);
		$vehicle->code				= $request->code;
		$vehicle->plate_no 			= $request->plate_no;
		$vehicle->chassis_no 		= $request->chassis_no;
		$vehicle->manufacturer 		= (($make) ? $make->name : null);
		$vehicle->modal 			= (($modal) ? $modal->name : null);
		$vehicle->color 			= $request->color;
		$vehicle->country 			= (($country) ? $country->name : null);
		$vehicle->fuel_type 		= (($fuel) ? $fuel->name : null);
		$vehicle->body_type 		= (($body) ? $body->name : null);
		$vehicle->department_id 	= (($division) ? $division->id : null );
		$vehicle->year 				= $request->year;
		$vehicle->is_dual 			= $request->dual_tyre;
		$vehicle->engine_no 		= $request->engine_no;
		$vehicle->cylinder_capacity = $request->cylinder_capacity;
		$vehicle->front_tyre_size	= $request->front_tyre;
		$vehicle->rear_tyre_size 	= $request->rear_tyre;
		$vehicle->driver_id 		= $request->driver;

		if($vehicle->save()){
			return redirect('admin/vehicle/index')->with(['success' => true,
                'success.message' => 'Vehicle updated successfully!',
                'success.title' => 'Well Done!']);
		}else{
			return redirect('admin/vehicle/edit',$id)->with(['error' => true,
                'success.message' => 'Vehicle update faild!',
                'success.title' => 'Error!']);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	/**
     * This function is used to search customer
     * @return Response
     */
    public function searchCustomer(Request $request){
        $vehicle_details = $this->customer->searchCustomer($request->get('name'), $request->get('credit_limit'), $request->get('credit_period'));
        
        return view("AdminVehicleManage::index")->with([
        	'vehicle_details' 	=> $vehicle_details,
        	'name' 				=> $request->get('name'),
        	'credit_limit' 		=> $request->get('credit_limit'),
			'credit_period' 	=> $request->get('credit_period')
        ]);
    }

}
