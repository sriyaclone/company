<?php


Route::group(['middleware' => ['auth']], function()
{	
	Route::group(['prefix'=>'admin/vehicle/','namespace' => 'App\Modules\AdminVehicleManage\Controllers'],function() {

        Route::get('index', [
            'as' => 'vehicle.index', 'uses' => 'AdminVehicleManageController@index'
        ]);

        Route::get('create', [
            'as' => 'vehicle.create', 'uses' => 'AdminVehicleManageController@create'
        ]);

        Route::get('edit/{id}', [
            'as' => 'vehicle.create', 'uses' => 'AdminVehicleManageController@edit'
        ]);

        Route::post('create', [
            'as' => 'vehicle.create', 'uses' => 'AdminVehicleManageController@store'
        ]);

        Route::post('edit/{id}', [
            'as' => 'vehicle.create', 'uses' => 'AdminVehicleManageController@update'
        ]);

    });

    Route::group(['prefix'=>'admin/vehicle/type/','namespace' => 'App\Modules\AdminVehicleManage\Controllers'],function() {

        Route::get('index', [
            'as' => 'vehicle.type.index', 'uses' => 'AdminVehicleTypeManageController@index'
        ]);

        Route::get('create', [
            'as' => 'vehicle.type.create', 'uses' => 'AdminVehicleTypeManageController@create'
        ]);

        Route::post('create', [
            'as' => 'vehicle.type.create', 'uses' => 'AdminVehicleTypeManageController@store'
        ]);

	});    
});