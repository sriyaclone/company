<?php namespace App\Modules\AdminVehicleManage\BusinessLogics;

/**
* Business Logics 
* Define all the busines logics in here
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use Illuminate\Database\Eloquent\Model;
use App\Modules\AdminVehicleManage\Models\Vehicle;
use App\Models\WarehouseVehicle;
use App\Models\Department;
use DB;

class vehicleLogic {

	private $vehicle;

	public function __construct(Vehicle $vehicle){
		$this->vehicle = $vehicle;
	}

	public function getVehicleById($id){
		$vehicle = Vehicle::with(['vehicleType'])->find($id);

		if(!$vehicle){
			throw new \Exception('Vehicle not found for ID:'.$id);
		}

		return $vehicle;
	}

	/**
     * This function is used to get all vehicle details
     * @parm -
     * @return vehicle object
     */
    public function getAllVehicle($param){

        $data=Vehicle::whereNull('deleted_at')->with(['vehicleType','vehicle_warehouse']);

        if($param['vehicle_type']!=null && $param['vehicle_type']!="" && $param['vehicle_type']!="0"){
            $data = $data->where('vehicle_type_id', '=', $param['vehicle_type']);
        }

        if($param['code']!=null && $param['code']!=""){
            $data = $data->where('code', 'like', '%' .$param['code']. '%');
        }

        if($param['department_id']!=null && $param['department_id']!=""){
            $data = $data->where('department_id', $param['department_id']);
        }

        if($param['plate_no']!=null && $param['plate_no']!=""){
            $data = $data->where('plate_no', 'like', '%' .$param['plate_no']. '%');
        }

        if($param['chassis_no']!=null && $param['chassis_no']!=""){
            $data = $data->where('chassis_no', 'like', '%' .$param['chassis_no']. '%');
        }

        if($param['manufacturer']!=null && $param['manufacturer']!=""){
            $data = $data->where('manufacturer', 'like', '%' .$param['manufacturer']. '%');
        }

        if($param['modal']!=null && $param['modal']!=""){
            $data = $data->where('modal', 'like', '%' .$param['modal']. '%');
        }
        // return $data->get();
        return $data->paginate(25);
    }

    /**
     * This function is used to insert vehicle
     * @param 
     * @response result
     */
    public function storeVehicle($param) {
            
        return $res=Vehicle::create($param);

    }

    /**
     * This function is used to insert warehouse's vehicle
     * @param 
     * @response result
     */
    public function storeWarehouseVehicle($param) {
            
        return $res=WarehouseVehicle::create($param);

    }

    /**
     * This function is used to search customer
     * @param integer $name, $code
     * @response product category object
     */
    public function searchCustomer($name, $limit, $period) {
        $search = Customer::select('*',DB::raw("CONCAT(`first_name`, ' ', `last_name`) as name"));
        if($name !== ''){
            $search = $search->where(DB::raw('CONCAT(first_name," ",last_name)'), 'LIKE', "%" . $name . "%");
        }
        if($limit !== ''){
            $search = $search->where('credit_limit', '=', $limit);
        }
        if($period !== ''){
            $search = $search->where('credit_period', '=', $period);
        }
        $search->orderBy('created_at','desc');
        return $search->paginate(10);
    }

}
