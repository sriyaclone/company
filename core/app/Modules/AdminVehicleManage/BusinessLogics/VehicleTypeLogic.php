<?php namespace App\Modules\AdminVehicleManage\BusinessLogics;

/**
* Business Logics 
* Define all the busines logics in here
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use Illuminate\Database\Eloquent\Model;
use App\Modules\AdminVehicleManage\Models\VehicleType;
use DB;

class VehicleTypeLogic {

	private $vehicle_type;

	public function __construct(VehicleType $vehicle_type){
		$this->vehicle_type = $vehicle_type;
	}

	public function getVehicleTypeById($id){
		$vehicle_type = VehicleType::find($id);

		if(!$vehicle_type){
			throw new \Exception('Vehicle Type not found for ID:'.$id);
		}

		return $vehicle_type;
	}

	/**
     * This function is used to get all vehicle details
     * @parm -
     * @return vehicle object
     */
    public function getAllVehicleType($param){

        $data=VehicleType::whereNull('deleted_at');

        if($param['name']!=null && $param['name']!=""){
            $data = $data->where('name', 'like', '%' .$param['name']. '%');
        }

        return $data->paginate(10);
    }

    /**
     * This function is used to insert vehicle
     * @param 
     * @response result
     */
    public function storeVehicleType($param) {
            
        return $res=VehicleType::create($param);

    }

    /**
     * This function is used to search customer
     * @param integer $name, $code
     * @response product category object
     */
    public function searchCustomer($name, $limit, $period) {
        $search = Customer::select('*',DB::raw("CONCAT(`first_name`, ' ', `last_name`) as name"));
        if($name !== ''){
            $search = $search->where(DB::raw('CONCAT(first_name," ",last_name)'), 'LIKE', "%" . $name . "%");
        }
        if($limit !== ''){
            $search = $search->where('credit_limit', '=', $limit);
        }
        if($period !== ''){
            $search = $search->where('credit_period', '=', $period);
        }
        $search->orderBy('created_at','desc');
        return $search->paginate(10);
    }

}
