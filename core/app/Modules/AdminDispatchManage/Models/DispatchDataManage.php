<?php namespace App\Modules\AdminDispatchManage\Models;

/**
*
* Model
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Baum\Node;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use DB;

class DispatchDataManage extends Model
{
    /**
     * table row delete
     */
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dispatch_data';

    protected $fillable = ['dispatch_note_id', 'vehicle_id', 'bayin_time', 'checkin_time' ,'checkout_time', 'out_mileage', 'in_mileage', 'timestamp', 'created_at'];

}
