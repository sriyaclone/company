<?php namespace App\Modules\AdminDispatchManage\Models;

/**
*
* Model
* @author Sriya <csriyarathne@gmail.com>
* @version 1.0.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Baum\Node;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use DB;

class DispatchDetailManage extends Model
{
    /**
     * table row delete
     */
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dispatch_details';

    protected $guarded=['id'];

    public function dispatchNote()
    {
        return $this->belongsTo('App\Modules\AdminDispatchManage\Models\DispatchDetailManage','dispatch_note_id','id');
    }

    public function invoice()
    {
        return $this->belongsTo('App\Modules\AdminInvoiceManage\Models\Invoice','invoice_id','id');
    }
}
