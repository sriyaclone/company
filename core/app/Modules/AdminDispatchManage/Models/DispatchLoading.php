<?php namespace App\Modules\AdminDispatchManage\Models;

use Illuminate\Database\Eloquent\Model;

class DispatchLoading extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dispatch_loading';

    protected $fillable = [
        'dispatch_note_id',
        'vehicle_id',
        'courier_id',
        'loader_id',
        'started_time',
        'finished_time',
        'status',
        'timestamp'
    ];

    public function dispatch(){
        return $this->belongsTo('App\Modules\AdminDispatchManage\Models\DispatchManage', 'dispatch_note_id', 'id');
    }

    public function picking_invoices(){
        return $this->belongsTo('App\Modules\AdminDispatchManage\Models\PickingInvoices', 'picking_id', 'id');
    }

}
