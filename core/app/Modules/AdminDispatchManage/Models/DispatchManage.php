<?php namespace App\Modules\AdminDispatchManage\Models;

/**
*
* Model
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Baum\Node;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use DB;

class DispatchManage extends Model
{
    /**
     * table row delete
     */
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dispatch_note';

    protected $guarded=['id'];

    public function driver()
    {
        return $this->belongsTo('Core\EmployeeManage\Models\Employee','driver_id','id');
    }

    public function helper()
    {
        return $this->belongsTo('Core\EmployeeManage\Models\Employee','helper_id','id');
    }

    public function vehicle()
    {
        return $this->belongsTo('App\Modules\AdminVehicleManage\Models\Vehicle','vehicle_id','id');
    }

    public function details()
    {
        return $this->hasMany('App\Modules\AdminDispatchManage\Models\DispatchDetailManage','dispatch_note_id','id');
    }

    public function warehouse()
    {
        return $this->belongsTo('App\Models\Warehouse','warehouse_id','id');
    }

    public function times()
    {
        return $this->hasMany('App\Modules\AdminDispatchManage\Models\DispatchDataManage','dispatch_note_id','id');
    }

    public function bay()
    {
        return $this->belongsTo('App\Models\Bay','bay_id','id');
    }

    public function courier()
    {
        return $this->belongsTo('App\Models\Courier','courier_id','id');
    }

    public function route()
    {
        return $this->belongsTo('App\Models\Location','route_id','id');
    }

    public function actionBy()
    {
        return $this->belongsTo('Core\EmployeeManage\Models\Employee','created_by','id');
    }

    public function dispatch()
    {
        return $this->hasOne($this,'id');
    }
}
