<?php
namespace App\Modules\DispatchManage\Requests;

use App\Http\Requests\Request;

class DispatchRequest extends Request {

	public function authorize(){
		return true;
	}

	public function rules(){
		$courier_status = $_REQUEST['courier_status'];
		$inv_count = $_REQUEST['invoices'];
		// if($courier_status == ""){
			if($inv_count == "1"){
				$rules = [
					'warehouse'			=> 'required',
					'bay'               => 'required',
					'vehicle_type'      => 'required',
					'vehicle'           => 'required',
					'driver'            => 'required',
					'helper'            => 'required',
					'location'          => 'required'
				];
			}elseif ($inv_count == "") {
				$rules = [
					'warehouse'			=> 'required',
					'bay'               => 'required',
					'vehicle_type'      => 'required',
					'vehicle'           => 'required',
					'driver'            => 'required',
					'helper'            => 'required',
					'location'          => 'required',
					'invoices'          => 'required'
				];
			}

		// }elseif ($courier_status == "1") {
		// 	$rules = [
		// 		'warehouse'			=> 'required',
		// 		'bay'               => 'required',
		// 		'courier'           => 'required',
		// 		'location'          => 'required'
		// 	];
		// }
	
		return $rules;
	}

}
