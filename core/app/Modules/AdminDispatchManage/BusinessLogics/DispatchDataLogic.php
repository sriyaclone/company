<?php namespace App\Modules\AdminDispatchManage\BusinessLogics;


/**
 * Business Logics
 * Define all the busines logics in here
 * @author Nilusha <thnsdesilva@gmail.com>
 * @version x.x.x
 * @copyright Copyright (c) 2017, OITS.Dev+
 *
 */
use Illuminate\Database\Eloquent\Model;
use App\Modules\AdminDispatchManage\Models\DispatchDataManage;
use DB;
use phpDocumentor\Reflection\Types\Null_;

class DispatchDataLogic {

    private $dispatchDataManage;

    public function __construct(DispatchDataManage $dispatchDataManage){
        $this->dispatchDataManage = $dispatchDataManage;
    }

    /**
     * This function is used to get all Vehicle details
     * @parm -
     * @return Vehicle object
     */
    public function getAllDispatcheData(){
        return DispatchDataManage::paginate(10);
    }

    /**
     * Get BayIn Dispatch Data
     * @parm -
     * @return Vehicle object
     */

    public function getDispatchDataByVehicleId($id){
        $dispatch = DispatchDataManage::where('vehicle_id',$id)->get();

        if(!$dispatch){
            throw new \Exception('Dispatch not found for Vehicle ID:'.$id);
        }

        return $dispatch;
    }


    public function getCheckedOutDataByVehicle($vehicle_id){
        $dispatch = DispatchDataManage::where('vehicle_id',$vehicle_id)
                        ->whereNull('checkin_time')
                        ->first();

        return $dispatch;
    }

    /**
     * This function is used to get all Vehicle details
     * @parm -
     * @return Vehicle object
     */
    public function getDispatchDataById($id){
        return DispatchDataManage::find($id);
    }

    public function add($data,$dispatch_id,$vehicle_id){

        DB::beginTransaction();

        if($data->action == "checkout"){
            DispatchDataManage::create([
                'dispatch_note_id' => $dispatch_id,
                'vehicle_id' => $vehicle_id,
                'checkout_time' =>  date('Y-m-d h:i:s'),
                'checkin_time' =>  null,
                'bayin_time' =>  null,
                'timestamp' => date('Y-m-d h:i:s')
            ]);
        }else if($data->action == "checkin"){
            DB::table('dms_dispatch_data')
                ->where('dispatch_note_id', $dispatch_id)
                ->update(['checkin_time' => date('Y-m-d h:i:s')]);
        }else if($data->action == "bayin"){
            DB::table('dms_dispatch_data')
                ->where('dispatch_note_id', $dispatch_id)
                ->update(['bayin_time' => date('Y-m-d h:i:s')]);
        }
        DB::commit();
    }

}
