<?php namespace App\Modules\AdminDispatchManage\BusinessLogics;


/**
 * Business Logics
 * Define all the busines logics in here
 * @author Nilusha <thnsdesilva@gmail.com>
 * @version x.x.x
 * @copyright Copyright (c) 2017, OITS.Dev+
 *
 */
use Illuminate\Database\Eloquent\Model;
use App\Modules\AdminDispatchManage\Models\DispatchManage;

use App\Classes\Contracts\BaseStockHandler;
use DB;

class DispatchLogic{

    private $dispatchManage;

    public function __construct(DispatchManage $dispatchManage){
        $this->dispatchManage = $dispatchManage;
    }

    /**
     * This function is used to get all Vehicle details
     * @parm -
     * @return Vehicle object
     */
    public function getAllDispatches(){
        return DispatchManage::paginate(10);
    }

    /**
     * Get BayIn Dispatch Data
     * @parm -
     * @return Vehicle object
     */

    public function getDispatchByVehicleId($id){
        $dispatch = DispatchManage::where('vehicle_id',$id)->whereIn('status', [DISPATCH_BAYIN,DISPATCH_LOADED,DISPATCH_CHECKOUT])->get();

        if(!$dispatch){
            throw new \Exception('Dispatch not found for Vehicle ID:'.$id);
        }

        return $dispatch;
    }

    /**
     * Get Pending Dispatch Data
     * @parm -
     * @return Vehicle object
     */

    public function getPendingDispatchByDispatchId($id){
        $dispatch = DispatchManage::where('vehicle_id',$id)->where('status','=',DISPATCH_PICKED)->get();

        if(!$dispatch){
            throw new \Exception('Dispatch not found for Vehicle ID:'.$id);
        }

        return $dispatch;
    }

    /**
     * This function is used to get  Vehicle details By Vehicle No
     * @parm -
     * @return Vehicle object
     */

    public function getDispatchByDispatchId($id){
        $dispatch = DispatchManage::find($id);

        if(!$dispatch){
            throw new \Exception('Dispatch not found for Dispatch ID:'.$id);
        }

        return $dispatch;
    }

    /**
     * This function is used to get  Vehicle details By Vehicle No
     * @parm -
     * @return Vehicle object
     */

    public function getDispatchByDispatchNo($dispatch_no){
        $dispatch = DispatchManage::where('dispatch_no',$dispatch_no)->get();

        if(!$dispatch){
            throw new \Exception('Dispatch not found for Dispatch No:'.$dispatch_no);
        }

        return $dispatch;
    }

    public function getDispatchByNo($dispatch_no){
        $dispatch = DispatchManage::with(['driver','helper'])
                        ->where('dispatch_no',$dispatch_no)
                        ->first();

        if(!$dispatch){
            throw new \Exception('Dispatch not found for Dispatch No:'.$dispatch_no);
        }

        return $dispatch;
    }

    public function loadedDispatchByNo($dispatch_no){
        $dispatch = DispatchManage::where('dispatch_no',$dispatch_no)
                            ->where('status',DISPATCH_LOADED)->first();

        if(!$dispatch){
            throw new \Exception('Dispatch not found for Dispatch No:'.$dispatch_no);
        }

        return $dispatch;
    }

    public function loadedDispatchByVehicleId($vehicle_id){
        $dispatch = DispatchManage::with(['driver','helper'])->where('vehicle_id', $vehicle_id)
                        ->where('status', DISPATCH_LOADED)
                        ->orderBy('dispatch_date','ASC')
                        ->first();

        return ($dispatch)?$dispatch:null;
    }

    public function checkedOutDispatchByVehicleId($vehicle_id){
        $dispatch = DispatchManage::with(['driver','helper'])->where('vehicle_id', $vehicle_id)
                        ->where('status', DISPATCH_CHECKOUT)
                        ->first();

        return ($dispatch)?$dispatch:null;
    }

    public function add($data,$dispatch_no){
        DB::beginTransaction();
        $status = 0;
        if($data->action == "checkout"){
            $status = DISPATCH_CHECKOUT;
        }else if($data->action == "checkin"){
            $status = DISPATCH_CHECKIN;
        }else if($data->action == "bayin"){
            $status = DISPATCH_BAYIN;
        }

        $dispatch=  DB::table('dms_dispatch_note')
            ->where('dispatch_no', $dispatch_no)
            ->update(['status' => $status]);
        if(!$dispatch){
            throw new \Exception('Record wasn\'t updated');
        }
        DB::commit();
    }

}
