<?php namespace App\Modules\AdminDispatchManage\BusinessLogics;


/**
 * Business Logics
 * Define all the busines logics in here
 * @author Nilusha <thnsdesilva@gmail.com>
 * @version x.x.x
 * @copyright Copyright (c) 2017, OITS.Dev+
 *
 */
use Illuminate\Database\Eloquent\Model;
use App\Modules\AdminDispatchManage\Models\DispatchManage;
use DB;

class DispatchLogic{

    private $dispatchManage;

    public function __construct(DispatchManage $dispatchManage){
        $this->dispatchManage = $dispatchManage;
    }

    /**
     * This function is used to get all Vehicle details
     * @parm -
     * @return Vehicle object
     */
    public function getAllDispatches(){
        return DispatchManage::paginate(10);
    }

    /**
     * Get BayIn Dispatch Data
     * @parm -
     * @return Vehicle object
     */

    public function getDispatchByVehicleId($id){
        $dispatch = DispatchManage::where('vehicle_id',$id)->get();

        if(!$dispatch){
            throw new \Exception('Dispatch not found for Vehicle ID:'.$id);
        }

        return $dispatch;
    }

    /**
     * This function is used to get  Vehicle details By Vehicle No
     * @parm -
     * @return Vehicle object
     */

    public function getDispatchByDispatchId($id){
        $dispatch = DispatchManage::find($id);

        if(!$dispatch){
            throw new \Exception('Dispatch not found for Dispatch ID:'.$id);
        }

        return $dispatch;
    }





}
