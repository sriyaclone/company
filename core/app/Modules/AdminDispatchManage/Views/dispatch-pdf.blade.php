<style type="text/css">
    @page {
        size: 7in 8in;
        margin: 1in 16mm 27mm 16mm;
    }

    table{
        font-size: 8px;
    }
</style>
@if(count($dispatch_details) > 0)

    <table>        
        <tr>
            <td style="vertical-align: bottom;"></td>
            <td style="vertical-align: bottom;"></td>
            <td style="vertical-align: bottom;font-size: 10px" width="100%">
                <strong>Dispatch NO: {{ $dispatch_details->dispatch_no }}</strong>
            </td>    
        </tr>
    </table>
    <table>
        <tr>
            <td style="vertical-align: bottom;">
                <strong style="font-size: 24px;color: #fd4f00">Dispatch Note</strong>
            </td>
            <td style="vertical-align: bottom;"></td>
            <td style="vertical-align: bottom;">
                <?php echo '<img src="data:image/png;base64,' . base64_encode($barcode->getBarcode($dispatch_details->dispatch_no, $barcode::TYPE_CODE_128)) . '" style="width: 150px;height: 30px">'; ?>
            </td>
        </tr>
    </table>
    

    <table style="border-collapse: collapse;">
        <tr><td colspan="7" style="line-height: 11px;"></td></tr>
        <tr><td colspan="7" style="line-height: 11px;"></td></tr>
        <tr>
            <td colspan="7" style="line-height: 11px;"><strong style="font-size: 12px">Dispatch Details</strong></td>
        </tr> 
        
        <tr>
            <td colspan="7" style="border-top: 1px solid #000;line-height: 11px;"></td>
        </tr>
        
        <tr>
            <td style="line-height:14px;font-size:8px;"><strong>Dispatch Date</strong></td>
            <td style="line-height:14px;font-size:8px;"><strong>Warehouse</strong></td>
            <td style="line-height:14px;font-size:8px;"><strong>Loading Bay</strong></td>
            <td style="line-height:14px;font-size:8px;"><strong>Route</strong></td>
            <td style="line-height:14px;font-size:8px;"><strong>Vehicle</strong></td>
            <td style="line-height:14px;font-size:8px;"><strong>Driver</strong></td>
            <td style="line-height:14px;font-size:8px;"><strong>Helper</strong></td>
        </tr>

        <tr>
            <td colspan="7" style="line-height: 5px;">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="7" style="line-height: 5px;">&nbsp;</td>
        </tr>
        
        <tr>
            <td style="line-height:15px;font-size:8px;border-top: 1px solid #ddd;text-align: left">
                {{$dispatch_details->dispatch_date}}
            </td>
            <td style="line-height:15px;font-size:8px;border-top: 1px solid #ddd;">
                @if(sizeof($dispatch_details->bay) > 0)
                    {{$dispatch_details->bay->warehouse->name}}
                @endif
            </td>
            <td style="line-height:15px;font-size:8px;border-top: 1px solid #ddd;">
                @if(sizeof($dispatch_details->bay) > 0)
                {{$dispatch_details->bay->name}}
                @endif
            </td>
            <td style="line-height:15px;font-size:8px;border-top: 1px solid #ddd;">
                {{$dispatch_details->route->name}}
            </td>
            @if($dispatch_details->type==COURIERDELIVERY)
                <td style="line-height:15px;font-size:8px;border-top: 1px solid #ddd;">
                    {{$dispatch_details->courier['name']}}<br>
                    <span class="label label-pink" style="">{{$dispatch_details->courier['code']}}</span>
                </td>
            @endif

            @if($dispatch_details->type==COMPANYDELIVERY)
                <td style="line-height:15px;font-size:8px;border-top: 1px solid #ddd;">
                    {{$dispatch_details->vehicle['code']}}
                </td>
                <td style="line-height:15px;font-size:8px;border-top: 1px solid #ddd;">
                    {{ $dispatch_details->driver['full_name']}}<br>
                </td>
                <td style="line-height:15px;font-size:8px;border-top: 1px solid #ddd;">
                    {{$dispatch_details->helper['full_name']}}
                </td>
            @endif
        </tr>
        
    </table>

    <table style="border-collapse: collapse;">
        <tr><td colspan="7" style="line-height: 11px;"></td></tr>
        <tr>
            <td colspan="7" style="line-height: 11px;"><strong style="font-size: 12px">Dispatched Invoices</strong></td>
        </tr> 
        
        <tr>
            <td colspan="7" style="border-top: 1px solid #000;line-height: 11px;"></td>
        </tr>
        
        <tr>
            <td style="line-height:14px;font-size:8px;" width="20%"><strong>Invoice No</strong></td>
            <td style="line-height:14px;font-size:8px;" width="20%"><strong>Invoice Date</strong></td>
            <td style="line-height:14px;font-size:8px;" width="40%"><strong>Customer</strong></td>
            <td style="line-height:14px;font-size:8px;" width="20%"><strong>Delivery Date</strong></td>
            <!-- <td style="line-height:14px;font-size:8px;" width="20%"><strong>Amount</strong></td> -->
        </tr>

        <tr>
            <td colspan="7" style="line-height: 5px;">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="7" style="line-height: 5px;">&nbsp;</td>
        </tr>

        @if(count($dispatch_details->details) > 0)
            <?php $j=1;?>
            @foreach($dispatch_details->details as $result_val)
                <tr>
                    <td style="line-height:15px;font-size:8px;border-top: 1px solid #ddd;">
                        {{$j}} - {{$result_val->invoice->invoice_no}}
                    </td>
                    <td style="line-height:15px;font-size:8px;border-top: 1px solid #ddd;">
                        {{$result_val->invoice->invoice_date}}
                    </td>
                    <td style="line-height:15px;font-size:8px;border-top: 1px solid #ddd;">
                        {{$result_val->invoice->customer->code}}  {{$result_val->invoice->customer->first_name}} {{$result_val->invoice->customer->last_name}}
                    </td>
                    <td style="line-height:15px;font-size:8px;border-top: 1px solid #ddd;">
                        {{$result_val->invoice->deliver_date}}
                    </td>
                    <!-- <td style="line-height:15px;font-size:8px;border-top: 1px solid #ddd;">
                        {{number_format($result_val->invoice->total_amount, 2)}}
                    </td> -->
                </tr>
                <?php $j++; ?>
            @endforeach
        @else
            <tr><td colspan="12" class="text-center">No data found.</td></tr>
        @endif
        
        <tr>
            <td colspan="7" style="line-height: 5px;">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="7" style="line-height: 5px;">&nbsp;</td>
        </tr><tr>
            <td colspan="7" style="line-height: 5px;">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="7" style="line-height: 5px;">&nbsp;</td>
        </tr><tr>
            <td colspan="7" style="line-height: 5px;">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="7" style="line-height: 5px;">&nbsp;</td>
        </tr>
        
    </table>    
    
@endif