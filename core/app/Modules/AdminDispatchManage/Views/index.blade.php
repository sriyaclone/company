@extends('layouts.back_master') @section('title','Admin - Dispatch Management')
@section('current_title','Dispatch List')

@section('css')
    <style type="text/css">
    
        .panel.panel-bordered {
            border: 1px solid #ccc;
        }

        .btn-primary {
            color: white;
            background-color: #C51C6A;
            border-color: #C51C6A;
        }

        .chosen-container {
            font-family: 'FontAwesome', 'Open Sans', sans-serif;
            width: 100% !important;
        }

        b, strong {
            font-weight: bold;
        }

        .top{
            margin-top: 10px;
        }
    </style>
@stop

@section('content')
<!-- Content-->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Dispatch Management</h2>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{url('admin/dispatch/list')}}">Dispatch Management</a></li>
            <li class="active">Dispatch List</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a class="btn btn-warning" href="{{ url('admin/dispatch/create') }}"><i class="fa fa-plus" aria-hidden="true"></i> Dispatch</a>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Dispatch List</h5>
            <div class="ibox-tools">
                
            </div>
        </div>

        <div class="ibox-content" style="display: block;">
            <table class="table table-bordered " id="orderTable">
                <thead>
                    <tr>    
                        <th style="text-align: center">#</th>
                        <th style="text-align: left">Dispatch Note</th>
                        <th style="text-align: center">Dispatch Date</th>
                        <th style="text-align: center">Deliver Type</th>
                        <th style="text-align: center">Route</th>
                        <th style="text-align: center">Status</th>
                        <th style="text-align: center">Created At</th>
                        <th colspan="3" style="text-align: center">Action</th>
                    </tr>
                </thead>
                <tbody>
                <?php $i = 1;?>
                @if(count($dispatch_list) > 0)
                    @foreach($dispatch_list as $result_val)
                        @include('AdminDispatchManage::template.dispatch')
                        <?php $i++;?>
                    @endforeach
                @else
                    <tr><td colspan="12" class="text-center">No data found.</td></tr>
                @endif
                </tbody>
            </table>
            @if(count($dispatch_list) > 0)
                <div class="row">
                    <div class="col-sm-12 text-left">
                         Row {{$dispatch_list->count()}} of {{$dispatch_list->total()}} 
                    </div>
                    <div class="col-sm-12 text-right">
                         {!! $dispatch_list->render() !!}
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
<!-- !!!Content -->
@stop

@section('js')
<script type="text/javascript">    
    $(document).ready(function () {
        $(".chosen").chosen();
    });
</script>
@stop