<style type="text/css">
    @page {
       size: 7in 8in;
       margin: 1in 16mm 27mm 16mm;
    }

    table{
        font-size: 8px;
    }
</style>
<table style="border-collapse: collapse;">
<tr>
    <td rowspan="4" style="border-bottom: 1px solid #000;vertical-align: bottom" width="64%">
        <br/><br/><br/><br/>Sales Person: asdsaddasd</td>
    <td rowspan="4" width="10%" style="vertical-align: top;border-bottom: 1px solid #000;"></td>
    <td width="12%">Proforma #</td>
    <td width="2%">:</td>
    <td width="12%">#TFT#%W^</td>
</tr>
<tr>
    <td width="12%">Proforma Date</td>
    <td width="2%">:</td>
    <td width="12%"></td>
</tr>
<tr>
    <td width="12%">Order Ref.#</td>
    <td width="2%">:</td>
    <td width="12%"></td>
</tr>
<tr>
    <td style="border-bottom: 1px solid #000;" width="12%">Order Date</td>
    <td style="border-bottom: 1px solid #000;" width="2%">:</td>
    <td style="border-bottom: 1px solid #000;" width="12%"></td>
</tr>
</table>
<table style="border-collapse: collapse;">
<tr>
    <td width="100%"></td>
</tr>
<tr>
    <td width="28%" style="line-height: 14px;"><strong>BILL TO</strong></td>
    <td width="28%" style="line-height: 14px;"><strong>SHIP TO</strong></td>
    <td width="44%" colspan="3" style="line-height: 14px;"><strong>TERMS & NOTES</strong></td>
</tr>
<tr>
    <td width="28%" style="font-size:8px;line-height:11px;">
        asdsad
    </td>
    <td width="28%" style="font-size:8px;line-height:11px;">
        asdsadsa
    </td>
    <td style="font-size: 8px;line-height:11px;" width="12%">Payment Terms</td>
    <td style="font-size: 8px;line-height:11px;" width="2%">:</td>
    <td style="font-size: 8px;line-height:11px;" width="30%">Within asdasdasd Days</td>
</tr>
<tr>
    <td width="28%" style="font-size:8px;line-height:11px;">
        asdasdd
        
    </td>
    <td width="28%" style="font-size:8px;line-height:11px;">
       asdsadaasd
    </td>
    <td style="font-size: 8px;line-height:11px;" width="12%">Price</td>
    <td style="font-size: 8px;line-height:11px;" width="2%">:</td>
    <td style="font-size: 8px;line-height:11px;" width="30%">All inclusive</td>
</tr>
<tr>
    <td width="28%" style="font-size:8px;line-height:11px;">
        sASa
    </td>
    <td width="28%" style="font-size:8px;line-height:11px;">
        DASDASDDSA
    </td>
    <td style="font-size: 8px;line-height:11px;" width="12%">Price Validity</td>
    <td style="font-size: 8px;line-height:11px;" width="2%">:</td>
    <td style="font-size: 8px;line-height:11px;" width="30%">14 days</td>
</tr>
<tr>
    <td width="28%" style="font-size:8px;line-height:11px;">SDSADASDSADDSADA
    </td>
    <td width="28%" style="font-size:8px;line-height:11px;">ASDSADSAADSAD
    </td>
    <td style="font-size: 8px;line-height:11px;" width="12%">Delivery</td>
    <td style="font-size: 8px;line-height:11px;" width="2%">:</td>
    <td style="font-size: 8px;line-height:11px;" width="30%">ASDASDADSAD</td>
</tr>
</table>
<table style="border-collapse: collapse;">
<tr>
    <td colspan="7" style="line-height: 11px;"></td>
</tr>   
<tr>
    <td colspan="7" style="border-top: 1px solid #000;line-height: 11px;"></td>
</tr>
<tr>
    <td style="line-height:14px;font-size:8px;" width="5%"><strong>NO</strong></td>
    <td style="line-height:14px;font-size:8px;" width="15%"><strong>ITEM</strong></td>
    <td style="line-height:14px;font-size:8px;" width="35%"><strong>DESCRIPTION</strong></td>
    <td style="line-height:14px;font-size:8px;" width="10%"><strong>QTY</strong></td>
    <td style="line-height:14px;font-size:8px;" width="7%"><strong>UNIT</strong></td>
    <td style="line-height:14px;font-size:8px;text-align: right;" width="10%"><strong>PRICE</strong></td>
    <td style="line-height:14px;font-size:8px;text-align: right;" width="18%"><strong>AMOUNT</strong></td>
</tr>
<tr>
    <td colspan="7" style="line-height: 5px;">&nbsp;</td>
</tr>

<tr>
    <td style="line-height:15px;font-size:8px;border-bottom: 1px solid #ddd;" 
        width="5%">asdsada</td>
    <td style="line-height:15px;font-size:8px;border-bottom: 1px solid #ddd;" 
        width="15%">asdad</td>
    <td style="line-height:15px;font-size:8px;border-bottom: 1px solid #ddd;" 
        width="35%">sadasd</td>
    <td style="line-height:15px;font-size:8px;border-bottom: 1px solid #ddd;" 
        width="10%">asdasdad</td>
    <td style="line-height:15px;font-size:8px;border-bottom: 1px solid #ddd;" 
        width="7%">asdasdsadsad</td>
    <td style="line-height:15px;font-size:8px;text-align: right;border-bottom: 1px solid #ddd;" 
        width="10%">
       dasdasdsdas
    </td>
    <td style="line-height:15px;font-size:8px;text-align: right;border-bottom: 1px solid #ddd;" 
        width="18%">
       sadasdasdas
    </td>
</tr>

<tr>
    <td style="line-height:14px;font-size:8px;" 
        width="5%">asdasd</td>
    <td style="line-height:14px;font-size:8px;" 
        width="15%">sadasdsa</td>
    <td style="line-height:14px;font-size:8px;" 
        width="35%">asdasd</td>
    <td style="line-height:14px;font-size:8px;" 
        width="10%">asdasddasd</td>
    <td style="line-height:14px;font-size:8px;" 
        width="7%">saddsadsad</td>
    <td style="line-height:14px;font-size:8px;text-align: right;" 
        width="10%">
        asdsadasdd
    </td>
    <td style="line-height:14px;font-size:8px;text-align: right;" 
        width="18%">
       asdasda
    </td>
</tr>

<tr>
    <td colspan="7" style="line-height: 5px;">&nbsp;</td>
</tr>
<tr>
    <td colspan="7" style="line-height: 5px;border-bottom: 1px solid #000;">&nbsp;</td>
</tr>
<tr>
    <td colspan="7" style="line-height: 3px;">&nbsp;</td>
</tr>
<tr>
    <td colspan="3" style="line-height: 14px;">&nbsp;</td>
    <td style="line-height: 14px;border-bottom: 1px solid #ddd;" 
        width="20%"><strong>Subtotal</strong></td>
    <td colspan="3" 
        style="line-height: 14px;text-align:right;border-bottom: 1px solid #ddd;" 
        width="25%">
        sadasdsadsa
    </td>
</tr>


<tr>
    <td colspan="3" style="line-height: 18px;border-bottom: 1px solid #000;">&nbsp;</td>
    <td style="line-height: 18px;border-bottom: 1px solid #000;" 
        width="20%"><strong>Grand Total (Rs)</strong></td>
    <td colspan="3" 
        style="line-height: 18px;text-align:right;border-bottom: 1px solid #000;" 
        width="25%">asdsadsad</td>
</tr>
</table>
<table style="border-collapse: collapse;">
<tr>
    <td colspan="4" style="line-height: 5px;"></td>
</tr>
<tr>
    <td colspan="4" style="line-height: 5px;"></td>
</tr>
<tr>
    <td width="17%" style="font-size:7px;">Payment</td>
    <td width="2%" style="font-size:7px;">:</td>
    <td width="51%" style="font-size:7px;">A/C Cheque</td>
    <td width="30%" style="font-size:7px;"></td>
</tr>
<tr>
    <td width="17%" style="font-size:7px;">Benificiary Details</td>
    <td width="2%" style="font-size:7px;">:</td>
    <td width="51%" style="font-size:7px;">Orel Corporation Pvt Ltd.</td>
    <td width="30%" style="font-size:7px;"></td>
</tr>
<tr>
    <td width="17%" style="font-size:7px;">Bank Details</td>
    <td width="2%" style="font-size:7px;">:</td>
    <td width="51%" style="font-size:7px;">Sampath Bank</td>
    <td width="30%" style="font-size:7px;"></td>
</tr>
<tr>
    <td width="17%" style="font-size:7px;">Bank A/C</td>
    <td width="2%" style="font-size:7px;">:</td>
    <td width="51%" style="font-size:7px;">0001 1008 3351</td>
    <td width="30%" style="font-size:7px;"></td>
</tr>
<tr>
    <td width="17%" style="font-size:7px;">Warranty</td>
    <td width="2%" style="font-size:7px;">:</td>
    <td width="51%" style="font-size:7px;">12 months for the switchgear, measuring equipments & assessories and 18 months of enclosures & associated components from the date of invoice</td>
    <td width="30%" style="font-size:7px;"></td>
</tr>
<tr>
    <td colspan="4" style="line-height: 5px;"></td>
</tr>
<tr>
    <td colspan="4" style="line-height: 5px;"></td>
</tr>
<tr>
    <td colspan="4" style="font-size: 7px;"><strong>Other Terms & Conditions :-</strong></td>
</tr>
<tr>
    <td colspan="4" style="font-size: 7px;">All payment to be made in a/c payee cheque in flavor of OREL COROPORATION (PVT) LTD.</td>
</tr>
<tr>
    <td colspan="4" style="font-size: 7px;">Items are non-returnable, unless otherwise due to a manufacturing defect.</td>
</tr>
<tr>
    <td colspan="4" style="line-height: 6px;"></td>
</tr>
<tr>
    <td colspan="4" style="font-size: 7px;text-align: center;">This is a computer generated document. no signature is required and deemed as official</td>
</tr>
<tr>
    <td colspan="4" style="line-height: 6px;"></td>
</tr>
<tr>
    <td colspan="4" style="font-size: 8px;text-align: center;line-height: 13px;">THANK YOU</td>
</tr>
</table>