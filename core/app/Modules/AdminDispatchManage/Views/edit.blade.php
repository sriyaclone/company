@extends('layouts.back_master') @section('title','Add Dispatch')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
<link rel="stylesheet" type="text/css" src="{{asset('assets/dist/bootstrap-datepicker/css/bootstrap-datepicker.css')}}"/>

<link rel="stylesheet" type="text/css" src="{{asset('assets/dist/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}"/>
<style type="text/css">
	.inv-selected {
	  background-color: #ddfdfa;
	}

	.has-error .help-block, .has-error .control-label{
		color:#e41212;
	}
	.has-error .chosen-container{
		border:1px solid #e41212;
	}

	.help-block{
		color:#e41212;
	}

	.bottom{
		margin-bottom: 5px;
	}

	td, h5{
		padding: 0;
    		margin: 0;
	}

	.pagination {
	     margin: 0; 
	}

	.sorting-item{
	    align-items: center;
	    width: 100%;
	    margin: 4px;
	    background-color: #fff;
	    border: 1px solid #e0e0e0;
	    color: #333;
	    font-weight: 400;
	}

	.sorting-item .handle{
		background: #4b8bc1;
	    color: #fff;
	    padding: 10px;
	}

	.sorting-item .sorting-content{
	    padding: 10px;
	}

th>input{
	font-weight: 400;
}
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Dispatch<small>Management</small></h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li><a href="{{url('admin/tour/index')}}">Dispatch Management</a></li>
		<li class="active">Add Dispatch</li>
	</ol>
</section>

<!-- Main content -->
<section class="content" ng-app="app" ng-controller="NgController">

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="box box-default">
	            <div class="box-body"> 
	            	<div class="row">
		            	<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
							<label for="" class="control-label required">Warehouse</label>
		            		{!! Form::select('warehouse',$warehouses,$dispatch->warehouse_id,['class'=>'form-control chosen whouse','style'=>'width:100%;','required','data-placeholder'=>'Choose Warehouse','id'=>'warehouse']) !!}
							@if ($errors->has('warehouse'))
		                        <span class="help-block">
		                            {{ $errors->first('warehouse') }}
		                        </span>
		                    @endif					            		
						</div>					
						<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
							<label for="" class="control-label required">Bay</label>
		            		{!! Form::select('bay',$bays,$dispatch->bay_id,['class'=>'form-control chosen','style'=>'width:100%;','required','data-placeholder'=>'Choose Bay','id'=>'bay']) !!}
							@if ($errors->has('bay'))
		                        <span class="help-block">
		                            {{ $errors->first('bay') }}
		                        </span>
		                    @endif							            		
						</div>
						<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
							<label for="" class="control-label required">Vehicle Type</label>
		            		{!! Form::select('vehicle_type',$vehicle_types,$dispatch->vehicle_type,['class'=>'form-control chosen vtype','style'=>'width:100%;','required','data-placeholder'=>'Choose Vehicle Type','id'=>'vehicle_type']) !!}
		            		@if ($errors->has('vehicle_type'))
		                        <span class="help-block">
		                            {{ $errors->first('vehicle_type') }}
		                        </span>
		                    @endif
						</div>
						<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
							<label for="" class="control-label required">Vehicle</label>
		            		{!! Form::select('vehicle',$vehicles,$dispatch->vehicle_id,['class'=>'form-control chosen','style'=>'width:100%;','required','data-placeholder'=>'Choose Vehicle','id'=>'vehicle']) !!}
		            		@if ($errors->has('vehicle'))
		                        <span class="help-block">
		                            {{ $errors->first('vehicle') }}
		                        </span>
		                    @endif
						</div>
					</div>

					<div class="row emp_row" style="margin-top: 5px">
						<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
							<label for="" class="control-label required">Driver</label>
		            		{!! Form::select('driver',$drivers,$dispatch->driver_id,['class'=>'form-control chosen','style'=>'width:100%;','required','data-placeholder'=>'Choose Driver','id'=>'driver']) !!}
		            		@if ($errors->has('driver'))
		                        <span class="help-block">
		                            {{ $errors->first('driver') }}
		                        </span>
		                    @endif
		            	</div>

						<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
							<label for="" class="control-label">Helper</label>
		            		{!! Form::select('helper',$helpers,$dispatch->helper_id,['class'=>'form-control chosen','style'=>'width:100%;','required','data-placeholder'=>'Choose Helper','id'=>'helper']) !!}
		            		@if ($errors->has('helper'))
		                        <span class="help-block">
		                            {{ $errors->first('helper') }}
		                        </span>
		                    @endif
		            	</div>

		            	<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
							<label for="" class="control-label required">Route</label>
		            		{!! Form::select('location',$locations,$dispatch->route_id,['class'=>'form-control chosen','style'=>'width:100%;','required','data-placeholder'=>'Choose Location','id'=>'location']) !!}
		            		@if ($errors->has('location'))
		                        <span class="help-block">
		                            {{ $errors->first('location') }}
		                        </span>
		                    @endif
		            	</div>

		            	<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
							<label for="" class="control-label required">Dispatch date time</label>
		            		<input id="dispatch_date" name="dispatch_date" type="text" class="form-control datepick" value="{{$dispatch->dispatch_date}}">
		            	</div>
					</div>

		             @if ($errors->has('invoices'))
		                <span class="help-block">
		                    {{ $errors->first('invoices') }}
		                </span>
		            @endif
	            </div>	            
	        	<div class="overlay" style="display: none;">
	              <i class="fa fa-refresh fa-spin"></i>
	            </div>
	        </div>
    	</div>
	</div>

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="box box-default">
	            <div class="box-body"> 
	            	<div class="row">
	            		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
	            			<h4>Invoices</h4>
	            		</div>
	            		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">
	            			<button type="button" class="btn btn-xs btn-default" ng-click="getInvoices(1)">Refresh</button>
	            		</div>
	            	</div>
	            	<table class="table table-bordered">
							<thead>
								<tr>
									<th>#</th>
									<th>Invoice No 
										<div class="input-group input-group-sm" style="width: 150px;">
						                  <input  name="no" type="text" class="form-control" value="" ng-model="inv_no">
						                  <div class="input-group-btn">
						                    <button type="submit" class="btn btn-default"
						                    ng-click="getInvoices(invoices.current_page)"><i class="fa fa-search"></i></button>
						                  </div>
						                </div>
						          	</th>
									<th>
										Invoice Date 
										<div class="input-group input-group-sm" style="width: 150px;">
						                  <input  name="no" type="text" class="form-control" value="" ng-model="inv_date">
						                  <div class="input-group-btn">
						                    <button type="submit" class="btn btn-default"
						                    ng-click="getInvoices(invoices.current_page)"><i class="fa fa-search"></i></button>
						                  </div>
						                </div>
									</th>
									<th>
										Customer 
										<div class="input-group input-group-sm" style="width: 250px;">
						                  <input  name="no" type="text" class="form-control" value="" ng-model="customer">
						                  <div class="input-group-btn">
						                    <button type="submit" class="btn btn-default" 
						                    	ng-click="getInvoices(invoices.current_page)"><i class="fa fa-search"></i></button>
						                  </div>
						                </div>
									</th>
									<th>
										Area 
										<div class="input-group input-group-sm" style="width: 150px;">
						                  <select name="" id="input" class="form-control" required="required" ng-model="area">
											<option value="">All</option>
						                  	@foreach($areas as $area)
											<option value="{{$area->name}}">{{$area->name}}</option>
											@endforeach
										  </select>
						                  <div class="input-group-btn">
						                    <button type="submit" class="btn btn-default"
						                    ng-click="getInvoices(invoices.current_page)"><i class="fa fa-search"></i></button>
						                  </div>
						                </div>										
									</th>
									<th style="text-align: center;" width="10%">
										Action
									</th>
								</tr>
							</thead>
							<tbody>
			                	<tr ng-repeat="invoice in invoices.data" 
			                	ng-class="isItemInList(invoice)>0?'inv-selected':''">
			                		<td style="text-align: left">@{{$index+1}}</td>
			                		<td style="text-align: left">@{{invoice.invoice_no}}</td>
									<td style="text-align: left">@{{invoice.invoice_date}}</td>
			                		<td style="text-align: left">@{{invoice.customer.first_name}} @{{invoice.customer.last_name}} <small>(@{{invoice.customer.code}})</small></td>
			                		<td style="text-align: left">@{{invoice.area}}</td>		                        		
			                		<td style="text-align: center;">
			                			<button type="button" class="btn btn-xs btn-default" 
			                				ng-click="add(invoice)">Add +</button> 
			                		</td>
			                	</tr>
							</tbody>
						</table>    
						
						<div class="row" style="margin-top: 10px;">
						 	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
								<paging
						            page="invoices.current_page" 
						            page-size="invoices.per_page" 
						            total="invoices.total"
						            show-prev-next="true"
						            show-first-last="true"
						            paging-action="getInvoices(page,search)">
					            </paging> 
							</div>
						</div> 
	            </div>
	            <div class="overlay" style="display: none;">
	              <i class="fa fa-refresh fa-spin"></i>
	            </div>
	        </div>
    	</div>
	</div>

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="box box-default">
	            <div class="box-body"> 
	            	<h4>Selected Invoices - For Dispatch 
	            		<span style="font-size: 10px;margin: 0;padding: 0">(drag and drop rows to set the priority)</span>
	            	</h4>

	            	<table class="table table-bordered">
						<thead>
							<tr>
								<th>#</th>
								<th>Invoice No 
					          	</th>
								<th>
									Invoice Date
								</th>
								<th>
									Customer 
								</th>
								<th>
									Area 										
								</th>
								<th style="text-align: center;" width="10%">
									Action <br>
								</th>
							</tr>
						</thead>
						<tbody sv-root sv-part="selectedInvoices">
		                	<tr sv-element ng-repeat="invoice in selectedInvoices" class="sorting-item"> 
		                		<td style="text-align: left">@{{$index+1}}</td>
		                		<td style="text-align: left">@{{invoice.invoice_no}}</td>
								<td style="text-align: left">@{{invoice.invoice_date}}</td>
		                		<td style="text-align: left">@{{invoice.customer.first_name}} @{{invoice.customer.last_name}} <small>(@{{invoice.customer.code}})</small></td>
		                		<td style="text-align: left">@{{invoice.area}}</td>			                        		
		                		<td style="text-align: center;">
		                			<button type="button" class="btn btn-xs btn-default" 
			                				ng-click="remove($index)">Rem X</button> 
		                		</td>
		                	</tr>
		                	<tr ng-show="!selectedInvoices.length>0">
		                		<td colspan="6" class="text-center">
		                			<h4>Nothing Selected</h4>
		                			<p>Select invoices from the invoice list to plan the dispatch</p>
		                		</td>
		                	</tr>
						</tbody>
					</table>  
				    <div class="row" style="margin-top: 10px;">
				    	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
				    		<button type="button" class="btn btn-default" ng-click="saveDispatch()">Add Dispatch</button>
				    	</div>
				    </div> 
	            </div>	            
			    <div class="overlay" style="display: none;">
	              <i class="fa fa-refresh fa-spin"></i>
	            </div>
	        </div>
    	</div>
	</div>

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" ng-show="errors.length>0">
			<div class="alert alert-warning">			
				<strong>Error!</strong> 
				<p ng-repeat="error in errors">@{{error.msg}}</p>
			</div>
		</div>
	</div>

</section><!-- /.content -->

@stop
@section('js')

<script src="{{asset('assets/dist/chosen/chosen.jquery.min.js')}}"></script>



<script src="{{asset('assets/dist/moment/moment.js')}}"></script>
<script src="{{asset('assets/dist/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>

<script src="{{asset('assets/dist/angular/angular/angular.min.js')}}"></script>
<script src="{{asset('assets/dist/nestable/jquery.nestable.js')}}"></script>
<script src="{{asset('assets/dist/angular-sortable/src/angular-sortable-view.js')}}"></script>
<script src="{{asset('assets/dist/ng-pagination/dist/paging.min.js')}}"></script>

<script type="text/javascript">


	$(document).ready(function() {
	  	$(".chosen").chosen(); 

	  	$(".chosen").chosen();

	  	$('.datepick').datetimepicker({format: 'YYYY-MM-DD HH:mm'});

		$('.overlay').show();
	  	ajaxRequest( '{{url('dispatch/json/data/list')}}' , { 'id' : $('.whouse').val()}, 'get', dataFunc);
	  	ajaxRequest( '{{url('dispatch/json/vehicle/list')}}' , { 'id' : $('.vtype').val(), 'warehouse_id' : $("#warehouse").val() }, 'get', resultFunc);

	  	$('.whouse').change(function(){
			$('.overlay').show();
			ajaxRequest( '{{url('dispatch/json/data/list')}}' , { 'id' : $(this).val()}, 'get', dataFunc);

		});

		$('.vtype').change(function(){
			$('.overlay').show();
			ajaxRequest( '{{url('dispatch/json/vehicle/list')}}' , { 'id' : $(this).val(), 'warehouse_id' : $("#warehouse").val() }, 'get', resultFunc);

		});
	});

	function resultFunc(data){
		$('#vehicle').html("");
		$.each(data,function(key,value){
            $('#vehicle').append('<option value="'+key+'">'+value+'</option>');            
        });
        $('#vehicle').val({{$dispatch->vehicle_id}});
        $('#vehicle').trigger("chosen:updated");
        $('.overlay').hide();
	}

	function dataFunc(data){
		$('#driver').html("");
		$('#helper').html("");
		$('#bay').html("");


		$.each(data.bays,function(key,value){
            $('#bay').append('<option value="'+key+'">'+value+'</option>');            
        });
        $('#bay').val({{$dispatch->bay_id}});
        $('#bay').trigger("chosen:updated");

		$.each(data.drivers,function(key,value){
            $('#driver').append('<option value="'+key+'">'+value+'</option>');            
        });
        $('#driver').val({{$dispatch->driver_id}});
        $('#driver').trigger("chosen:updated");

        $.each(data.helpers,function(key,value){
            $('#helper').append('<option value="'+key+'">'+value+'</option>');            
        });
        $('#helper').val({{$dispatch->helper_id}});
        $('#helper').trigger("chosen:updated");
        
        $('.overlay').hide();
	}



	var app = angular.module('app', ['bw.paging','angular-sortable-view']);

	app.controller('NgController', function NgController($scope) {

		$scope.invoices         = [];
		$scope.selectedInvoices = <?php echo json_encode($invocies)?>;
		$scope.invoice_counts   = [];
		$scope.errors           = [];
		$scope.dispatch         = {};

		$scope.getInvoices = function(page) {
		  $('.overlay').show();
	      $.ajax({
	          url: "{{URL::to('dispatch/json/getInvoices')}}",
	          method: 'GET',
	          data: {'page': page,'customer':$scope.customer,'inv_date':$scope.inv_date,'inv_no':$scope.inv_no,'area':$scope.area},
	          async: true,
	          success: function (data) {
	            $scope.invoices = data;
	            $scope.$apply();
	            $('.overlay').hide();
	          },
	          error: function () {
	          	$('.overlay').hide();
	              
	          }
	      });
	    }

	    $scope.getInvoiceCounts = function() {
	      $.ajax({
	          url: "{{URL::to('dispatch/json/getInvoiceCounts')}}",
	          method: 'GET',
	          async: true,
	          success: function (data) {
	            $scope.invoice_counts = data;
	            $scope.$apply();
	          },
	          error: function () {
	              
	          }
	      });
	    }

		$scope.add = function(item) {
			if($scope.isItemInList(item)<0){
				$scope.selectedInvoices.push(item); 
			}	      
	    }

	    $scope.remove = function(index) {
	      $scope.selectedInvoices.splice(index,1); 
	    }
	    

	    $scope.isItemInList = function (inv) {	
		    for($i=0;$i<$scope.selectedInvoices.length;$i++){
	            if ($scope.selectedInvoices[$i].id == inv.id) {
	                return 1;
	            } 
	        };
		    return -1;	      	
	    }

	    $scope.saveDispatch = function(item) {	    	
	    	if($scope.isValid()){
	    		$('.overlay').show();
	    		$.ajax({
		          url: "{{URL::to('dispatch/json/editDispatch')}}",
		          method: 'POST',
		          async: true,
		          data: {'dispatch': $scope.dispatch,'dispatch_id': {{$dispatch->id}}},
		          success: function (data) {
		          	if(data.success){
		          		sweetAlert('Dispatch Created', data.message,0);	
		          		location.reload();
		          	}else{
		          		sweetAlert('Dispatch Create Error', data.message,2);
		          	}
		            $('.overlay').hide();		            
		          },
		          error: function () {
		            $('.overlay').hide();  
		          }
		      });
	    	}
	    }

	    $scope.isValid = function () {
	    	$scope.errors   = [];	       	
	    	$scope.dispatch = {};	       	
	    	if($scope.selectedInvoices.length==0){
	    		$scope.errors.push({'msg':'you have not selected any invoices'});
			}else{
				$scope.dispatch.invoices = $scope.selectedInvoices;
			}

			if(!$('#warehouse').val()){
	    		$scope.errors.push({'msg':'you have not selected any warehouse'});
			}else{
				$scope.dispatch.warehouse = $('#warehouse').val();
			}

			if(!$('#bay').val()){
	    		$scope.errors.push({'msg':'you have not selected any bay'});
			}else{
				$scope.dispatch.bay= $('#bay').val();
			}

			if(!$('#vehicle_type').val()){
	    		$scope.errors.push({'msg':'you have not selected any vehicle type'});
			}else{
				$scope.dispatch.vehicle_type = $('#vehicle_type').val();
			}

			if(!$('#vehicle').val()){
	    		$scope.errors.push({'msg':'you have not selected any vehicle'});
			}else{
				$scope.dispatch.vehicle = $('#vehicle').val();
			}

			if(!$('#driver').val()){
	    		$scope.errors.push({'msg':'you have not selected any driver'});
			}else{
				$scope.dispatch.driver=$('#driver').val();
			}

			if(!$('#location').val()){
	    		$scope.errors.push({'msg':'you have not selected any route'});
			}else{
				$scope.dispatch.location=$('#location').val();
			}

			$scope.dispatch.helper = $('#helper').val();
			$scope.dispatch.dispatch_date = $('#dispatch_date').val();

			console.log($scope.dispatch);

			return $scope.errors.length==0;

	    }


		$scope.getInvoices(1);
	});

</script>
@stop
