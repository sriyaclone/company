@extends('layouts.back_master') @section('title','Admin - Dispatch Details')
@section('current_title','Dispatch Details')

@section('css')
<style type="text/css">
    .port-image
    {
        width: 100%;
    }

    .gallery_product
    {
        margin-bottom: 30px;
    }

    .mediam
    {
        font-size: medium;
    }

    .timeline{position:relative;margin:0 0 30px 0;padding:0;list-style:none}

    .timeline:before{content:'';position:absolute;top:0;bottom:0;width:4px;background:#ddd;left:31px;margin:0;border-radius:2px}

    .timeline>li{position:relative;margin-right:10px;margin-bottom:15px}

    .timeline>li:before,.timeline>li:after{content:" ";display:table}

    .timeline>li:after{clear:both}

    .timeline>li>.timeline-item{-webkit-box-shadow:0 1px 1px rgba(0,0,0,0.1);box-shadow:0 1px 1px rgba(0,0,0,0.1);border-radius:3px;margin-top:0;background:#fff;color:#444;margin-left:60px;margin-right:15px;padding:0;position:relative}

    .timeline>li>.timeline-item>.time{color:#999;float:right;padding:10px;font-size:12px}

    .timeline>li>.timeline-item>.timeline-header{margin:0;color:#555;border-bottom:1px solid #f4f4f4;padding:10px;font-size:16px;line-height:1.1}

    .timeline>li>.timeline-item>.timeline-header>a{font-weight:600}

    .timeline>li>.timeline-item>.timeline-body,.timeline>li>.timeline-item>.timeline-footer{padding:10px}

    .timeline>li>.fa,.timeline>li>.glyphicon,.timeline>li>.ion{width:30px;height:30px;font-size:15px;line-height:30px;position:absolute;color:#666;background:#d2d6de;border-radius:50%;text-align:center;left:18px;top:0}

    .timeline>.time-label>span{font-weight:600;padding:5px;display:inline-block;background-color:#fff;border-radius:4px}

    .timeline-inverse>li>.timeline-item{background:#f0f0f0;border:1px solid #ddd;-webkit-box-shadow:none;box-shadow:none}

    .timeline-inverse>li>.timeline-item>.timeline-header{border-bottom-color:#ddd}

    .profile-user-img {
        margin: 0px auto;
        width: 100px;
        height: 100px;
        padding: 3px;
        border: 3px solid #D2D6DE;
    }

    .img-circle {
        border-radius: 50%;
    }

    .inquiry-details span{
        font-size: 12px;
    }

    .inquiry-status{
        font-size: 12px
    }

    .mailbox-attachment img {
        max-width: 100%;
        max-height: 100%;
    }


    .square {
        height: 75px;
        width: 75px;
    }
    .mailbox-attachment-name{
        font-size: 12px;
    }

    .label-pink{
        background-color: pink;
        color: #444;
    }

    .ibox-tools a {
        color: black;
        background: silver !important;
        border-color: silver !important;
    }

    .ibox-tools a:hover {
        color: #fff !important;
        background: #2f4050 !important;
        border-color: #2f4050 !important;
    }
</style>  
@stop

@section('content')
<!-- Content-->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Dispatch Management</h2>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{url('admin/dispatch/list')}}">Dispatch Management</a></li>
            <li class="active">Dispatch Details</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a class="btn btn-primary" href="{{ url('admin/dispatch/list') }}"><i class="fa fa-th" aria-hidden="true"></i> Dispatch List</a>
            <a class="btn btn-warning" href="{{ url('admin/dispatch/create') }}"><i class="fa fa-plus" aria-hidden="true"></i> Add Dispatch</a>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Dispatch Detail - {{$dispatch_details->dispatch_no}}</h5>
            <h5 class="profile-username">
                @if($dispatch_details->status==DISPATCH_PENDING)
                    <label class="label label-warning" style="">Pending</label>
                @elseif($dispatch_details->status==DISPATCH_PICKED)
                    <span class="label label-warning" style="">Picked</span>
                @elseif($dispatch_details->status==DISPATCH_BAYIN)
                    <span class="label label-warning" style="">Bay In</span>
                @elseif($dispatch_details->status==DISPATCH_LOADED)
                    <span class="label label-warning" style="">Loaded</span>
                @elseif($dispatch_details->status==DISPATCH_CHECKOUT)
                    <span class="label label-warning" style="">Checkout</span>
                @elseif($dispatch_details->status==DISPATCH_CHECKIN)
                    <span class="label label-warning" style="">Checkin</span>
                @elseif($dispatch_details->status==DISPATCH_CANCEL)
                    <span class="label label-danger" style="">Cancel</span>
                @endif    
            </h5>
            <div class="ibox-tools">
                @if($dispatch_details->status==DISPATCH_LOADED || $dispatch_details->status==DISPATCH_CHECKOUT || $dispatch_details->status==DISPATCH_CHECKIN)
                    <a class="btn btn-default" href="{{url('admin/dispatch/note/')}}/{{$dispatch_details->id}}" target="_blank">
                        Print <i class="fa fa-print" style="padding-right: 5px;"></i>
                    </a>
                @endif
            </div>
        </div>

        <div class="ibox-content" style="display: block;">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <table class="table table-bordered">
                                    <thead>
                                        <tr>    
                                            <th style="text-align: left">Dispatch Date</th>
                                            <th style="text-align: left">Warehouse</th>
                                            <th style="text-align: left">Route</th>
                                            @if($dispatch_details->type==COURIERDELIVERY)
                                                <th style="text-align: left">Courier</th>
                                            @endif

                                            @if($dispatch_details->type==COMPANYDELIVERY)
                                                <th style="text-align: left">Vehicle</th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="text-align: left">{{$dispatch_details->dispatch_date}}</td>
                                            <td style="text-align: left">
                                                @if(sizeof($dispatch_details->warehouse) > 0)
                                                {{$dispatch_details->warehouse->name}}<br>
                                                @endif
                                            </td>
                                            <td style="text-align: left">
                                                @if($dispatch_details->route)
                                                    {{$dispatch_details->route->name}}
                                                @endif<br>
                                            </td>
                                            @if($dispatch_details->type==COURIERDELIVERY)
                                                <td style="text-align: left">
                                                    {{$dispatch_details->courier['name']}}<br>
                                                    <span class="label label-pink" style="">{{$dispatch_details->courier['code']}}</span>
                                                </td>
                                            @endif

                                            @if($dispatch_details->type==COMPANYDELIVERY)
                                                <td style="text-align: left">
                                                    {{$dispatch_details->vehicle['code']}}<br>
                                                </td>                                                
                                            @endif
                                        </tr>
                                    </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-default">
                        <div class="box-header" style="padding: 5px">    
                            <h4 class="header-title">Dispatch - Invoice Details </h4>
                        </div>
                        <div class="box-body">    
                            <table class="table table-bordered" id="orderTable">
                                <thead>
                                    <tr>    
                                        <th style="text-align: left">Invoice No</th>
                                        <th style="text-align: left">Invoice Date</th>
                                        <th style="text-align: left">Deliver Date</th>
                                        <th style="text-align: left">Customer</th>
                                        <th style="text-align: right">Total (Rs.)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1; $_tot = 0; ?>
                                @if(count($dispatch_details->details) > 0)
                                    @foreach($dispatch_details->details as $result_val)
                                        <tr>
                                            <td style="text-align: left">
                                                <a href="{{url('admin/invoice/listdetail')}}/{{$result_val->invoice->id}}" class="btn btn-xs btn-default" title="View Invoice Details">#{{$result_val->invoice->invoice_no}}</a>
                                            </td>
                                            <td style="text-align: left"> {{$result_val->invoice->invoice_date}}</td>
                                            <td style="text-align: left">{{$result_val->invoice->deliver_date}}</td>
                                            <td style="text-align: left">
                                                {{$result_val->invoice->customer->code}}  {{$result_val->invoice->customer->first_name}} {{$result_val->invoice->customer->last_name}}
                                            </td>
                                            <td style="text-align: right">{{number_format($result_val->invoice->total_amount, 2)}}</td>
                                        </tr>
                                        <?php $_tot += $result_val->invoice->total_amount; ?>
                                        <?php $i++;?>
                                    @endforeach
                                @else
                                    <tr><td colspan="12" class="text-center">No data found.</td></tr>
                                @endif
                                </tbody>
                                <tfoot>
                                    <tr style="color: #000;font-weight: bolder;">
                                        <td colspan="4" style="text-align: right">Total</td>
                                        <td style="text-align: right">{{number_format($_tot,2)}}</td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>                
                </div> 
            </div>

        </div>
    </div>
</div>
@stop

@section('js')
<script type="text/javascript">
    $(document).ready(function(){
        
    });
</script>
@stop
