@extends('layouts.back_master') @section('title','Add Dispatch')
@section('css')
<link rel="stylesheet" type="text/css" src="{{asset('assets/inspinia/css/plugins/datepicker/datepicker3.css')}}"/>
<style type="text/css">
	.inv-selected {
	  	background-color: #ddfdfa;
	}

	.dpt-selected {
	  	background-color: silver !important;
	  	color: #000 !important;
	}

	.has-error .help-block, .has-error .control-label{
		color:#e41212;
	}
	.has-error .chosen-container{
		border:1px solid #e41212;
	}

	.help-block{
		color:#e41212;
	}

	.bottom{
		margin-bottom: 5px;
	}

	td, h5{
		padding: 0;
    		margin: 0;
	}

	.pagination {
	     margin: 0; 
	}

	.sorting-item{
	    align-items: center;
	    width: 100%;
	    margin: 4px;
	    background-color: #fff;
	    border: 1px solid #e0e0e0;
	    color: #333;
	    font-weight: 400;
	}

	.sorting-item .handle{
		background: #4b8bc1;
	    color: #fff;
	    padding: 10px;
	}

	.sorting-item .sorting-content{
	    padding: 10px;
	}

	th>input{
		font-weight: 400;
	}
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Dispatch Management</h2>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{url('admin/dispatch/list')}}">Dispatch Management</a></li>
            <li class="active">Dispatch Create</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a class="btn btn-warning" href="{{ url('admin/dispatch/list') }}"><i class="fa fa-th" aria-hidden="true"></i> Dispatch List</a>
        </div>
    </div>
</div>

<!-- Main content -->
<div ng-app="app" ng-controller="NgController">
	<div class="wrapper wrapper-content animated fadeInRight" ng-cloak >
        <div class="ibox float-e-margins">            

            <div class="ibox-content" style="display: block;">            	

				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="box box-default">
							<div class="box-header">
				            	<h2>Invoices </h2>
				            </div>
				            <div class="box-body">
			            		<table class="table table-bordered">
									<thead>
										<tr>
											<th>#</th>
											<th>Invoice No 
												<div class="input-group input-group-sm" style="width: 150px;">
									                <input  name="no" type="text" class="form-control" value="" ng-model="inv_no">
									                <div class="input-group-btn">
									        	        <button type="submit" class="btn btn-default"
									                    ng-click="getInvoices(invoices.current_page)"><i class="fa fa-search"></i></button>
									                </div>
									            </div>
									        </th>
											<th>
												Invoice Date 
												<div class="input-group input-group-sm" style="width: 150px;">
								                  <input  name="no" type="text" class="form-control" value="" ng-model="inv_date">
								                  <div class="input-group-btn">
								                    <button type="submit" class="btn btn-default"
								                    ng-click="getInvoices(invoices.current_page)"><i class="fa fa-search"></i></button>
								                  </div>
								                </div>
											</th>
											<th>
												Customer 
												<div class="input-group input-group-sm" style="width: 250px;">
									                <input  name="no" type="text" class="form-control" value="" ng-model="customer">
									                <div class="input-group-btn">
									            	    <button type="submit" class="btn btn-default" 
									                    	ng-click="getInvoices(invoices.current_page)"><i class="fa fa-search"></i></button>
									                </div>
									            </div>
											</th>
											<th style="text-align: center;" width="10%">
												Action
											</th>
										</tr>
									</thead>
									<tbody>
						               	<tr ng-repeat="invoice in invoices.data" 
						                	ng-class="isItemInList(invoice)>0?'inv-selected':''">
						               		<td style="text-align: left">@{{$index+1}}</td>
						               		<td style="text-align: left">@{{invoice.invoice_no}}</td>
											<td style="text-align: left">@{{invoice.invoice_date}}</td>
						               		<td style="text-align: left">@{{invoice.customer.first_name}} @{{invoice.customer.last_name}} <small>(@{{invoice.customer.code}})</small></td>
						               		<td style="text-align: center;">
						               			<button type="button" class="btn btn-xs btn-default" ng-click="add(invoice)">Add +</button> 
						                	</td>
						                </tr>
									</tbody>
								</table>    
									
								<div class="row" style="margin-top: 10px;">
								 	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
										<paging
								            page="invoices.current_page" 
								            page-size="invoices.per_page" 
								            total="invoices.total"
								            show-prev-next="true"
								            show-first-last="true"
								            paging-action="getInvoices(page,search)">
							            </paging> 
									</div>
								</div> 
				            </div>
				            <div class="overlay" style="display: none;">
				              	<i class="fa fa-refresh fa-spin"></i>
				            </div>
				        </div>
			    	</div>
				</div>

				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="box box-default">
				            <div class="box-header">
				            	<h2>Dispatch Note - Selected Invoices </h2>
				            </div>
				            <div class="box-body">
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
									<div class="row">
										<label for="" class="control-label required">Warehouse</label>
					            		{!! Form::select('warehouse',$warehouses, [],['class'=>'form-control chosen whouse','style'=>'width:100%;','required','data-placeholder'=>'Choose Warehouse','id'=>'warehouse']) !!}
										@if ($errors->has('warehouse'))
					                        <span class="help-block">
					                            {{ $errors->first('warehouse') }}
					                        </span>
					                    @endif					            		
									</div>					
									<div class="row">
										<label for="" class="control-label required">Vehicle Type</label>
					            		{!! Form::select('vehicle_type',$vehicle_types, [],['class'=>'form-control chosen vtype','style'=>'width:100%;','required','data-placeholder'=>'Choose Vehicle Type','id'=>'vehicle_type']) !!}
					            		@if ($errors->has('vehicle_type'))
					                        <span class="help-block">
					                            {{ $errors->first('vehicle_type') }}
					                        </span>
					                    @endif
									</div>
									<div class="row">
										<label for="" class="control-label required">Vehicle</label>
					            		{!! Form::select('vehicle',$vehicles, [],['class'=>'form-control chosen','style'=>'width:100%;','required','data-placeholder'=>'Choose Vehicle','id'=>'vehicle']) !!}
					            		@if ($errors->has('vehicle'))
					                        <span class="help-block">
					                            {{ $errors->first('vehicle') }}
					                        </span>
					                    @endif
									</div>
									<div class="row">
										<label for="" class="control-label required">Route</label>
					            		{!! Form::select('location',$locations, [],['class'=>'form-control chosen','style'=>'width:100%;','required','data-placeholder'=>'Choose Location','id'=>'location']) !!}
					            		@if ($errors->has('location'))
					                        <span class="help-block">
					                            {{ $errors->first('location') }}
					                        </span>
					                    @endif
					            	</div>
									<div class="row emp_row" style="margin-top: 5px">
										<label for="" class="control-label required">Dispatch date time</label>
					            		<input id="dispatch_date" name="dispatch_date" type="text" class="form-control datepick" value="@if(isset($old->dispatch_date)){{$old->dispatch_date}}@else{{date('Y-m-01')}}@endif">
									</div>

						            @if ($errors->has('invoices'))
						                <span class="help-block">
						                    {{ $errors->first('invoices') }}
						                </span>
						            @endif
								</div>
							
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<div class="box box-default">
							            <div class="box-body"> 
							            	<h4>
							            		<span style="font-size: 10px;margin: 0;padding: 0">(drag and drop rows to set the priority)</span>
							            	</h4>

							            	<table class="table table-bordered">
												<thead>
													<tr>
														<th>#</th>
														<th>Invoice No 
											          	</th>
														<th>
															Invoice Date
														</th>
														<th>
															Customer 
														</th>
														<th style="text-align: center;" width="10%">
															Action <br>
														</th>
													</tr>
												</thead>
												<tbody sv-root sv-part="selectedInvoices">
								                	<tr sv-element ng-repeat="invoice in selectedInvoices" class="sorting-item dpt-selected"> 
								                		<td style="text-align: left">@{{$index+1}}</td>
								                		<td style="text-align: left">@{{invoice.invoice_no}}</td>
														<td style="text-align: left">@{{invoice.invoice_date}}</td>
								                		<td style="text-align: left">@{{invoice.customer.first_name}} @{{invoice.customer.last_name}} <small>(@{{invoice.customer.code}})</small></td>
								                		<td style="text-align: center;">
								                			<button type="button" class="btn btn-xs btn-default" 
									                				ng-click="remove($index)">Rem X</button> 
								                		</td>
								                	</tr>
								                	<tr ng-show="!selectedInvoices.length>0">
								                		<td colspan="6" class="text-center">
								                			<h4>Nothing Selected</h4>
								                			<p>Select invoices from the invoice list to plan the dispatch</p>
								                		</td>
								                	</tr>
												</tbody>
											</table>  
										    <div class="row" style="margin-top: 10px;">
										    	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
										    		<button type="button" class="btn btn-primary" ng-click="saveDispatch()">Save Dispatch</button>
										    	</div>
										    </div> 
							            </div>	            
									    <div class="overlay" style="display: none;">
							              	<i class="fa fa-refresh fa-spin"></i>
							            </div>
							        </div>
						    	</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" ng-show="errors.length>0">
						<div class="alert alert-warning">			
							<strong>Error!</strong> 
							<p ng-repeat="error in errors">@{{error.msg}}</p>
						</div>
					</div>
				</div>
			</div>
        </div>
    </div>
</div><!-- /.content -->

@stop
@section('js')
<script src="{{asset('assets/angular/angular/angular.min.js')}}"></script>
<script src="{{asset('assets/ng-pagination/dist/paging.min.js')}}"></script>
<script src="{{asset('assets/angular/angular-sortable/src/angular-sortable-view.js')}}"></script>
<script src="{{asset('assets/inspinia/js/plugins/datepicker/bootstrap-datepicker.js')}}"></script>

<script type="text/javascript">


	$(document).ready(function() {
	  	$(".chosen").chosen(); 

	  	$('.datepick').datepicker({format: 'yyyy-mm-dd'});

		$('.vtype').change(function(){
			$('.overlay').show();
			ajaxRequest( '{{url('admin/dispatch/json/vehicle/list')}}' , { 'id' : $(this).val(), 'warehouse_id' : $("#warehouse").val() }, 'get', resultFunc);
		});
	});

	function resultFunc(data){
		$('#vehicle').html("");
		$('#vehicle').append('<option value="" selected>--Select vehicle--</option>');   
		$.each(data,function(key,value){
            $('#vehicle').append('<option value="'+key+'">'+value+'</option>');            
        });
        $('#vehicle').trigger("chosen:updated");
        $('.overlay').hide();
	}

	var app = angular.module('app', ['bw.paging','angular-sortable-view']);

	app.controller('NgController', function NgController($scope) {

		$scope.invoices         = [];
		$scope.selectedInvoices = [];
		$scope.invoice_counts   = [];
		$scope.errors           = [];
		$scope.dispatch         = {};

		$scope.getInvoices = function(page) {
		  $('.overlay').show(); 	
	      $.ajax({
	          url: "{{URL::to('admin/dispatch/json/getInvoices')}}",
	          method: 'GET',
	          data: {'page': page,'customer':$scope.customer,'inv_date':$scope.inv_date,'inv_no':$scope.inv_no,'area':$scope.area},
	          async: true,
	          success: function (data) {
	            $scope.invoices = data;
	            $scope.$apply();
	            $('.overlay').hide();
	            $('.showbox').hide();
	          },
	          error: function () {
	            $('.overlay').hide();
	          }
	      });
	    }

	    $scope.getInvoiceCounts = function() {
	      $.ajax({
	          url: "{{URL::to('admin/dispatch/json/getInvoiceCounts')}}",
	          method: 'GET',
	          async: true,
	          success: function (data) {
	            $scope.invoice_counts = data;
	            $scope.$apply();
	          },
	          error: function () {
	              
	          }
	      });
	    }

		$scope.add = function(item) {
			if($scope.isItemInList(item)<0){
				$scope.selectedInvoices.push(item); 
			}	      
	    }

	    $scope.remove = function(index) {
	      $scope.selectedInvoices.splice(index,1); 
	    }
	    

	    $scope.isItemInList = function (inv) {	
		    for($i=0;$i<$scope.selectedInvoices.length;$i++){
	            if ($scope.selectedInvoices[$i].id == inv.id) {
	                return 1;
	            } 
	        };
		    return -1;	      	
	    }

	    $scope.saveDispatch = function(item) {	    	
	    	if($scope.isValid()){
	    		$('.overlay').show();
	    		$.ajax({
		          url: "{{URL::to('admin/dispatch/json/saveDispatch')}}",
		          method: 'POST',
		          async: true,
		          data: {'dispatch': $scope.dispatch},
		          success: function (data) {
		          	if(data.success){
		          		sweetAlert('Dispatch Created', data.message,0);	
		          		location.reload();
		          	}else{
		          		sweetAlert('Dispatch Create Error', data.message,2);
		          	}
		            $('.overlay').hide();		            
		          },
		          error: function () {
		            $('.overlay').hide();  
		          }
		      });
	    	}
	    }

	    $scope.isValid = function () {
	    	$scope.errors   = [];	       	
	    	$scope.dispatch = {};	       	
	    	if($scope.selectedInvoices.length==0){
	    		$scope.errors.push({'msg':'you have not selected any invoices'});
			}else{
				$scope.dispatch.invoices = $scope.selectedInvoices;
			}

			if(!$('#warehouse').val()){
	    		$scope.errors.push({'msg':'you have not selected any warehouse'});
			}else{
				$scope.dispatch.warehouse = $('#warehouse').val();
			}

			if(!$('#vehicle_type').val()){
	    		$scope.errors.push({'msg':'you have not selected any vehicle type'});
			}else{
				$scope.dispatch.vehicle_type = $('#vehicle_type').val();
			}

			if(!$('#vehicle').val()){
	    		$scope.errors.push({'msg':'you have not selected any vehicle'});
			}else{
				$scope.dispatch.vehicle = $('#vehicle').val();
			}

			if(!$('#location').val()){
	    		$scope.errors.push({'msg':'you have not selected any location'});
			}else{
				$scope.dispatch.location = $('#location').val();
			}

			$scope.dispatch.dispatch_date = $('#dispatch_date').val();

			console.log($scope.dispatch);

			return $scope.errors.length==0;

	    }

		$scope.getInvoices(1);
	});

</script>
@stop
