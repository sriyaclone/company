<tr id="{{$i}}">
    
    <td style="text-align: center">{{$i}}</td>
    
    <td style="text-align: left">
        <a href="{{url('admin/dispatch/detail')}}/{{$result_val->id}}" class="btn btn-xs btn-default" title="View Dispatch Details">#{{ $result_val->dispatch_no }}</a>
    </td>
    
    <td style="text-align: center">{{$result_val->dispatch_date}}</td>
    
    <td style="text-align: center">
        @if($result_val->type==COMPANYDELIVERY)
            By Company
        @elseif($result_val->type==COURIERDELIVERY)
            By Courier
        @else
            On Premises
        @endif
    </td>  
    
    
    <td style="text-align: center">
        @if($result_val->route)
            {{$result_val->route->name}}
        @endif
    </td>
    
    <td style="text-align: center">
        @if($result_val->status==DISPATCH_PENDING)
            Pending
        @elseif($result_val->status==DISPATCH_PICKED)
            Picked
        @elseif($result_val->status==DISPATCH_BAYIN)
            Bay In
        @elseif($result_val->status==DISPATCH_LOADED)
            Loaded
        @elseif($result_val->status==DISPATCH_CHECKOUT)
            Checkout
        @elseif($result_val->status==DISPATCH_CHECKIN)
            Checkin
        @elseif($result_val->status==DISPATCH_CANCEL)
            Cancel
        @endif
    </td>    
    <td style="text-align: center">{{date_format($result_val->created_at, 'D d M-Y h:i:s')}}</td>    
    <td style="text-align: center">
        <a href="{{ url('admin/dispatch/detail') }}/{{$result_val->id}}"><i class="fa fa-th" aria-hidden="true"></i></a>
    </td>
    <td style="text-align: center">
        <a href="{{ url('admin/dispatch/edit') }}/{{$result_val->id}}"><i class="fa fa-pencil-square" aria-hidden="true"></i></a>
    </td>
    <td style="text-align: center">
        @if($result_val->status==DISPATCH_LOADED || $result_val->status==DISPATCH_CHECKOUT || $result_val->status==DISPATCH_CHECKIN)
            <a href="{{ url('admin/dispatch/listdetail') }}/{{$result_val->id}}"><i class="fa fa-print" aria-hidden="true"></i></a>
        @endif
    </td>
</tr>

