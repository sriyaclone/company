<?php

Route::group(['middleware' => ['auth']], function()
{

	Route::group(['prefix'=>'admin/dispatch/','namespace' => 'App\Modules\AdminDispatchManage\Controllers'],function() {

	    Route::get('list', [
	        'as' => 'dispatch.index', 'uses' => 'AdminDispatchManageController@index'
	    ]);

	    Route::get('note/{id}', [
	        'as' => 'dispatch.index', 'uses' => 'AdminDispatchManageController@dispatchNotePdf'
	    ]);

	    Route::get('detail/{id}', [
	        'as' => 'dispatch.index', 'uses' => 'AdminDispatchManageController@detailView'
	    ]);

	    Route::get('create', [
	        'as' => 'dispatch.create', 'uses' => 'AdminDispatchManageController@create'
	    ]);

	    Route::get('edit/{id}', [
	        'as' => 'dispatch.create', 'uses' => 'AdminDispatchManageController@editView'
	    ]);

	    Route::get('json/vehicle/list', [
	        'as' => 'dispatch.create', 'uses' => 'AdminDispatchManageController@getVehicles'
	    ]);

	    Route::get('json/data/list', [
	        'as' => 'dispatch.create', 'uses' => 'AdminDispatchManageController@getData'
	    ]);

	     Route::get('json/getInvoices', [
	        'as' => 'dispatch.create', 'uses' => 'AdminDispatchManageController@getInvoices'
	    ]);

	    Route::get('json/getInvoiceCounts', [
	        'as' => 'dispatch.create', 'uses' => 'AdminDispatchManageController@getInvoiceCounts'
	    ]);


	    Route::post('json/saveDispatch', [
	        'as' => 'dispatch.create', 'uses' => 'AdminDispatchManageController@saveDispatch'
	    ]);

	    Route::post('json/editDispatch', [
	        'as' => 'dispatch.create', 'uses' => 'AdminDispatchManageController@editDispatch'
	    ]);

	    Route::get('json/is_vehicle', [
	        'as' => 'dispatch.create', 'uses' => 'AdminDispatchManageController@isVehicle'
	    ]);


	});
});