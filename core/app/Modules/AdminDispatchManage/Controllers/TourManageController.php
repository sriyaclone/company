<?php namespace App\Modules\DispatchManage\Controllers;

/**
* Controller class
* @author Sriya <csriyarathne@gmail.com>
* @version 1.0.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Classes\Functions;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Exceptions\TransactionException;

use App\Models\Invoice;
use App\Models\Courier;
use App\Models\Location;
use App\Models\Warehouse;
use App\Models\Bay;
use App\Models\WarehouseEmployee;
use App\Models\WarehouseVehicle;
use App\Modules\AdminVehicleManage\Models\VehicleType;
use App\Modules\AdminVehicleManage\Models\Vehicle;
use Core\EmployeeManage\Models\Employee;
use App\Modules\DispatchManage\Models\DispatchManage;
use App\Modules\DispatchManage\Models\DispatchDetailManage;
use App\Modules\DispatchManage\Models\DispatchLoading;
use App\Modules\DispatchManage\Models\PickingInvoices;
use App\Modules\DispatchManage\Requests\DispatchRequest;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Http\Request;
use App\Models\Area;
use DB;
use Sentinel;
use Pusher\Pusher;
use PDF;
use QrCode;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use LaravelFCM\Message\Topics;
use FCM;

class TourManageController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$user = Sentinel::getUser();
		$dispatch_list = DispatchManage::where('created_by',$user->employee_id)
			->with(['driver','helper','vehicle','details','bay.warehouse','courier','route'])
			->orderBy('created_at','desc')
			->paginate(30);
		
		return view("DispatchManage::tour.index",compact('dispatch_list'));
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function dispatchNotePdf(Request $request,$id)
	{
        
        // $page = view("DispatchManage::tour.dispatch_note")->render();
        $dispatch_details = DispatchManage::where('id',$id)->with(['driver','helper','vehicle','details.invoice.customer','bay.warehouse','courier','route','actionBy'])->get();

		$dispatch_details = $dispatch_details[0];

		$barcode = new \Picqer\Barcode\BarcodeGeneratorPNG();

        $fileName = 'dispatch/'.$dispatch_details->dispatch_no.'-'.date('YmdHis').'.pdf';
        // return view("DispatchManage::tour.dispatch-pdf", compact('dispatch_details','barcode'));
        $page = view("DispatchManage::tour.dispatch-pdf", compact('dispatch_details','barcode'))->render();

		PDF::reset();
		PDF::setMargins(10,10,10);
		PDF::setAutoPageBreak(TRUE, 32);	

		PDF::AddPage();
        PDF::writeHTML($page);
        return PDF::Output('dispatch_note','I');
	}

	public function detailView($id)
	{	
		$dispatch_details = DispatchManage::where('id',$id)->with(['driver','helper','vehicle','details.invoice.customer','bay.warehouse','courier','route','actionBy'])->get();

		$dispatch_details = $dispatch_details[0];

		$barcode = new \Picqer\Barcode\BarcodeGeneratorPNG();
		// $barcode = $barcode->getBarcode('081231723897', $barcode::TYPE_CODE_128);
		
		return view("DispatchManage::detail",compact('dispatch_details','barcode'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		
		$warehouses    = Warehouse::all()->lists('name','id')->prepend('--- Select Warehouse ---','');		
		$vehicle_types = VehicleType::all()->lists('name','id')->prepend('--- Select Vehicle Type ---','');		
		$locations     = Location::where('type_id','5')->lists('name','id')->prepend('--- Select Location ---','');	
		$areas         = Area::all();	
		$bays          = [];		
		$vehicles      = [];		
		$drivers       = [];		
		$helpers       = [];				
		$couriers=Courier::whereNull('deleted_at')
			->select(DB::raw('CONCAT(code," ",name) as name'),'id')
			->lists('name','id')
			->prepend('--- Select Courier ---','');
		
		return view("DispatchManage::tour.create",compact('warehouses','bays','vehicle_types','vehicles','drivers','helpers','locations','couriers','tree','areas'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function editView(Request $request,$id)
	{
		$dispatch = DispatchManage::find($id);
		$aa       = DispatchDetailManage::with(['invoice.customer'])->where('dispatch_note_id',$id)->get();
		$areas    = Area::all();
		$invocies = [];					
		foreach ($aa as $key => $invoice) {
			array_push($invocies,$invoice->invoice);
		}


		$warehouses    = Warehouse::all()->lists('name','id')->prepend('--- Select Warehouse ---','');		
		$vehicle_types = VehicleType::all()->lists('name','id')->prepend('--- Select Vehicle Type ---','');		
		$locations     = Location::where('type_id','5')->lists('name','id')->prepend('--- Select Location ---','');	
		$bays          = [];		
		$vehicles      = [];		
		$drivers       = [];		
		$helpers       = [];				
		$couriers=Courier::whereNull('deleted_at')
			->select(DB::raw('CONCAT(code," ",name) as name'),'id')
			->lists('name','id')
			->prepend('--- Select Courier ---','');
		
		return view("DispatchManage::tour.edit",compact('warehouses','bays','vehicle_types','vehicles','drivers','helpers','locations','couriers','tree','invocies','dispatch','areas'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function editDispatch(Request $request)
	{	

		$dispatch = DispatchManage::find($request->dispatch_id);

		try{
			DB::transaction(function () use ($request,$dispatch) {
				$dispatch->dispatch_date = $request->dispatch['dispatch_date'];
				$dispatch->vehicle_id    = $request->dispatch['vehicle'];
				$dispatch->vehicle_type  = $request->dispatch['vehicle_type'];
				$dispatch->warehouse_id  = $request->dispatch['warehouse'];
				$dispatch->driver_id     = $request->dispatch['driver'];
				$dispatch->helper_id     = $request->dispatch['helper'];
				$dispatch->route_id      = $request->dispatch['location'];
				$dispatch->bay_id        = $request->dispatch['bay'];
				$dispatch->save();

				$dispatch_loading = DispatchLoading::where('dispatch_note_id',$request->dispatch_id)->first();
				$dispatch_loading->vehicle_id =$request->dispatch['vehicle'];
				$dispatch_loading->save();


				$update_invoices  = Invoice::whereIn('id',function($qry) use ($request){
					$qry->select('invoice_id')->from('dms_dispatch_details')
					->where('dispatch_note_id',$request->dispatch_id);
				})->update(['status'=>0]);

				$dispatch_details = DispatchDetailManage::where('dispatch_note_id',$request->dispatch_id);


				if(sizeof($request->dispatch['invoices']) > 0){
					$newDetails = collect($request->dispatch['invoices']);
					$newDetails = $newDetails->pluck('id');
					$dispatch_details = $dispatch_details->pluck('invoice_id');
					$removedInvoices = $dispatch_details->diff($newDetails);
					//$removedInvoices = $removedInvoices->all();

					foreach ($request->dispatch['invoices'] as $value) {
						$inv = Invoice::find($value['id']);

						if($inv){

							$res_dd = DispatchDetailManage::where('dispatch_note_id',$request->dispatch_id)
									->where('invoice_id',$value['id'])->first();

							if(!$res_dd){
								$res_dd = DispatchDetailManage::create([
									'dispatch_note_id' 	=> $dispatch->id,
									'invoice_id' 		=> $inv->id,
									'delivery_date' 	=> $inv->deliver_date,
								]);
							}

							if($res_dd){
								$inv->status=5;
								$inv->save();
							}else{								
								throw new TransactionException('Something wrong. Dispatch detail wasn\'t created', 101);
							}

							$old_pick_inv = PickingInvoices::where('dispatch_note_id',$request->dispatch_id)->where('invoice_id',$value['id'])->first();

							if(!$old_pick_inv){
								$res_pi = PickingInvoices::create([
									'dispatch_note_id' 	=> $dispatch->id,
									'invoice_id' 		=> $inv->id,
									'status' 			=> PICKING_PENDING,
								]);

								if(!$res_pi){
									throw new TransactionException('Something wrong. Dispatch detail wasn\'t created', 102);
								}
							}
						}
					}

					foreach($removedInvoices as $deletedInvoice){
						DispatchDetailManage::where('invoice_id',$deletedInvoice)
							->where('dispatch_note_id',$dispatch->id)->delete();

						PickingInvoices::where('dispatch_note_id',$request->dispatch_id)
							->where('invoice_id',$deletedInvoice)->delete();
					}
				}else{
					throw new TransactionException('Something wrong. Dispatch wasn\'t created', 100);	
				}


			});


			$options = array('cluster' =>CLUSTER,'encrypted' => true);
            $pusher = new Pusher(KEY,SECRET,APP_ID,$options);

            $data['message'] = 'hello booo';
            $pusher->trigger('my-channel', 'my-event', $data);

            $notificationBuilder = new PayloadNotificationBuilder('NEW_JOB_BAY'.$dispatch->bay_id);
            $notificationBuilder->setBody('NEW-JOB')->setSound('default');
            $notification = $notificationBuilder->build();
            $topic = new Topics();
            $topic->topic('NEW_JOB_BAY'.$dispatch->bay_id);
            $topicResponse = FCM::sendToTopic($topic, null, $notification, null);
            $topicResponse->isSuccess();
            $topicResponse->shouldRetry();
            $fcm = $topicResponse->error();

            return response()->json([
            	'success' => true,
            	'message' => 'Dispatch updated successfully! #'.$dispatch->dispatch_no,
            	'fcm'=>$fcm]);
	    } catch (TransactionException $e) {
            return response()->json(['success' => false,'message' => $e->getMessage()]);
        } catch (Exception $e) {
            return response()->json(['success' => false,'message' => $e->getMessage()]);
        }
	}

	/**
	 * get the data of warehouse
	 * Driver/Helper/Bay
	 * @return Response
	 */
	public function getData(Request $request)
	{
		$warehouse_id=$request->id;

		$drivers = Employee::whereIn('id',function($query1)use($warehouse_id){
			$query1->select('employee_id')->from('dms_warehouse_employee')->where('warehouse_id',$warehouse_id);
		})->whereIn('employee_type_id',function($query2){
			$query2->select('id')->from('dms_employee_type')->where('name', 'like', '%Driver%');
		})->select(DB::raw('CONCAT(short_code," ",first_name," ",last_name) as name'),'id')
		->lists('name','id');

		$helpers = Employee::whereIn('id',function($query1)use($warehouse_id){
			$query1->select('employee_id')->from('dms_warehouse_employee')->where('warehouse_id',$warehouse_id);
		})->whereIn('employee_type_id',function($query2){
			$query2->select('id')->from('dms_employee_type')->where('name', 'like', '%Helper%');
		})->select(DB::raw('CONCAT(short_code," ",first_name," ",last_name) as name'),'id')
		->lists('name','id');

		$bays = Bay::where('warehouse_id',$warehouse_id)
				->select(DB::raw('CONCAT(code," ",name) as name'),'id')
				->lists('name','id');

		return ['drivers'=>$drivers, 'helpers'=>$helpers, 'bays'=>$bays];
	}

	/**
	 * get the vehicle of vehicle type
	 *
	 * @return Response
	 */
	public function getVehicles(Request $request)
	{

		$warehouse_id=$request->warehouse_id;
		$type_id=$request->id;

		return $vehicles=Vehicle::whereIn('id',function($query)use($warehouse_id,$type_id){
			$query->select('vehicle_id')->from('dms_warehouse_vehicle')->where('warehouse_id',$warehouse_id);
		})->where('vehicle_type_id',$type_id)
		->select(DB::raw('CONCAT("Plate : ",plate_no," Code : ",code) as name'),'id')
		->lists('name','id');

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function saveDispatch(Request $request)
	{
		$dispatch_no = Functions::dispatchNo();
		$user        = Sentinel::getUser();
		$dispatch    = $request->dispatch;

		try {
            DB::transaction(function () use ($dispatch,$dispatch_no,$user) {

            	$res=DispatchManage::create([
					'dispatch_no'   =>$dispatch_no,
					'dispatch_date' =>$dispatch['dispatch_date'],
					'vehicle_id'    =>$dispatch['vehicle'],
					'vehicle_type'  =>$dispatch['vehicle_type'],
					'warehouse_id'  =>$dispatch['warehouse'],
					'driver_id'     =>$dispatch['driver'],
					'helper_id'     =>$dispatch['helper'],
					'route_id'      =>$dispatch['location'],
					'bay_id'        =>$dispatch['bay'],
					'type'          =>COMPANYDELIVERY,
					'status'        =>DISPATCH_PENDING,
					'created_by'    =>$user->employee_id
				]);

				if($res){

					$res_dl = DispatchLoading::create([
						'dispatch_note_id' 	=> $res->id,
						'vehicle_id' 		=> $res->vehicle_id,
						'status'			=> LOADING_NO
					]);

					if(!$res_dl){
						throw new TransactionException('Something wrong. Dispatch detail wasn\'t created', 103);
					}


					if(sizeof($dispatch['invoices']) > 0){
						foreach ($dispatch['invoices'] as $value) {
							$inv = Invoice::find($value['id']);

							if($inv){
								$res_dd = DispatchDetailManage::create([
									'dispatch_note_id' 	=> $res->id,
									'invoice_id' 		=> $inv->id,
									'delivery_date' 	=> $inv->deliver_date,
								]);

								if($res_dd){
									$inv->status=5;
									$inv->save();
								}else{								
									throw new TransactionException('Something wrong. Dispatch detail wasn\'t created', 101);
								}

								$res_pi = PickingInvoices::create([
									'dispatch_note_id' 	=> $res->id,
									'invoice_id' 		=> $inv->id,
									'status' 			=> PICKING_PENDING,
								]);

								if(!$res_pi){
									throw new TransactionException('Something wrong. Dispatch detail wasn\'t created', 102);
								}

							}
						}
					}else{
						throw new TransactionException('Something wrong. Dispatch wasn\'t created', 100);	
					}
				}else{					
					throw new TransactionException('Something wrong. Dispatch wasn\'t created', 100);
				}
			});

			$options = array('cluster' =>CLUSTER,'encrypted' => true);
            $pusher = new Pusher(KEY,SECRET,APP_ID,$options);

            $data['message'] = 'hello booo';
            $pusher->trigger('my-channel', 'my-event', $data);

    		

            return response()->json(['success' => true,'message' => 'Dispatch added successfully! #'.$dispatch_no]);

        } catch (TransactionException $e) {
            return response()->json(['success' => false,'message' => $e->getMessage()]);
        } catch (Exception $e) {
            return response()->json(['success' => false,'message' => $e->getMessage()]);
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function getInvoices(Request $request)
	{
		$invoices = Invoice::selectRaw('*,0 is_selected')
								->where('status',0)
								->orderBy('area','ASC')
								->orderBy('customer_name','ASC')
								->orderBy('invoice_date','ASC')
								->with(['customer']);

		if($request->inv_no){
			$invoices = $invoices->where('invoice_no','like','%'.$request->inv_no.'%');	
		}

		if($request->inv_date){
			$invoices = $invoices->where('invoice_date','like','%'.$request->inv_date.'%');	
		}

		if($request->area){
			$invoices = $invoices->where('area','like','%'.$request->area.'%');	
		}

		if($request->customer){
			$invoices = $invoices->whereIn('customer_id',function($qry) use($request)
			{
				$qry->select('id')->from('dms_customer')
					->where('first_name','like','%'.$request->customer.'%')
					->orWhere('code','like','%'.$request->customer.'%');
			});	
		}

		return $invoices = $invoices->paginate(20);
	}

	public function getInvoiceCounts()
	{
		return $invoices = Invoice::selectRaw('area,ifnull(count(*),0) as count')
									->where('status',0)
									->groupBy('area')
									->orderBy('invoice_date','ASC')
									->get();
	}


	public function isVehicle(Request $request)
	{
		$is = DispatchManage::where('vehicle_id',$request->id)
								->where('status','>',0)
								->where('bay_id',$request->bay_id)
								->get();

		return response()->json(['is_valid'=>count($is)>0]);	
	}

}
