<?php namespace App\Modules\AdminDispatchManage\Controllers;


/**
* Controller class
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Exceptions\TransactionException;

use App\Classes\Contracts\BaseStockHandler;
use App\Classes\Functions;

use App\Models\Courier;
use App\Models\Location;
use App\Models\Warehouse;
use App\Models\Bay;

use App\Modules\AdminInvoiceManage\Models\Invoice;
use App\Modules\AdminInvoiceManage\Models\InvoiceDetail;
use App\Modules\AdminVehicleManage\Models\VehicleType;
use App\Modules\AdminVehicleManage\Models\Vehicle;
use App\Modules\AdminDispatchManage\Models\DispatchManage;
use App\Modules\AdminDispatchManage\Models\DispatchDetailManage;
use App\Modules\AdminDispatchManage\Models\DispatchLoading;
use App\Modules\AdminDispatchManage\Requests\DispatchRequest;

use App\Modules\AdminDispatchManage\BusinessLogics\DispatchLogic;

use Core\EmployeeManage\Models\Employee;

use DB;
use Sentinel;
use PDF;
use QrCode;

class AdminDispatchManageController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	protected $baseStockHandler;

	public function __construct(BaseStockHandler $baseStockHandler){
		$this->baseStockHandler = $baseStockHandler;
	}

	public function index()
	{
		$user = Sentinel::getUser();
		
		$dispatch_list = DispatchManage::whereNull('deleted_at')->with(['driver','helper','vehicle','details','bay.warehouse','courier','route'])->orderBy('created_at','desc')->paginate(10);
		
		return view("AdminDispatchManage::index",compact('dispatch_list'));
	}

	public function detailView($id)
	{	
		$dispatch_details = DispatchManage::where('id',$id)->with(['driver','helper','vehicle','details.invoice.customer','warehouse','courier','route','actionBy'])->get();

		$dispatch_details = $dispatch_details[0];

		$barcode = new \Picqer\Barcode\BarcodeGeneratorPNG();
		// $barcode = $barcode->getBarcode('081231723897', $barcode::TYPE_CODE_128);
		
		return view("AdminDispatchManage::detail",compact('dispatch_details','barcode'));
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function dispatchNotePdf(Request $request,$id)
	{
        
        // $page = view("DispatchManage::tour.dispatch_note")->render();
        $dispatch_details = DispatchManage::where('id',$id)->with(['vehicle','details.invoice.customer','warehouse','courier','route','actionBy'])->get();

		$dispatch_details = $dispatch_details[0];

		$barcode = new \Picqer\Barcode\BarcodeGeneratorPNG();

        $fileName = 'dispatch/'.$dispatch_details->dispatch_no.'-'.date('YmdHis').'.pdf';
        // return view("DispatchManage::tour.dispatch-pdf", compact('dispatch_details','barcode'));
        $page = view("AdminDispatchManage::dispatch-pdf", compact('dispatch_details','barcode'))->render();

		PDF::reset();
		PDF::setMargins(10,10,10);
		PDF::setAutoPageBreak(TRUE, 32);	

		PDF::AddPage();
        PDF::writeHTML($page);
        return PDF::Output('dispatch_note','I');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		
		$warehouses    = Warehouse::all()->lists('name','id')->prepend('--- Select Warehouse ---','');		
		$vehicle_types = VehicleType::all()->lists('name','id')->prepend('--- Select Vehicle Type ---','');		
		$locations     = Location::where('type_id','5')->lists('name','id')->prepend('--- Select Location ---','');	
		$bays          = [];		
		$vehicles      = [];		
		$drivers       = [];		
		$helpers       = [];				
		$couriers=Courier::whereNull('deleted_at')
			->select(DB::raw('CONCAT(code," ",name) as name'),'id')
			->lists('name','id')
			->prepend('--- Select Courier ---','');
		
		return view("AdminDispatchManage::create",compact('warehouses','bays','vehicle_types','vehicles','drivers','helpers','locations','couriers'));
	}

	public function getInvoices(Request $request)
	{
		$invoices = Invoice::selectRaw('*,0 is_selected')
								->where('status',0)
								->orderBy('invoice_no','ASC')
								->orderBy('invoice_date','ASC')
								->with(['customer']);

		if($request->inv_no){
			$invoices = $invoices->where('invoice_no','like','%'.$request->inv_no.'%');	
		}

		if($request->inv_date){
			$invoices = $invoices->where('invoice_date','like','%'.$request->inv_date.'%');	
		}

		if($request->area){
			$invoices = $invoices->where('area','like','%'.$request->area.'%');	
		}

		if($request->customer){
			$invoices = $invoices->whereIn('customer_id',function($qry) use($request)
			{
				$qry->select('id')->from('customer')
					->where('first_name','like','%'.$request->customer.'%')
					->orWhere('code','like','%'.$request->customer.'%');
			});	
		}

		return $invoices = $invoices->paginate(20);
	}

	public function getInvoiceCounts()
	{
		return $invoices = Invoice::selectRaw('ifnull(count(*),0) as count')
									->where('status',0)
									->orderBy('invoice_date','ASC')
									->get();
	}

	public function getVehicles(Request $request)
	{

		$type_id=$request->id;

		return $vehicles=Vehicle::where('vehicle_type_id',$type_id)
				->select(DB::raw('CONCAT("Plate : ",plate_no," Code : ",code) as name'),'id')
				->lists('name','id');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function saveDispatch(Request $request)
	{
		$dispatch_no = Functions::dispatchNo();
		$user        = Sentinel::getUser();
		$dispatch    = $request->dispatch;

		try {
            DB::transaction(function () use ($dispatch,$dispatch_no,$user) {

            	$res=DispatchManage::create([
					'dispatch_no'   =>$dispatch_no,
					'dispatch_date' =>$dispatch['dispatch_date'],
					'vehicle_id'    =>$dispatch['vehicle'],
					'vehicle_type'  =>$dispatch['vehicle_type'],
					'warehouse_id'  =>$dispatch['warehouse'],
					'route_id'  	=>$dispatch['location'],
					'type'          =>COMPANYDELIVERY,
					'status'        =>DISPATCH_PENDING,
					'created_by'    =>$user->employee_id
				]);

				if($res){

					if(sizeof($dispatch['invoices']) > 0){
						foreach ($dispatch['invoices'] as $value) {
							$inv = Invoice::find($value['id']);

							if($inv){
								$res_dd = DispatchDetailManage::create([
									'dispatch_note_id' 	=> $res->id,
									'invoice_id' 		=> $inv->id,
									'delivery_date' 	=> $inv->deliver_date,
								]);

								if($res_dd){
									$inv->status=5;
									$inv->save();

									$dets = InvoiceDetail::where('invoice_id',$inv->id)->get();
									if(count($dets)>0){
										foreach ($dets as $line_det) {
											$trans = $this->baseStockHandler->stockOut(
								            	$line_det->product_id,
								            	$line_det->qty,
								            	0, 
								            	$dispatch['warehouse'],
								            	$inv->id,
								            	1, 
								            	1
								            );									
										}
									}

								}else{								
									throw new TransactionException('Something wrong. Dispatch detail wasn\'t created', 101);
								}

							}
						}
					}else{
						throw new TransactionException('Something wrong. Dispatch wasn\'t created', 100);	
					}
				}else{					
					throw new TransactionException('Something wrong. Dispatch wasn\'t created', 100);
				}
			});

			$data['message'] = 'hello booo';

            return response()->json(['success' => true,'message' => 'Dispatch added successfully! #'.$dispatch_no]);

        } catch (TransactionException $e) {
            return response()->json(['success' => false,'message' => $e->getMessage()]);
        } catch (Exception $e) {
            return response()->json(['success' => false,'message' => $e->getMessage()]);
        }
	}


}
