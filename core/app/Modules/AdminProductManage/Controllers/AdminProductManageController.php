<?php namespace App\Modules\AdminProductManage\Controllers;


/**
* Controller class
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Modules\AdminProductManage\BusinessLogics\ProductLogic;
use App\Modules\AdminProductCategory\BusinessLogics\ProductCategoryLogic;


use App\Models\SalesOffice;

use Illuminate\Http\Request;
use Excel;	

class AdminProductManageController extends Controller {

	protected $productLogic;
	protected $categoryLogic;

	public function __construct(ProductLogic $productLogic, ProductCategoryLogic $categoryLogic){
		$this->productLogic = $productLogic;
		$this->categoryLogic = $categoryLogic;
	}

	public function addView()
	{
		$categories = $this->categoryLogic->getCategoryList()->prepend('Select a Category', '');

		return view("AdminProductManage::add")->with([
			'categories' => $categories
		]);
	}

	public function add(Request $request)
	{
		$this->validate($request, [
			'code'     	   => 'required|alpha_dash|unique:product,code,NULL,id,deleted_at,NULL',
			'name'    	   => 'required|unique:product,name',
			'category' 	   => 'required',	
			'image' 	   => 'required',
    		'image.*'      => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
			'max_discount' => 'min:0|max:100|numeric',
			'udp'		   => 'min:0|max:100|numeric'
		]);

		try{

			$product = $this->productLogic->saveProduct($request);
			return redirect('admin/product/add')->with([
				'success' => true,
				'success.message' => 'Product added successfully',
				'success.title' => 'Successfull'
			]);

			}catch(\Exception $e){
			return redirect('admin/product/add')->with([
				'error' => true,
				'error.message' => $e->getMessage(),
				'error.title' => 'Error Occured'
			])->withInput();
		}
	}

	public function editView($id)
	{
		try{
			$product = $this->productLogic->getProductById($id);
		}catch(\Exception $e){
			return redirect('admin/product/list')->with([
				'error' => true,
				'error.title' => 'Error Occurred',
				'error.message' => $e->getMessage()
			]);
		}

		$categories = $this->categoryLogic->getCategoryList()->prepend('Select a Category', '');

		return view("AdminProductManage::edit")->with([
			'product' => $product,
			'categories' => $categories
		]);
	}

	public function edit($id, Request $request)
	{
		$this->validate($request, [
			'code' 		   => 'required|alpha_dash|unique:product,code,'.$id.',id,deleted_at,NULL',
			'name' 	       => 'required',
			'category'	   => 'required',
    		'image.*'      => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
			'max_discount' => 'min:0|max:100|numeric',
			'udp' 		   => 'min:0|max:100|numeric'
		]);

		try{
			$product = $this->productLogic->updateProduct($id,$request);

			return redirect('admin/product/list')->with([
				'success' => true,
				'success.message' => 'Product updated successfully',
				'success.title' => 'Successfull'
			]);

		}catch(\Exception $e){
			return redirect('admin/product/edit/'.$id)->with([
				'error' => true,
				'error.message' => $e->getMessage(),
				'error.title' => 'A1-Error Occured'
			])->withInput();
		}
	}

	public function listView(Request $request)
	{
		$old = $request;

		$categories = $this->categoryLogic->getCategoryList()->prepend('All Categories', '');

		$data = $this->productLogic->getRecords($old);

		return view("AdminProductManage::list", compact('old', 'data', 'categories'));
	}

	public function stockView(Request $request)
	{
		$old = $request;

		$categories = $this->categoryLogic->getCategoryList()->prepend('All Categories', '');

		$data = $this->productLogic->getStockRecords($old);

		return view("AdminProductManage::stock", compact('old', 'data', 'categories'));
	}

	public function uploadView(Request $request)
	{
		$categories = $this->categoryLogic->getCategoryList()->prepend('Select a Category', '');

		return view("AdminProductManage::upload")->with(compact('categories'));
	}

	public function upload(Request $request)
	{
		$this->validate($request, [
			'category' => 'required',
			'file' => 'required'
		]);

		set_time_limit(0);
		$data = $this->productLogic->uploadExcel($request->category, $request->file);

		return redirect('admin/product/upload')->with([
			'data' => $data
		]);
	}

	public function delete(Request $request)
	{
		try{
			$this->productLogic->deleteProduct($request->id);
			return response()->json([
				'status' => 1
			]);
		}catch(\Exception $e){
			return response()->json([
				'status' => 0,
				'message' => $e->getMessage()
			]);
		}
	}

	public function syncProduct()
	{
		return $this->productLogic->syncProduct();
	}
}
