@extends('layouts.back_master') @section('title','List Product')
@section('css')
<style type="text/css">
    
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Product Management</h2>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{url('admin/product/list')}}">Product Management</a></li>
            <li class="active">Product List</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a href="{{url('admin/product/add')}}" class="btn btn-warning"><i class="fa fa-plus"></i> Create Product</a>
            <button type="button" class="btn btn-danger" onclick="window.location.href='{{url('admin/product/upload')}}'"><i class="fa fa-upload"></i> Products</button>
            <button type="button" class="btn btn-default" onclick="syncProducts()"><i class="fa fa-refresh"></i> Sync</button>
        </div>
    </div>
</div>

<!-- Content-->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>List Product</h5>
            <div class="ibox-tools">
                
            </div>
        </div>

        <div class="ibox-content" style="display: block;">
            
            <table class="table table-bordered">
                <thead>
                    <form id="form" role="form" method="get">
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">Vertical</th>
                            <th class="text-center">Code <br>
                                <input type="text" class="form-control" placeholder="Enter Code" name="code" value="{{$old->code}}">
                            </th>
                            <th class="text-center">Name<br>
                                <input type="text" class="form-control" placeholder="Enter Name" name="keyword" value="{{$old->keyword}}">
                            </th>
                            <th class="text-center">Category<br>
                                <select name="category" class="form-control chosen">
                                    @if(count($categories) > 0)
                                        @foreach($categories as $key => $value)
                                            <option value="{{$key}}" @if($key == $old->category) selected @endif>{{$value}}</option>
                                        @endforeach
                                    @else
                                        <option value="">All Categories</option>
                                    @endif
                                </select>
                            </th>
                            <th class="text-center">Actions<br>
                                <button class="btn btn-default pull-right" type="submit"><i class="fa fa-search"></i> Filter</button>
                            </th>
                        </tr>
                    </form>
                </thead>
                <tbody>
                    @if(count($data) > 0)
                        @foreach($data as $key => $row)
                            <tr>
                                <td class="text-center">{{ (($data->currentPage()-1)*$data->perPage())+($key+1) }}</td>
                                <td>{{ ($row->product_vertical != '') ? $row->product_vertical : '-' }}</td>
                                <td class="text-center">
                                    <img src="{{asset('core/storage/uploads')}}/{{$row->main_image_url}}" style="width: 50px; height: 50px"/><br>
                                    <strong>{{ ($row->code != '') ? $row->code : '-' }}</strong><br>
                                </td>
                                <td> 
                                    {{ ($row->name != '') ? $row->name : '-' }}
                                </td>
                                <td class="text-center">{{ ($row->category) ? $row->category->name : '-' }}</td>
                                <td class="text-center">
                                    <div class="btn-group">
                                    @if($user->hasAnyAccess(['product.edit', 'admin']))
                                        <button class="btn btn-default" onclick="window.location.href='{{url('admin/product/edit/'.$row->id)}}'"><i class="fa fa-pencil"></i></button>
                                    @else
                                        <button class="btn btn-default" disabled><i class="fa fa-pencil"></i></button>
                                    @endif

                                    @if($user->hasAnyAccess(['product.delete', 'admin']))
                                        <button class="btn btn-default btn-delete" data-id="{{$row->id}}" onclick="deleteData('{{url('admin/product/delete?id='.$row->id)}}');"><i class="fa fa-trash-o"></i></button>
                                    @else
                                        <button class="btn btn-default" disabled><i class="fa fa-trash-o"></i></button>
                                    @endif
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9" align="center"> - No Data to Display - </td>
                        </tr>
                    @endif
                </tbody>
            </table>
            <div class="row">
                <div class="col-sm-12 text-left">
                     Row {{$data->count()}} of {{$data->total()}} 
                </div>
                <div class="col-sm-12 text-right">
                     {!! $data->appends($old->except('page'))->render() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@stop
@section('js')
<script type="text/javascript">
    $(document).ready(function() {
        $(".chosen").chosen();
    });

    function deleteData(_url){
        swal({
            title: "Are you sure?",
            text: "you wanna delete this product?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        },
        function(){
            $.ajax({
                url: _url,
                method: 'get',
                cache: false,
                data: [],
                success: function(response){
                    if(response.status == 1){
                        swal("Done!", "product has been deleted!.", "success");
                        location.reload();
                    }else{
                        swal("Error!", response.message, "error");
                    }
                },
                error: function(xhr){
                    console.log(xhr);
                    swal("Error!", 'Error occurred. Please try again', "error");
                } 
            });
            //window.open(_url, '_self');
        });
    }

    function syncProducts(){
        $.ajax({
            url: "{{URL::to('admin/product/sync')}}",
            method: 'GET',
            cache: false,
            data: [],
            success: function(response){
                if(response.length>0){
                    swal("Done!", "product has been synced!.", "success");
                    location.reload();
                }else{
                    swal("Error!", 'Sync not completed', "error");
                }
            },
            error: function(xhr){
                console.log(xhr);
                swal("Error!", 'Error occurred. Please try again', "error");
            } 
        });
    }
</script>
@stop
