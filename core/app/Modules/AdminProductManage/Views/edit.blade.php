@extends('layouts.back_master') @section('title','Edit Product')
@section('css')
<style type="text/css">

</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Product Management</h2>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{url('admin/product/list')}}">Product Management</a></li>
            <li class="active">Product Edit</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a href="{{url('admin/product/add')}}" class="btn btn-warning"><i class="fa fa-plus"></i> Create Product</a>
            <button type="button" class="btn btn-danger" onclick="window.location.href='{{url('admin/product/upload')}}'"><i class="fa fa-upload"></i> Products</button>
            <button type="button" class="btn btn-warning" onclick="syncProducts()"><i class="fa fa-refresh"></i> Sync</button>
        </div>
    </div>
</div>

<!-- Content-->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>List Product</h5>
            <div class="ibox-tools">
                
            </div>
        </div>

        <div class="ibox-content" style="display: block;">
            <form action="" class="form-horizontal" method="post" enctype="multipart/form-data">
                {!!Form::token()!!}

                <div class="form-group @if($errors->has('category')) has-error @endif">
                    <label for="" class="col-sm-2 control-label required">Product Category</label>
                    <div class="col-sm-10">
                        <select name="category" class="form-control chosen">
                            @if(count($categories) > 0)
                                @foreach($categories as $key => $value)
                                    <option value="{{$key}}" @if($key == $product->product_category_id) selected @endif>{{$value}}</option>
                                @endforeach
                            @else
                                <option value="">Select a Category</option>
                            @endif
                        </select>
                        @if($errors->has('category'))
                            <span class="help-block">{{$errors->first('category')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group @if($errors->has('code')) has-error @endif">
                    <label for="" class="col-sm-2 control-label required">Short Code</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="code" value="{{$product->code}}">
                        @if($errors->has('code'))
                            <span class="help-block">{{$errors->first('code')}}</span>
                        @endif
                    </div>
                </div>
                
                <div class="form-group @if($errors->has('name')) has-error @endif">
                    <label for="" class="col-sm-2 control-label required">Product Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="name" value="{{$product->name}}">
                        @if($errors->has('name'))
                            <span class="help-block">{{$errors->first('name')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group @if($errors->has('description')) has-error @endif">
                    <label for="" class="col-sm-2 control-label">Product Description</label>
                    <div class="col-sm-10">
                        <textarea name="description" class="form-control" rows="10">{{$product->description}}</textarea>
                        @if($errors->has('description'))
                            <span class="help-block">{{$errors->first('description')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group @if($errors->has('image')) has-error @endif">
                    <label for="" class="col-sm-2 control-label">Product image</label>
                    <div class="col-sm-10">
                        {!! Form::file('image') !!}
                        <img src= "{{asset('core/storage/uploads')}}/{{$product->main_image_url}}" style="width: 50px; height: 50px;"/>
                         @if($errors->has('image'))
                            <span class="help-block">{{$errors->first("image")}}</span>
                         @endif
                    </div>
                </div>

                <div class="form-group @if($errors->has('vat')) has-error @endif">
                    <label for="" class="col-sm-2 control-label required">VAT</label>
                    <div class="col-sm-10">
                        <select name="vat" class="form-control chosen">
                            <option value="0" @if($product->is_vat == 0) selected @endif>No</option>
                            <option value="1" @if($product->is_vat == 1) selected @endif>Yes</option>
                        </select>
                        @if($errors->has('vat'))
                            <span class="help-block">{{$errors->first('vat')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group @if($errors->has('max_discount')) has-error @endif">
                    <label for="" class="col-sm-2 control-label">Max Discount (%)</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" name="max_discount" value="{{$product->max_discount}}">
                        @if($errors->has('max_discount'))
                            <span class="help-block">{{$errors->first('max_discount')}}</span>
                        @endif
                    </div>
                </div>
                
                <div class="form-group @if($errors->has('udp')) has-error @endif">
                    <label for="" class="col-sm-2 control-label">UDP (%)</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" name="udp" value="{{$product->udp}}">
                        @if($errors->has('udp'))
                            <span class="help-block">{{$errors->first('udp')}}</span>
                        @endif
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-success pull-right"><i class="fa fa-floppy-o"></i> Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@stop
@section('js')
<script type="text/javascript">
    $(document).ready(function() {
        $(".chosen").chosen();
    });
</script>
@stop
