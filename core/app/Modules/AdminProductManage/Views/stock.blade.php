@extends('layouts.back_master') @section('title','List Stock')
@section('css')

<style type="text/css">
  
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Product Management</h2>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{url('admin/product/list')}}">Product Management</a></li>
            <li class="active">Stock List</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a class="btn btn-primary" href="{{ url('admin/product/list') }}"><i aria-hidden="true" class="fa fa-th"></i> Product List</a>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Mrp List</h5>
            <div class="ibox-tools">
                
            </div>
        </div>

        <div class="ibox-content" style="display: block;">
            
            <table class="table table-bordered">
                <thead>
                    <form method="get">
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-Left">Short Code <br>
                                <input type="text" class="form-control" placeholder="Enter Code" name="code" value="{{$old->code}}">
                            </th>
                            <th class="text-left">Name<br>
                                <input type="text" class="form-control" placeholder="Enter Name" name="keyword" value="{{$old->keyword}}">
                            </th>
                            <th class="text-left">Category <br>
                                <select name="category" class="form-control chosen">
                                    @if(count($categories) > 0)
                                        @foreach($categories as $key => $value)
                                            <option value="{{$key}}" @if($key == $old->category) selected @endif>{{$value}}</option>
                                        @endforeach
                                    @else
                                        <option value="0">All Categories</option>
                                    @endif
                                </select>
                            </th>
                            <th class="text-right">Stock</th>
                            <th class="text-right">Action <br>
                                <button class="btn btn-default pull-right" type="submit"><i class="fa fa-search"></i> Filter</button>
                            </th>
                        </tr>
                    </form>
                </thead>
                <tbody>
                    @if(count($data) > 0)
                        @foreach($data as $key => $row)
                            <tr>
                                <td class="text-center">{{ (($data->currentPage()-1)*$data->perPage())+($key+1) }}</td>
                                <td class="text-left">{{ ($row->code != '') ? $row->code : '-' }}</td>
                                <td class="text-left">{{ ($row->name != '') ? $row->name : '-' }}</td>
                                <td class="text-left">{{ ($row->category) ? $row->category->name : '-' }}</td>
                                <td class="text-right">{{ ($row->stock != '') ? $row->stock : '-' }}</td>
                                <td class="text-right"></td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="8" align="center"> - No Data to Display - </td>
                        </tr>
                    @endif
                </tbody>
            </table>
            
            <div class="row">
                <div class="col-sm-12 text-left">
                    Row {{$data->lastItem()}} of {{$data->total()}} 
                </div>
                <div class="col-sm-12 text-right">
                    {!! $data->appends($old->except('page'))->render() !!}
                </div>
            </div>

        </div>
    </div>
</div>
@stop
@section('js')

    <script type="text/javascript">
        $(document).ready(function() {
            $(".chosen").chosen();
        });

        function deleteData(_url){
            swal({
                title: "Are you sure?",
                text: "you wanna delete this product?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function(){
                $.ajax({
                    url: _url,
                    method: 'get',
                    cache: false,
                    data: [],
                    success: function(response){
                        if(response.status == 1){
                            swal("Done!", "product has been deleted!.", "success");
                            location.reload();
                        }else{
                            swal("Error!", response.message, "error");
                        }
                    },
                    error: function(xhr){
                        console.log(xhr);
                        swal("Error!", 'Error occurred. Please try again', "error");
                    } 
                });                
            });
        }
    </script>
@stop
