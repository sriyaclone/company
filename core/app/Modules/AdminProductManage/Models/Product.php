<?php namespace App\Modules\AdminProductManage\Models;

/**
*
* Model
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Product extends Model {

	use SoftDeletes;

	protected $table = 'product';

	protected $dates = ['deleted_at'];

	protected $guarded = array('id');

	public function category(){
		return $this->belongsTo('App\Modules\AdminProductCategory\Models\AdminProductCategory', 'product_category_id', 'id')
			->withTrashed();
	}

	public function sales_office(){
		return $this->belongsTo('App\Models\SalesOffice', 'sales_office_id', 'id')
			->withTrashed();
	}

	public function mrpprice(){
		return $this->belongsTo('App\Modules\AdminMRP\Models\AdminMRP','id','product_id')
			->whereNull('ended_at')
			->withTrashed();
	}

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function stock(){
        return $this->hasMany('App\Models\Stock','product_id','id')->where('qty','>',0);

    }

}
