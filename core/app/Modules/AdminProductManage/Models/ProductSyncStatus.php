<?php namespace App\Modules\AdminProductManage\Models;

/**
*
* Model
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ProductSyncStatus extends Model {

	use SoftDeletes;

	protected $table = 'product_sync_status';

	protected $dates = ['deleted_at'];

	protected $guarded = array('id');

}
