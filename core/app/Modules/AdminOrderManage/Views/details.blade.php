@extends('layouts.back_master') @section('title','Order Details')
@section('css')
<style type="text/css">
    
</style>
@stop
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Order Management</h2>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{url('admin/order/list')}}">Order Management</a></li>
            <li class="active">Order List</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a class="btn btn-primary" href="{{ url('admin/order/list') }}"><i class="fa fa-th" aria-hidden="true"></i> Order</a>
            <a class="btn btn-warning" href="{{ url('admin/order/create') }}"><i class="fa fa-plus" aria-hidden="true"></i> Order</a>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Order Details</h5>
            <div class="ibox-tools">
                
            </div>
        </div>

        <div class="ibox-content" style="display: block;">
            <div class="row">
                <div class="col-md-12">
                    <strong>
                        <h3 class="text-center">
                            Purchase Order
                        </h3>
                    </strong>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <h3 style="margin-top: 2px;margin-bottom: 0px;">{{ $list->reference_no?:'-' }}</h3>
                    <h4 style="margin-bottom: 0;margin-top: 8px;">{{ $list->supplier_name?:'-' }}</h4>
                    <h5 style="margin-top: 5px;">{{ $list->supplier_code?:'-' }}</h5>
                </div>
                <div class="col-md-6">
                    <h4 class="text-right" style="margin-bottom: 0">
                        {{ ($list->created_at)?date('jS M, Y',strtotime($list->created_at)):'-' }}
                    </h4>
                    <h5 class="text-right" style="margin-top:3px;margin-bottom: 0;">
                        {{ $list->action_by?:'-' }}
                    </h5>
                    <h5 class="text-right" style="margin-top:3px;margin-bottom: 0;">
                        {{ $list->warehouse_name?:'-' }}
                    </h5>
                    <h5 class="text-right" style="margin-top: 8px;">
                        @if(ORDER_PENDING == $list->status)
                            <span class="text-warning">Pending</span>
                        @elseif(ORDER_APPROVED == $list->status)
                            <span class="text-success">Approved</span>
                        @elseif(ORDER_GRN == $list->status)
                            <span class="text-success">GRN Done</span>
                        @elseif(ORDER_REJECTED == $list->status)
                            <span class="text-danger">Rejected</span>
                        @elseif(ORDER_PAYMENT_DONE == $list->status)
                            <span class="text-primary">Paid</span>
                        @elseif(ORDER_PAYMENT_PARTIAL == $list->status)
                            <span class="text-success">Partialy Paid</span>
                        @elseif(ORDER_DELIVERED == $list->status)
                            <span class="text-success">Delivered</span>
                        @else
                            -
                        @endif
                    </h5>
                  
                    @if($list->status != ORDER_PENDING && $list->status != ORDER_REJECTED)
                        <div class="text-right">
                            <a href="{{url('admin/order/order_pdf/')}}/{{$list->id}}" target="_blank"><i class="fa fa-print"></i> PDF</a>
                        </div>
                    @endif
                </div>
            </div>

            <div class="row">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-left" width="15%">Product Code</th>
                            <th class="text-left">Product</th>
                            <th class="text-right">Mrp (Rs)</th>
                            <th class="text-right">Qty</th>
                            <th class="text-right">Amount (Rs)</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($list->details) > 0)
                            @foreach($list->details as $key => $detail)
                                <tr>
                                    <td>{{ (++$key) }}</td>
                                    <td>{{ $detail->productWithTrash->code?:'-' }}</td>
                                    <td>{{ $detail->productWithTrash->name?:'-' }}</td>
                                    <td align="right">{{ number_format($detail->mrp, 2)?:'-' }}</td>
                                    <td align="right">{{ number_format($detail->qty, 2)?:'-' }}</td>
                                    <td align="right">{{ number_format($detail->net_amount, 2)?:'-' }}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="5" align="right">Amount</td>
                                <td align="right">{{ number_format($list->details->sum('net_amount'), 2)?:'-' }}</td>
                            </tr>                            

                            <tr>
                                <td colspan="5" align="right"><strong>Total Amount</strong></td>
                                <td align="right"><strong>{{ number_format(
                                    $list->details->sum('net_amount'), 2)?:'-' }}</strong>
                                </td>
                            </tr>
                        @else
                        <tr>
                            <td colspan="9" align="center"> - No Data to Display - </td>
                        </tr>
                    @endif  
                    </tbody>
                </table>
            </div>

        </div>

    </div>
</div>

@stop
@section('js')

<script type="text/javascript">
$(document).ready(function() {
  $(".chosen").chosen();

  $('.datepick').datepicker({
      keyboardNavigation: false,
      forceParse: false,
      format: 'yyyy-mm-dd'
  });
});
</script>
@stop
