@extends('layouts.back_master') @section('title','List Order')
@section('css')
<style type="text/css">
    
</style>
@stop
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Order Management</h2>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{url('admin/grn/list')}}">Order Management</a></li>
            <li class="active">Order List</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a class="btn btn-warning" href="{{ url('admin/order/create') }}"><i class="fa fa-plus" aria-hidden="true"></i> Order</a>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Order List</h5>
            <div class="ibox-tools">
                
            </div>
        </div>

        <div class="ibox-content" style="display: block;">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th class="text-left">Reference No</th>
                        <th class="text-left">Supplier</th>
                        <th class="text-left">Warehouse</th>
                        <th class="text-left">Created Date</th>
                        <th class="text-right">Amount (Rs)</th>
                        <th class="text-right">Action By</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($data) > 0)
                        @foreach($data as $key => $val)
                            <tr>
                                <td align="center">
                                    {{ (($data->currentPage()-1)*$data->perPage())+($key+1) }}
                                </td>
                                <td align="left">
                                    {{$val->reference_no}}
                                </td>
                                <td align="left">
                                    {{$val['supplier']->name}}<br>{{$val['supplier']->code}}
                                </td>
                                <td align="left">
                                    {{$val['warehouse']->name}}<br>{{$val['warehouse']->code}}
                                </td>
                                <td align="left">
                                    {{$val->created_at}}
                                </td>
                                <td align="right">
                                    {{number_format($val->total_amount,2)}}
                                </td>
                                <td align="right">
                                    {{$val['createdBy']->first_name}} {{$val['createdBy']->last_name}}
                                </td>
                                <td align="center">
                                    @if($val->status==ORDER_PENDING)
                                        @if($user->hasAnyAccess(['order.approve', 'admin']))
                                            <button data-id="{{$val->id}}" data-status="1" class="btn btn-default btn-xs approve_order" data-toggle="tooltip" data-placement="top" title="Approve Order"><i class="fa fa-check-circle" style="color: green"></i> Approve</button>
                                        @else
                                            <button class="disabled btn btn-default btn-xs approve_order" data-toggle="tooltip" data-placement="top" title="Approve Disabled"><i class="fa fa-check-circle"></i></button>
                                        @endif
                                    
                                        @if($user->hasAnyAccess(['order.reject', 'admin']))
                                            <button data-id="{{$val->id}}" data-status="1" class="btn btn-default btn-xs reject_order" data-toggle="tooltip" data-placement="top" title="Reject Order"><i class="fa fa-times" style="color: red"> Reject</i></button>
                                        @else
                                            <button class="disabled btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Reject Disabled"><i class="fa fa-times"></i></button>
                                        @endif

                                    @elseif($val->status==ORDER_APPROVED)
                                    
                                        @if($user->hasAnyAccess(['order.grn', 'admin']))
                                            <button data-id="{{$val->id}}" data-status="1" class="btn btn-primary btn-xs grn_order" data-toggle="tooltip" data-placement="top" title="GRN Order"><i class="fa fa-save"></i> Create GRN</button>
                                        @else
                                            <button class="disabled btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="GRN Disabled"><i class="fa fa-check-circle"></i></button>
                                        @endif

                                    @elseif($val->status==ORDER_GRN)
                                            
                                            <label class="label label-success">GRN ISSUED</label>

                                    @elseif($val->status==ORDER_PAYMENT_DONE)
                                            
                                            <label class="label label-success">Payement Collected</label>

                                    @elseif($val->status==ORDER_PAYMENT_PARTIAL)
                                            
                                            <label class="label label-warning">Partially Paid</label>

                                    @elseif($val->status==ORDER_DELIVERED)
                                            
                                            <label class="label label-success">Delivered</label>

                                    @elseif($val->status==ORDER_REJECTED)
                                            
                                            <label class="label label-danger">Rejected</label>
                                        
                                    @endif
                                </td>
                                <td align="center">
                                    <a href="{{ url('admin/order/listdetail') }}/{{$val->id}}"><i class="fa fa-th" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9" align="center"> - No Data to Display - </td>
                        </tr>
                    @endif  
                </tbody>
            </table>
      
            <div class="row">
                <div class="col-sm-12 text-left">
                     Row {{$data->count()}} of {{$data->total()}} 
                </div>
                <div class="col-sm-12 text-right">
                    {!! $data->appends($old->except('page'))->render() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@stop
@section('js')

<script type="text/javascript">
    $(document).ready(function() {
        
        $(".chosen").chosen();

        $('.datepick').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            format: 'yyyy-mm-dd'
        });

        $('.approve_order').click(function(){
            id = $(this).data('id');
            status = $(this).data('status');
            $.ajax({
                url: "{{URL::to('admin/order/approve-order')}}",
                method: 'POST',
                data: { 'order_id' : id, 'status' : status},
                async: true,
                success: function (data) {
                    if(data == 1){
                        swal('Success!','Purchase Order Approved','success');
                        window.location.reload();
                    }else if(data = 0){
                        swal('Warning!','Purchase order cannot approved. Contact Admin','error');
                    }
                },
                error: function () {
                  
                }
            });
        });

        $('.reject_order').click(function(){
            id = $(this).data('id');
            status = $(this).data('status');
            $.ajax({
                url: "{{URL::to('admin/order/reject-order')}}",
                method: 'POST',
                data: { 'order_id' : id, 'status' : status},
                async: true,
                success: function (data) {
                    if(data == 1){
                        swal('Success!','Purchase Order Rejected','success');
                        window.location.reload();
                    }else if(data = 0){
                        swal('Warning!','Purchase order cannot reject. Contact Admin','error');
                    }
                },
                error: function () {
                  
                }
            });
        });

        $('.grn_order').click(function(){
            id = $(this).data('id');
            status = $(this).data('status');
            $.ajax({
                url: "{{URL::to('admin/order/grn-order')}}",
                method: 'POST',
                data: { 'order_id' : id, 'status' : status},
                async: true,
                success: function (data) {
                    console.log(data);
                    // if(data == 1){
                    //     swal('Success!','GRN Created','success');
                    //     window.location.reload();
                    // }else if(data = 0){
                    //     swal('Warning!','Cannot create GRN. Contact Admin','error');
                    // }
                },
                error: function () {
                  
                }
            });
        });

    });
</script>
@stop
