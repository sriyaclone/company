<?php


Route::group(['middleware' => ['auth']], function(){

	Route::group(array('prefix'=>'admin/order/','module' => 'AdminOrderManage', 'namespace' => 'App\Modules\AdminOrderManage\Controllers'), function() {

		/*** GET Routes**/

		Route::get('list', [
	      'as' => 'order.list', 'uses' => 'AdminOrderManageController@listView'
	    ]);

	    Route::get('create', [
	      'as' => 'order.create', 'uses' => 'AdminOrderManageController@createView'
	    ]);

	    Route::get('order_pdf/{id}', [
	      'as' => 'order.view', 'uses' => 'AdminOrderManageController@orderPdf'
	    ]);

		Route::get('listdetail/{id}', [
		  'as' => 'order.list.detail', 'uses' => 'AdminOrderManageController@listViewDetail'
	    ]);

	    Route::get('view/{id}', [
		  'as' => 'order.view', 'uses' => 'AdminOrderManageController@orderView'
	    ]);

		/***JSON Routes**/
        Route::get('getItems', [
            'as' => 'order.create', 'uses' => 'AdminOrderManageController@getItems' 
        ]);

        Route::get('getWarehouses', [
            'as' => 'order.create', 'uses' => 'AdminOrderManageController@getWarehouses' 
        ]);

        Route::get('getOrderWithDetails', [
            'as' => 'order.revise', 'uses' => 'AdminOrderManageController@getOrderWithDetails' 
        ]);

		/***POST Routes**/

        Route::post('edit', [
        	'as' => 'order.edit', 'uses' => 'AdminOrderManageController@edit' 
        ]);

        Route::post('create', [
        	'as' => 'order.create', 'uses' => 'AdminOrderManageController@create' 
        ]);

        Route::post('approve-order', [
        	'as' => 'order.approve', 'uses' => 'AdminOrderManageController@approveOrder'
        ]);

        Route::post('reject-order', [
        	'as' => 'order.reject', 'uses' => 'AdminOrderManageController@rejectOrder'
        ]);

        Route::post('grn-order', [
        	'as' => 'order.to-grn', 'uses' => 'AdminOrderManageController@grnOrder'
        ]);

	}); 

}); 