<?php namespace App\Modules\AdminOrderManage\BusinessLogics;

/**
* Business Logics 
* Define all the busines logics in here
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use Illuminate\Database\Eloquent\Model;
use Core\EmployeeManage\Models\Employee;

use App\Classes\Functions;
use App\Classes\Contracts\BaseStockHandler;

use App\Models\User;
use App\Models\DeliveryTerms;
use App\Models\Warehouse;
use App\Models\WarehouseType;

use Core\UserRoles\Models\UserRole;

use App\Modules\AdminProductManage\Models\Product;
use App\Modules\AdminOrderManage\Models\Order;
use App\Modules\AdminOrderManage\Models\OrderDetail;
use App\Modules\AdminSupplierManage\Models\Supplier;

use App\Modules\AdminGrnManage\BusinessLogics\GrnLogic;

use DB;
use Excel;
use File;
use Response;
use Sentinel;
use PDF;
use QrCode;

class OrderLogic extends Model {

	private $order;
	private $orderDetail;
	private $functions;
	private $baseStockHandler;
	private $grnLogic;

	public function __construct(Order $order , OrderDetail $orderDetail,Functions $functions,BaseStockHandler $baseStockHandler,GrnLogic $grnLogic){
		$this->order 			= $order;
		$this->orderDetail 		= $orderDetail;
		$this->functions 		= $functions;
		$this->baseStockHandler = $baseStockHandler;
		$this->grnLogic 		= $grnLogic;
	}

	public function getOrderRecords($filters, $perPage = 10)
	{
		$data = Order::with(['createdBy','supplier','warehouse']);

		if($filters->from!="" && $filters->to!=""){
			$data=$data->whereBetween('order.order_date', array($filters->from, $filters->to));
		}

		if($filters->reference != ""){
			$data->where('order.reference_no','LIKE',$filters->reference);
		}

		if($filters->status != ""){
			$data->where('order.status','=',$filters->status);
		}

		$data = $data->orderBy('order.id','desc');
		return $data->paginate($perPage);
	}	

	public function getOrderDetailRecords($id)
	{
		$data = OrderDetail::with(['productWithTrash'])->where('order_id','=',$id)->get();

        return $data;
	}

	public function getRecordsOrderId($id)
	{
		return $data = Order::with(['supplier'])->where('id',$id)->first();
	}

	public function getRecordsOrderDetailId($id)
	{
		return $data = OrderDetail::with(['product'])->where('order_id',$id)->get();
	}

	
	public function getDeliveryTerms()
	{
		return $data = DeliveryTerms::lists('name','id');
	}

	public function getItems($limit,$search)
	{
		$data = Product::where('code','like','%'.$search.'%')->orWhere('name','like','%'.$search.'%')->whereNull('deleted_at');

        return $data = $data->paginate($limit);
	}

	public function getWarehouseTypes()
	{
		$data = WarehouseType::whereNull('deleted_at')->selectRaw('id,name')->get()->prepend(['id'=>'','name'=>'Select a Warehouse Type']);

        return $data;
	}

	public function getWarehouses()
	{
		$data = Warehouse::whereNull('deleted_at')->selectRaw('id,name')->get()->prepend(['id'=>'','name'=>'Select a Warehouse']);

        return $data;
	}

	public function getSuppliers()
	{
		$data = Supplier::whereNull('deleted_at')->selectRaw('id, CONCAT(code,\'-\',name) as name')->get()->prepend(['id'=>'','name'=>'Select a Supplier']);

		return $data;
	}

	public function getOrderTypes()
	{
		$data = GrnType::whereNull('deleted_at')->selectRaw('id,name')->get()->prepend(['id'=>'','name'=>'Select a Grn Type']);

		return $data;
	}

	public function getAllItems($limit)
	{
		$data = Product::whereNull('deleted_at');

		return $data = $data->paginate($limit);
	}

	public function createNewOrder($data)
	{

		$data->all();

		$user = Sentinel::getUser();

		$action_by = $user->employee_id;	
		
		$order = Order::create([
			'supplier_id'         => $data->supplier,
			'reference_no'        => null,
			'warehouse_id'        => $data->warehouse,
			'order_date'          => date('Y-m-d'),
			'status'              => ORDER_PENDING,
			'remarks' 			  => $data->remark,
			'action_by'           => $action_by
        ]);

		$total=0;
		
        foreach ($data->product_id as $key => $details) {

            $order_details = OrderDetail::create([
				'order_id'   			=> $order->id,
				'product_id'     		=> $data->product_id[$key],
				'qty'            		=> $data->qty[$key],
				'mrp'          			=> $data->mrp[$key],
				'net_amount'            => $data->line_total_amount[$key]
            ]);

            $total += $data->line_total_amount[$key];
	        
	    }

	    $code = "OD"."_".$data->supplier."_".$order->id;
	    $order->total_amount = $total;
		$order->reference_no = $code;
		$order->save();

	    return $order;
	}

	public function getOrderPdfOutput($id, $type='N')
	{
		$order = $this->getOrderById($id);

        $qr = QrCode::format('png')
        		->errorCorrection('L')
        		->size(200)
        		->margin(2)
        		->generate($order->reference_no.'|'.$order->total_amount); 

        $fileName = 'order/order-'.$order->id.'-'.date('YmdHis').'.pdf';
        $page = view("AdminOrderManage::order-pdf", compact('order','qr'))
        		->render();
		PDF::reset();
		//PDF::setFont('myriadpro');
		PDF::setMargins(10,18,10);
		PDF::setAutoPageBreak(TRUE, 32);
		PDF::setHeaderCallback(function($pdf){
			$image_file = url('assets/pi-images/h-img02.png');
        	$pdf->Image($image_file, 10, 5, 195, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		});

		PDF::setFooterCallback(function($pdf){
			$image_file = url('assets/pi-images/f-img01.png');
        	$pdf->Image($image_file, 10, 265, 195, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		});
        PDF::AddPage();
        PDF::writeHTML($page);
        if (ob_get_contents()) ob_end_clean();
        return PDF::Output(public_path($fileName),'I');
	}

	public function getOrderById($id)
	{
		try {
			$data = Order::select('*', 
					DB::raw('order.id AS id'),
					DB::raw('order.reference_no as reference_no'),
					DB::raw('warehouse.name as warehouse_name'),
					DB::raw('supplier.name as supplier_name'),
					DB::raw('supplier.code as supplier_code'),
					DB::raw('order.status AS order_status'),
					DB::raw('concat(employee.first_name," ",employee.last_name) AS action_by'))
				->join('supplier','supplier.id','=','order.supplier_id')
				->join('warehouse','warehouse.id','=','order.warehouse_id')
				->join('employee','employee.id','=','order.action_by')
				->where('order.id', $id)
				->orderBy('order.id','asc')
				->with(['details.product','details.productWithTrash','createdBy'])
				->first();

		    return $data;	

		} catch (\Exception $e) {
			return $e->getMessage();
		}
	}

	public function approveOrder($order_id,$status)
	{
		$order = Order::find($order_id);
		if($order){
			$order->status = ORDER_APPROVED;
			if($order->save()){
				return 1;
			}else{
				return 0;
			}
		}
	}

	public function rejectOrder($order_id,$status)
	{
		$order = Order::find($order_id);
		if($order){
			$order->status = ORDER_REJECTED;
			if($order->save()){
				return 1;
			}else{
				return 0;
			}
		}
	}

	public function grnOrder($order_id,$status)
	{
		$order = Order::find($order_id);
		if($order){

			$grn = $this->grnLogic->createGrnUsingOrder($order);

			if($grn->id){
				$order->status = ORDER_GRN;
				if($order->save()){
					return 1;
				}else{
					return 0;
				}				
			}else{
				return 0;
			}

		}
	}

	public function deliverOrder($order_id,$status)
	{
		$order = Order::find($order_id);
		if($order){
			$order->status = ORDER_DELIVERED;
			if($order->save()){
				return 1;
			}else{
				return 0;
			}
		}
	}

	public function finishOrderPayment($order_id,$status)
	{
		$order = Order::find($order_id);
		if($order){
			$order->status = ORDER_PAYMENT_DONE;
			if($order->save()){
				return 1;
			}else{
				return 0;
			}
		}
	}

	public function partialPaymentOrder($order_id,$status)
	{
		$order = Order::find($order_id);
		if($order){
			$order->status = ORDER_PAYMENT_PARTIAL;
			if($order->save()){
				return 1;
			}else{
				return 0;
			}
		}
	}

}