<?php namespace App\Modules\AdminOrderManage\Controllers;


/**
* Controller class
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use Sentinel;
use PDF;
use DB;
use File;
use Mail;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Vat;
use App\Models\User;
use App\Models\Customer;
use App\Models\AppUser;

use Core\EmployeeManage\Models\Employee;

use App\Modules\AdminOrderManage\Requests\OrderRequest;
use App\Modules\AdminOrderManage\BusinessLogics\OrderLogic;

class AdminOrderManageController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	protected $orderLogic;

	public function __construct(OrderLogic $orderLogic){
		$this->orderLogic = $orderLogic;
	}

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listView(Request $request)
	{
		$old = $request;

		$data = $this->orderLogic->getOrderRecords($old);

		return view("AdminOrderManage::list", compact('data','old'));
	}

	public function approveView(Request $request)
	{
		$old = $request;
		$action_by = $this->orderLogic->getEmployees();
		$data = $this->orderLogic->getApprovalQuotations($old);
		return view("AdminOrderManage::approval", compact('data','old','action_by'));
	}

	public function createView(Request $request)
	{
		$warehouses = $this->orderLogic->getWarehouses();

		$suppliers = $this->orderLogic->getSuppliers();

		return view("AdminOrderManage::create",compact('products', 'warehouses', 'suppliers'));
	}

	public function create(Request $request)
	{	
		$order = $this->orderLogic->createNewOrder($request);

		$url = url('admin/order/order_pdf/')."/".$order->id;
		return redirect('admin/order/create')->with(
			[
			'link'           => true,
			'link.link'      => $url,
			'link.linktitle' => ' Show order',
			'link.message'   => 'New Order saved! #('.$order->reference_no.')',
			'link.title'     => 'Success..!'
        ]);
	}

	public function orderPdf(Request $request,$id)
	{
		return  $this->orderLogic->getOrderPdfOutput($id);
	}

	public function listViewDetail($id, Request $request)
	{
		$id        = $request->id;
		$details   = $this->orderLogic->getOrderDetailRecords($id);
		$list      = $this->orderLogic->getOrderById($id);

		return view("AdminOrderManage::details", compact('details', 'list', 'vat'));
	}

	public function show($id, Request $request)
	{
		$user_role = $this->orderLogic->getUserRole();
		$max_discount = ($user_role)?$user_role->discount:0;

		$id      = $request->id;
		$details = $this->orderLogic->getOrderDetailRecords($id);
		$list    = $this->orderLogic->getOrderById($id);

		if($list->quotation_status != PENDING || 
			$max_discount < 1 || 
			$list->approval_role_id != $user_role->id){
			return view('errors.404');
		}

		$vat = Vat::where('id', $list->vat_id)->first();
		$nbt = Vat::where('id', $list->nbt_id)->first();

		if($list->vat_type == 'vat' || $list->vat_type == 'svat'){
			$list = $this->orderLogic->calculateVatCustomerQuotation($list);
		}else{
			$list = $this->orderLogic->calculateNonVatCustomerQuotation($list);
			//$vat = null;
		}

		//return $list;

		/*return view("AdminOrderManage::approvaldetail", compact('data','quotation','quotation_detail','max_discount'));*/

		return view("AdminOrderManage::approve_view", compact('details', 'list', 'vat', 'max_discount', 'nbt'));
	}

	public function getItems(Request $request){
		return $this->orderLogic->getItems(3,$request->search);
	}

	public function getWarehouses(Request $request){
		return $this->orderLogic->getWarehouses();
	}

	public function approveOrder(Request $request){
		return $this->orderLogic->approveOrder($request->order_id,$request->status);
	}

	public function rejectOrder(Request $request){
		return $this->orderLogic->rejectOrder($request->order_id,$request->status);
	}

	public function grnOrder(Request $request){
		return $this->orderLogic->grnOrder($request->order_id,$request->status);
	}

	public function deliverOrder(Request $request){
		return $this->orderLogic->deliverOrder($request->order_id,$request->status);
	}

	public function finishOrderPayment(Request $request){
		return $this->orderLogic->finishOrderPayment($request->order_id,$request->status);
	}

	public function partialPaymentOrder(Request $request){
		return $this->orderLogic->partialPaymentOrder($request->order_id,$request->status);
	}

}