@extends('layouts.back_master') @section('title','Admin - Product Category Management')
@section('current_title','Product Category List')

@section('css')
@stop

@section('content')
<!-- Content-->
<section>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      TAX
      <small>Management</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li>Tax Management</li>
    </ol>
  </section>
  <!-- !!Content Header (Page header) -->

  <!-- Main content -->
  <section class="content">  
    <div class="box box-default">
        <div class="box-body">  
           <form role="form" class="form-horizontal form-validation" method="post">
      		{!!Form::token()!!}
      			<div class="form-group">
            		<label class="col-sm-3 control-label required">VAT Value (%)</label>
            		<div class="col-sm-6">
            			<input type="text" class="form-control @if($errors->has('vat_value')) error @endif" name="vat_value" placeholder="Value [ Do not enter percentage symbol ]" required value="@if(sizeof($vat)>0){{$vat->value}}@endif">
            			@if($errors->has('vat_value'))
            				<label id="label-error" class="error" for="label">{{$errors->first('vat_value')}}</label>
            			@endif
            		</div>
                </div>
				<div class="form-group">
					<div class="col-sm-6 col-md-offset-3">
						<div class="pull-right">
							<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" style="padding-right: 16px;width:13px;"></i> Save</button>
						</div>
					</div>
				</div>
        	</form>
        </div>
    </div>

  </section>
  <!-- !!Main content -->

</section>
<!-- !!!Content -->
@stop

@section('js')
<script type="text/javascript">
    /*$(document).ready(function(){
        $('.delete').click(function(e){
            e.preventDefault();
            id = $(this).data('id');
            $('#form').trigger('submit', [ { 'variable_name': true } ]);
        });
    });*/
</script>
@stop

