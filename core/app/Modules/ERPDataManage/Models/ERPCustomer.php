<?php namespace App\Modules\ERPDataManage\Models;

/**
*
* Model
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;

use DB;

class ERPDataManage extends Model {

	public function getProjectCustomers()
	{

		$sql = 	"SELECT a.t_itbp,
				       q.nam,
				       a.t_cbtp AS cus_group,
				       a.t_cpay AS terms_payment,
				       s.t_pper AS new_credit_days,
				       a.t_crlr AS cr_limit,
				       t.t_fovn
				FROM ttccom112200 a WITH(nolock)
				LEFT OUTER JOIN ttccom001200 b WITH(nolock) ON a.t_ccra =b.t_emno
				INNER JOIN
				  (SELECT a.t_bpid,
				          a.t_nama nam,
				          Sum(c.amt) AS amt,
				          Sum(c.amt2)AS amt2,
				          a.t_stdt,
				          a.t_prst,
				          a.t_crdt
				   FROM ttccom100200 a WITH(nolock)
				   LEFT OUTER JOIN
				     (SELECT b.t_ofbp sbp,
				             Sum(b.t_amth_1)AS amt,
				             Sum(b.t_amth_1)AS amt2,
				      FROM tcisli305555 b WITH(nolock)
				      WHERE b.t_idat BETWEEN Dateadd(MONTH,-7,Getdate()) AND Dateadd(MONTH,-1,Getdate())
				        AND b.t_stat = 6
				      GROUP BY b.t_ofbp
				      UNION SELECT b.t_ofbp sbp,
				                   sum(b.t_amth_1)AS amt,
				                   sum(b.t_amth_1)AS amt2,
				      FROM tcisli205703 b
				      WHERE b.t_idat BETWEEN dateadd(MONTH,-7,getdate()) AND dateadd(MONTH,-1,getdate())
				        AND b.t_stat = 6
				      GROUP BY b.t_ofbp
				      UNION SELECT b.t_ofbp sbp,
				                   sum(b.t_amth_1)AS amt,
				                   sum(b.t_amth_1)AS amt2,
				      FROM tcisli205702 b
				      WHERE b.t_idat BETWEEN dateadd(MONTH,-7,getdate()) AND dateadd(MONTH,-1,getdate())
				        AND b.t_stat = 6
				      GROUP BY b.t_ofbp)c ON a.t_bpid = c.sbp
				   AND a.t_bprl IN (2,
				                    4)
				   GROUP BY a.t_bpid,
				            a.t_nama,
				            a.t_stdt,
				            a.t_prst,
				            a.t_crdt) q ON q.t_bpid = a.t_itbp
				LEFT OUTER JOIN
				  (SELECT t_stbp,
				          max(t_invd)AS linvdate,
				          min(t_invd)AS linvdate2
				   FROM ttdsls406200 WITH(nolock)
				   GROUP BY t_stbp) r ON q.t_bpid = r.t_stbp
				INNER JOIN ttcmcs013200 s WITH(nolock) ON a.t_cpay = s.t_cpay
				LEFT OUTER JOIN ttctax400200 t ON a.t_itbp = t.t_bpid";

		return $data = DB::connection('sqlsrv')->select($sql);
	}

}
