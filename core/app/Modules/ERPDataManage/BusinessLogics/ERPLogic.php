<?php namespace App\Modules\ERPDataManage\BusinessLogics;


/**
* Business Logics 
* Define all the busines logics in here
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use Illuminate\Database\Eloquent\Model;

use DB;
use Response;
use Excel;

use App\Models\Quotation;
use Core\EmployeeManage\Models\Employee;

use App\Modules\AdminQuotationManage\BusinessLogics\QuotationLogic;

class ERPLogic{

	private $csvPath = '/media/LN_SALES_UPLOAD';

	public function getProjectCustomers($page=1, $numRecords=2000){
		$sql = 	"SELECT * FROM (SELECT a.t_itbp,
				       q.nam,
				       a.t_cbtp AS Cus_Group,
				       a.t_cpay AS Terms_Payment,
				       s.t_pper AS New_Credit_Days,
				       a.t_crlr AS Cr_Limit,
				       t.t_fovn AS VAT,
				       q.svt,
				       a.t_cadr,
				       ROW_NUMBER() OVER ( ORDER BY a.t_itbp ) AS RowNum
				FROM ttccom112200 a WITH (nolock)
				     LEFT OUTER JOIN ttccom001200 b WITH (nolock) ON a.t_ccra = b.t_emno
				     INNER JOIN
				(
				    SELECT a.t_bpid,
				           a.t_nama nam,
				           SUM(c.AMT) AS AMT,
				           SUM(c.AMT2) AS AMT2,
				           a.t_stdt,
				           a.t_prst,
				           a.t_crdt,
				           a.t_lgid AS svt
				    FROM ttccom100200 a WITH (nolock)
				         LEFT OUTER JOIN
				    (
				        SELECT b.t_ofbp SBP,
				               SUM(b.t_amth_1) AS AMT,
				               SUM(b.t_amth_1) AS AMT2
				        FROM tcisli305555 b WITH (nolock)
				        WHERE b.t_idat BETWEEN DATEADD(month, -7, GETDATE()) AND DATEADD(month, -1, GETDATE())
				              AND b.t_stat = 6
				        GROUP BY b.t_ofbp
				        UNION
				        SELECT b.t_ofbp SBP,
				               SUM(b.t_amth_1) AS AMT,
				               SUM(b.t_amth_1) AS AMT2
				        FROM tcisli205703 b
				        WHERE b.t_idat BETWEEN DATEADD(month, -7, GETDATE()) AND DATEADD(month, -1, GETDATE())
				              AND b.t_stat = 6
				        GROUP BY b.t_ofbp
				        UNION
				        SELECT b.t_ofbp SBP,
				               SUM(b.t_amth_1) AS AMT,
				               SUM(b.t_amth_1) AS AMT2
				        FROM tcisli205702 b
				        WHERE b.t_idat BETWEEN DATEADD(month, -7, GETDATE()) AND DATEADD(month, -1, GETDATE())
				              AND b.t_stat = 6
				        GROUP BY b.t_ofbp
				    ) c ON a.t_bpid = c.SBP
				           AND a.t_bprl IN(2, 4)
				    GROUP BY a.t_bpid,
				             a.t_nama,
				             a.t_stdt,
				             a.t_prst,
				             a.t_crdt,
				             a.t_lgid
				) q ON q.t_bpid = a.t_itbp
				     LEFT OUTER JOIN
				(
				    SELECT t_stbp,
				           MAX(t_invd) AS linvdate,
				           MIN(t_invd) AS linvdate2
				    FROM ttdsls406200 WITH (nolock)
				    GROUP BY t_stbp
				) r ON q.t_bpid = r.t_stbp
				     INNER JOIN ttcmcs013200 s WITH (nolock) ON a.t_cpay = s.t_cpay
				     LEFT OUTER JOIN ttctax400200 t ON a.t_itbp = t.t_bpid) AS customers WHERE RowNum >= ".($page*$numRecords-$numRecords+1)." 
					AND RowNum <= ".($page*$numRecords)."
				ORDER BY RowNum";

		return $data = DB::connection('sqlsrv')->select($sql);
	}

	public function getAllProduct($page=1, $numRecords=1000){
		/*$sql = "SELECT * FROM 
					(SELECT a.t_item, a.t_dsca,b.t_cvat, ROW_NUMBER() OVER ( ORDER BY a.t_item ) AS RowNum 
						FROM ttcibd001200 a WITH(nolock), 
							ttdisa001200 b WITH(nolock) 
						WHERE a.t_item = b.t_item) AS items
				WHERE RowNum >= ".($page*$numRecords-$numRecords+1)."
					AND RowNum <= ".($page*$numRecords)."
				ORDER BY RowNum";*/

		$sql = "SELECT * FROM (SELECT a.t_citg,c.t_dsca AS item_group, c.t_cdf_mctg AS item_pillar, a.t_item, a.t_dsca,a.t_cuni,b.t_cvat, ROW_NUMBER() OVER ( ORDER BY a.t_item ) AS RowNum FROM ttcibd001200 a WITH(nolock), ttdisa001200 b WITH(nolock), ttcmcs023200 c with(nolock) WHERE a.t_item = b.t_item AND a.t_citg = c.t_citg) AS items WHERE RowNum >= ".($page*$numRecords-($numRecords+1))."
					AND RowNum <= ".($page*$numRecords)."
				ORDER BY RowNum";

		return $data = DB::connection('sqlsrv')->select($sql);
	}

	public function getProduct($code){
		$sql = "SELECT * FROM (SELECT a.t_citg,c.t_dsca AS item_group,a.t_item,a.t_dsca,a.t_cuni,b.t_cvat, ROW_NUMBER() OVER ( ORDER BY a.t_item ) AS RowNum FROM ttcibd001200 a WITH(nolock), ttdisa001200 b WITH(nolock), ttcmcs023200 c with(nolock) WHERE a.t_item = "+$code+") AS items WHERE RowNum >= ".($page*$numRecords-$numRecords+1)."
					AND RowNum <= ".($page*$numRecords)."
				ORDER BY RowNum";

		return $data = DB::connection('sqlsrv')->select($sql);
	}


	public function getProductPageCount($numRecords = 2000){
		$sql = "SELECT COUNT(a.t_citg) AS cc FROM ttcibd001200 a WITH(nolock), ttdisa001200 b WITH(nolock), ttcmcs023200 c with(nolock) WHERE a.t_item = b.t_item AND a.t_citg = c.t_citg";

		$count = DB::connection('sqlsrv')->select($sql);

		return ceil(($count[0]->cc/$numRecords));
	}

	public function getCustomerPageCount($numRecords = 2000){
		$sql = "SELECT count(a.t_itbp) as cc
				FROM ttccom112200 a WITH (nolock)
				     LEFT OUTER JOIN ttccom001200 b WITH (nolock) ON a.t_ccra = b.t_emno
				     INNER JOIN
				(
				    SELECT a.t_bpid,
				           a.t_nama nam,
				           SUM(c.AMT) AS AMT,
				           SUM(c.AMT2) AS AMT2,
				           a.t_stdt,
				           a.t_prst,
				           a.t_crdt,
				           a.t_lgid AS svt
				    FROM ttccom100200 a WITH (nolock)
				         LEFT OUTER JOIN
				    (
				        SELECT b.t_ofbp SBP,
				               SUM(b.t_amth_1) AS AMT,
				               SUM(b.t_amth_1) AS AMT2
				        FROM tcisli305555 b WITH (nolock)
				        WHERE b.t_idat BETWEEN DATEADD(month, -7, GETDATE()) AND DATEADD(month, -1, GETDATE())
				              AND b.t_stat = 6
				        GROUP BY b.t_ofbp
				        UNION
				        SELECT b.t_ofbp SBP,
				               SUM(b.t_amth_1) AS AMT,
				               SUM(b.t_amth_1) AS AMT2
				        FROM tcisli205703 b
				        WHERE b.t_idat BETWEEN DATEADD(month, -7, GETDATE()) AND DATEADD(month, -1, GETDATE())
				              AND b.t_stat = 6
				        GROUP BY b.t_ofbp
				        UNION
				        SELECT b.t_ofbp SBP,
				               SUM(b.t_amth_1) AS AMT,
				               SUM(b.t_amth_1) AS AMT2
				        FROM tcisli205702 b
				        WHERE b.t_idat BETWEEN DATEADD(month, -7, GETDATE()) AND DATEADD(month, -1, GETDATE())
				              AND b.t_stat = 6
				        GROUP BY b.t_ofbp
				    ) c ON a.t_bpid = c.SBP
				           AND a.t_bprl IN(2, 4)
				    GROUP BY a.t_bpid,
				             a.t_nama,
				             a.t_stdt,
				             a.t_prst,
				             a.t_crdt,
				             a.t_lgid
				) q ON q.t_bpid = a.t_itbp
				     LEFT OUTER JOIN
				(
				    SELECT t_stbp,
				           MAX(t_invd) AS linvdate,
				           MIN(t_invd) AS linvdate2
				    FROM ttdsls406200 WITH (nolock)
				    GROUP BY t_stbp
				) r ON q.t_bpid = r.t_stbp
				     INNER JOIN ttcmcs013200 s WITH (nolock) ON a.t_cpay = s.t_cpay
				     LEFT OUTER JOIN ttctax400200 t ON a.t_itbp = t.t_bpid";

		$count = DB::connection('sqlsrv')->select($sql);

		return ceil(($count[0]->cc/$numRecords));
	}

	public function getBlockStatus(){
		$sql = "SELECT a.t_itbp AS OutletID,
			       CASE
			           WHEN(b.t_crlr - (SUM(a.t_obra) + SUM(a.t_buor) + SUM(a.t_buin))) >= 0
			           THEN 0
			           ELSE 1
			       END AS CreditBlockStatus
			FROM ttccom113200 a,
			     ttccom112200 b,
			     ttccom110200 c
			WHERE a.t_itbp= b.t_itbp
			      AND a.t_itbp = c.t_ofbp
			      AND c.t_cbtp = 'DEA'
			GROUP BY a.t_itbp,
			         b.t_crlr";
		
		return $data = DB::connection('sqlsrv')->select($sql);			         
	}

	public function getCustomerAddresses(){
		$sql = "SELECT
					a.t_itbp,
					b.t_nama,
					b.t_namb,
					b.t_namc,
					b.t_namd,
					c.t_dsca,
					b.t_telp
				FROM
					ttccom112200 a WITH(nolock),
					ttccom130200 b WITH(nolock),
					ttccom139200 c WITH(nolock)
				WHERE
					a.t_cadr = b.t_cadr
					AND b.t_ccit = c.t_city
					AND a.t_itbp LIKE 'BPR%'
					AND a.t_itst = 1
				GROUP BY
					a.t_itbp,
					b.t_nama,
					b.t_namb,
					b.t_namc,
					b.t_namd,
					c.t_dsca,
					b.t_telp";
		
		return $data = DB::connection('sqlsrv')->select($sql);
	}


	/*public function getInvoices($quotationNo){
		$ddArray = [];

		$header = $this->getInvoiceHeader($quotationNo);
		$details = $this->getInvoiceHeaderDetails($quotationNo);

		if(count($header) > 0){
			foreach($header as $head){
				if($head->total_amount > 0){
					$ddDetail = [];
					foreach($details as $detail){
						if($detail->sales_order_no == $head->sales_order_no){
							array_push($ddDetail, $detail);
						}
					}

					array_push($ddArray, (object)[
						'header' => $head,
						'details' => $ddDetail
					]);
				}
			}
		}

		if(count($ddArray) > 0){
			return $ddArray;
		}else{
			return null;
		}
	}*/

	public function getInvoices($timestamp){
		$ddArray = [];

		//Change local time to UTC;
		$date = new \DateTime($timestamp, new \DateTimeZone('Asia/Colombo'));
		$date->setTimezone(new \DateTimeZone('UTC'));
		$timestamp = $date->format('Y-m-d H:i:s');

		$header = $this->getInvoiceHeader($timestamp);
		$details = $this->getInvoiceHeaderDetails($timestamp);

		if(count($header) > 0){
			foreach($header as $head){
				if($head->total_amount > 0){
					$ddDetail = [];
					foreach($details as $detail){
						if($detail->invoice_no == $head->invoice_no){
							array_push($ddDetail, $detail);
						}
					}

					array_push($ddArray, (object)[
						'header' => $head,
						'details' => $ddDetail
					]);
				}
			}
		}

		if(count($ddArray) > 0){
			return $ddArray;
		}else{
			return null;
		}
	}

	/*public function getInvoiceHeader($quotationNo){
		if(!$quotationNo && $quotationNo != ""){
			throw new \Exception("Quotation No not found");
		}

		$sql = "SELECT 
					a.t_tran AS invoice_type, 
					a.t_idoc AS invoice_document, 
					a.t_idat AS invoice_date,
					c.t_refa AS reference,
					b.t_orno AS sales_order_no,
					a.t_itbp AS bpr,
					COUNT(b.t_line) AS line_count,
					SUM(b.t_amth_1) AS total_amount,
					SUM(b.t_ldah_1) AS discount_amount, 
					SUM(b.t_txah_1) AS vat_amount
				FROM 
					tcisli305555 a with (nolock), 
					tcisli310555 b with (nolock), 
					ttdsls400200 c with (nolock)
				WHERE 
					a.t_tran = b.t_tran
					AND a.t_idoc = b.t_idoc
					AND b.t_orno = c.t_orno
					AND c.t_refa LIKE '".$quotationNo."%'
				GROUP BY 
					a.t_tran, 
					a.t_idoc, 
					a.t_idat,
					c.t_refa,
					b.t_orno,
					a.t_itbp";

		return $data = DB::connection('sqlsrv')->select($sql);
	}*/

	public function getInvoiceHeader($timestamp){
		$sql = "SELECT 
					a.t_tran AS invoice_type, 
					a.t_idoc AS invoice_document, 
					CONCAT(a.t_tran,'-',a.t_idoc) AS invoice_no,
					a.t_idat AS invoice_date,
					c.t_refa AS reference,
					b.t_orno AS sales_order_no,
					a.t_itbp AS bpr,
					c.t_cwar AS WH,
               		c.t_creg AS Area,
					COUNT(b.t_line) AS line_count,
					SUM(b.t_amth_1) AS total_amount,
					SUM(b.t_ldah_1) AS discount_amount, 
					SUM(b.t_txah_1) AS vat_amount
				FROM 
					tcisli305555 a with (nolock), 
					tcisli310555 b with (nolock), 
					ttdsls400200 c with (nolock)
				WHERE 
					a.t_tran = b.t_tran
					AND a.t_idoc = b.t_idoc
					AND b.t_orno = c.t_orno
					AND c.t_cofc = '555PRJ'
					and a.t_idat >= '".$timestamp."'
				GROUP BY 
					a.t_tran, 
					a.t_idoc, 
					a.t_idat,
					c.t_refa,
					b.t_orno,
					a.t_itbp,
					c.t_cwar,
					c.t_creg";

		return $data = DB::connection('sqlsrv')->select($sql);
	}

	/*public function getInvoiceHeaderDetails($quotationNo){
		if(!$quotationNo && $quotationNo != ""){
			throw new \Exception("Quotation No not found");
		}

		$sql = "SELECT 
					b.t_orno AS sales_order_no,
					b.t_line AS invoice_line,
					b.t_item AS item_code,
					b.t_dqua AS delivery_qty,
					b.t_cuqs AS order_unit,
					b.t_pric AS unit_price,
					b.t_amth_1 AS line_amount,
					b.t_ldah_1 AS line_discount
				FROM 
					tcisli305555 a with (nolock), 
					tcisli310555 b with (nolock), 
					ttdsls400200 c with (nolock)
				WHERE 
					a.t_tran = b.t_tran
					AND a.t_idoc = b.t_idoc
					AND b.t_orno = c.t_orno
					AND c.t_refa LIKE '".$quotationNo."%'
				GROUP BY 
					b.t_line,
					b.t_item,
					b.t_dqua,
					b.t_cuqs,
					b.t_pric,
					b.t_amth_1,
					b.t_ldah_1,
					b.t_orno";

		return $data = DB::connection('sqlsrv')->select($sql);
	}*/

	public function getInvoiceHeaderDetails($timestamp){
		$sql = "SELECT 
					CONCAT(a.t_tran,'-',a.t_idoc) as invoice_no,
					b.t_line AS invoice_line,
					b.t_item AS item_code,
					b.t_dqua AS delivery_qty,
					b.t_cuqs AS order_unit,
					b.t_pric AS unit_price,
					b.t_amth_1 AS line_amount,
					b.t_ldah_1 AS line_discount
				FROM 
					tcisli305555 a with (nolock), 
					tcisli310555 b with (nolock), 
					ttdsls400200 c with (nolock)
				WHERE 
					a.t_tran = b.t_tran
					AND a.t_idoc = b.t_idoc
					AND b.t_orno = c.t_orno
					AND c.t_cofc = '555PRJ'
					AND a.t_idat >= '".$timestamp."'
				GROUP BY 
					b.t_line,
					b.t_item,
					b.t_dqua,
					b.t_cuqs,
					b.t_pric,
					b.t_amth_1,
					b.t_ldah_1,
					a.t_tran,
					a.t_idoc";

		return $data = DB::connection('sqlsrv')->select($sql);
	}

	public function getPrices(){
		$sql = "SELECT 
					a.t_prbk,
					a.t_efdt,
					a.t_item, 
					b.t_dsca,
					a.t_bapr
				FROM 
					ttdpcg031200 a with (nolock), 
					ttcibd001200 b with (nolock)
				WHERE 
					a.t_item = b.t_item
					AND a.t_prbk = 'SAL000009'
					AND a.t_exdt = '01-jan-1970'";

		return $data = DB::connection('sqlsrv')->select($sql);
	}

	public function validateDate($date, $format = 'Y-m-d'){
	    $d = \DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

	public function downloadHeader($data, $isDownload=false){

		$quotations = Quotation::with(['customer','createdBy', 'inquiry.leadOwner', 'customerPo'])
							->where('status',CLOSED);

		if($data->start && $data->start != ''){
			$quotations->where('quotation_date', '>=', $data->start.' 00:00:00');
		}

		if($data->end && $data->end != ''){
			$quotations->where('quotation_date', '<=', $data->end.' 00:00:00');
		}

		if($data->quotation_no && $data->quotation_no != ''){
			$quotations->where('reference_no', 'like', '%'.$data->quotation_no.'%');
		}

		if($data->customer && $data->customer != ''){
			$quotations->whereIn('customer_id', function($q) use ($data){
				return $q->select('id')
					->from('customer')
					->where('code', 'like', '%'. $data->customer.'%')
					->orWhere('first_name', 'like', '%'. $data->customer.'%')
					->orWhere('last_name', 'like', '%'. $data->customer.'%');
			});
		}

		if($data->quotation_id && $data->quotation_id != ''){
			$quotations->where('id',$data->quotation_id);
		}

		$quotations = $quotations->get();

		if(count($quotations) > 0){
			$data = array();

			foreach($quotations as $key => $value){
				$dd = array();

				//Quotation
				/*array_push($dd,
					$value->distributor->qu_series.STR_PAD($value->order_id,6,'0',STR_PAD_LEFT));*/
				array_push($dd, 'PSQ'.STR_PAD($value->id,6,'0',STR_PAD_LEFT));

				//Sold to Business Partner
				array_push($dd,$value->customer->code);

				//Sold to Address
				//array_push($dd,$value->distributor->sold_to_address);
				array_push($dd,$value->customer->customer_address_code);

				//Sold to Contact
				array_push($dd,'');

				//Ship to Business Partnet
				array_push($dd,$value->customer->code);

				//Ship to address
				//array_push($dd,$value->distributor->ship_to_address);
				array_push($dd,$value->customer->customer_address_code);

				//Ship to contact
				array_push($dd,'');

				//Invoice to business
				array_push($dd,$value->customer->code);

				//Invoice to address
				//array_push($dd,$value->distributor->invoice_to_address);
				array_push($dd,$value->customer->customer_address_code);

				//Invoice to contact
				array_push($dd,'');

				//Pay by business partner
				array_push($dd,$value->customer->code);

				//Pay by address
				//array_push($dd,$value->distributor->pay_by_address);
				array_push($dd,$value->customer->customer_address_code);

				//Pay by contact
				array_push($dd,'');

				//Origin [Hard Coded]
				array_push($dd,'4');

				//Quotation Status [Hard Coded]
				array_push($dd,'1');

				//Quotation Date
				array_push($dd,date('dmy',strtotime($value->quotation_date)));

				//Expire Date
				array_push($dd,date('dmy',strtotime($value->quotation_date)));

				//Plan Delivery Date
				array_push($dd,date('dmy',strtotime($value->quotation_date)));

				//Time of delivery [Hard Coded]
				array_push($dd,'0');

				//Order discount [Hard Coded]
				array_push($dd,'0');

				//Order currency [Hard Coded]
				//array_push($dd,$value->distributor->currency);
				array_push($dd,'SLR');

				//Rate determiner [Hard Coded]
				array_push($dd,'6');

				//Currency rate sales [Hard Coded]
				array_push($dd,'1');

				//Currency rate sales [Hard Coded]
				array_push($dd,'111.1972');

				//Currency rate sales [Hard Coded]
				array_push($dd,'0.000001');

				//Rate factor 1 [Hard Coded]
				array_push($dd,'1');

				//Rate factor 2 [Hard Coded]
				array_push($dd,'1');

				//Rate factor 3 [Hard Coded]
				array_push($dd,'1');

				//Rate date [Hard Coded]
				array_push($dd,'161114');

				//Exchange rate type [Hard Coded]
				array_push($dd,'SLS');

				//Tax classification
				array_push($dd,'');

				//Sales order type [Hard Coded]
				if($value->discount_type == CASH){
					array_push($dd,'S27');
				}elseif($value->discount_type == PROJECT){
					array_push($dd,'S14');
				}

				//Internal sales rep
				//array_push($dd,$value->distributor->internal_sales_rep.'   ');
				array_push($dd,$value->createdBy->code);

				//External sales rep
				//array_push($dd,$value->distributor->external_sales_rep.'   ');
				if($value->inquiry && $value->inquiry->leadOwner){
					array_push($dd,$value->inquiry->leadOwner->code);
				}else{
					array_push($dd,'');
				}

				//Obselete [Hard Coded]
				array_push($dd,'1');

				//Late payment suchar
				array_push($dd,'');

				//Carrier/LSP
				array_push($dd,'');

				//Price list
				//array_push($dd,$value->distributor->sales_price_list);
				array_push($dd,'');

				//Price list for direct
				//array_push($dd,$value->distributor->price_list_for_direc);
				array_push($dd,'');

				//BP Prices/Discounts
				array_push($dd,'');

				//Business partner TAX
				array_push($dd,'');

				//Obselete
				array_push($dd,'');

				//Terms of delivery
				array_push($dd,'');

				//Point of title passa
				array_push($dd,'');

				//Route
				array_push($dd,'');

				//Terms of payment
				array_push($dd,str_pad($value->customer->payment_terms,3," ",STR_PAD_RIGHT));

				//Line of business
				array_push($dd,'');

				//Area
				//array_push($dd,$value->distributor->area);
				array_push($dd,'');

				//Customer RFQ Number
				if($value->customerPo){
					array_push($dd,$value->customerPo->po_number);
				}else{
					array_push($dd,'');
				}

				//Reference A ##NOT FINALIZED##
				array_push($dd,$value->reference_no);

				//Reference B
				array_push($dd,'');

				//Success Percentage [Hard Coded]
				array_push($dd,'0');

				//Shipping constraint [Hard Coded]
				array_push($dd,'1');

				//Plan receipt date
				array_push($dd,date('dmy',strtotime($value->quotation_date)));

				//Sales office [Hard Coded]
				array_push($dd,'555PRJ');

				//Financial Department [Hard Coded]
				array_push($dd,'555PRJ');

				//Payment method
				array_push($dd,'');

				//Header quote reference
				array_push($dd,'');

				//Invoice for freight [Hard Coded]
				array_push($dd,'2');

				//Invoice for freight cost [Hard Coded]
				array_push($dd,'20');

				//Order amount
				array_push($dd,'');

				//Obselete
				array_push($dd,'');

				//Footer text [Hard Coded]
				array_push($dd,'0');

				//Header text [Hard Coded]
				array_push($dd,'0');

				array_push($data, $dd);
			}

			if(file_exists($this->csvPath.'/tdsls100.csv')){
				rename($this->csvPath.'/tdsls100.csv',
					$this->csvPath.'/tdsls100_'.date('YmdHis').'.csv');
			}

			$excl = Excel::create('tdsls100',function($excel) use ($data) {
				$excel->sheet('Sheetname', function($sheet) use($data) {
			        $sheet->fromArray($data, null, 'A1', false, false);
			    });
			});

			if($isDownload){
				$excl->download('csv');
			}else{
				return $excl->store('csv', $this->csvPath);
			}

			/**
			 * Quotation Header History
			 */
			//$this->downloadHeaderHistory($start, $end);

			/**
			 * Quotation Line
			 */
			//$this->downloadLine($start, $end);

			/**
			 * Quotation Line History
			 */
			//$this->downloadLineHistory($start, $end);

			return true;
		}
	}

	public function downloadHeaderHistory($data, $isDownload=false){
		$quotations = Quotation::with(['customer','createdBy', 'inquiry.leadOwner', 'customerPo'])
							->where('status',CLOSED);

		if($data->start && $data->start != ''){
			$quotations->where('quotation_date', '>=', $data->start.' 00:00:00');
		}

		if($data->end && $data->end != ''){
			$quotations->where('quotation_date', '<=', $data->end.' 00:00:00');
		}

		if($data->quotation_no && $data->quotation_no != ''){
			$quotations->where('reference_no', 'like', '%'.$data->quotation_no.'%');
		}

		if($data->customer && $data->customer != ''){
			$quotations->whereIn('customer_id', function($q) use ($data){
				return $q->select('id')
					->from('customer')
					->where('code', 'like', '%'. $data->customer.'%')
					->orWhere('first_name', 'like', '%'. $data->customer.'%')
					->orWhere('last_name', 'like', '%'. $data->customer.'%');
			});
		}

		if($data->quotation_id && $data->quotation_id != ''){
			$quotations->where('id',$data->quotation_id);
		}

		$quotations = $quotations->get();

		if(count($quotations) > 0){
			$data = array();

			foreach($quotations as $key => $value){
				$dd = array();

				//Quotation
				/*array_push($dd,$value->distributor->qu_series.STR_PAD($value->order_id,6,'0',STR_PAD_LEFT));*/
				array_push($dd,'PSQ'.STR_PAD($value->id,6,'0',STR_PAD_LEFT));

				//Transaction Date
				array_push($dd,date('dmy',strtotime($value->quotation_date)));

				//Sold to Business Partner
				array_push($dd,$value->customer->code);

				//Sold to Address
				//array_push($dd,$value->distributor->sold_to_address);
				array_push($dd,$value->customer->customer_address_code);

				//Sold to Contact
				array_push($dd,'');

				//Ship to Business Partner
				array_push($dd,$value->customer->code);

				//Ship to address
				//array_push($dd,$value->distributor->ship_to_address);
				array_push($dd,$value->customer->customer_address_code);

				//Ship to contact
				array_push($dd,'');

				//Invoice to business
				array_push($dd,$value->customer->code);

				//Invoice to address
				//array_push($dd,$value->distributor->invoice_to_address);
				array_push($dd,$value->customer->customer_address_code);

				//Invoice to contact
				array_push($dd,'');

				//Pay by business partner
				array_push($dd,$value->customer->code);

				//Pay by address
				//array_push($dd,$value->distributor->pay_by_address);
				array_push($dd,$value->customer->customer_address_code);

				//Pay by contact
				array_push($dd,'');

				//Origin [Hard Coded]
				array_push($dd,'4');

				//Quotation Status [Hard Coded]
				//array_push($dd,1);
				
				//Quotation Date
				array_push($dd,date('dmy',strtotime($value->quotation_date)));

				//Expire Date
				array_push($dd,date('dmy',strtotime($value->quotation_date)));

				//Plan Delivery Date
				array_push($dd,date('dmy',strtotime($value->quotation_date)));

				//Time of delivery [Hard Coded]
				array_push($dd,'0');

				//Order discount [Hard Coded]
				array_push($dd,'0');

				//Order currency [Hard Coded]
				//array_push($dd,$value->distributor->currency);
				array_push($dd,'SLR');

				//Rate determiner [Hard Coded]
				array_push($dd,'6');

				//Currency rate sales [Hard Coded]
				array_push($dd,'1');

				//Currency rate sales [Hard Coded]
				array_push($dd,'111.1972');

				//Currency rate sales [Hard Coded]
				array_push($dd,'0.000001');

				//Rate factor 1 [Hard Coded]
				array_push($dd,'1');

				//Rate factor 2 [Hard Coded]
				array_push($dd,'1');

				//Rate factor 3 [Hard Coded]
				array_push($dd,'1');

				//Rate date [Hard Coded]
				//array_push($dd,161114);
				
				//Rate Date
				array_push($dd,date('dmy',strtotime($value->quotation_date)));

				//Exchange rate type [Hard Coded]
				array_push($dd,'SLS');

				//Tax classification
				array_push($dd,'');

				//Sales order type [Hard Coded]
				if($value->discount_type == CASH){
					array_push($dd,'S27');
				}elseif($value->discount_type == PROJECT){
					array_push($dd,'S14');
				}

				//Internal sales rep
				//array_push($dd,$value->distributor->internal_sales_rep.'   ');
				array_push($dd,$value->createdBy->code);

				//External sales rep
				//array_push($dd,$value->distributor->external_sales_rep.'   ');
				if($value->inquiry && $value->inquiry->leadOwner){
					array_push($dd,$value->inquiry->leadOwner->code);
				}else{
					array_push($dd,'');
				}

				//Obselete [Hard Coded]
				array_push($dd,'2');

				//Late payment suchar
				array_push($dd,'');

				//Carrier/LSP
				array_push($dd,'');

				//Price list
				//array_push($dd,$value->distributor->sales_price_list);
				array_push($dd,'');

				//Price list for direct
				//array_push($dd,$value->distributor->price_list_for_direc);
				array_push($dd,'');

				//BP Prices/Discounts
				array_push($dd,'');

				//Business partner TAX
				array_push($dd,'');

				//Obselete
				array_push($dd,'');

				//Terms of delivery
				array_push($dd,'');

				//Point of title passa
				array_push($dd,'');

				//Route
				array_push($dd,'');

				//Terms of payment
				array_push($dd,str_pad($value->customer->payment_terms,3," ",STR_PAD_RIGHT));

				//Payment Method
				array_push($dd,'');

				//Line of business
				array_push($dd,'');

				//Area
				//array_push($dd,$value->distributor->area);
				array_push($dd,'');

				//Customer RFQ Number
				if($value->customerPo){
					array_push($dd,$value->customerPo->po_number);
				}else{
					array_push($dd,'');
				}

				//Reference A ##NOT FINALIZED##
				array_push($dd,$value->reference_no);

				//Reference B
				array_push($dd,'');

				//Success Percentage [Hard Coded]
				array_push($dd,'0');

				//Shipping constraint [Hard Coded]
				array_push($dd,'1');

				//Plan receipt date
				array_push($dd,date('dmy',strtotime($value->quotation_date)));

				//Sales office [Hard Coded]
				array_push($dd,'555PRJ');

				//Financial Department [Hard Coded]
				array_push($dd,'555PRJ');

				//Invoice for freight [Hard Coded]
				array_push($dd,'2');

				//Invoice for freight cost [Hard Coded]
				array_push($dd,'20');

				//Header quote reference
				array_push($dd,'');

				//Order amount
				//array_push($dd,'');
				
				//Obselete
				array_push($dd,'');

				//Footer text [Hard Coded]
				array_push($dd,'0');

				//Header text [Hard Coded]
				array_push($dd,'0');

				array_push($data, $dd);
			}

			if(file_exists($this->csvPath.'/tdsls150.csv')){
				rename($this->csvPath.'/tdsls150.csv',
					$this->csvPath.'/tdsls150_'.date('YmdHis').'.csv');
			}

			$excl = Excel::create('tdsls150',function($excel) use ($data) {
				$excel->sheet('Sheetname', function($sheet) use($data) {
			        $sheet->fromArray($data, null, 'A1', false, false);
			    });
			});

			if($isDownload){
				$excl->download('csv');
			}else{
				return $excl->store('csv', $this->csvPath);
			}
		}

		return true;
	}

	public function downloadLine($data, $isDownload=false){
		$quotations = Quotation::with(['customer','createdBy', 'details.product'])
							->where('status',CLOSED);

		if($data->start && $data->start != ''){
			$quotations->where('quotation_date', '>=', $data->start.' 00:00:00');
		}

		if($data->end && $data->end != ''){
			$quotations->where('quotation_date', '<=', $data->end.' 00:00:00');
		}

		if($data->quotation_no && $data->quotation_no != ''){
			$quotations->where('reference_no', 'like', '%'.$data->quotation_no.'%');
		}

		if($data->customer && $data->customer != ''){
			$quotations->whereIn('customer_id', function($q) use ($data){
				return $q->select('id')
					->from('customer')
					->where('code', 'like', '%'. $data->customer.'%')
					->orWhere('first_name', 'like', '%'. $data->customer.'%')
					->orWhere('last_name', 'like', '%'. $data->customer.'%');
			});
		}

		if($data->quotation_id && $data->quotation_id != ''){
			$quotations->where('id',$data->quotation_id);
		}

		$quotations = $quotations->get();

		if(count($quotations) > 0){
			$data = array();

			foreach($quotations as $key1 => $value){
				$count = 0;
				foreach ($value->details as $key => $detail) {
					$count++;
					$dd = array();

					//Quotation
					/*array_push($dd,
						$value->distributor->qu_series.STR_PAD($value->order_id,6,'0',STR_PAD_LEFT));*/
					//array_push($dd, $value->reference_no);
					array_push($dd,'PSQ'.STR_PAD($value->id,6,'0',STR_PAD_LEFT));

					//Position
					array_push($dd,$count*10);

					//Alternative [Hard Coded]
					array_push($dd,'0');

					//Sold to business partner
					array_push($dd,$value->customer->code);

					//Item code
					array_push($dd,'         '.$detail->product->code);

					//Effectivity Unit [Hard Coded]
					array_push($dd,'0');

					//Product Variant [Hard Coded]
					array_push($dd,'0');

					//Make Customized [Hard Coded]
					array_push($dd,'2');

					//Direct Delivery [Hard Coded]
					array_push($dd,'2');

					//Standard Description [Hard Coded]
					array_push($dd,'1');

					//Price
					array_push($dd,$detail->price);

					//Price Origin [Hard Coded]
					array_push($dd,'18');

					//Price Matrix Definition [Hard Coded]
					array_push($dd,'SLS000007');

					//Price Matrix Sequence [Hard Coded]
					array_push($dd,'35');

					//Default Price Book [Hard Coded]
					array_push($dd,'1');

					//Sales Price Unit
					//array_push($dd,$detail->product->sales_unit);
					array_push($dd,$detail->product->unit);

					//Conversion Factor [Hard Coded]
					array_push($dd,'1');

					//Quotation Date
					array_push($dd,date('dmy',strtotime($value->quotation_date)));

					//Plan Delivery Date
					array_push($dd,date('dmy',strtotime($value->quotation_date)));

					//Time of Delivery
					array_push($dd,'0');

					//Order Quantity
					array_push($dd,$detail->qty);										

					//array_push($dd,$detail->product->sales_unit);
					array_push($dd,$detail->product->unit);

					//Conversion Factor SA [Hard Coded]
					array_push($dd,'1');

					//Length [Hard Coded]
					array_push($dd,'0');

					//Width [Hard Coded]
					array_push($dd,'0');

					//Height [Hard Coded]
					array_push($dd,'0');

					//Discount 1 [Hard Coded]
					array_push($dd,$detail->udp);

					//Discount 2 [Hard Coded]
					array_push($dd,$detail->discount!=null?$detail->discount:0);

					//Discount 3 [Hard Coded]
					array_push($dd,'0');

					//Discount 4 [Hard Coded]
					array_push($dd,'0');

					//Discount 5 [Hard Coded]
					array_push($dd,'0');

					//Discount 6 [Hard Coded]
					array_push($dd,'0');

					//Discount 7 [Hard Coded]
					array_push($dd,'0');

					//Discount 8 [Hard Coded]
					array_push($dd,'0');

					//Discount 9 [Hard Coded]
					array_push($dd,'0');

					//Discount 1'0' [Hard Coded]
					array_push($dd,'0');

					//Discount 11 [Hard Coded]
					array_push($dd,'0');

					//Discount Amount 1 [Hard Coded]
					array_push($dd,'0');

					//Discount Amount 2 [Hard Coded]
					array_push($dd,'0');

					//Discount Amount 3 [Hard Coded]
					array_push($dd,'0');

					//Discount Amount 4 [Hard Coded]
					array_push($dd,'0');

					//Discount Amount 5 [Hard Coded]
					array_push($dd,'0');

					//Discount Amount 6 [Hard Coded]
					array_push($dd,'0');

					//Discount Amount 7 [Hard Coded]
					array_push($dd,'0');

					//Discount Amount 8 [Hard Coded]
					array_push($dd,'0');

					//Discount Amount 9 [Hard Coded]
					array_push($dd,'0');

					//Discount Amount 1'0' [Hard Coded]
					array_push($dd,'0');

					//Discount Amount 11 [Hard Coded]
					array_push($dd,'0');

					//Discount Origin 1 [Hard Coded]
					array_push($dd,'8');

					//Discount Origin '2' [Hard Coded]
					array_push($dd,'2');

					//Discount Origin 3 [Hard Coded]
					array_push($dd,'2');

					//Discount Origin 4 [Hard Coded]
					array_push($dd,'2');

					//Discount Origin 5 [Hard Coded]
					array_push($dd,'2');

					//Discount Origin 6 [Hard Coded]
					array_push($dd,'2');

					//Discount Origin 7 [Hard Coded]
					array_push($dd,'2');

					//Discount Origin 8 [Hard Coded]
					array_push($dd,'2');

					//Discount Origin 9 [Hard Coded]
					array_push($dd,'2');

					//Discount Origin 10 [Hard Coded]
					array_push($dd,'2');

					//Discount Origin 11 [Hard Coded]
					array_push($dd,'2');

					//Discount Matrix Type [Hard Coded]
					array_push($dd,'2');

					//Discount Matrix Type [Hard Coded]
					array_push($dd,'100');

					//Discount Matrix Type [Hard Coded]
					array_push($dd,'100');

					//Discount Matrix Type [Hard Coded]
					array_push($dd,'100');

					//Discount Matrix Type [Hard Coded]
					array_push($dd,'100');

					//Discount Matrix Type [Hard Coded]
					array_push($dd,'100');

					//Discount Matrix Type [Hard Coded]
					array_push($dd,'100');

					//Discount Matrix Type [Hard Coded]
					array_push($dd,'100');

					//Discount Matrix Type [Hard Coded]
					array_push($dd,'100');

					//Discount Matrix Type [Hard Coded]
					array_push($dd,'100');

					//Discount Matrix Type [Hard Coded]
					array_push($dd,'100');

					//Discount Matrix Defi [Hard Coded]
					array_push($dd,'SLS000011');

					//Discount Matrix Defi [Hard Coded]
					array_push($dd,'');

					//Discount Matrix Defi [Hard Coded]
					array_push($dd,'');

					//Discount Matrix Defi [Hard Coded]
					array_push($dd,'');

					//Discount Matrix Defi [Hard Coded]
					array_push($dd,'');

					//Discount Matrix Defi [Hard Coded]
					array_push($dd,'');

					//Discount Matrix Defi [Hard Coded]
					array_push($dd,'');

					//Discount Matrix Defi [Hard Coded]
					array_push($dd,'');

					//Discount Matrix Defi [Hard Coded]
					array_push($dd,'');

					//Discount Matrix Defi [Hard Coded]
					array_push($dd,'');

					//Discount Matrix Defi [Hard Coded]
					array_push($dd,'');

					//Discount Matrix Sequ [Hard Coded]
					array_push($dd,'15');

					//Discount Matrix Sequ [Hard Coded]
					array_push($dd,'0');

					//Discount Matrix Sequ [Hard Coded]
					array_push($dd,'0');

					//Discount Matrix Sequ [Hard Coded]
					array_push($dd,'0');

					//Discount Matrix Sequ [Hard Coded]
					array_push($dd,'0');

					//Discount Matrix Sequ [Hard Coded]
					array_push($dd,'0');

					//Discount Matrix Sequ [Hard Coded]
					array_push($dd,'0');

					//Discount Matrix Sequ [Hard Coded]
					array_push($dd,'0');

					//Discount Matrix Sequ [Hard Coded]
					array_push($dd,'0');

					//Discount Matrix Sequ [Hard Coded]
					array_push($dd,'0');

					//Discount Matrix Sequ [Hard Coded]
					array_push($dd,'0');

					//Discount Method 1 [Hard Coded]
					array_push($dd,'1');

					//Discount Method 2 [Hard Coded]
					array_push($dd,'1');

					//Discount Method 3 [Hard Coded]
					array_push($dd,'1');

					//Discount Method 4 [Hard Coded]
					array_push($dd,'1');

					//Discount Method 5 [Hard Coded]
					array_push($dd,'1');

					//Discount Method 6 [Hard Coded]
					array_push($dd,'1');

					//Discount Method 7 [Hard Coded]
					array_push($dd,'1');

					//Discount Method 8 [Hard Coded]
					array_push($dd,'1');

					//Discount Method 9 [Hard Coded]
					array_push($dd,'1');

					//Discount Method 10 [Hard Coded]
					array_push($dd,'1');

					//Discount Method 11 [Hard Coded]
					array_push($dd,'1');

					//Discount Code 1
					array_push($dd,'');

					//Discount Code 2
					array_push($dd,'');

					//Discount Code 3
					array_push($dd,'');

					//Discount Code 4
					array_push($dd,'');

					//Discount Code 5
					array_push($dd,'');

					//Discount Code 6
					array_push($dd,'');

					//Discount Code 7
					array_push($dd,'');

					//Discount Code 8
					array_push($dd,'');

					//Discount Code 9
					array_push($dd,'');

					//Discount Code 10
					array_push($dd,'');

					//Discount Code 11
					array_push($dd,'');

					//Determining [Hard Coded]
					array_push($dd,'1');

					//Eligible [Hard Coded]
					array_push($dd,'1');

					//Structure Discount [Hard Coded]
					array_push($dd,'21');

					//Amount
					array_push($dd,$detail->price*$detail->qty);

					//Tax Country [Hard Coded]
					array_push($dd,'SLK');

					//Tax Code
					array_push($dd,$detail->product->vat_code);

					//BP Tax Country [Hard Coded]
					//array_push($dd,$value->distributor->country);
					array_push($dd,'SLK');

					//Exempt [Hard Coded]
					array_push($dd,'2');

					//Tax Classification
					array_push($dd,'');

					//Warehouse [MEGODA WAREHOUSE]
					array_push($dd,'555FP2');

					//Channels
					array_push($dd,'');

					//Work Center
					array_push($dd,'');

					//Budget
					array_push($dd,'');

					//Success Percentage [Hard Coded]
					array_push($dd,'0');

					//Reason for Success
					array_push($dd,'');

					//Competitor
					array_push($dd,'');

					//Inventory Handling [Hard Coded]
					array_push($dd,'1');

					//Shipping Constraint [Hard Coded]
					array_push($dd,'1');

					//Ship to business partner
					array_push($dd,$value->customer->code);

					//Ship to address
					//array_push($dd,$value->distributor->ship_to_address);
					array_push($dd,$value->customer->customer_address_code);

					//Ship to contact
					array_push($dd,'');

					//Status [Hard Coded]
					array_push($dd,'1');

					//Item Code
					array_push($dd,'');

					//Carrier/LSP
					array_push($dd,'');

					//Route
					array_push($dd,'');

					//Plan Receipt Date
					array_push($dd,date('dmy',strtotime($value->quotation_date)));

					//Project
					array_push($dd,'');

					//Line Quote Reference
					array_push($dd,'');

					//Exempt Certificate
					array_push($dd,'');

					//Exempt Reason
					array_push($dd,'');

					//Generate freight ord [Hard Coded]
					array_push($dd,'2');

					//invoice for freight [Hard Coded]
					array_push($dd,'2');

					//freight amount [Hard Coded]
					array_push($dd,'0');

					//carrier binding [Hard Coded]
					array_push($dd,'2');

					//Freight service leave [Hard Coded]
					array_push($dd,'');

					//List group [Hard Coded]
					array_push($dd,'');

					//Freight amount bindi [Hard Coded]
					array_push($dd,'2');

					//Terms of delivery [Hard Coded]
					array_push($dd,'');

					//Point of title passa [Hard Coded]
					array_push($dd,'');

					//Order promising state [Hard Coded]
					array_push($dd,'3');

					//Obselete [Hard Coded]
					array_push($dd,'');

					//Quotaion line text [Hard Coded]
					array_push($dd,'0');

					array_push($data, $dd);
				}
			}

			if(file_exists($this->csvPath.'/tdsls101.csv')){
				rename($this->csvPath.'/tdsls101.csv',
					$this->csvPath.'/tdsls101_'.date('YmdHis').'.csv');
			}

			$excl = Excel::create('tdsls101',function($excel) use ($data) {
				$excel->sheet('Sheetname', function($sheet) use($data) {
			        $sheet->fromArray($data, null, 'A1', false, false);
			    });
			});

			if($isDownload){
				$excl->download('csv');
			}else{
				return $excl->store('csv', $this->csvPath);
			}
		}

		return true;
	}

	public function downloadLineHistory($data, $isDownload=false){
		$quotations = Quotation::with(['customer','createdBy', 'inquiry.leadOwner', 'details.product'])
							->where('status',CLOSED);

		if($data->start && $data->start != ''){
			$quotations->where('quotation_date', '>=', $data->start.' 00:00:00');
		}

		if($data->end && $data->end != ''){
			$quotations->where('quotation_date', '<=', $data->end.' 00:00:00');
		}

		if($data->quotation_no && $data->quotation_no != ''){
			$quotations->where('reference_no', 'like', '%'.$data->quotation_no.'%');
		}

		if($data->customer && $data->customer != ''){
			$quotations->whereIn('customer_id', function($q) use ($data){
				return $q->select('id')
					->from('customer')
					->where('code', 'like', '%'. $data->customer.'%')
					->orWhere('first_name', 'like', '%'. $data->customer.'%')
					->orWhere('last_name', 'like', '%'. $data->customer.'%');
			});
		}

		if($data->quotation_id && $data->quotation_id != ''){
			$quotations->where('id',$data->quotation_id);
		}

		$quotations = $quotations->get();

		if(count($quotations) > 0){
			$data = array();

			foreach($quotations as $key1 => $value){
				$count = 0;
				foreach ($value->details as $key => $detail) {
					$count++;
					$dd = array();

					//Quotation
					/*array_push($dd,
						$value->distributor->qu_series.STR_PAD($value->order_id,6,'0',STR_PAD_LEFT));*/
					//array_push($dd, $value->reference_no);
					array_push($dd,'PSQ'.STR_PAD($value->id,6,'0',STR_PAD_LEFT));

					//Position
					array_push($dd,$count*10);

					//Alternative [Hard Coded]
					array_push($dd,'0');

					//Transaction Date
					array_push($dd,date('dmy',strtotime($value->quotation_date)));

					//Record Type (1-4) [Hard Coded]
					array_push($dd,'1');

					//Sequence No [Hard Coded]
					array_push($dd,'4069705');

					//Sold to business partner ##NOT DONE##
					array_push($dd,$value->customer->code);
					
					//Budget
					array_push($dd,'');

					//Item code
					array_push($dd,'         '.$detail->product->code);

					//Effectivity Unit [Hard Coded]
					array_push($dd,'0');

					//Product Variant [Hard Coded]
					array_push($dd,'0');

					//Make Customized [Hard Coded]
					array_push($dd,'2');

					//Direct Delivery [Hard Coded]
					array_push($dd,'2');

					//Standard Description [Hard Coded]
					array_push($dd,'1');

					//Price
					array_push($dd,$detail->price);

					//Price Origin [Hard Coded]
					array_push($dd,'18');

					//Price Matrix Definition [Hard Coded]
					array_push($dd,'SLS000007');

					//Price Matrix Sequence [Hard Coded]
					array_push($dd,'35');

					//Default Price Book [Hard Coded]
					array_push($dd,'1');

					//Sales Price Unit
					//array_push($dd,$detail->product->sales_unit);
					array_push($dd,$detail->product->unit);

					//Conversion Factor [Hard Coded]
					array_push($dd,'1');

					//Quotation Date
					array_push($dd,date('dmy',strtotime($value->quotation_date)));

					//Plan Delivery Date
					array_push($dd,date('dmy',strtotime($value->quotation_date)));

					//Time of Delivery
					//array_push($dd,$value->distributor->time_of_delivery);
					
					//Time of Delivery
					array_push($dd,'0');

					//Order Quantity
					array_push($dd,$detail->qty);

					/*array_push($dd,$detail->product->sales_unit);*/
					array_push($dd,$detail->product->unit);

					//Conversion Factor SA [Hard Coded]
					array_push($dd,'1');

					//Length [Hard Coded]
					array_push($dd,'0');

					//Width [Hard Coded]
					array_push($dd,'0');

					//Height [Hard Coded]
					array_push($dd,'0');

					//Discount 1 [Hard Coded]
					array_push($dd,$detail->udp);

					//Discount 2 [Hard Coded]
					array_push($dd,$detail->discount!=null?$detail->discount:0);

					//Discount 3 [Hard Coded]
					array_push($dd,'0');

					//Discount 4 [Hard Coded]
					array_push($dd,'0');

					//Discount 5 [Hard Coded]
					array_push($dd,'0');

					//Discount 6 [Hard Coded]
					array_push($dd,'0');

					//Discount 7 [Hard Coded]
					array_push($dd,'0');

					//Discount 8 [Hard Coded]
					array_push($dd,'0');

					//Discount 9 [Hard Coded]
					array_push($dd,'0');

					//Discount 10 [Hard Coded]
					array_push($dd,'0');

					//Discount 11 [Hard Coded]
					array_push($dd,'0');

					//Discount Amount 1 [Hard Coded]
					array_push($dd,'0');

					//Discount Amount 2 [Hard Coded]
					array_push($dd,'0');

					//Discount Amount 3 [Hard Coded]
					array_push($dd,'0');

					//Discount Amount 4 [Hard Coded]
					array_push($dd,'0');

					//Discount Amount 5 [Hard Coded]
					array_push($dd,'0');

					//Discount Amount 6 [Hard Coded]
					array_push($dd,'0');

					//Discount Amount 7 [Hard Coded]
					array_push($dd,'0');

					//Discount Amount 8 [Hard Coded]
					array_push($dd,'0');

					//Discount Amount 9 [Hard Coded]
					array_push($dd,'0');

					//Discount Amount 10 [Hard Coded]
					array_push($dd,'0');

					//Discount Amount 11 [Hard Coded]
					array_push($dd,'0');

					//Discount Origin 1 [Hard Coded]
					array_push($dd,'2');

					//Discount Origin 2 [Hard Coded]
					array_push($dd,'2');

					//Discount Origin 3 [Hard Coded]
					array_push($dd,'2');

					//Discount Origin 4 [Hard Coded]
					array_push($dd,'2');

					//Discount Origin 5 [Hard Coded]
					array_push($dd,'2');

					//Discount Origin 6 [Hard Coded]
					array_push($dd,'2');

					//Discount Origin 7 [Hard Coded]
					array_push($dd,'2');

					//Discount Origin 8 [Hard Coded]
					array_push($dd,'2');

					//Discount Origin 9 [Hard Coded]
					array_push($dd,'2');

					//Discount Origin 10 [Hard Coded]
					array_push($dd,'2');

					//Discount Origin 11 [Hard Coded]
					array_push($dd,'2');

					//Discount Matrix Type [Hard Coded]
					array_push($dd,'2');

					//Discount Matrix Type [Hard Coded]
					array_push($dd,'100');

					//Discount Matrix Type [Hard Coded]
					array_push($dd,'100');

					//Discount Matrix Type [Hard Coded]
					array_push($dd,'100');

					//Discount Matrix Type [Hard Coded]
					array_push($dd,'100');

					//Discount Matrix Type [Hard Coded]
					array_push($dd,'100');

					//Discount Matrix Type [Hard Coded]
					array_push($dd,'100');

					//Discount Matrix Type [Hard Coded]
					array_push($dd,'100');

					//Discount Matrix Type [Hard Coded]
					array_push($dd,'100');

					//Discount Matrix Type [Hard Coded]
					array_push($dd,'100');

					//Discount Matrix Type [Hard Coded]
					array_push($dd,'100');

					//Discount Matrix Defi [Hard Coded]
					array_push($dd,'SLS000011');

					//Discount Matrix Defi [Hard Coded]
					array_push($dd,'');

					//Discount Matrix Defi [Hard Coded]
					array_push($dd,'');

					//Discount Matrix Defi [Hard Coded]
					array_push($dd,'');

					//Discount Matrix Defi [Hard Coded]
					array_push($dd,'');

					//Discount Matrix Defi [Hard Coded]
					array_push($dd,'');

					//Discount Matrix Defi [Hard Coded]
					array_push($dd,'');

					//Discount Matrix Defi [Hard Coded]
					array_push($dd,'');

					//Discount Matrix Defi [Hard Coded]
					array_push($dd,'');

					//Discount Matrix Defi [Hard Coded]
					array_push($dd,'');

					//Discount Matrix Defi [Hard Coded]
					array_push($dd,'');

					//Discount Matrix Sequ [Hard Coded]
					array_push($dd,'15');

					//Discount Matrix Sequ [Hard Coded]
					array_push($dd,'0');

					//Discount Matrix Sequ [Hard Coded]
					array_push($dd,'0');

					//Discount Matrix Sequ [Hard Coded]
					array_push($dd,'0');

					//Discount Matrix Sequ [Hard Coded]
					array_push($dd,'0');

					//Discount Matrix Sequ [Hard Coded]
					array_push($dd,'0');

					//Discount Matrix Sequ [Hard Coded]
					array_push($dd,'0');

					//Discount Matrix Sequ [Hard Coded]
					array_push($dd,'0');

					//Discount Matrix Sequ [Hard Coded]
					array_push($dd,'0');

					//Discount Matrix Sequ [Hard Coded]
					array_push($dd,'0');

					//Discount Matrix Sequ [Hard Coded]
					array_push($dd,'0');

					//Discount Method 1 [Hard Coded]
					array_push($dd,'1');

					//Discount Method 2 [Hard Coded]
					array_push($dd,'1');

					//Discount Method 3 [Hard Coded]
					array_push($dd,'1');

					//Discount Method 4 [Hard Coded]
					array_push($dd,'1');

					//Discount Method 5 [Hard Coded]
					array_push($dd,'1');

					//Discount Method 6 [Hard Coded]
					array_push($dd,'1');

					//Discount Method 7 [Hard Coded]
					array_push($dd,'1');

					//Discount Method 8 [Hard Coded]
					array_push($dd,'1');

					//Discount Method 9 [Hard Coded]
					array_push($dd,'1');

					//Discount Method 10 [Hard Coded]
					array_push($dd,'1');

					//Discount Method 11 [Hard Coded]
					array_push($dd,'1');

					//Discount Code 1
					array_push($dd,'');

					//Discount Code 2
					array_push($dd,'');

					//Discount Code 3
					array_push($dd,'');

					//Discount Code 4
					array_push($dd,'');

					//Discount Code 5
					array_push($dd,'');

					//Discount Code 6
					array_push($dd,'');

					//Discount Code 7
					array_push($dd,'');

					//Discount Code 8
					array_push($dd,'');

					//Discount Code 9
					array_push($dd,'');

					//Discount Code 10
					array_push($dd,'');

					//Discount Code 11
					array_push($dd,'');

					//Determining [Hard Coded]
					array_push($dd,'1');

					//Eligible [Hard Coded]
					array_push($dd,'1');

					//Structure Discount [Hard Coded]
					array_push($dd,'21');

					//Amount
					array_push($dd,$detail->price*$detail->qty);

					//Standard Cost Price ##NOT DONE##
					array_push($dd,'0');

					//Standard Cost Price ##NOT DONE##
					array_push($dd,'0');

					//Standard Cost Price ##NOT DONE##
					array_push($dd,'0');

					//Tax Country [Hard Coded]
					array_push($dd,'SLK');

					//Tax Code
					array_push($dd,$detail->product->vat_code);

					//BP Tax Country [Hard Coded]
					// array_push($dd,$value->distributor->country);
					array_push($dd,'SLK');

					//Exempt [Hard Coded]
					array_push($dd,'2');

					//Tax Classification
					array_push($dd,'');

					//Warehouse [MEGODA WAREHOUSE]
					array_push($dd,'555FP2');

					//Channels
					//array_push($dd,'');
					
					//Work Center
					array_push($dd,'');

					//Success Percentage [Hard Coded]
					array_push($dd,'0');

					//Reason for Success
					array_push($dd,'');

					//Competitor
					array_push($dd,'');

					//Inventory Handling [Hard Coded]
					array_push($dd,'1');

					//Shipping Constraint [Hard Coded]
					array_push($dd,'1');

					//Ship to business partner
					//array_push($dd,$value->distributor->ship_to_business_par);
					array_push($dd,$value->customer->code);

					//Ship to address
					//array_push($dd,$value->distributor->ship_to_address);
					array_push($dd,$value->customer->customer_address_code);

					//Ship to contact
					array_push($dd,'');

					//Status [Hard Coded]
					array_push($dd,'1');

					//Item Code System
					array_push($dd,'');

					//Route
					array_push($dd,'');

					//Plan Receipt Date
					array_push($dd,date('dmy',strtotime($value->quotation_date)));

					//Carrier/LSP
					array_push($dd,'');

					//Project
					//array_push($dd,'');
					
					//Exempt Certificate
					array_push($dd,'');

					//Exempt Reason
					array_push($dd,'');

					//Line Quote Reference
					array_push($dd,'');

					//Generate freight ord [Hard Coded]
					array_push($dd,'2');

					//invoice for freight [Hard Coded]
					array_push($dd,'2');

					//freight amount [Hard Coded]
					array_push($dd,'0');

					//Freight amount bindi [Hard Coded]
					array_push($dd,'2');

					//carrier binding [Hard Coded]
					array_push($dd,'2');

					//Freight service leave [Hard Coded]
					array_push($dd,'');

					//List group [Hard Coded]
					array_push($dd,'');

					//Terms of delivery [Hard Coded]
					array_push($dd,'');

					//Point of title passa [Hard Coded]
					array_push($dd,'');

					//Order promising state [Hard Coded]
					array_push($dd,'3');

					//Obselete [Hard Coded]
					array_push($dd,'');

					//Obselete [Hard Coded]
					array_push($dd,'0');

					//Quotaion line text [Hard Coded]
					array_push($dd,'0');

					array_push($data, $dd);
				}
			}

			if(file_exists($this->csvPath.'/tdsls151.csv')){
				rename($this->csvPath.'/tdsls151.csv',
					$this->csvPath.'/tdsls151_'.date('YmdHis').'.csv');
			}

			$excl = Excel::create('tdsls151',function($excel) use ($data) {
				$excel->sheet('Sheetname', function($sheet) use($data) {
			        $sheet->fromArray($data, null, 'A1', false, false);
			    });
			});

			if($isDownload){
				$excl->download('csv');
			}else{
				return $excl->store('csv', $this->csvPath);
			}
		}

		return true;
	}

	public function changeStatusToPOProcessing($data){
		$quotations = Quotation::where('status',CLOSED);

		if($data->start && $data->start != ''){
			$quotations->where('quotation_date', '>=', $data->start.' 00:00:00');
		}

		if($data->end && $data->end != ''){
			$quotations->where('quotation_date', '<=', $data->end.' 00:00:00');
		}

		if($data->quotation_no && $data->quotation_no != ''){
			$quotations->where('reference_no', 'like', '%'.$data->quotation_no.'%');
		}

		if($data->customer && $data->customer != ''){
			$quotations->whereIn('customer_id', function($q) use ($data){
				return $q->select('id')
					->from('customer')
					->where('code', 'like', '%'. $data->customer.'%')
					->orWhere('first_name', 'like', '%'. $data->customer.'%')
					->orWhere('last_name', 'like', '%'. $data->customer.'%');
			});
		}

		if($data->quotation_id && $data->quotation_id != ''){
			$quotations->where('id',$data->quotation_id);
		}

		$quotations = $quotations->update(['status' => PO_PROCESSING]);

	}
}
