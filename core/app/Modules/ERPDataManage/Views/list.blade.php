
@extends('layouts.back_master')
@section('title','Download Data')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
    <link rel="stylesheet" type="text/css" src="{{asset('assets/dist/bootstrap-datepicker/css/bootstrap-datepicker.css')}}"/>
    <style type="text/css">
        table .btn{
            padding: 2px 6px;
        }

        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
            padding-top:5px;
            padding-bottom:5px;
        }
    </style>
@stop
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            InforLN Data
            <small> Management</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
            <li class="active">Sync SAP Data</li>
        </ol>
    </section>

    <!-- SEARCH -->
    <section class="content">
        <!-- Default box -->
        <form  class="form-horizontal form-validation" role="form"  action="{{URL('admin/erp/add')}}" method="post" id="sync_form"
               name="sync_form">
            {!!Form::token()!!}
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Sync Data</h3>
            </div>
            <div class="panel box-body">
                @if(isset($sync_type))
                    <div class="row">
                        <div class="col-lg-12">
                            <table  class="table table-bordered">
                                <thead>
                                    <tr>
                                        <td class="text-center">#</td>
                                        <td class="text-center">Type</td>
                                        <td class="text-center">Last Sync Date</td>
                                        <td class="text-center">Synced by</td>
                                        <td class="text-center">Action</td>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $i =1; ?>
                                    <input type="hidden" id="type" name="type">
                                    @foreach($sync_type as $data)
                                        <tr>
                                            <td class="text-center" width="10%">{{$i}}</td>
                                            <td class="text-left" width="30%">{{$data->name}}</td>
                                            <td class="text-left" width="30%">{{$data->last_sync}}</td>
                                            <td class="text-left" width="10%">{{$data->employee}}</td>
                                            <td class="text-center" width="10%"> <button  onclick="download_data({{$data->id}})" type="button"  class="btn btn-primary"><i  class="fa  fa-refresh" style="padding-right: 16px;width: 12px;"></i>Sync</button></td>
                                        </tr>
                                        <?php $i++; ?>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @else
                    <br>
                    <h4 align="center" style="color: #6d6d6d;">Data not found!.</h4>
                    <br>
                    <br>
                @endif
            </div>
            <div class="overlay" style="display:none;">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
        </form>
    </section>
    <!-- Default box -->
@stop
@section('js')
    <script type="text/javascript">
        var table = '';
        

        function download_data(id) {
            $(".overlay").show();
            $('#type').val(' ');
            $('#type').val(id);
            document.sync_form.submit();
            // $('.panel').removeClass('panel-refreshing');
        }

    </script>
@stop




































