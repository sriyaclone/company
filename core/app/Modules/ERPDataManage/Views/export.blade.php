
@extends('layouts.back_master')
@section('title','Download Data')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
    <link rel="stylesheet" type="text/css" src="{{asset('assets/dist/bootstrap-datepicker/css/bootstrap-datepicker.css')}}"/>
    <style type="text/css">
        table .btn{
            padding: 2px 6px;
        }

        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
            padding-top:5px;
            padding-bottom:5px;
        }
    </style>
@stop
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            InforLN Data
            <small>Management</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
            <li class="active">Export InforLN</li>
        </ol>
    </section>

    <!-- SEARCH -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Export Data</h3>
            </div>
            <div class="panel box-body">
                <form method="get">
                    <div class="row">
                        <div class="col-sm-4">
                            <label for="" class="control-label">Customer</label>
                            <input name="customer" class="form-control" value="{{$old->customer}}" type="text">
                        </div>
                        <div class="col-sm-2">
                            <label for="" class="control-label">Quotation #</label>
                            <input name="quotation_no" class="form-control" value="{{$old->quotation_no}}" type="text">
                        </div>
                        <div class="col-sm-3">
                            <label for="" class="control-label">Start</label>
                            <input name="start" class="form-control datepick" value="{{$old->start}}" type="text">
                        </div>
                        <div class="col-sm-3">
                            <label for="" class="control-label">End</label>
                            <input name="end" class="form-control datepick" value="{{$old->end}}" type="text">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" style="margin-top:10px;margin-bottom:10px;">
                            <button class="btn btn-primary pull-right" type="submit"><i class="fa fa-search"></i> Search</button>
                        </div>
                    </div>
                </form>

                @if($quotations && count($quotations) > 0)
                <div class="row">
                    <form  class="form-horizontal form-validation" method="post"
               name="sync_form">
                        {!!Form::token()!!}
                    <div class="col-md-12" style="margin-top:10px;margin-bottom:10px;">
                        <input type="hidden" name="start1" value="{{$old->start}}">
                        <input type="hidden" name="end1" value="{{$old->end}}">
                        <input name="quotation_no1" value="{{$old->quotation_no}}" type="hidden">
                        <input name="customer1" value="{{$old->customer}}" type="hidden">
                        <button class="btn btn-primary pull-right btn-export" type="submit">Export</button>
                    </div>
                    </form>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <table  class="table table-bordered">
                            <thead>
                                <tr>
                                    <td class="text-center">#</td>
                                    <td class="text-center">Customer Name</td>
                                    <td class="text-center">Quotation No</td>
                                    <td class="text-center">Quotation Date</td>
                                    <td class="text-center">Total Amount</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($quotations as $key => $value)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{ $value->customer->first_name.' '.(($value->customer->last_name)?$value->customer->last_name:'') }}</td>
                                        <td>{{ $value->reference_no }}</td>
                                        <td>{{ $value->quotation_date }}</td>
                                        <td>{{ $value->total_amount }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                @else
                <div class="row">
                    <div class="col-lg-12">
                        <table  class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td align="center"><h3> - No Records Found - </h3></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                @endif
            </div>
            <div class="overlay" style="display:none;">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </section>
    <iframe id="my_iframe" style="display:none;"></iframe>
    <!-- Default box -->
@stop
@section('js')
<script src="{{asset('assets/dist/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript">
    function Download(url) {
        //window.open(url,'_blank');
        window.location = url;
        //document.getElementById('my_iframe').src = url;
    };

    function sleep(milliseconds) {
      var start = new Date().getTime();
      for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds){
          break;
        }
      }
    }

    $(document).ready(function(){
        $('.datepick').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            format: 'yyyy-mm-dd'
        });

        $('.btn-export').click(function(e){
            //e.preventDefault();
            $(".overlay").show();

            /*var start = $('input[name="start1"]').val();
            var end = $('input[name="end1"]').val();
            var customer = $('input[name="customer1"]').val();
            var quotation = $('input[name="quotation_no1"]').val();

            Download('{{url('admin/erp/export/csv')}}?start='+start+'&end='+end+'&type=1&customer='+customer+'&quotation_no='+quotation);
            setTimeout(function(){
                Download('{{url('admin/erp/export/csv')}}?start='+start+'&end='+end+'&type=2&customer='+customer+'&quotation_no='+quotation);
                setTimeout(function(){
                    Download('{{url('admin/erp/export/csv')}}?start='+start+'&end='+end+'&type=3&customer='+customer+'&quotation_no='+quotation);
                    setTimeout(function(){
                        Download('{{url('admin/erp/export/csv')}}?start='+start+'&end='+end+'&type=4&customer='+customer+'&quotation_no='+quotation);
                        $(".overlay").hide();
                    },3000);
                },3000);
            },3000);*/
        });
    });

</script>
@stop




































