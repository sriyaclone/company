@extends('layouts.back_master') @section('title','Grn Details')
@section('css')
<style type="text/css">
    table .btn{
        padding: 2px 6px;
    }

    .table>tbody>tr>td, 
    .table>tbody>tr>th, 
    .table>tfoot>tr>td, 
    .table>tfoot>tr>th, 
    .table>thead>tr>td, 
    .table>thead>tr>th{
        padding-top:5px; 
        padding-bottom:5px; 
    }
</style>
@stop
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Grn Management</h2>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{url('admin/grn/list')}}">Grn Management</a></li>
            <li class="active">Grn List</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a class="btn btn-warning" href="{{ url('admin/grn/list') }}"><i class="fa fa-th" aria-hidden="true"></i> Grn</a>
            <a class="btn btn-warning" href="{{ url('admin/grn/create') }}"><i class="fa fa-plus" aria-hidden="true"></i> Grn</a>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Grn Details</h5>
            <div class="ibox-tools">
                
            </div>
        </div>

        <div class="ibox-content" style="display: block;">
            <div class="row">
                <div class="col-md-12">
                    <strong>
                        <h3 class="text-center">
                            Regular Grn
                        </h3>
                    </strong>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <h3 style="margin-top: 2px;margin-bottom: 0px;">{{ $list->manual_id?:'-' }}</h3>
                    <h4 style="margin-bottom: 0;margin-top: 8px;">{{ $list->supplier_name?:'-' }}</h4>
                    <h5 style="margin-top: 5px;">{{ $list->supplier_company?:'-' }}</h5>
                </div>
                <div class="col-md-6">
                    <h4 class="text-right" style="margin-bottom: 0">
                        {{ ($list->created_at)?date('jS M, Y',strtotime($list->created_at)):'-' }}
                    </h4>
                    <h5 class="text-right" style="margin-top:3px;margin-bottom: 0;">
                        {{ $list['createdBy']->first_name?:'-' }} {{ $list['createdBy']->last_name?:'-' }}
                    </h5>
                    <h5 class="text-right" style="margin-top:3px;margin-bottom: 0;">
                        {{ $list->warehouse_name?:'-' }}
                    </h5>
                    <h5 class="text-right" style="margin-top: 8px;">
                        @if(PENDING == $list->status)
                            <span class="text-warning">Pending</span>
                        @elseif(APPROVED == $list->status)
                            <span class="text-success">Approved</span>
                        @elseif(REJECTED == $list->status)
                            <span class="text-danger">Rejected</span>
                        @elseif(REVISED == $list->status)
                            <span class="text-primary">Revised</span>
                        @elseif(CLOSED == $list->status)
                            <span class="text-success">Closed</span>
                        @elseif(INVOICED == $list->status)
                            <span class="text-success">Invoiced</span>
                        @elseif(PO_PROCESSING == $list->status)
                            <span class="text-success">PO Processing</span>
                        @else
                            -
                        @endif
                    </h5>
                  
                    @if($list->status != PENDING && $list->status != REJECTED)
                        <div class="text-right">
                            <a href="{{url('admin/grn/grn_pdf/')}}/{{$list->id}}" target="_blank"><i class="fa fa-print"></i> PDF</a>
                        </div>
                    @endif
                </div>
            </div>

            <div class="row">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-left" width="15%">Product Code</th>
                            <th class="text-left">Product</th>
                            <th class="text-right">Mrp (Rs)</th>
                            <th class="text-right">Qty</th>
                            <th class="text-right">Amount (Rs)</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($list->details) > 0)
                            @foreach($list->details as $key => $detail)
                                <tr>
                                    <td>{{ (++$key) }}</td>
                                    <td>{{ $detail->product->code?:'-' }}</td>
                                    <td>{{ $detail->product->name?:'-' }}</td>
                                    <td align="right">{{ number_format($detail->mrp, 2)?:'-' }}</td>
                                    <td align="right">{{ number_format($detail->qty, 0)?:'-' }}</td>
                                    <td align="right">{{ number_format($detail->net_amount, 2)?:'-' }}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="5" align="right">Amount</td>
                                <td align="right">{{ number_format($list->details->sum('net_amount'), 2)?:'-' }}</td>
                            </tr>                            

                            <tr>
                                <td colspan="5" align="right"><strong>Total Amount</strong></td>
                                <td align="right"><strong>{{ number_format(
                                    $list->details->sum('net_amount'), 2)?:'-' }}</strong>
                                </td>
                            </tr>
                        @else
                        <tr>
                            <td colspan="9" align="center"> - No Data to Display - </td>
                        </tr>
                    @endif  
                    </tbody>
                </table>
            </div>

        </div>

    </div>
</div>

@stop
@section('js')
<script src="{{asset('assets/dist/chosen/chosen.jquery.min.js')}}"></script>
<script src="{{asset('assets/dist/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>

<script type="text/javascript">
$(document).ready(function() {
  $(".chosen").chosen();

  $('.datepick').datepicker({
      keyboardNavigation: false,
      forceParse: false,
      format: 'yyyy-mm-dd'
  });
});
</script>
@stop
