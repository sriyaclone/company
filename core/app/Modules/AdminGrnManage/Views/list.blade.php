@extends('layouts.back_master') @section('title','List Grn')
@section('css')
<style type="text/css">
    
</style>
@stop
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Grn Management</h2>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{url('admin/grn/list')}}">Grn Management</a></li>
            <li class="active">Grn List</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a class="btn btn-warning" href="{{ url('admin/grn/create') }}"><i class="fa fa-plus" aria-hidden="true"></i> Grn</a>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Grn List</h5>
            <div class="ibox-tools">
                
            </div>
        </div>

        <div class="ibox-content" style="display: block;">
            <table class="table table-bordered">
                <thead>
                    <form role="form" method="get">
                    {!!Form::token()!!}
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-left">Manual No
                                <input type="text" class="form-control" name="manual_id" placeholder="Enter manual no" value="{{$old->manual_id}}">
                            </th>
                            <th class="text-left">Supplier
                                <input type="text" class="form-control" name="supplier" placeholder="Enter supplier name" value="{{$old->supplier}}">
                            </th>
                            <th class="text-left">Warehouse
                                <input type="text" class="form-control" name="warehouse" placeholder="Enter warehouse name" value="{{$old->warehouse}}">
                            </th>
                            <th class="text-left">Created Date</th>
                            <th class="text-right">Amount (Rs)</th>
                            <th class="text-right">Action By</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Actions<br>
                                <button class="btn btn-default pull-center" type="submit"><i class="fa fa-search"></i> Filter</button>
                            </th>
                        </tr>
                    </form>
                </thead>
                <tbody>
                    @if(count($data) > 0)
                        @foreach($data as $key => $val)
                            <tr>
                                <td align="center">
                                    {{ (($data->currentPage()-1)*$data->perPage())+($key+1) }}
                                </td>
                                <td align="left">
                                    {{$val->manual_id}}
                                </td>
                                <td align="left">
                                    @if($val['supplier'])
                                        {{$val['supplier']->name}}<br>{{$val['supplier']->code}}
                                    @else
                                        -
                                    @endif
                                </td>
                                <td align="left">
                                    @if($val['warehouse'])
                                        {{$val['warehouse']->name}}<br>{{$val['warehouse']->code}}
                                    @else
                                        -
                                    @endif
                                </td>
                                <td align="left">
                                    {{$val->created_at}}
                                </td>
                                <td align="right">
                                    {{number_format($val->total_amount,2)}}
                                </td>
                                <td align="right">
                                    {{$val['createdBy']->first_name}} {{$val['createdBy']->last_name}}
                                </td>
                                <td align="left"> </td>
                                <td align="center">
                                    <a href="{{ url('admin/grn/listdetail') }}/{{$val->id}}" target="_blank"><i class="fa fa-th" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9" align="center"> - No Data to Display - </td>
                        </tr>
                    @endif  
                </tbody>
            </table>
      
            <div class="row">
                <div class="col-sm-12 text-left">
                     Row {{$data->count()}} of {{$data->total()}} 
                </div>
                <div class="col-sm-12 text-right">
                    {!! $data->appends($old->except('page'))->render() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@stop
@section('js')

<script type="text/javascript">
    $(document).ready(function() {
        $(".chosen").chosen();

        $('.datepick').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            format: 'yyyy-mm-dd'
        });
    });
</script>
@stop
