<?php


Route::group(['middleware' => ['auth']], function(){

	Route::group(array('prefix'=>'admin/grn/','module' => 'AdminGrnManage', 'namespace' => 'App\Modules\AdminGrnManage\Controllers'), function() {

		/*** GET Routes**/

		Route::get('list', [
	      'as' => 'grn.list', 'uses' => 'AdminGrnManageController@listView'
	    ]);

	    Route::get('create', [
	      'as' => 'grn.create', 'uses' => 'AdminGrnManageController@createView'
	    ]);

	    Route::get('grn_pdf/{id}', [
	      'as' => 'grn.view', 'uses' => 'AdminGrnManageController@grnPdf'
	    ]);

		Route::get('listdetail/{id}', [
		  'as' => 'grn.list.detail', 'uses' => 'AdminGrnManageController@listViewDetail'
	    ]);

	    Route::get('view/{id}', [
		  'as' => 'grn.view', 'uses' => 'AdminGrnManageController@grnView'
	    ]);

		/***JSON Routes**/
        Route::get('getItems', [
            'as' => 'grn.create', 'uses' => 'AdminGrnManageController@getItems' 
        ]);

        Route::get('getWarehouses', [
            'as' => 'grn.create', 'uses' => 'AdminGrnManageController@getWarehouses' 
        ]);

        Route::get('getGrnWithDetails', [
            'as' => 'grn.revise', 'uses' => 'AdminGrnManageController@getGrnWithDetails' 
        ]);

		/***POST Routes**/

        Route::post('edit', [
        	'as' => 'grn.edit', 'uses' => 'AdminGrnManageController@edit' 
        ]);

        Route::post('create', [
        	'as' => 'grn.create', 'uses' => 'AdminGrnManageController@create' 
        ]);

	}); 

}); 