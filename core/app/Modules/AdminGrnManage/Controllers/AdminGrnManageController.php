<?php namespace App\Modules\AdminGrnManage\Controllers;


/**
* Controller class
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use Sentinel;
use PDF;
use DB;
use File;
use Mail;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Vat;
use App\Models\User;
use App\Models\AppUser;
use App\Models\Warehouse;
use App\Modules\AdminSupplierManage\Models\Supplier;

use Core\EmployeeManage\Models\Employee;

use App\Modules\AdminGrnManage\Requests\Grn;
use App\Modules\AdminGrnManage\Requests\GrnRequest;
use App\Modules\AdminGrnManage\BusinessLogics\GrnLogic;

class AdminGrnManageController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	protected $grnLogic;

	public function __construct(GrnLogic $grnLogic){
		$this->grnLogic = $grnLogic;
	}

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listView(Request $request)
	{
		$old = $request;

		$data = $this->grnLogic->getGrnRecords($old);

		return view("AdminGrnManage::list", compact('data','old'));
	}

	public function approveView(Request $request)
	{
		$old = $request;
		$action_by = $this->grnLogic->getEmployees();
		$data = $this->grnLogic->getApprovalQuotations($old);
		return view("AdminGrnManage::approval", compact('data','old','action_by'));
	}

	public function createView(Request $request)
	{
		$grnTypes = $this->grnLogic->getGrnTypes();

		$warehouseTypes = $this->grnLogic->getWarehouseTypes();

		$warehouses = $this->grnLogic->getWarehouses();

		$suppliers = $this->grnLogic->getSuppliers();

		return view("AdminGrnManage::create",compact('products','grnTypes','warehouseTypes','warehouses','suppliers'));
	}

	public function create(Request $request)
	{	
		$grn = $this->grnLogic->createNewGrn($request);
		$url = url('admin/grn/grn_pdf/')."/".$grn->id;
		
		return redirect('admin/grn/create')->with([
			'link'           => true,
			'link.link'      => $url,
			'link.linktitle' => 'Show Grn',
			'link.message'   => 'New Grn saved! #('.$grn->code.')',
			'link.title'     => 'Success..!'
        ]);
	}


	public function grnPdf(Request $request,$id)
	{
		return  $this->grnLogic->getGrnPdfOutput($id);
	}

	public function listViewDetail($id, Request $request)
	{
		$id        = $request->id;
		$details   = $this->grnLogic->getGrnDetailRecords($id);
		$list      = $this->grnLogic->getGrnById($id);

		return view("AdminGrnManage::details", compact('details', 'list', 'vat'));
	}

	public function show($id, Request $request)
	{
		$user_role = $this->grnLogic->getUserRole();
		$max_discount = ($user_role)?$user_role->discount:0;
		/*$quotation = $this->grnLogic->getRecordsQuotationId($id);

		$quotation_detail = $this->grnLogic->getRecordsQuotationDetailId($id);
		
		$inquiry_id = $quotation->inquiry_id;
		$data = $this->grnLogic->getInquiryData($inquiry_id);*/

		$id      = $request->id;
		$details = $this->grnLogic->getGrnDetailRecords($id);
		$list    = $this->grnLogic->getGrnById($id);

		if($list->quotation_status != PENDING || 
			$max_discount < 1 || 
			$list->approval_role_id != $user_role->id){
			return view('errors.404');
		}

		$vat = Vat::where('id', $list->vat_id)->first();
		$nbt = Vat::where('id', $list->nbt_id)->first();

		if($list->vat_type == 'vat' || $list->vat_type == 'svat'){
			$list = $this->grnLogic->calculateVatCustomerQuotation($list);
		}else{
			$list = $this->grnLogic->calculateNonVatCustomerQuotation($list);
			//$vat = null;
		}

		//return $list;

		/*return view("AdminGrnManage::approvaldetail", compact('data','quotation','quotation_detail','max_discount'));*/

		return view("AdminGrnManage::approve_view", compact('details', 'list', 'vat', 'max_discount', 'nbt'));
	}

	public function getItems(Request $request){
		return $this->grnLogic->getItems(3,$request->search);
	}

	public function getWarehouses(Request $request){
		return $this->grnLogic->getWarehouses();
	}

}