<?php namespace App\Modules\AdminGrnManage\BusinessLogics;


/**
* Business Logics 
* Define all the busines logics in here
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use Illuminate\Database\Eloquent\Model;
use Core\EmployeeManage\Models\Employee;

use App\Classes\Functions;
use App\Classes\Contracts\BaseStockHandler;

use App\Models\User;
use App\Models\DeliveryTerms;
use App\Models\Warehouse;
use App\Models\WarehouseType;

use Core\UserRoles\Models\UserRole;

use App\Modules\AdminProductManage\Models\Product;
use App\Modules\AdminGrnManage\Models\Grn;
use App\Modules\AdminGrnManage\Models\GrnDetail;
use App\Modules\AdminGrnManage\Models\GrnType;
use App\Modules\AdminSupplierManage\Models\Supplier;
use App\Modules\AdminOrderManage\Models\OrderDetail;

use DB;
use Excel;
use File;
use Response;
use Sentinel;
use PDF;
use QrCode;

class GrnLogic extends Model {

	private $grn;
	private $grnDetail;
	private $functions;
	private $baseStockHandler;

	public function __construct(Grn $grn , GrnDetail $grnDetail,Functions $functions,BaseStockHandler $baseStockHandler){
		$this->grn = $grn;
		$this->grnDetail = $grnDetail;
		$this->functions = $functions;
		$this->baseStockHandler = $baseStockHandler;
	}

	public function getGrnRecords($filters, $perPage = 10)
	{
		$data = Grn::with(['createdBy','supplier','warehouse','details']);

		if($filters->manual_id != ""){
			$data->where('grn.manual_id','LIKE','%'.$filters->manual_id.'%');
		}

		if($filters->supplier != ""){
			$ts = $filters->supplier;
			$data->whereIn('grn.supplier_id',function($query)use($ts){
				$query->select('id')
					->from('supplier')
					->where('supplier.name','like','%'.$ts.'%');
			});
		}

		if($filters->warehouse != ""){
			$tp = $filters->warehouse;
			$data->whereIn('grn.warehouse_id',function($query)use($tp){
				$query->select('id')
					->from('warehouse')
					->where('warehouse.name','like','%'.$tp.'%');
			});
		}

		$data = $data->orderBy('grn.id','desc');
		return $data->paginate($perPage);
	}	

	public function getGrnDetailRecords($id)
	{
		$data = GrnDetail::with(['product'])->where('grn_id','=',$id)->get();

        return $data;
	}

	public function getRecordsgrnId($id)
	{
		return $data = Grn::with(['customer'])->where('id',$id)->first();
	}

	public function getRecordsgrnDetailId($id)
	{
		return $data = GrnDetail::with(['product'])->where('grn_id',$id)->get();
	}

	
	public function getDeliveryTerms()
	{
		return $data = DeliveryTerms::lists('name','id');
	}

	public function getItems($limit,$search)
	{
		$data = Product::where('code','like','%'.$search.'%')->orWhere('name','like','%'.$search.'%')->whereNull('deleted_at');

        return $data = $data->paginate($limit);
	}

	public function getWarehouseTypes()
	{
		$data = WarehouseType::whereNull('deleted_at')->selectRaw('id,name')->get()->prepend(['id'=>'','name'=>'Select a Warehouse Type']);

        return $data;
	}

	public function getWarehouses()
	{
		$data = Warehouse::whereNull('deleted_at')->selectRaw('id,name')->get()->prepend(['id'=>'','name'=>'Select a Warehouse']);

        return $data;
	}

	public function getSuppliers()
	{
		$data = Supplier::whereNull('deleted_at')->selectRaw('id, CONCAT(code,\'-\',name) as name')->get()->prepend(['id'=>'','name'=>'Select a Supplier']);

		return $data;
	}

	public function getGrnTypes()
	{
		$data = GrnType::whereNull('deleted_at')->selectRaw('id,name')->get()->prepend(['id'=>'','name'=>'Select a Grn Type']);

		return $data;
	}

	public function getAllItems($limit)
	{
		$data = Product::whereNull('deleted_at');

		return $data = $data->paginate($limit);
	}

	public function createNewGrn($data)
	{

		$user = Sentinel::getUser();

		$created_by = $user->employee_id;	
		
		$grn = Grn::create([
			'code'        		  => null,
			'supplier_id'         => $data->supplier,
			'warehouse_id'        => $data->warehouse,
			'status'              => 0,
			'created_by'          => $created_by,
			'remark' 			  => $data->remark,
			'manual_id' 		  => $data->manual_no
        ]);

		 $total=0;

         foreach ($data->product_id as $key => $details) {

            $grn_details = GrnDetail::create([
				'grn_id'   				=> $grn->id,
				'batch'     			=> $grn->id,
				'product_id'     		=> $data->product_id[$key],
				'mrp'          			=> $data->mrp[$key],
				'qty'            		=> $data->qty[$key],
				'net_amount'            => $data->line_total_amount[$key]
            ]);

            $total += $data->line_total_amount[$key];

            $trans = $this->baseStockHandler->stockIn(
            	$data->product_id[$key], 
            	(int)$data->qty[$key], 
            	$data->warehouse, 
            	$grn->id, 
            	2, 
            	$data->mrp[$key]
            );
	        
	    }

	    $code = "GRN"."_".$user->employee_id."_".$grn->id;
		$grn->total_amount = $total;
		$grn->code = $code;
		$grn->save();

	    return $grn;
	}

	public function getGrnPdfOutput($id, $type='N')
	{
		$grn = $this->getGrnById($id);

        $qr = QrCode::format('png')
        		->errorCorrection('L')
        		->size(200)
        		->margin(2)
        		->generate($grn->manual_id.'|'.$grn->total_amount); 

        $fileName = 'grn/grn-'.$grn->id.'-'.date('YmdHis').'.pdf';
        $page = view("AdminGrnManage::new-grn-pdf", compact('grn','qr'))
        		->render();
		PDF::reset();
		//PDF::setFont('myriadpro');
		PDF::setMargins(10,18,10);
		PDF::setAutoPageBreak(TRUE, 32);
		PDF::setHeaderCallback(function($pdf){
			$image_file = url('assets/pi-images/h-img02.png');
        	$pdf->Image($image_file, 10, 5, 195, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		});

		PDF::setFooterCallback(function($pdf){
			$image_file = url('assets/pi-images/f-img01.png');
        	$pdf->Image($image_file, 10, 265, 195, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		});
        PDF::AddPage();
        PDF::writeHTML($page);
        if (ob_get_contents()) ob_end_clean();
        return PDF::Output(public_path($fileName),'I');
	}

	public function getGrnById($id)
	{
		try {
			$data = Grn::select('*', 
					DB::raw('grn.id AS id'),
					DB::raw('warehouse.name as warehouse_name'),
					DB::raw('supplier.name as supplier_name'),
					DB::raw('supplier.company as supplier_company'),
					DB::raw('grn.status AS grn_status'),
					DB::raw('supplier.code as supplier_code'),
					DB::raw('supplier.address AS supplier_address'),
					DB::raw('grn.status AS grn_status'),
					DB::raw('grn.created_by'))
				->join('supplier','supplier.id','=','grn.supplier_id')
				->join('warehouse','warehouse.id','=','grn.warehouse_id')
				->where('grn.id', $id)
				->orderBy('grn.id','asc')
				->with(['details.product','createdBy'])
				->first();

		    return $data;	

		} catch (\Exception $e) {
			return $e->getMessage();
		}
	}

	public function createGrnUsingOrder($data)
	{

		$user = Sentinel::getUser();

		$created_by = $user->employee_id;	
		
		$grn = Grn::create([
			'code'        		  => null,
			'supplier_id'         => $data->supplier_id,
			'warehouse_id'        => $data->warehouse_id,
			'order_id'        	  => $data->id,
			'status'              => 0,
			'type'     			  => GRN_BY_ORDER,
			'created_by'          => $created_by
        ]);

		$total=0;

		$details = OrderDetail::where('order_id',$data->id)->get();

        foreach ($details as $key => $detail) {

            $grn_details = GrnDetail::create([
				'grn_id'   				=> $grn->id,
				'batch'     			=> $grn->id,
				'product_id'     		=> $detail->product_id,
				'mrp'          			=> $detail->mrp,
				'qty'            		=> $detail->qty,
				'net_amount'            => $detail->net_amount
            ]);

            $total += $detail->net_amount;

            $trans = $this->baseStockHandler->stockIn(
            	$detail->product_id, 
            	(int)$detail->qty, 
            	$data->warehouse_id, 
            	$grn->id, 
            	2, 
            	$detail->mrp
            );
	        
	    }

	    $code = "GRN"."_".$user->employee_id."_".$grn->id;
		$grn->total_amount = $total;
		$grn->code = $code;
		$grn->save();

	    return $grn;
	}

}