<?php namespace App\Modules\AdminGrnManage\Models;

/**
*
* Model
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Baum\Node;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GrnType extends Model{

	use SoftDeletes;

	protected $table = 'grn_type';

	protected $guarded = ['id'];

	public function grns(){
		return $this->hasMany('App\Modules\AdminGrnManage\Models\Grn', 'type_id')->whereNull('deleted_at');
	}

}
