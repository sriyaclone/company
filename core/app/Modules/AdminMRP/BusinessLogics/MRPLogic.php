<?php namespace App\Modules\AdminMRP\BusinessLogics;


/**
* Business Logics 
* Define all the busines logics in here
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use Illuminate\Database\Eloquent\Model;
use App\Modules\AdminMRP\Models\AdminMRP;
use Illuminate\Support\Facades\DB;
use Exception;
use Sentinel;
use Excel;

use App\Modules\AdminProductManage\BusinessLogics\ProductLogic;

class MRPLogic extends Model {

    private $product;

    public function __construct(ProductLogic $product){
        $this->product = $product;
    }

	public function add($data, $user=null){
    	DB::transaction(function() use($data, $user){

            if(!$user)
	    	  $user = Sentinel::getUser();

			AdminMRP::where('product_id', $data->product_id)
						->whereNull('ended_at')
						->update(['ended_at' => date('Y-m-d H:i:s')]);

	        $mrp = AdminMRP::create([
	            'product_id' 	=>  $data->product_id, 
	            'mrp'           =>  $data->mrp,
	            'created_by'    =>  $user->id
	        ]);

	        if($mrp) {
                $this->product->updateProduct($data->product_id, $data,false);
                
	        	return $mrp;
	        }else{
	            throw new TransactionException('Record wasn\'t updated', 101);
	        }
	    });
    }

    /**
     * This function is used to get all mrp details
     * @parm  integer $id that need to get mrp details
     * @return mrp object
     */
    public function getMRPDetails($id){
        $mrp_details = AdminMRP::where('id','=',$id)->whereNull('ended_at')->get();
        if(count($mrp_details) > 0){
            return $mrp_details;
        }else{
            return false;
        }
    }    

    /**
     * This function is used to get all product category details
     * @parm -
     * @return product category object
     */
    public function getAllMRP(){
        return AdminMRP::with(['product'])->whereNull('ended_at')->paginate(10);
    }

    /**
     * This function is used to search mrp
     * @param integer $name, $code
     * @response mrp object
     */
    public function searchMRP($name, $code,$price) {
        $search = AdminMRP::select('mrp.*')->with(['product'])
                ->leftJoin('product as products', function($product_join){
                    $product_join->on('mrp.product_id', '=', 'products.id');
                })->whereNull('mrp.ended_at');

        if($name !== ''){
            $array=explode(" ", $name);
            foreach ($array as $value) {
                $search->where('products.name','LIKE', '%' . $value . '%');                
            }
        }

        if($code !== ''){
            $search = $search->where('products.code','LIKE', '%' . $code . '%');
        }

        if($price !== ''){
            $search = $search->where('products.mrp','LIKE', '%' . $price . '%');
        }

        $search->orderBy('id','asc');
        return $search->paginate(25);
    }

    /**
     * This function is used to search mrp
     * @param integer $name, $code
     * @response mrp object
     */
    public function searchAllMRP($old) {
        $search = AdminMRP::select('mrp.*')->with(['product'])
                ->leftJoin('product as products', function($product_join){
                    $product_join->on('mrp.product_id', '=', 'products.id');
                })->whereNull('mrp.ended_at');

        if($old->name !== ''){
            $array=explode(" ", $old->name);
            foreach ($array as $value) {
                $search->where('products.name','LIKE', '%' . $value . '%');                
            }
        }

        if($old->code !== ''){
            $search = $search->where('products.code','LIKE', '%' . $old->code . '%');
        }

        if($old->price !== ''){
            $search = $search->where('products.mrp','LIKE', '%' . $old->price . '%');
        }

        $search->orderBy('id','asc');
        return $search->paginate(10);
    }

    public function uploadExcel($file){
        $data = [];
        $user = Sentinel::getUser();
        Excel::load($file, function($reader) use (&$data, $user){

            $reader->each(function($row) use (&$data, $user){
                try{

                    $this->validateRequiredData($row);

                    try{
                        $product = $this->product->getProductByCode(trim($row->code));
                    }catch(\Exception $e){
                        $product = null;
                    }

                    if(!$product){
                        array_push($data, [
                            'code' => $row->code,
                            'mrp' => $row->mrp,
                            'status' => 0,
                            'message' => 'Product not found.'
                        ]);
                    }else{
                        $this->product->updateProduct($product->id, $row, false);

                        AdminMRP::where('product_id', $product->id)
                                    ->whereNull('ended_at')
                                    ->update(['ended_at' => date('Y-m-d H:i:s')]);

                        $mrp = AdminMRP::create([
                            'product_id'    =>  $product->id, 
                            'mrp'           =>  $row->mrp,
                            'created_by'    =>  $user->id
                        ]);

                        if($mrp) {
                            array_push($data, [
                                'code' => $row->code,
                                'mrp' => $row->mrp,
                                'status' => 1,
                                'message' => 'Updated existing data.'
                            ]);
                        }else{
                            array_push($data, [
                                'code' => $row->code,
                                'mrp' => $row->mrp,
                                'status' => 0,
                                'message' => 'Record wasn\'t updated.'
                            ]);
                        }
                    }
                }catch(\Exception $e){
                    array_push($data, [
                        'code' => $row->code, 
                        'mrp' => $row->mrp,
                        'status' => 0,
                        'message' => $e->getMessage()
                    ]);
                }
            });
        });

        return $data;
    }

    public function validateRequiredData($data){
        if(!$data->code)
            throw new \Exception('PV1-Short Code is required');

        if(!$data->mrp)
            throw new \Exception('PV2-MRP is required');

        return 1;
    }

}
