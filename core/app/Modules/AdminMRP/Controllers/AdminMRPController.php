<?php namespace App\Modules\AdminMRP\Controllers;


/**
* Controller class
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Modules\AdminMRP\Models\AdminMRP;
use App\Modules\AdminMRP\BusinessLogics\MRPLogic;
use App\Modules\AdminMRP\Requests\MRPRequest;
use App\Modules\AdminProductManage\Models\Product;
use App\Modules\AdminProductManage\BusinessLogics\ProductLogic;
use Response;

class AdminMRPController extends Controller {

	protected $mrp;
	protected $product;

    public function __construct(MRPLogic $mrpLogic, ProductLogic $productLogic){
        $this->mrp = $mrpLogic;
        $this->product = $productLogic;
    }
      
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$mrp_details = $this->mrp->searchAllMRP($request);

		// $mrp_details = $this->mrp->getAllMRP();

		return view("AdminMRP::index")->with([
			'mrp_details' 	=> $mrp_details,
			'old' 			=> $request
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		$old = $request;

		$products = $this->product->getRecords($old);

		return view("AdminMRP::create", compact('old', 'products'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(MRPRequest $request)
	{
		try {
	        $bool = $this->mrp->add($request);

	        return Response::json([
				'status' => 'success',
				'title' => 'Success..!',				
				'msg' => 'MRP Updated successfully!'
			]);
	    }
	    catch(Exception $e){
            return Response::json([
				'status' => 'error',
				'title' => 'Error..!',				
				'msg' => $e->getMessage()
			]);
        }
	}

	/**
     * This function is used to search mrp
     * @return Response
     */
    public function searchMRP(Request $request){
        $mrp_details = $this->mrp->searchMRP($request->get('name'), $request->get('code'), $request->get('price'));
        
        return view("AdminMRP::index")->with([
        	'mrp_details' 	=> $mrp_details,
        	'name' 			=> $request->get('name'),
        	'code' 			=> $request->get('code'),
        	'price' 		=> $request->get('price')
        ]);
    }

    public function uploadView(Request $request)
	{
		return view("AdminMRP::upload");
	}

	public function upload(Request $request)
	{
		$this->validate($request, [
			'file' => 'required|mimes:xls'
		]);

		set_time_limit(0);
		$data = $this->mrp->uploadExcel($request->file);

		return redirect('admin/mrp/upload')->with([
			'data' => $data
		]);
	}

}
