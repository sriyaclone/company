<?php


Route::group(['middleware' => ['auth']], function()
{	
	Route::group(['prefix'=>'admin/mrp/','namespace' => 'App\Modules\AdminMRP\Controllers'],function() {

	    Route::get('create', [
	      	'as' => 'mrp.create', 'uses' => 'AdminMRPController@create'
	    ]);

        Route::get('index', [
            'as' => 'mrp.index', 'uses' => 'AdminMRPController@index'
        ]);

        Route::get('search', [
            'as' => 'mrp.search', 'uses' => 'AdminMRPController@searchMRP'
        ]);

        Route::get('upload', [
        	'as' => 'mrp.upload', 'uses' => 'AdminMRPController@uploadView'
        ]);
	    
	    Route::post('create', [
	      	'as' => 'mrp.create', 'uses' => 'AdminMRPController@store'
	    ]);

        Route::post('upload', [
        	'as' => 'mrp.upload', 'uses' => 'AdminMRPController@upload'
        ]);

	});    
});