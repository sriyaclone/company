@extends('layouts.back_master') @section('title','Admin - MRP Management')
@section('current_title','MRP List')

@section('css')
<style type="text/css">
    
</style>
@stop

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Mrp Management</h2>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{url('admin/mrp/list')}}">Mrp Management</a></li>
            <li class="active">Mrp List</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a class="btn btn-warning" href="{{ url('admin/mrp/create') }}"><i class="fa fa-plus" aria-hidden="true"></i> Mrp</a>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Mrp List</h5>
            <div class="ibox-tools">
                
            </div>
        </div>

        <div class="ibox-content" style="display: block;">
            
            <table class="table table-bordered">
                <thead>
                    <form id="form" role="form" method="get">
                        <tr>
                            <th width="5%">#</th>
                            <th>Product Code <br>
                                <input type="text" class="form-control input-sm" name="code" placeholder="Enter Code" value="{{$old['code']}}">
                            </th>
                            <th>Product Name <br>
                                <input type="text" class="form-control input-sm" name="name" placeholder="Enter Name" value="{{$old['name']}}">
                            </th>
                            <th>MRP <br>
                                <input type="text" class="form-control input-sm" name="price" placeholder="Enter MRP" value="{{$old['price']}}">
                            </th>
                            <th>Created At<br>
                            <button class="btn btn-default pull-left" type="submit"><i class="fa fa-search"></i> Filter</button></th> 
                        </tr>
                    </form>
                </thead>
                <tbody>
                    <?php $i = 1;?>
                        @if(count($mrp_details) > 0)
                            @foreach($mrp_details as $result_val)
                                @include('AdminMRP::template.mrp')
                                <?php $i++;?>
                            @endforeach
                        @else
                            <tr><td colspan="5" class="text-center">No data found.</td></tr>
                        @endif
                </tbody>
            </table>

            @if($mrp_details != null)
              <div style="float: right;">{!! $mrp_details->render() !!}</div>
            @endif

        </div>
    </div>
</div>
<!-- Content-->
@stop

@section('js')
<script type="text/javascript">
    $(document).ready(function(){
        $(".chosen").chosen();
    });
</script>
@stop
