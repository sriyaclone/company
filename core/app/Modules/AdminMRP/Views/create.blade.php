@extends('layouts.back_master') @section('title','Admin - MRP Management')
@section('current_title','Product Category Create')

@section('css')
<style type="text/css">
  
</style>
@stop

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Mrp Management</h2>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{url('admin/mrp/list')}}">Mrp Management</a></li>
            <li class="active">Create Mrp</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a class="btn btn-warning" href="{{ url('admin/mrp/index') }}"><i class="fa fa-th" aria-hidden="true"></i> Mrp List</a>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Add Mrp</h5>
            <div class="ibox-tools">
                
            </div>
        </div>

        <div class="ibox-content" style="display: block;">

            <form method="get">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label>Product Code</label>
                                <input type="text" class="form-control input-sm" name="code" placeholder="Enter Code" value="{{$old->code}}">
                            </div>
                        </div>
                        
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label>Product Name</label>
                                <input type="text" class="form-control input-sm" name="name" placeholder="Enter Name" value="{{$old->name}}">
                            </div>
                        </div>              
                    </div>
                
                    <div class="row">
                        <div class="col-md-3 col-md-offset-9">
                            <div class="pull-right">
                                <button type="submit" class="btn btn-default btn-sm" id="plan"><i class="fa fa-search"></i> Find</button>
                                <a href="create" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top"><i class="fa fa-refresh"></i> Refresh</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Product Code</th>
                        <th>Product Name</th>
                        <th>MRP</th>
                        <th>Created At</th>
                        <th width="10%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($products) > 0)
                        @foreach($products as $result)
                            <tr>
                                <td>{{$result->code}}</td>
                                <td>{{$result->name}}</td>
                                <td>
                                    <input type="text" class="form-control" name="mrp-{{$result->id}}" placeholder="Add MRP value" value="{{$result->mrp}}">
                                </td>
                                <td>{{$result->created_at}}</td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <a href="javascript:void(0);" class="btn btn-xs btn-success save" data-toggle="tooltip" data-placement="top" title="Save MRP" data-id="{{$result->id}}"><i class="fa fa-check"></i> Save</a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr><td colspan="6" class="text-center">No data found.</td></tr>
                    @endif
                </tbody>
            </table>

            @if($products != null)
                <div style="float: right;">{!! $products->render() !!}</div>
            @endif
        </div>
    </div>
</div>
@stop
@section('js')  
  <!-- CORE JS -->
	<script type="text/javascript">
    	$(document).ready(function() {
            $('.save').click(function(e){
                e.preventDefault();
                id = $(this).data('id');
                mrp = $('input[name=mrp-'+id+']').val();
                if(!mrp) {
                    alert('Please enter MRP value.');
                    return false;
                }else {
                    var validatePrice = function(mrp) {
                        // return /^(?:\d+|\d{1,2}(?:,\d{2})+)(?:\.\d+)?$/.test(mrp);
                        
                        return /^\d*(\.\d{1,2})?$/.test(mrp);
                    }

                    if(validatePrice(mrp) == false){
                        alert('Not a valid price format.');
                    }else{
                        ajaxRequest( '{{url('admin/mrp/create')}}' , { 'product_id' : id, 'mrp' : mrp }, 'post', successFunc);
                    }
                }
            });
	  	});

        function successFunc(response){
            if(response.status == 'success'){
                swal(response.title, response.msg+' !', 'success');
            }else if(response.status=='error'){
                swal('Oops', response.msg+' !', 'error');
            }
        }
	</script>
  <!-- //CORE JS -->
@stop
