<?php namespace App\Modules\BankMange\Controllers;


/**
* Controller class
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\BankMange\BusinessLogics\BankLogic;
use Illuminate\Http\Request;

class BankController extends Controller {


	private $logic;

	public function __construct(BankLogic $logic){
        $this->logic = $logic;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function list(Request $request)
	{
		$banks = $this->logic->list($request);
		return view("BankMange::list",compact('banks','request'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('BankMange::create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		try {
	        $bank = $this->logic->add($request);
	        return redirect('admin/bank/create')->with([
                'success' => true,
                'success.message' => 'Bank created successfuly!',
                'success.title'   => 'Bank!'
            ]);
	    }catch(Exception $e){
            return redirect('admin/bank/create')->with([
                'error' => true,
                'error.message' => 'Faild to add new bank!',
                'error.title'   => 'Faild..!'
            ]);
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$bank = $this->logic->getBank($id);
		return view('BankMange::update',compact('bank'));

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id,Request $request)
	{		
		try {
			$aa   = $this->logic->updateBank($id,$request);
	        return redirect('admin/bank/edit/'.$id)->with([
                'success' => true,
                'success.message' => 'Bank updated successfuly!',
                'success.title'   => 'Bank!'
            ]);
	    }catch(Exception $e){
            return redirect('admin/bank/edit/'.$id)->with([
                'error' => true,
                'error.message' => 'Faild to update bank!',
                'error.title'   => 'Faild..!'
            ]);
        }
	}

	public function delete(Request $request)
	{
		try{
			$this->logic->deleteBank($request->id);
			return response()->json([
				'status' => 1
			]);
		}catch(\Exception $e){
			return response()->json([
				'status' => 0,
				'message' => $e->getMessage()
			]);
		}
	}

}
