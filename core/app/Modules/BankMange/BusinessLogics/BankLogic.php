<?php namespace App\Modules\BankMange\BusinessLogics;


/**
* Business Logics 
* Define all the busines logics in here
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use App\Modules\BankMange\Models\Bank;
use DB;

class BankLogic {


	public function add($data){
    	DB::transaction(function() use($data){
            $bank = Bank::create([
				'bank_name'  => $data->bank_name,
				'status'     => 1,
				'short_code' => $data->code
            ]);
			
	        if($bank) {                
	        	return $bank;
	        }else{
	            throw new TransactionException('Record wasn\'t inserted', 101);
	        }
	    });
    }


    public function list($request){    	
    	$bank = Bank::where('status',1);

    	if($request->bank_name){
    		$bank = $bank->where('bank_name','like','%'.$request->bank_name.'%');
    	}

    	if($request->code){
    		$bank = $bank->where('short_code','like','%'.$request->code.'%');
    	}


		return $bank->paginate(15);
    }

    public function getBank($id){    	
		return Bank::find($id);
    }
	
	public function updateBank($id,$data){    	
		DB::transaction(function() use($id,$data){			
			$bank = $this->getBank($id);
            $bank->bank_name  = $data->bank_name;
			$bank->status     = 1;
			$bank->short_code = $data->short_code;
			$bank->save();			
	        if($bank) {                
	        	return $bank;
	        }else{
	            throw new TransactionException('Record wasn\'t updated', 101);
	        }
	    });
    }	

    public function deleteBank($id){
		$bank = $this->getBank($id);

		if(!$bank->delete()){
			throw new \Exception('P3-Could not delete Bank ID:'.$id);
		}

		return 1;
	}
}
