<?php namespace App\Modules\AdminSalesOrderManage\Controllers;


/**
* Controller class
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use Sentinel;
use PDF;
use DB;
use File;
use Mail;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Vat;
use App\Models\User;
use App\Models\Customer;
use App\Models\AppUser;

use Core\EmployeeManage\Models\Employee;

use App\Modules\AdminSalesOrderManage\Requests\SalesOrderRequest;
use App\Modules\AdminSalesOrderManage\BusinessLogics\SalesOrderLogic;

class AdminSalesOrderManageController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	protected $salesOrderLogic;

	public function __construct(SalesOrderLogic $salesOrderLogic){
		$this->salesOrderLogic = $salesOrderLogic;
	}

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listView(Request $request)
	{
		$old = $request;

		$data = $this->salesOrderLogic->getSalesOrderRecords($old);

		return view("AdminSalesOrderManage::list", compact('data','old'));
	}

	public function approveView(Request $request)
	{
		$old = $request;
		$action_by = $this->salesOrderLogic->getEmployees();
		$data = $this->salesOrderLogic->getApprovalQuotations($old);
		return view("AdminSalesOrderManage::approval", compact('data','old','action_by'));
	}


	public function createView(Request $request)
	{
		$warehouses = $this->salesOrderLogic->getWarehouses();

		$employee_types = $this->salesOrderLogic->getEmployeeTypes();

		$customers = Customer::whereNull('deleted_at')->selectRaw('id, CONCAT(code,\'-\',first_name) as name')->get()->prepend(['id'=>'','name'=>'Select a Customer']);

		return view("AdminSalesOrderManage::create",compact('products','warehouses','customers','employee_types'));
	}

	public function create(Request $request)
	{	

		// return $request->all();
		$sales_order = $this->salesOrderLogic->createNewSalesOrder($request);

		$url = url('admin/sales-order/sales_order_pdf/')."/".$sales_order->id;
		return redirect('admin/sales-order/create')->with(
			[
			'link'           => true,
			'link.link'      => $url,
			'link.linktitle' => 'Show sales order',
			'link.message'   => 'New Sales Order saved! #('.$sales_order->reference_no.')',
			'link.title'     => 'Success..!'
        ]);

	}

	public function salesOrderPdf(Request $request,$id)
	{
		return  $this->salesOrderLogic->getSalesOrderPdfOutput($id);
	}

	public function listViewDetail($id, Request $request)
	{
		$id        = $request->id;
		$details   = $this->salesOrderLogic->getSalesOrderDetailRecords($id);
		$list      = $this->salesOrderLogic->getSalesOrderById($id);

		return view("AdminSalesOrderManage::details", compact('details', 'list', 'vat'));
	}

	public function show($id, Request $request)
	{
		$user_role = $this->salesOrderLogic->getUserRole();
		$max_discount = ($user_role)?$user_role->discount:0;

		$id      = $request->id;
		$details = $this->salesOrderLogic->getSalesOrderDetailRecords($id);
		$list    = $this->salesOrderLogic->getSalesOrderById($id);

		if($list->quotation_status != PENDING || 
			$max_discount < 1 || 
			$list->approval_role_id != $user_role->id){
			return view('errors.404');
		}

		$vat = Vat::where('id', $list->vat_id)->first();
		$nbt = Vat::where('id', $list->nbt_id)->first();

		if($list->vat_type == 'vat' || $list->vat_type == 'svat'){
			$list = $this->salesOrderLogic->calculateVatCustomerQuotation($list);
		}else{
			$list = $this->salesOrderLogic->calculateNonVatCustomerQuotation($list);
			//$vat = null;
		}

		//return $list;

		/*return view("AdminSalesOrderManage::approvaldetail", compact('data','quotation','quotation_detail','max_discount'));*/

		return view("AdminSalesOrderManage::approve_view", compact('details', 'list', 'vat', 'max_discount', 'nbt'));
	}

	public function getItems(Request $request){
		return $this->salesOrderLogic->getItems(3,$request->search);
	}

	public function getWarehouses(Request $request){
		return $this->salesOrderLogic->getWarehouses();
	}

	public function getEmployeesByType(Request $request){
		return $this->salesOrderLogic->getEmployeesByType($request);
	}

}