<?php namespace App\Modules\AdminSalesOrderManage\BusinessLogics;

/**
* Business Logics 
* Define all the busines logics in here
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use Illuminate\Database\Eloquent\Model;

use App\Classes\Functions;
use App\Classes\Contracts\BaseStockHandler;

use App\Models\User;
use App\Models\DeliveryTerms;
use App\Models\Warehouse;
use App\Models\WarehouseType;

use Core\UserRoles\Models\UserRole;
use Core\EmployeeManage\Models\EmployeeType;
use Core\EmployeeManage\Models\Employee;

use App\Modules\AdminProductManage\Models\Product;
use App\Modules\AdminSalesOrderManage\Models\SalesOrder;
use App\Modules\AdminSalesOrderManage\Models\SalesOrderDetail;
use App\Modules\AdminSupplierManage\Models\Supplier;

use DB;
use Excel;
use File;
use Response;
use Sentinel;
use PDF;
use QrCode;

class SalesOrderLogic extends Model {

	private $salesOrder;
	private $salesOrderDetail;
	private $functions;
	private $baseStockHandler;

	public function __construct(
		SalesOrder $salesOrder,
		SalesOrderDetail $salesOrderDetail,
		Functions $functions,
		BaseStockHandler $baseStockHandler
	){
		$this->salesOrder = $salesOrder;
		$this->salesOrderDetail = $salesOrderDetail;
		$this->functions = $functions;
		$this->baseStockHandler = $baseStockHandler;
	}

	public function getSalesOrderRecords($filters, $perPage = 10)
	{
		$data = SalesOrder::with(['createdBy','employee','warehouse']);

		if($filters->from!="" && $filters->to!=""){
			$data=$data->whereBetween('sales_order.order_date', array($filters->from, $filters->to));
		}

		if($filters->reference != ""){
			$data->where('sales_order.reference_no','LIKE',$filters->reference);
		}

		if($filters->status != ""){
			$data->where('sales_order.status','=',$filters->status);
		}

		$data = $data->orderBy('sales_order.id','desc');
		return $data->paginate($perPage);
	}	

	public function getSalesOrderDetailRecords($id)
	{
		$data = SalesOrderDetail::with(['productWithTrash'])->where('sales_order_id','=',$id)->get();

        return $data;
	}

	public function getRecordsSalesOrderId($id)
	{
		return $data = SalesOrder::with(['customer'])->where('id',$id)->first();
	}

	public function getRecordsSalesOrderDetailId($id)
	{
		return $data = SalesOrderDetail::with(['product'])->where('order_id',$id)->get();
	}

	
	public function getDeliveryTerms()
	{
		return $data = DeliveryTerms::lists('name','id');
	}

	public function getItems($limit,$search)
	{
		$data = Product::where('code','like','%'.$search.'%')->orWhere('name','like','%'.$search.'%')->whereNull('deleted_at');

        return $data = $data->paginate($limit);
	}

	public function getWarehouseTypes()
	{
		$data = WarehouseType::whereNull('deleted_at')->selectRaw('id,name')->get()->prepend(['id'=>'','name'=>'Select a Warehouse Type']);

        return $data;
	}

	public function getWarehouses()
	{
		$data = Warehouse::whereNull('deleted_at')->selectRaw('id,name')->get()->prepend(['id'=>'','name'=>'Select a Warehouse']);

        return $data;
	}

	public function getSuppliers()
	{
		$data = Supplier::whereNull('deleted_at')->selectRaw('id, CONCAT(code,\'-\',name) as name')->get()->prepend(['id'=>'','name'=>'Select a Supplier']);

		return $data;
	}

	public function getSalesOrderTypes()
	{
		$data = GrnType::whereNull('deleted_at')->selectRaw('id,name')->get()->prepend(['id'=>'','name'=>'Select a Grn Type']);

		return $data;
	}

	public function getAllItems($limit)
	{
		$data = Product::whereNull('deleted_at');

		return $data = $data->paginate($limit);
	}

	public function createNewSalesOrder($data)
	{

		$user = Sentinel::getUser();

		$action_by = $user->employee_id;	
		
		$sales_order = SalesOrder::create([
			'employee_id'         => $data->employee,
			'reference_no'        => null,
			'warehouse_id'        => $data->warehouse,
			'order_date'          => date('Y-m-d'),
			'status'              => 0,
			'remarks' 			  => $data->remark,
			'action_by'           => $action_by
        ]);

		$total=0;
		
        foreach ($data->product_id as $key => $details) {

            $sales_order_details = SalesOrderDetail::create([
				'order_id'   			=> $sales_order->id,
				'product_id'     		=> $data->product_id[$key],
				'qty'            		=> $data->qty[$key],
				'mrp'          			=> $data->mrp[$key],
				'net_amount'            => $data->line_total_amount[$key]
            ]);

            $total += $data->line_total_amount[$key];
	        
	    }
	    return $sales_order;
	    $code = "OD"."_".$data->employee."_".$sales_order->id;
	    $sales_order->total_amount = $total;
		$sale_order->reference_no = $code;
		$sales_order->save();

	    return $sales_order;
	}

	public function getSalesOrderPdfOutput($id, $type='N')
	{
		$sales_order = $this->getSalesOrderById($id);

        $qr = QrCode::format('png')
        		->errorCorrection('L')
        		->size(200)
        		->margin(2)
        		->generate($sales_order->reference_no.'|'.$sales_order->total_amount); 

        $fileName = 'sales-order/sales-order-'.$sales_order->id.'-'.date('YmdHis').'.pdf';
        $page = view("AdminSalesOrderManage::sales-order-pdf", compact('sales_order','qr'))
        		->render();
		PDF::reset();
		//PDF::setFont('myriadpro');
		PDF::setMargins(10,18,10);
		PDF::setAutoPageBreak(TRUE, 32);
		PDF::setHeaderCallback(function($pdf){
			$image_file = url('assets/pi-images/h-img02.png');
        	$pdf->Image($image_file, 10, 5, 195, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		});

		PDF::setFooterCallback(function($pdf){
			$image_file = url('assets/pi-images/f-img01.png');
        	$pdf->Image($image_file, 10, 265, 195, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		});
        PDF::AddPage();
        PDF::writeHTML($page);
        if (ob_get_contents()) ob_end_clean();
        return PDF::Output(public_path($fileName),'I');
	}

	public function getSalesOrderById($id){
		try {
			$data = SalesOrder::select('*', 
					DB::raw('sales_order.id AS id'),
					DB::raw('sales_order.reference_no as reference_no'),
					DB::raw('warehouse.name as warehouse_name'),
					DB::raw('concat(employee.first_name," ",employee.last_name)'),
					DB::raw('employee.code as employee_code'),
					DB::raw('sales_order.status AS order_status'))
				->join('warehouse','warehouse.id','=','sales_order.warehouse_id')
				->join('employee','employee.id','=','sales_order.employee_id')
				->where('sales_order.id', $id)
				->orderBy('sales_order.id','asc')
				->with(['details.product','details.productWithTrash','createdBy'])
				->first();

		    return $data;	

		} catch (\Exception $e) {
			return $e->getMessage();
		}
	}

	public function getEmployeeTypes()
	{
		$data = EmployeeType::whereIn('id',[4,6])->whereNull('deleted_at')->selectRaw('id,name')->get()->prepend(['id'=>'','name'=>'Select a Employee Type']);

        return $data;
	}

	public function getEmployeesByType($type)
	{
		$data = Employee::where('employee_type_id',$type->type_id)->whereNull('deleted_at')->selectRaw('id,concat(first_name," ",last_name) as name')->get()->prepend(['id'=>'','name'=>'Select a Employee']);

        return $data;
	}

}