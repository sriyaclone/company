<?php namespace App\Modules\AdminSalesOrderManage\Models;

/**
*
* Model
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;


class SalesOrder extends Model {
	
	 /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sales_order';

    /**
     * The attributes that are not assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function createdBy(){
        return $this->belongsTo('Core\EmployeeManage\Models\Employee', 'action_by', 'id');
    }

    public function employee(){
        return $this->belongsTo('Core\EmployeeManage\Models\Employee', 'employee_id', 'id');
    }

    public function warehouse(){
        return $this->belongsTo('App\Models\Warehouse', 'warehouse_id', 'id');
    }

    public function details(){
        return $this->hasMany('App\Modules\AdminSalesOrderManage\Models\SalesOrderDetail', 'sales_order_id', 'id');
    }    
}
