<?php
namespace App\Modules\AdminSalesOrderManage\Requests;

use App\Http\Requests\Request;

class SalesOrderRequest extends Request
{
    public function authorize(){
        return true;
    }

    public function rules(){
        $rules = [
            'customer_id' => 'required',
	        'warehouse_id' => 'required',
	        'qty' => 'required|numeric|min:1'
        ];

        return $rules;
    }

    public function messages(){

        return [
            'customer_id.min' => 'Please select customer',
            'warehouse_id.min' => 'Please select warehouse'
        ];

    }

}