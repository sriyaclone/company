<?php


Route::group(['middleware' => ['auth']], function(){

	Route::group(array('prefix'=>'admin/sales-order/','module' => 'AdminSalesOrderManage', 'namespace' => 'App\Modules\AdminSalesOrderManage\Controllers'), function() {

		/*** GET Routes**/

		Route::get('list', [
	      'as' => 'sales-order.list', 'uses' => 'AdminSalesOrderManageController@listView'
	    ]);

	    Route::get('create', [
	      'as' => 'sales-order.create', 'uses' => 'AdminSalesOrderManageController@createView'
	    ]);

	    Route::get('sales_order_pdf/{id}', [
	      'as' => 'sales-order.view', 'uses' => 'AdminSalesOrderManageController@salesOrderPdf'
	    ]);

		Route::get('listdetail/{id}', [
		  'as' => 'sales-order.list.detail', 'uses' => 'AdminSalesOrderManageController@listViewDetail'
	    ]);

	    Route::get('view/{id}', [
		  'as' => 'sales-order.view', 'uses' => 'AdminSalesOrderManageController@orderView'
	    ]);

		/***JSON Routes**/
        Route::get('getItems', [
            'as' => 'sales-order.create', 'uses' => 'AdminSalesOrderManageController@getItems' 
        ]);

        Route::get('getWarehouses', [
            'as' => 'sales-order.create', 'uses' => 'AdminSalesOrderManageController@getWarehouses' 
        ]);

        Route::get('getOrderWithDetails', [
            'as' => 'sales-order.revise', 'uses' => 'AdminSalesOrderManageController@getOrderWithDetails' 
        ]);

        Route::get('getEmployees', [
            'as' => 'sales-order.employees', 'uses' => 'AdminSalesOrderManageController@getEmployeesByType' 
        ]);

		/***POST Routes**/

        Route::post('edit', [
        	'as' => 'sales-order.edit', 'uses' => 'AdminSalesOrderManageController@edit' 
        ]);

        Route::post('create', [
        	'as' => 'sales-order.create', 'uses' => 'AdminSalesOrderManageController@create' 
        ]);

	}); 

}); 