@extends('layouts.back_master') @section('title','List Sales Order')
@section('css')
<style type="text/css">
    
</style>
@stop
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Sales Order Management</h2>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{url('admin/grn/list')}}">Sales Order Management</a></li>
            <li class="active">Sales Order List</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a class="btn btn-warning" href="{{ url('admin/order/create') }}"><i class="fa fa-plus" aria-hidden="true"></i> Order</a>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Sales Order List</h5>
            <div class="ibox-tools">
                
            </div>
        </div>

        <div class="ibox-content" style="display: block;">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th class="text-left">Reference No</th>
                        <th class="text-left">Employee</th>
                        <th class="text-left">Warehouse</th>
                        <th class="text-left">Created Date</th>
                        <th class="text-right">Amount (Rs)</th>
                        <th class="text-right">Action By</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($data) > 0)
                        @foreach($data as $key => $val)
                            <tr>
                                <td align="center">
                                    {{ (($data->currentPage()-1)*$data->perPage())+($key+1) }}
                                </td>
                                <td align="left">
                                    {{$val->reference_no}}
                                </td>
                                <td align="left">
                                    {{$val['employee']->first_name}} {{$val['employee']->last_name}}<br>{{$val['employee']->code}}
                                </td>
                                <td align="left">
                                    {{$val['warehouse']->name}}<br>{{$val['warehouse']->code}}
                                </td>
                                <td align="left">
                                    {{$val->created_at}}
                                </td>
                                <td align="right">
                                    {{number_format($val->total_amount,2)}}
                                </td>
                                <td align="right">
                                    {{$val['createdBy']->first_name}} {{$val['createdBy']->last_name}}
                                </td>
                                <td align="left"> </td>
                                <td align="center">
                                    <a href="{{ url('admin/sales-order/listdetail') }}/{{$val->id}}"><i class="fa fa-th" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9" align="center"> - No Data to Display - </td>
                        </tr>
                    @endif  
                </tbody>
            </table>
      
            <div class="row">
                <div class="col-sm-12 text-left">
                     Row {{$data->count()}} of {{$data->total()}} 
                </div>
                <div class="col-sm-12 text-right">
                    {!! $data->appends($old->except('page'))->render() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@stop
@section('js')

<script type="text/javascript">
    $(document).ready(function() {
        $(".chosen").chosen();

        $('.datepick').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            format: 'yyyy-mm-dd'
        });
    });
</script>
@stop
