<style type="text/css">
	@page {
	   size: 7in 8in;
	   margin: 1in 16mm 27mm 16mm;
	}

	table{
		font-size: 8px;
	}
</style>
@if(count($sales_order->details) > 0)
	<table style="border-collapse: collapse;">
		<tr>
			<td rowspan="4" style="border-bottom: 1px solid #000;vertical-align: bottom" width="64%">
				<br/><br/><br/><br/>Employee: {{ $sales_order['employee']->first_name }} {{ $sales_order['employee']->last_name }} | {{$sales_order->customer_code}}</td>
			<td rowspan="4" width="10%" style="vertical-align: top;border-bottom: 1px solid #000;"><img src="data:image/png;base64,{!!base64_encode($qr)!!}"></td>
			<td width="12%">Purchase Order #</td>
			<td width="2%">:</td>
			<td width="12%">{{ $sales_order->reference_no }}</td>
		</tr>
		<tr>
			<td width="12%">Order Date</td>
			<td width="2%">:</td>
			<td width="12%">{{ date_format(new DateTime($sales_order->created_at), 'Y-m-d') }}</td>
		</tr>
		<tr>
			<td width="12%">Order Ref.#</td>
			<td width="2%">:</td>
			<td width="12%"></td>
		</tr>
		<tr>
			<td style="border-bottom: 1px solid #000;" width="12%">Order Date</td>
			<td style="border-bottom: 1px solid #000;" width="2%">:</td>
			<td style="border-bottom: 1px solid #000;" width="12%"></td>
		</tr>
	</table>
	<table style="border-collapse: collapse;">
		<tr>
			<td width="100%"></td>
		</tr>
		<tr>
			<td width="28%" style="line-height: 14px;"><strong>Warehouse</strong></td>
			<td width="28%" style="line-height: 14px;"><strong>SHIP TO</strong></td>
			<td width="44%" colspan="3" style="line-height: 14px;"><strong>TERMS & NOTES</strong></td>
		</tr>
		<tr>
			<td width="28%" style="font-size:8px;line-height:11px;">
				
			</td>
			<td width="28%" style="font-size:8px;line-height:11px;">
				
			</td>
			<td style="font-size: 8px;line-height:11px;" width="12%">Payment Terms</td>
			<td style="font-size: 8px;line-height:11px;" width="2%">:</td>
			<td style="font-size: 8px;line-height:11px;" width="30%">
				
			</td>
		</tr>
		<tr>
			<td width="28%" style="font-size:8px;line-height:11px;">
				{{$sales_order->warehouse_name}}
			</td>
			<td width="28%" style="font-size:8px;line-height:11px;">
				
			</td>
			<td style="font-size: 8px;line-height:11px;" width="12%">Price</td>
			<td style="font-size: 8px;line-height:11px;" width="2%">:</td>
			<td style="font-size: 8px;line-height:11px;" width="30%">All inclusive</td>
		</tr>
		<tr>
			<td width="28%" style="font-size:8px;line-height:11px;">
				
			</td>
			<td width="28%" style="font-size:8px;line-height:11px;">
				
			</td>
			<td style="font-size: 8px;line-height:11px;" width="12%">Price Validity</td>
			<td style="font-size: 8px;line-height:11px;" width="2%">:</td>
			<td style="font-size: 8px;line-height:11px;" width="30%">14 days</td>
		</tr>
		<tr>
			<td width="28%" style="font-size:8px;line-height:11px;">
			</td>
			<td width="28%" style="font-size:8px;line-height:11px;">
			</td>
			<td style="font-size: 8px;line-height:11px;" width="12%">Delivery</td>
			<td style="font-size: 8px;line-height:11px;" width="2%">:</td>
			<td style="font-size: 8px;line-height:11px;" width="30%"></td>
		</tr>
	</table>
	<table style="border-collapse: collapse;">
		<tr>
			<td colspan="7" style="line-height: 11px;"></td>
		</tr>	
		<tr>
			<td colspan="7" style="border-top: 1px solid #000;line-height: 11px;"></td>
		</tr>
		<tr>
			<td style="line-height:14px;font-size:8px;" width="5%"><strong>NO</strong></td>
			<td style="line-height:14px;font-size:8px;" width="15%"><strong>ITEM</strong></td>
			<td style="line-height:14px;font-size:8px;" width="35%"><strong>DESCRIPTION</strong></td>
			<td style="line-height:14px;font-size:8px;" width="10%"><strong>QTY</strong></td>
			<td style="line-height:14px;font-size:8px;" width="7%"><strong>UNIT</strong></td>
			<td style="line-height:14px;font-size:8px;text-align: right;" width="10%"><strong>PRICE</strong></td>
			<td style="line-height:14px;font-size:8px;text-align: right;" width="18%"><strong>AMOUNT</strong></td>
		</tr>
		<tr>
			<td colspan="7" style="line-height: 5px;">&nbsp;</td>
		</tr>
		@foreach($sales_order->details as $key => $detail)
		@if(count($sales_order->details) != $key+1)
		<tr>
			<td style="line-height:15px;font-size:8px;border-bottom: 1px solid #ddd;" 
				width="5%">{{$key+1}}</td>
			<td style="line-height:15px;font-size:8px;border-bottom: 1px solid #ddd;" 
				width="15%">{{ $detail->productWithTrash->code?:'-' }}</td>
			<td style="line-height:15px;font-size:8px;border-bottom: 1px solid #ddd;" 
				width="35%">{{ $detail->productWithTrash->name?:'-' }}</td>
			<td style="line-height:15px;font-size:8px;border-bottom: 1px solid #ddd;" 
				width="10%">{{ number_format($detail->qty, 2)?:'-' }}</td>
			<td style="line-height:15px;font-size:8px;border-bottom: 1px solid #ddd;" 
				width="7%">{{ ($detail->productWithTrash->unit)?:'-' }}</td>
			<td style="line-height:15px;font-size:8px;text-align: right;border-bottom: 1px solid #ddd;" 
				width="10%">
				{{ number_format($detail->mrp, 2)?:'-' }}
			</td>
			<td style="line-height:15px;font-size:8px;text-align: right;border-bottom: 1px solid #ddd;" 
				width="18%">
				{{ number_format($detail->net_amount, 2)?:'-' }}
			</td>
		</tr>
		@else
		<tr>
			<td style="line-height:14px;font-size:8px;" 
				width="5%">{{$key+1}}</td>
			<td style="line-height:14px;font-size:8px;" 
				width="15%">{{ $detail->productWithTrash->code?:'-' }}</td>
			<td style="line-height:14px;font-size:8px;" 
				width="35%">{{ $detail->productWithTrash->name?:'-' }}</td>
			<td style="line-height:14px;font-size:8px;" 
				width="10%">{{ number_format($detail->qty, 2)?:'-' }}</td>
			<td style="line-height:14px;font-size:8px;" 
				width="7%">{{ ($detail->productWithTrash->unit)?:'-' }}</td>
			<td style="line-height:14px;font-size:8px;text-align: right;" 
				width="10%">
				{{ number_format($detail->mrp, 2)?:'-' }}
			</td>
			<td style="line-height:14px;font-size:8px;text-align: right;" 
				width="18%">
				{{ number_format($detail->net_amount, 2)?:'-' }}				
			</td>
		</tr>
		@endif
		@endforeach
		<tr>
			<td colspan="7" style="line-height: 5px;">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="7" style="line-height: 5px;border-bottom: 1px solid #000;">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="7" style="line-height: 3px;">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3" style="line-height: 14px;">&nbsp;</td>
			<td style="line-height: 14px;border-bottom: 1px solid #ddd;" 
				width="20%"><strong>Subtotal</strong></td>
			<td colspan="3" 
				style="line-height: 14px;text-align:right;border-bottom: 1px solid #ddd;" 
				width="25%">
				{{ number_format($sales_order->details->sum('net_amount'), 2)?:'-' }}
			</td>
		</tr>
		<tr>
			<td colspan="3" style="line-height: 18px;border-bottom: 1px solid #000;">&nbsp;</td>
			<td style="line-height: 18px;border-bottom: 1px solid #000;" 
				width="20%"><strong>Grand Total (Rs)</strong></td>
			<td colspan="3" 
				style="line-height: 18px;text-align:right;border-bottom: 1px solid #000;" 
				width="25%">
				{{ number_format($sales_order->details->sum('net_amount'), 2)?:'-' }}</td>
		</tr>
	</table>
	<table style="border-collapse: collapse;">
		<tr>
			<td colspan="4" style="line-height: 5px;"></td>
		</tr>
		<tr>
			<td colspan="4" style="line-height: 5px;"></td>
		</tr>
		<tr>
			<td width="17%" style="font-size:7px;">Benificiary Details</td>
			<td width="2%" style="font-size:7px;">:</td>
			<td width="51%" style="font-size:7px;">Orel Corporation Pvt Ltd.</td>
			<td width="30%" style="font-size:7px;"></td>
		</tr>
		<tr>
			<td width="17%" style="font-size:7px;">Bank Details</td>
			<td width="2%" style="font-size:7px;">:</td>
			<td width="51%" style="font-size:7px;">Sampath Bank</td>
			<td width="30%" style="font-size:7px;"></td>
		</tr>
		<tr>
			<td width="17%" style="font-size:7px;">Bank A/C</td>
			<td width="2%" style="font-size:7px;">:</td>
			<td width="51%" style="font-size:7px;">0001 1008 3351</td>
			<td width="30%" style="font-size:7px;"></td>
		</tr>
		<tr>
			<td width="17%" style="font-size:7px;">Warranty</td>
			<td width="2%" style="font-size:7px;">:</td>
			<td width="81%" style="font-size:7px;" colspan="2">For ORANGE ELECTRIC Products, Lifetime-Warranty for all Switches and Sockets, Low Voltage Switchgear 10kA & SIGMA Ranges. 5-Years Warranty for all Fan Controllers, Shaver-Outlet, Low Voltage Switchgear Alpha.<br/>
			24-Months Warranty for LED Lamps and Industrial Products. 12 to 18-Months Warranty for all CFLs. 12 Months Warranty for Home Appliances.<br/>
			The above Warranties would be applicable from Date of Invoice.
			</td>
		</tr>
		<tr>
			<td colspan="4" style="line-height: 5px;"></td>
		</tr>
		<tr>
			<td colspan="4" style="line-height: 5px;"></td>
		</tr>
		<tr>
			<td colspan="4" style="font-size: 7px;"><strong>Other Terms & Conditions :-</strong></td>
		</tr>
		<tr>
			<td colspan="4" style="font-size: 7px;">All payment to be made in a/c payee cheque in favor of OREL CORPORATION (PVT) LTD.</td>
		</tr>
		<tr>
			<td colspan="4" style="font-size: 7px;">Items are non-returnable, unless otherwise due to a manufacturing defect.</td>
		</tr>
		<tr>
			<td colspan="4" style="line-height: 6px;"></td>
		</tr>
		<tr>
			<td colspan="4" style="font-size: 7px;text-align: center;">This is a computer generated document. no signature is required and deemed as official</td>
		</tr>
		<tr>
			<td colspan="4" style="line-height: 6px;"></td>
		</tr>
		<tr>
			<td colspan="4" style="font-size: 8px;text-align: center;line-height: 13px;">THANK YOU</td>
		</tr>
	</table>
@endif