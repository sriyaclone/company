@extends('layouts.back_master') @section('title','Admin - Product Category Management') 
@section('css')
<style type="text/css">
    
</style>
@stop

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Product Category Management</h2>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{url('admin/product-category/index')}}">Product Category</a></li>
            <li class="active">List Product Category</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a href="{{url('admin/product-category/create')}}" class="btn btn-warning"><i class="fa fa-plus"></i> Add Category</a>
        </div>
    </div>
</div>

<!-- Content-->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>List Product Category</h5>
            <div class="ibox-tools">
                
            </div>
        </div>

        <div class="ibox-content" style="display: block;">
                <table class="table table-striped table-bordered table-hover dataTables-example">
                    <thead>
                        <form id="form" role="form" method="get" action="{{url('admin/product-category/search')}}">
                            <tr>
                                <th width="5%">#</th>
                                <th>Category Code </br>
                                    <input type="text" class="form-control" name="code" placeholder="Enter Code" value="{{$code}}">
                                </th>
                                <th>Category Name </br>
                                    <input type="text" class="form-control" name="name" placeholder="Enter Name" value="{{$name}}">
                                </th>
                                <th>Main Category </br>
                                    {!! Form::select('parent', $main_categories, $parent,['class'=>'chosen','style'=>'width:100%;','required','data-placeholder'=>'Choose Parent','id'=>'parent']) !!}
                                </th>
                                <th>Created At</th>
                                <th width="10%">Action <br>
                                    <button type="submit" class="btn btn-default btn-sm" id="plan"><i class="fa fa-search" style="padding-right: 16px;width: 28px;"></i>Find</button>
                                </th>
                            </tr>
                        </form>
                    </thead>
                    <tbody>
                    @if(count($product_category_details) > 0)
                        @foreach($product_category_details as $key => $row)
                            <tr>                           
                                <td class="text-center">
                                    {{ (($product_category_details->currentPage()-1)*$product_category_details->perPage())+($key+1) }}</td>
                                <td> 
                                    {{ ($row->code != '') ? $row->code : '-' }}
                                </td>
                                <td class="text-center">
                                    {{ ($row->name !='') ? $row->name : '-' }}
                                </td>
                                <td class="text-center">
                                    {{ ($row->mainCategory != '') ? $row->mainCategory->name : '-' }}
                                </td>
                                <td>
                                    {{ ($row->created_at != '') ? $row->created_at : '-' }}
                                </td>
                                <td class="text-center">
                                    <div class="btn-group">
                                    @if($user->hasAnyAccess(['product.edit', 'admin']))
                                        <button class="btn btn-default" onclick="window.location.href='{{url('admin/product-category/edit/'.$row->id)}}'"><i class="fa fa-pencil"></i></button>
                                    @else
                                        <button class="btn btn-default" disabled><i class="fa fa-pencil"></i></button>
                                    @endif

                                    @if($user->hasAnyAccess(['product.category.delete', 'admin']))
                                        <button class="btn btn-default btn-delete" data-id="{{$row->id}}" onclick="deleteData('{{url('admin/product-category/delete?id='.$row->id)}}');"><i class="fa fa-trash-o"></i></button>
                                    @else
                                        <button class="btn btn-default" disabled><i class="fa fa-trash-o"></i></button>
                                    @endif
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9" align="center"> - No Data to Display - </td>
                        </tr>
                    @endif
                    </tbody>
                </table>
                @if($product_category_details != null)
                <div class="row">
                    <div class="col-sm-12 text-left">
                         Row {{$product_category_details->count()}} of {{$product_category_details->total()}} 
                    </div>
                    <div class="col-sm-12 text-right">
                         {!! $product_category_details->render() !!}
                    </div>
                </div>
                @endif
        </div>
    </div>
</div>

@stop

@section('js')
<script type="text/javascript">
    $(document).ready(function() {
        $(".chosen").chosen();

        $('#parent').change(function(){
            $("form").submit();
        });
    });

    function deleteData(_url){
        swal({
            title: "Are you sure?",
            text: "you wanna delete this product-category?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        },
        function(){
            $.ajax({
                url: _url,
                method: 'get',
                cache: false,
                data: [],
                success: function(response){
                    if(response.status == 1){
                        swal("Done!", "product-category has been deleted!.", "success");
                        location.reload();
                    }else{
                        swal("Error!", response.message, "error");
                    }
                },
                error: function(xhr){
                    console.log(xhr);
                    swal("Error!", 'Error occurred. Please try again', "error");
                } 
            });
      
        });
    }
</script>
@stop
