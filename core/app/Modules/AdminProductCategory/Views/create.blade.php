@extends('layouts.back_master') @section('title','Admin - Product Category Management')
@section('css')
<style type="text/css">
	
</style>
@stop
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Product Category Management</h2>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{url('admin/product-category/index')}}">Product Category</a></li>
            <li class="active">List Product Category</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a href="{{url('admin/product-category/index')}}" class="btn btn-primary"><i class="fa fa-th"></i> List Category</a>
        </div>
    </div>
</div>

<!-- Content-->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Add Product Category</h5>
            <div class="ibox-tools">
                
            </div>
        </div>

        <div class="ibox-content" style="display: block;">
            <form role="form" class="form-horizontal form-validation" method="post" id="myform">
                {!!Form::token()!!}
                <div class="form-group">
                    <label class="col-sm-2 control-label required">Parent Category</label>
                    <div class="col-sm-8">                      
                        {!! Form::select('parent', $main_categories, Input::old('parent'),['class'=>'chosen','style'=>'width:100%;','required','data-placeholder'=>'Choose Parent']) !!}
                        @if($errors->has('parent'))
                            <span class="help-block">{{$errors->first('parent')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label required">Code</label>
                    <div class="col-sm-8 @if($errors->has('code')) has-error @endif">
                        <input type="text" class="form-control" name="code" placeholder="Code" value="{{Input::old('code')}}">
                        @if($errors->has('code'))
                            <span class="help-block">{{$errors->first('code')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label required">Name</label>
                    <div class="col-sm-8 @if($errors->has('name')) has-error @endif">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{Input::old('name')}}">
                        @if($errors->has('name'))
                            <span class="help-block">{{$errors->first('name')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-success pull-right"><i class="fa fa-floppy-o"></i> Save</button>
                    </div>
                </div>
            </form>
        </div>

    </div>
</div>


@stop

@section('js')
	<!-- CORE JS -->
	<script type="text/javascript">
    	$(document).ready(function() {
	  		$(".chosen").chosen();
	  	});

        $('#myform').submit(function() {
            var txt = $('#name').val();
            
            $('#name').val(txt.trim());

            $('#myform').submit();
        });
	</script>
  	<!-- //CORE JS -->
@stop
