@extends('layouts.back_master') @section('title','Admin - Product Category Management')
@section('current_title','Product Category Edit')

@section('css')
<style type="text/css">
  
</style>
@stop

@section('content')
    <!-- Content Header (Page header) -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Product Category Management</h2>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{url('admin/product-category/index')}}">Product Category</a></li>
            <li class="active">Edit Product Category</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a href="{{url('admin/product-category/index')}}" class="btn btn-primary"><i class="fa fa-th"></i> List Category</a>
            <a href="{{url('admin/product-category/create')}}" class="btn btn-warning"><i class="fa fa-plus"></i> Add Category</a>
        </div>
    </div>
</div>
<!-- !!Content Header (Page header) -->

<!-- Main content -->
<div class="wrapper wrapper-content animated fadenRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Edit Product Category</h5>
            <div class="ibox-tools">
                
            </div>
        </div>

        <div class="ibox-content" style="display: block;">
            <form role="form" class="form-horizontal form-validation" method="post">
                {!!Form::token()!!}
                <div class="form-group">
                    <div class=" @if($errors->has('name')) has-error @endif">
                        <label class="col-sm-2 control-label required">Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @if($errors->has('name')) error @endif" name="name" placeholder="Name" value="{{$product_category_details[0]->name}}">
                            @if($errors->has('name'))
                                <span class="help-block">{{$errors->first('name')}}</span>
                            @endif
                        </div>
                    </div>
                </div>
                    
                <div class="form-group">
                    <div class="@if($errors->has('code')) has-error @endif">
                        <label class="col-sm-2 control-label required">Code</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @if($errors->has('code')) error @endif" name="code" placeholder="Code" value="{{$product_category_details[0]->code}}">
                            @if($errors->has('code'))
                                <span class="help-block">{{$errors->first('code')}}</span>
                            @endif
                        </div>
                    </div>
                </div>
        
                <div class="form-group">
                    <label class="col-sm-2 control-label">Main Category</label>
                    <div class="col-sm-10 @if($errors->has('main_category')) has-error @endif">
                        {!! Form::select('main_category', $main_categories, $product_category_details[0]->main_category_id,['class'=>'chosen','style'=>'width:100%;','required','data-placeholder'=>'Choose Main Category']) !!}
                        @if($errors->has('main_category'))
                            <span class="help-block">{{$errors->first('main_category')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-success pull-right"><i class="fa fa-floppy-o"></i> Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div> 
<!-- !!!Content -->

@stop
@section('js')
    <!-- CORE JS -->
  <script type="text/javascript">
      $(document).ready(function() {
        $(".chosen").chosen();
      });
  </script>
    <!-- //CORE JS -->
@stop
