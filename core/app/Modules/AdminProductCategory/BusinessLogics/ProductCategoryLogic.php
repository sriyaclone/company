<?php namespace App\Modules\AdminProductCategory\BusinessLogics;


/**
* Business Logics 
* Define all the busines logics in here
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use App\Modules\AdminProductCategory\Models\AdminProductCategory;
use App\Models\ManagerHasProductCategory;
use App\Models\MainCategory;

use App\Models\User;
use App\Exceptions\TransactionException;

use Exception;
use Sentinel;

class ProductCategoryLogic extends Model {

    public function add($data){

        DB::transaction(function() use($data){
            $user = Sentinel::getUser();

            $parent = MainCategory::find($data->parent);

            $productCategory = AdminProductCategory::create([
                'name'                =>  $data->name, 
                'code'                =>  $data->code,
                'main_category_id'    =>  $data->parent,
                'created_by'          =>  $user->id,
            ]);

            if($productCategory) {
                return $productCategory;
            }else{
                throw new TransactionException('Record wasn\'t updated', 101);
            }
        });
    }

    /**
     * This function is used to get all product category details
     * @parm  integer $id that need to get product category details
     * @return supplier object
     */
    public function getProductCategoryDetails($id){
        $product_category_details = AdminProductCategory::where('id','=',$id)->get();
        if(count($product_category_details) > 0){
            return $product_category_details;
        }else{
            return false;
        }
    }

    /**
     * This function is used to update product category
     * @param integer id and array $data
     * @response is boolean
     */
    public function updatePoductCategory($id, $data){

        DB::transaction(function() use($id, $data){
            
            $productCategory        = AdminProductCategory::find($id);

            $productCategory->name = $data->name;
            $productCategory->code = $data->code;
            $productCategory->main_category_id = $data->main_category;
            $productCategory->save();

            if($productCategory){
                return true;
            }else{
                throw new Exception("Error occurred while updating product category.");
            }
        });
    }

    /**
     * This function is used to get all product category details
     * @parm -
     * @return product category object
     */
    public function getAllProductCategories(){
        return AdminProductCategory::with(['mainCategory'])->paginate(10);
    }

    /**
     * This function is used to search prodcut category
     * @param integer $name, $code
     * @response product category object
     */
    public function searchProductCategory($name, $code, $parent) {
        $search = AdminProductCategory::with(['mainCategory']);
        if($name != ''){
            $search = $search->where('name','LIKE', '%' . $name . '%');
        }
        if($code != ''){
            $search = $search->where('code','LIKE', '%' . $code . '%');
        }
        if($parent != ''){
            if($parent!='0'){
                $search = $search->where('main_category_id', $parent);
            }
        }
        $search->orderBy('created_at','desc');
        return $search->paginate(10);
    }

    public function getCategoryList(){
        $categories = AdminProductCategory::lists('name', 'id');

        return $categories;
    }

    public function getSessionCategoryList(){
        $user = Sentinel::getUser();

        $userDetails = User::with('emp')->find($user->id);

        $categories = ManagerHasProductCategory::select('*')
            ->leftJoin('product_category as product_categories', function($product_category){
                $product_category->on('manager_has_product_category.product_category_id', '=', 'product_categories.id');
            })
            ->where('employee_id', $userDetails->emp->id)
            ->orderBy('product_categories.id')
            ->lists('product_categories.name','product_categories.id');

        return $categories;
    }

    public function getProductCategoryById($id){

        $productCategory = AdminProductCategory::find($id);

        if(!$productCategory){
            throw new \Exception('P2-Product category not found for ID:'.$id);
        }

        return $productCategory;
    }

    public function deleteProductCategory($id){
        $productCategory = $this->getProductCategoryById($id);

        if(!$productCategory->delete()){
            throw new \Exception('P3-Could not delete product category ID:'.$id);
        }

        return 1;
    }

}