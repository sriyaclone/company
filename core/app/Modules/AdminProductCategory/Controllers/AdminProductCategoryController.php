<?php namespace App\Modules\AdminProductCategory\Controllers;


/**
* Controller class
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Modules\AdminProductCategory\Models\AdminProductCategory;
use App\Modules\AdminProductCategory\BusinessLogics\ProductCategoryLogic;
use App\Modules\AdminProductCategory\Requests\ProductCategoryRequest;

use App\Models\SalesOffice;
use App\Models\MainCategory;

use DB;

class AdminProductCategoryController extends Controller {

	protected $product_category;

    public function __construct(ProductCategoryLogic $productCategoryLogic){
        $this->product_category = $productCategoryLogic;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$product_category_details = $this->product_category->getAllProductCategories();
		
		$main_categories = MainCategory::all()->lists('name', 'id')->prepend('All','0');

		return view("AdminProductCategory::index")->with([
			'product_category_details' 	=> $product_category_details,
			'main_categories' 			=> $main_categories,
			'parent'					=> 0,
			'name' 						=> '',
			'code' 						=> ''
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$main_categories = MainCategory::all()->lists('name', 'id')->prepend('Select Main Category','0');
		
		return view('AdminProductCategory::create')->with([ 
			'main_categories' => $main_categories
		]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(ProductCategoryRequest $request)
	{
		try {
	        
	        $bool = $this->product_category->add($request);

	        return redirect('admin/product-category/index')->with([
                'success' => true,
                'success.message' => 'Product category added successfully!',
                'success.title'   => 'Success..!'
            ]);
	    }
	    catch(Exception $e){
            return redirect('admin/product-category/index')->with([
                'error' => true,
                'error.title' => 'Error..!',
                'error.message' => $e->getMessage()
            ])->withInput();
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $product_category_details = $this->product_category->getProductCategoryDetails($id);
        
        $productCategories = AdminProductCategory::all()->lists('name', 'id')->prepend('Root','0');

        $main_categories = MainCategory::all()->lists('name','id')->prepend('Select Main Category',0);

        if($product_category_details) {
            return view("AdminProductCategory::edit")->with([
            	'product_category_details' 	=> $product_category_details,
            	'categories' 				=> $productCategories,
            	'main_categories' 			=> $main_categories
           	]);
        }else{
            return response()->view('errors.404');
        }
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(ProductCategoryRequest $request, $id)
	{
		try {
            $bool = $this->product_category->updatePoductCategory($id, $request);

            return redirect('admin/product-category/index')->with([
                'success' => true,
                'success.message' => 'Product category updated successfully!',
                'success.title'   => 'Success..!'
            ]);
        }catch(Exception $e){
            return back()->with([
                'error' => true,
                'error.title' => 'Error..!',
                'error.message' => $e->getMessage()
            ])->withInput();
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function delete(Request $request)
	{
		try{
			$this->product_category->deleteProductCategory($request->id);
			return response()->json([
				'status' => 1
			]);
		}catch(\Exception $e){
			return response()->json([
				'status' => 0,
				'message' => $e->getMessage()
			]);
		}
	}

	/**
     * This function is used to search product category
     * @return Response
     */
    public function searchProductCategory(Request $request){
        $product_category_details = $this->product_category->searchProductCategory($request->get('name'), $request->get('code'), $request->get('parent'));

        $main_categories = MainCategory::all()->lists('name', 'id')->prepend('All','0');
        
        return view("AdminProductCategory::index")->with([
        	'product_category_details' 	=> $product_category_details,
        	'main_categories' 			=> $main_categories,
        	'parent'					=> $request->get('parent'),
        	'name' 						=> $request->get('name'),
        	'code' 						=> $request->get('code')
        ]);
    }

}
