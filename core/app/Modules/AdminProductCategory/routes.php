<?php


Route::group(['middleware' => ['auth']], function()
{	
	Route::group(['prefix'=>'admin/product-category/','namespace' => 'App\Modules\AdminProductCategory\Controllers'],function() {

	    Route::get('create', [
	      'as' => 'product.category.create', 'uses' => 'AdminProductCategoryController@create'
	    ]);

	    Route::get('edit/{id}', [
	      'as' => 'product.category.edit', 'uses' => 'AdminProductCategoryController@edit'
	    ]);

	    Route::post('edit/{id}', [
            'as' => 'product.category.edit','uses' => 'AdminProductCategoryController@update'
        ]);

        Route::get('index', [
            'as' => 'product.category.index', 'uses' => 'AdminProductCategoryController@index'
        ]);

        Route::get('search', [
            'as' => 'product.category.search', 'uses' => 'AdminProductCategoryController@searchProductCategory'
        ]);

	    Route::post('create', [
	      'as' => 'product.category.create', 'uses' => 'AdminProductCategoryController@store'
	    ]);
	    
	    Route::get('delete', [ 
            'as' => 'product.category.delete', 'uses' => 'AdminProductCategoryController@delete'
        ]);
	});    
});