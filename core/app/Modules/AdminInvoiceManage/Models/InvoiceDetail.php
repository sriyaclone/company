<?php namespace App\Modules\AdminInvoiceManage\Models;

/**
*
* Model
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Invoice;

class InvoiceDetail extends Model {

	 /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'invoice_detail';

    /**
     * The attributes that are not assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function product(){
        return $this->belongsTo('App\Modules\AdminProductManage\Models\Product', 'product_id', 'id');
    }

    public function productWithTrash(){
        return $this->belongsTo('App\Modules\AdminProductManage\Models\Product', 'product_id', 'id')->withTrashed();
    }

}
