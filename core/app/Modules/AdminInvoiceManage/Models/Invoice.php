<?php namespace App\Modules\AdminInvoiceManage\Models;

/**
*
* Model
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;
use App\Models\InvoiceDetail;


class Invoice extends Model {
	
	 /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'invoice';

    /**
     * The attributes that are not assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function createdBy(){
        return $this->belongsTo('Core\EmployeeManage\Models\Employee', 'action_by', 'id');
    }

    public function customer(){
        return $this->belongsTo('App\Models\Customer', 'customer_id', 'id');
    }

    public function details(){
        return $this->hasMany('App\Modules\AdminInvoiceManage\Models\invoiceDetail', 'invoice_id', 'id');
    }    
}
