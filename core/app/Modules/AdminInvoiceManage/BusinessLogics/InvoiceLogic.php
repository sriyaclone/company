<?php namespace App\Modules\AdminInvoiceManage\BusinessLogics;

/**
* Business Logics 
* Define all the busines logics in here
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use Illuminate\Database\Eloquent\Model;
use Core\EmployeeManage\Models\Employee;

use App\Classes\Functions;
use App\Classes\Contracts\BaseStockHandler;

use App\Models\User;
use App\Models\DeliveryTerms;
use App\Models\Warehouse;
use App\Models\WarehouseType;

use Core\UserRoles\Models\UserRole;

use App\Modules\AdminProductManage\Models\Product;
use App\Modules\AdminInvoiceManage\Models\Invoice;
use App\Modules\AdminInvoiceManage\Models\InvoiceDetail;
use App\Modules\AdminSupplierManage\Models\Supplier;

use DB;
use Excel;
use File;
use Response;
use Sentinel;
use PDF;
use QrCode;

class InvoiceLogic extends Model {

	private $invoice;
	private $invoiceDetail;
	private $functions;
	private $baseStockHandler;

	public function __construct(Invoice $invoice , InvoiceDetail $invoiceDetail,Functions $functions,BaseStockHandler $baseStockHandler){
		$this->invoice = $invoice;
		$this->invoiceDetail = $invoiceDetail;
		$this->functions = $functions;
		$this->baseStockHandler = $baseStockHandler;
	}

	public function getInvoiceRecords($filters, $perPage = 10)
	{
		$data = Invoice::with(['createdBy','customer']);

		if($filters->from!="" && $filters->to!=""){
			$data=$data->whereBetween('invoice.invoice_date', array($filters->from, $filters->to));
		}

		if($filters->reference != ""){
			$data->where('invoice.reference_no','LIKE',$filters->reference);
		}

		if($filters->status != ""){
			$data->where('invoice.status','=',$filters->status);
		}

		$data = $data->orderBy('invoice.id','desc');
		return $data->paginate($perPage);
	}	

	public function getInvoiceDetailRecords($id)
	{
		$data = InvoiceDetail::with(['productWithTrash'])->where('invoice_id','=',$id)->get();

        return $data;
	}

	public function getRecordsInvoiceId($id)
	{
		return $data = Invoice::with(['customer'])->where('id',$id)->first();
	}

	public function getRecordsInvoiceDetailId($id)
	{
		return $data = InvoiceDetail::with(['product'])->where('invoice_id',$id)->get();
	}

	
	public function getDeliveryTerms()
	{
		return $data = DeliveryTerms::lists('name','id');
	}

	public function getItems($limit,$search)
	{
		$data = Product::where('code','like','%'.$search.'%')->orWhere('name','like','%'.$search.'%')->whereNull('deleted_at');

        return $data = $data->paginate($limit);
	}

	public function getWarehouseTypes()
	{
		$data = WarehouseType::whereNull('deleted_at')->selectRaw('id,name')->get()->prepend(['id'=>'','name'=>'Select a Warehouse Type']);

        return $data;
	}

	public function getWarehouses()
	{
		$data = Warehouse::whereNull('deleted_at')->selectRaw('id,name')->get()->prepend(['id'=>'','name'=>'Select a Warehouse']);

        return $data;
	}

	public function getSuppliers()
	{
		$data = Supplier::whereNull('deleted_at')->selectRaw('id, CONCAT(code,\'-\',name) as name')->get()->prepend(['id'=>'','name'=>'Select a Supplier']);

		return $data;
	}

	public function getInvoiceTypes()
	{
		$data = GrnType::whereNull('deleted_at')->selectRaw('id,name')->get()->prepend(['id'=>'','name'=>'Select a Grn Type']);

		return $data;
	}

	public function getAllItems($limit)
	{
		$data = Product::whereNull('deleted_at');

		return $data = $data->paginate($limit);
	}

	public function createNewInvoice($data)
	{

		$user = Sentinel::getUser();

		$action_by = $user->employee_id;	

		if(isset($data->product_id)){

			$invoice = Invoice::create([
				'customer_id'         => $data->customer_id,
				'invoice_date'        => date('Y-m-d H:i:s'),
				'delivery_date'       => $data->delivery_date,
				'status'              => 0,
				'remark' 			  => $data->remark,
				'action_by'           => $action_by
	        ]);

	        $total_amount = 0;


	        foreach ($data->product_id as $key => $details) {
	            
	            $invoice_details = InvoiceDetail::create([
					'invoice_id'   			=> $invoice->id,
					'product_id'     		=> $data->product_id[$key],
					'qty'            		=> $data->qty[$key],
					'mrp'          			=> $data->mrp[$key],
					'net_amount'          	=> $data->line_total_amount[$key]
	            ]);

	            $total_amount += (float)$data->line_total_amount[$key];
		        
		    }

		    $code = "INV"."_".$data->customer."_".$invoice->id;

			$invoice->invoice_no = $code;
			$invoice->total_amount = $total_amount;
			$invoice->save();

		    return $invoice;
		}else{
			
		}
		
	}

	public function getInvoicePdfOutput($id, $type='N')
	{
		$invoice = $this->getInvoiceById($id);

        $qr = QrCode::format('png')
        		->errorCorrection('L')
        		->size(200)
        		->margin(2)
        		->generate($invoice->invoice_no.'|'.$invoice->total_amount); 

        $fileName = 'invoice/invoice-'.$invoice->id.'-'.date('YmdHis').'.pdf';
        $page = view("AdminInvoiceManage::invoice-pdf", compact('invoice','qr'))
        		->render();
		PDF::reset();
		//PDF::setFont('myriadpro');
		PDF::setMargins(10,18,10);
		PDF::setAutoPageBreak(TRUE, 32);
		PDF::setHeaderCallback(function($pdf){
			$image_file = url('assets/pi-images/h-img02.png');
        	$pdf->Image($image_file, 10, 5, 195, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		});

		PDF::setFooterCallback(function($pdf){
			$image_file = url('assets/pi-images/f-img01.png');
        	$pdf->Image($image_file, 10, 265, 195, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		});
        PDF::AddPage();
        PDF::writeHTML($page);
        if (ob_get_contents()) ob_end_clean();
        return PDF::Output(public_path($fileName),'I');
	}

	public function getInvoiceById($id){
		try {
			$data = Invoice::select('*', 
					DB::raw('invoice.id AS id'),
					DB::raw('invoice.invoice_no as invoice_no'),
					DB::raw('invoice.total_amount as total_amount'),
					DB::raw('customer.first_name as customer_name'),
					DB::raw('customer.code as customer_code'),
					DB::raw('invoice.status AS invoice_status'),
					DB::raw('concat(employee.first_name," ",employee.last_name) AS action_by'))
				->join('customer','customer.id','=','invoice.customer_id')
				->join('employee','employee.id','=','invoice.action_by')
				->where('invoice.id', $id)
				->orderBy('invoice.id','asc')
				->with(['details.product','details.productWithTrash','createdBy'])
				->first();

		    return $data;	

		} catch (\Exception $e) {
			return $e->getMessage();
		}
	}

}