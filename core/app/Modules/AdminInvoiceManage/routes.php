<?php


Route::group(['middleware' => ['auth']], function(){

	Route::group(array('prefix'=>'admin/invoice/','module' => 'AdminInvoiceManage', 'namespace' => 'App\Modules\AdminInvoiceManage\Controllers'), function() {

		/*** GET Routes**/

		Route::get('list', [
	      'as' => 'invoice.list', 'uses' => 'AdminInvoiceManageController@listView'
	    ]);

	    Route::get('create', [
	      'as' => 'invoice.create', 'uses' => 'AdminInvoiceManageController@createView'
	    ]);

	    Route::get('invoice_pdf/{id}', [
	      'as' => 'invoice.view', 'uses' => 'AdminInvoiceManageController@invoicePdf'
	    ]);

		Route::get('listdetail/{id}', [
		  'as' => 'invoice.list.detail', 'uses' => 'AdminInvoiceManageController@listViewDetail'
	    ]);

	    Route::get('view/{id}', [
		  'as' => 'invoice.view', 'uses' => 'AdminInvoiceManageController@invoiceView'
	    ]);

		/***JSON Routes**/
        Route::get('getItems', [
            'as' => 'invoice.create', 'uses' => 'AdminInvoiceManageController@getItems' 
        ]);

        Route::get('getWarehouses', [
            'as' => 'invoice.create', 'uses' => 'AdminInvoiceManageController@getWarehouses' 
        ]);

        Route::get('getInvoiceWithDetails', [
            'as' => 'invoice.revise', 'uses' => 'AdminInvoiceManageController@getInvoiceWithDetails' 
        ]);

		/***POST Routes**/

        Route::post('edit', [
        	'as' => 'invoice.edit', 'uses' => 'AdminInvoiceManageController@edit' 
        ]);

        Route::post('create', [
        	'as' => 'invoice.create', 'uses' => 'AdminInvoiceManageController@create' 
        ]);

	}); 

}); 