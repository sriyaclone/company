<?php
namespace App\Modules\AdminInvoiceManage\Requests;

use App\Http\Requests\Request;

class InvoiceRequest extends Request
{
    public function authorize(){
        return true;
    }

    public function rules(){
        $rules = [

         'customer_id' => 'required',
	     'qty' => 'required|numeric|min:1'
        ];

        return $rules;
    }

    public function messages(){

        return [
            'customer_id.min' => 'Please select customer',
            
        ];

    }

}