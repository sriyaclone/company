@extends('layouts.back_master') @section('title','List Invoice')
@section('css')
<style type="text/css">
    
</style>
@stop
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Invoice Management</h2>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{url('admin/invoice/list')}}">Invoice Management</a></li>
            <li class="active">Invoice List</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a class="btn btn-warning" href="{{ url('admin/invoice/create') }}"><i class="fa fa-plus" aria-hidden="true"></i> Invoice</a>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Invoice List</h5>
            <div class="ibox-tools">
                
            </div>
        </div>

        <div class="ibox-content" style="display: block;">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th class="text-left">Invoice No</th>
                        <th class="text-left">Customer</th>
                        <th class="text-right">Amount (Rs)</th>
                        <th class="text-left">Invoice Date</th>
                        <th class="text-left">Delivery Date</th>
                        <th class="text-right">Action By</th>
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($data) > 0)
                        @foreach($data as $key => $val)
                            <tr>
                                <td align="center">
                                    {{ (($data->currentPage()-1)*$data->perPage())+($key+1) }}
                                </td>
                                <td align="left">
                                    {{$val->invoice_no}}
                                </td>
                                <td align="left">
                                    {{$val['customer']->first_name}}<br><strong>{{$val['customer']->code}}</strong>
                                </td>
                                <td align="right">
                                    {{number_format($val->total_amount,2)}}
                                </td>
                                <td align="left">
                                    {{$val->invoice_date}}
                                </td>
                                <td align="left">
                                    {{$val->delivery_date}}
                                </td>
                                <td align="right">
                                    {{$val['createdBy']->first_name}} {{$val['createdBy']->last_name}}
                                </td>
                                <td align="center">
                                    <a href="{{ url('admin/invoice/listdetail') }}/{{$val->id}}"><i class="fa fa-th" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9" align="center"> - No Data to Display - </td>
                        </tr>
                    @endif  
                </tbody>
            </table>
      
            <div class="row">
                <div class="col-sm-12 text-left">
                     Row {{$data->count()}} of {{$data->total()}} 
                </div>
                <div class="col-sm-12 text-right">
                    {!! $data->appends($old->except('page'))->render() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@stop
@section('js')

<script type="text/javascript">
    $(document).ready(function() {
        $(".chosen").chosen();

        $('.datepick').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            format: 'yyyy-mm-dd'
        });
    });
</script>
@stop
