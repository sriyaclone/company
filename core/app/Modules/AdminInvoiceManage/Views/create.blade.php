@extends('layouts.back_master') @section('title','Create Invoice')
@section('css')
<style type="text/css">
    .btn-md-1{
        margin-bottom: 10px;
    }
    .pagination {
        display: inline-block;
        padding-left: 0;
        margin: 0 0; 
        border-radius: 4px;
    }
    .modal .overlay, .overlay-wrapper .overlay {
        z-index: 50;
        background: rgba(255,255,255,0.7);
        border-radius: 3px;
    }
    .modal .overlay, .overlay-wrapper>.overlay, .box>.loading-img, .overlay-wrapper>.loading-img {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
    .modal  .overlay>.fa, .overlay-wrapper .overlay>.fa {
        position: absolute;
        top: 50%;
        left: 50%;
        margin-left: -15px;
        margin-top: -15px;
        color: #000;
        font-size: 30px;
    }
    .add-item{
      color: #6aa8e8;
    }
    .rem-item{
      color: #e45656;
    }

    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
      display: none !important;
    }

    .item-selected{
      background: #b0d3ff;
    }

    .error{
        color: red;
    }

    .default-error{
        padding: 0px !important;
        margin-bottom: 0px !important;
        margin-top: 5px;
        width: 50%;
        text-align: left !important;
        padding-left: 10px !important;
    }


    .customer-div{
      margin-left: 0%;
      width: 32%;
    }

</style>

@stop
@section('content')
<div  ng-app="app" ng-controller="AdminInvoiceController" >
    <form action="{{url('admin/invoice/create')}}" method="post" name="myform" id="myform">
    {!!Form::token()!!}
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-4">
                <h2>Invoice Management</h2>
                <ol class="breadcrumb">
                    <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
                    <li><a href="{{url('admin/invoice/list')}}">Invoice Management</a></li>
                    <li class="active">Create Invoice</li>
                </ol>
            </div>
            <div class="col-lg-8">
                <div class="title-action">
                    <button type="button" class="btn btn-md btn-primary" style="background-color: brown;border-color: brown" data-toggle="modal" href='#modal-items' ng-click="getItems(1,'')">Add Item</button>                
                    <a class="btn btn-primary" href="{{ url('admin/invoice/list') }}"><i class="fa fa-th" aria-hidden="true"></i> Invoice List</a>
                </div>
            </div>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight" ng-cloak >
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Customer Invoice</h5>
                    <div class="ibox-tools">
                        
                    </div>
                </div>
                
                <div class="ibox-content" style="display: block;">
                    
                    <div class="row btn-md-1">
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 @if($errors->has('customer_id')) has-error @endif">
                            <select class="form-control chosen" name="customer_id" id="customer_id">
                                @foreach($customers as $customer)
                                    <option value="{{$customer['id']}}">{{$customer['name']}}</option>
                                @endforeach
                            </select>
                            @if($errors->has('customer_id'))
                                <span class="help-block">{{$errors->first('customer_id')}}</span>
                            @endif
                        </div>
                        
                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                            <h4 class="text-right" style="margin-top:3px;margin-bottom: 0;">
                                <p class="text-right" style="margin-bottom: 0">
                                    ( Date : {{date('Y-m-d')}} )
                                </p>                                
                            </h4>          
                        </div>
                    </div>
                        <div class="alert alert-danger default-error customer-div" style="display: none;" id="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h6 id="error-customer"></h6>
                        </div> 
                         <br>
                    <div class="row">
                        <table class="table table-bordered items-table" align="center" style="width: 98% !important">
                            <thead>
                                <tr>
                                    <th class="text-center" width="5%">#</th>
                                    <th class="text-center">Product / Code</th>
                                    <th class="text-right">Price (Rs)</th>
                                    <th class="text-right">Qty</th>
                                    <th class="text-right">Amount (Rs)</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="invoiceDetail in invoiceDetails" class="item-rows">
                                    <td class="text-center">
                                        <input type="hidden" name="product_id[]" value="@{{invoiceDetail.id}}">
                                        @{{$index+1}}
                                    </td>
                                    <td class="text-center">
                                        <strong>@{{invoiceDetail.code}}</strong><br>
                                        <strong>@{{invoiceDetail.name}}</strong>
                                    </td>
                                    <td class="text-right">
                                        <input type="hidden" name="mrp[]" value="@{{invoiceDetail.mrp}}">
                                        <strong>@{{invoiceDetail.mrp}}</strong>
                                    </td>
                                    <td class="text-right">
                                        <input type="number" min="0" 
                                           ng-min="0"
                                           ng-model="invoiceDetail.qty"
                                           name="qty[]" 
                                           step="1" class="form-control qty" ng-change="calculateInvoice()">
                                    </td>
                                    <td class="text-right">
                                        <span class="row_amount">
                                            @{{invoiceDetail.totalAmount|number:2}}
                                            <input type="hidden" name="line_total_amount[]" value="@{{invoiceDetail.totalAmount}}">
                                        </span>
                                    </td>
                                    <td class="text-center" class="text-center">
                                        <a href="" ng-click="removeItem($index)" class="rem-item"><i class="fa fa-minus-circle fa-lg" aria-hidden="true"></i></a>
                                        <a  data-toggle="modal" href='#modal-items' ng-click="getItems(1,'')" class="add-item"><i class="fa fa-plus-circle fa-lg" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="4" align="right"><strong>Total Amount</strong></td>
                                    <td align="right">@{{ (invoice_details|total:'totalAmount')|number:2 }}</td>
                                    <td></td>
                                </tr>                        
                            </tfoot>
                        </table> 
                    </div>

                    <div class="alert alert-danger default-error" style="display: none;" id="alert1" align="right">
                            <button type="button" class="close" data-dismiss="alert2" aria-hidden="true">&times;</button>
                            <h6 id="error-items"></h6>
                    </div>
                        
                    <br>

                    <div class="alert alert-danger default-error" style="display: none;" id="alert2" align="right">
                        <button type="button" class="close" data-dismiss="alert3" aria-hidden="true">&times;</button>
                        <h6 id="error-quantity"></h6>
                    </div>

                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-12" style="margin-top: 10px;">
                            <div class="form-group">
                                <label>Delivery Date</label>
                                <input type="text" id="delivery_date" name="delivery_date" class="form-control datepick" value="@if(isset($old->delivery_date)){{$old->delivery_date}}@else{{date('Y-m-d')}}@endif">
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-12" style="margin-top: 10px;">
                            <div class="form-group">
                                <label>Remark</label>
                                <input type="text" id="remark" name="remark" class="form-control" placeholder="Enter Remark">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right" style="margin-top: 10px;">
                            <button type="submit" class="btn btn-primary">Save Invoice</button>
                        </div>
                    </div>

                </div>
            </div>

            <div class="modal fade" id="modal-items">
                <div class="modal-dialog modal-lg">
                    <div class="overlay" id="modal-overlay-2">
                        <i class="fa fa-refresh fa-spin"></i>
                    </div>
                    <div class="modal-content">
                        <div class="modal-header">
                            <div class="row">
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <h4 style="margin-top: 5px">Items</h4>
                                </div>
                                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                    <div class="input-group">
                                        <input type="text" id="search" name="search" autofocus placeholder="Type Message ..." class="form-control" style="height: 30px">
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-defaukt btn-flat btn-sm" ng-click="getItems(items.current_page,search)">Find</button>
                                        </span>
                                    </div>
                                </div>  
                            </div>
                        </div>
                        <div class="modal-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th width="40%">Code</th>
                                        <th width="40%">Name</th>
                                        <th width="18%">Price</th>
                                        <th width="5%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item in items.data track by $index" ng-class="isItemInList(item)>0?'item-selected':''">
                                    
                                        <td>@{{item.code}}</td>
                                        <td>@{{item.name}}</td>
                                        <td>@{{item.mrp}}</td>
                                        <td>
                                            <a class="btn btn-sm btn-default" data-dismiss="modal" href="" ng-click="addItem(item)">add</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            
                        </div>
                        <div class="modal-footer">
                            <paging
                                page="items.current_page" 
                                page-size="items.per_page" 
                                total="items.total"
                                show-prev-next="true"
                                show-first-last="true"
                                paging-action="getItems(page,search)">
                              </paging>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </form>

</div>

@stop
@section('js')
<script src="{{asset('assets/angular/angular/angular.min.js')}}"></script>
<script src="{{asset('assets/ng-pagination/dist/paging.min.js')}}"></script>
<script src="{{asset('assets/logics.js')}}"></script>
<script type="text/javascript">

    $(document).ready(function() { 
        $(".chosen").chosen();

        $('.datepick').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            format: 'yyyy-mm-dd'
        });

        $(".btn-group > .btn").click(function(){
            $(".btn-group > .btn").removeClass("active");
            $(this).addClass("active");
        });

        $('.modal-items').on('shown.bs.modal', function() {
            $(this).find('[autofocus]').focus();
        });

        $('#myform').submit(function(event) {
            event.preventDefault(); //this will prevent the default submit

            $('.showbox').hide();

            tmp = 0;

            var customer_id = $("#customer_id").val();
            $('#alert').hide();
            if(customer_id==null || customer_id==""){
                $('#alert').show();
                $('#error-customer').text('Please select customer');
                tmp++;
            }

            if(numberOfItems()==0){
                $('#alert1').show();
                $('#error-items').text('You cannot create invoice without items');
                tmp++;
            }; 

            if(isQtyEmpty()){
                $('#alert2').show();
                $('#error-quantity').text('You cannot create invoice without items quantity');
                tmp++;
            };

            if(tmp==0){
                $(this).unbind('submit').submit(); // continue the submit unbind preventDefault
            }
        });

    });

    function formSubmit() {
        // var customer = $("#customer").val();
        // $('#alert').hide();
        // console.log(isQtyEmpty()); 
        // if(customer==null || customer==""){
        //     $('#alert').show();
        //     $('.alert-error #message').text('Please select customer');
        //     return false;
        // }

        // if(numberOfItems()==0){
        //     $('#alert').show();
        //     $('.alert-error #message').text('You cannot create quotation without items');
        //     return false;
        // }; 

        // if(isQtyEmpty()){
        //     $('#alert').show();
        //     $('.alert-error #message').text('You cannot create quotation without items quantity');
        //     return false;
        //     $('#alert').hide();
        // };
        return true;
    }

    function isQtyEmpty() {
        var result = false;
        $('.items-table tbody .item-rows td .qty').each(function (index) {
            var qty = $(this).val();       
            $(this).parent().parent().css( "background-color", "transparent" );
            if(qty==null || qty=="" || qty==0){
                $(this).parent().parent().css( "background-color", "#ffc6c6" );
                result = true;
                return true; 
            }
        });
        return result;
    }

    function numberOfItems() {
        return $('.items-table tbody tr td .qty').length;
    }

    var app = angular.module('app', ['bw.paging']);

    app.filter('total', function(){
        return function(array, type){
            var total = 0;
            angular.forEach(array, function(value, index){
                if(type.indexOf("allTotal") === -1){
                    if(!isNaN(value[type])){
                        total = total + (parseFloat(value[type]));
                    }
                }else{
                    total = total + (parseFloat(value['totalAmount']-value['totalDiscount']+value['totalVat']));
                }
            })
            return total;
        }
    });

    app.controller('AdminInvoiceController', function AdminInvoiceController($scope,$filter) {
        
        $scope.items            = [];
        $scope.invoiceDetails   = [];

        $scope.getItems = function(page,search) {
            search = $('#search').val();
            $("#modal-overlay-2").show();
            $.ajax({
                url: "{{URL::to('admin/invoice/getItems')}}",
                method: 'GET',
                data: {'page': page,'search':search},
                async: true,
                success: function (data) {
                    $scope.items = data;
                    $scope.$apply();
                    $("#modal-overlay-2").hide();
                },
                error: function () {
                  
                }
            });
        }

        $scope.calculateInvoice = function(){
            
            $scope.invoice_details = calculateInvoiceDetails($scope.invoiceDetails);

        };

        $scope.addItem = function(item) {
            var isItem = $scope.isItemInList(item);
            if(isItem<0){
                $scope.invoiceDetails.push(item);
                $scope.calculateInvoice();
            }else{
            
            }      
        }

        $scope.isItemInList = function (pro) {
            for($i=0;$i<$scope.invoiceDetails.length;$i++){
                if ($scope.invoiceDetails[$i].id == pro.id) {
                    return 1;
                } 
            };
            return -1;
        }

        $scope.removeItem = function(index) {
            $scope.invoiceDetails.splice(index,1);    
        }

        $scope.clearAll = function(item) {
            sweetAlertConfirm("Are you sure ?","This will clear all the items are you sure you want to do this ?",3,function() {
                $scope.invoiceDetails = [];  
                $scope.$apply(); 
            });         
        }

        $scope.submit = function(){
            $scope.submitted = true;
        }

    });

</script>
@stop