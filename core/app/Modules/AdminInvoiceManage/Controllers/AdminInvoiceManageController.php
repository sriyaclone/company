<?php namespace App\Modules\AdminInvoiceManage\Controllers;

/**
* Controller class
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use Sentinel;
use PDF;
use DB;
use File;
use Mail;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Vat;
use App\Models\User;
use App\Models\Customer;
use App\Models\AppUser;

use Core\EmployeeManage\Models\Employee;

use App\Modules\AdminInvoiceManage\Requests\InvoiceRequest;
use App\Modules\AdminInvoiceManage\BusinessLogics\InvoiceLogic;

class AdminInvoiceManageController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	protected $invoiceLogic;

	public function __construct(InvoiceLogic $invoiceLogic){
		$this->invoiceLogic = $invoiceLogic;
	}

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listView(Request $request)
	{
		$old = $request;

		$data = $this->invoiceLogic->getInvoiceRecords($old);

		return view("AdminInvoiceManage::list", compact('data','old'));
	}

	public function approveView(Request $request)
	{
		$old = $request;
		$action_by = $this->invoiceLogic->getEmployees();
		$data = $this->invoiceLogic->getApprovalQuotations($old);
		return view("AdminInvoiceManage::approval", compact('data','old','action_by'));
	}


	public function createView(Request $request)
	{
		$customers = Customer::whereNull('deleted_at')->selectRaw('id, CONCAT(code,\'-\',first_name) as name')->get()->prepend(['id'=>'','name'=>'Select a Customer']);

		return view("AdminInvoiceManage::create",compact('products','customers'));
	}

	public function create(Request $request)
	{	
		$invoice = $this->invoiceLogic->createNewInvoice($request);
		$url = url('admin/invoice/invoice_pdf/')."/".$invoice->id;
		return redirect('admin/invoice/create')->with([
			'link'           => true,
			'link.link'      => $url,
			'link.linktitle' => ' Show invoice',
			'link.message'   => 'New Invoice saved! #('.$invoice->invoice_no.')',
			'link.title'     => 'Success..!'
        ]);

	}

	public function invoicePdf(Request $request,$id)
	{
		return  $this->invoiceLogic->getInvoicePdfOutput($id);
	}

	public function listViewDetail($id, Request $request)
	{
		$id        = $request->id;
		$details   = $this->invoiceLogic->getInvoiceDetailRecords($id);
		$list      = $this->invoiceLogic->getInvoiceById($id);

		return view("AdminInvoiceManage::details", compact('details', 'list', 'vat'));
	}

	public function show($id, Request $request)
	{
		$user_role = $this->invoiceLogic->getUserRole();
		$max_discount = ($user_role)?$user_role->discount:0;

		$id      = $request->id;
		$details = $this->invoiceLogic->getInvoiceDetailRecords($id);
		$list    = $this->invoiceLogic->getInvoiceById($id);

		if($list->quotation_status != PENDING || 
			$max_discount < 1 || 
			$list->approval_role_id != $user_role->id){
			return view('errors.404');
		}

		$vat = Vat::where('id', $list->vat_id)->first();
		$nbt = Vat::where('id', $list->nbt_id)->first();

		if($list->vat_type == 'vat' || $list->vat_type == 'svat'){
			$list = $this->invoiceLogic->calculateVatCustomerQuotation($list);
		}else{
			$list = $this->invoiceLogic->calculateNonVatCustomerQuotation($list);
			//$vat = null;
		}

		//return $list;

		/*return view("AdminInvoiceManage::approvaldetail", compact('data','quotation','quotation_detail','max_discount'));*/

		return view("AdminInvoiceManage::approve_view", compact('details', 'list', 'vat', 'max_discount', 'nbt'));
	}

	public function getItems(Request $request){
		return $this->invoiceLogic->getItems(3,$request->search);
	}

	public function getWarehouses(Request $request){
		return $this->invoiceLogic->getWarehouses();
	}

}