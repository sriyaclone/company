<?php namespace App\Modules\AdminWarehouseManage\BusinessLogics;


/**
* Business Logics 
* Define all the busines logics in here
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use App\Modules\AdminWarehouseManage\Models\AdminWarehouse;
use App\Modules\AdminLocationManage\Models\Location;
use App\Models\City;

use App\Models\User;
use App\Exceptions\TransactionException;

use Exception;
use Sentinel;

class WarehouseLogic extends Model {

    public function add($data){

        DB::transaction(function() use($data){
            $user = Sentinel::getUser();

            $location = Location::find($data->location);

            $warehouse = AdminWarehouse::create([
                'name'                =>  $data->name, 
                'code'                =>  $data->code,
                'city_id'             =>  $data->city,
                'type_id'             =>  $data->type,
                'email'               =>  $data->email,
                'contact_first'       =>  $data->contact_first,
                'contact_second'      =>  $data->contact_second,
                'address'             =>  $data->address,
                'location_id'         =>  $data->location,
                'lng'                 =>  $data->longtitude,
                'lat'                 =>  $data->latitude
            ]);

            if($warehouse) {
                return $warehouse;
            }else{
                throw new TransactionException('Record wasn\'t updated', 101);
            }
        });
    }

    /**
     * This function is used to get all warehouse details
     * @parm  integer $id that need to get warehouse details
     * @return warehouse object
     */
    public function getWarehouseDetails($id){
        $warehouse_details = AdminWarehouse::where('id','=',$id)->get();
        if(count($warehouse_details) > 0){
            return $warehouse_details;
        }else{
            return false;
        }
    }

    /**
     * This function is used to update warehouse
     * @param integer id and array $data
     * @response is boolean
     */
    public function updateWarehouse($id, $data){

        DB::transaction(function() use($id, $data){
            
            $warehouse        = AdminWarehouse::find($id);

            $warehouse->name            = $data->name;
            $warehouse->code            = $data->code;
            $warehouse->city_id         = $data->city;
            $warehouse->type_id         = $data->type;
            $warehouse->email           = $data->email;
            $warehouse->contact_first   = $data->contact_first;
            $warehouse->contact_second  = $data->contact_second;
            $warehouse->address         = $data->address;
            $warehouse->location_id     = $data->location;
            $warehouse->lng             = $data->longtitude;
            $warehouse->lat             = $data->latitude;
            $warehouse->save();

            if($warehouse){
                return true;
            }else{
                throw new Exception("Error occurred while updating warehouse.");
            }
        });
    }

    /**
     * This function is used to get all warehouse details
     * @parm -
     * @return warehouse object
     */
    public function getAllWarehouses(){
        return AdminWarehouse::with(['location','city','types'])->paginate(10);
    }

    /**
     * This function is used to search warehouse
     * @param integer $name, $type, $location
     * @response location object
     */
    public function searchWarehouse($code,$name, $type, $location, $city) {
        $search = AdminWarehouse::with(['location','city','types']);
        if($code != ''){
            $search = $search->where('code','LIKE', '%' . $code . '%');
        }
        if($name != ''){
            $search = $search->where('name','LIKE', '%' . $name . '%');
        }
        if($type != ''){
            if($type!='0'){
                $search = $search->where('type_id', $type);
            }
        }
        if($location != ''){
            if($location!='0'){
                $search = $search->where('location_id', $location);
            }
        }
        if($city != ''){
            if($city!='0'){
                $search = $search->where('city_id', $city);
            }
        }
        $search->orderBy('created_at','desc');
        return $search->paginate(10);
    }

    public function getWarehouseList(){
        
        $warehouses = AdminWarehouse::lists('name', 'id');

        return $warehouses;
    }

    public function getWarehouseById($id){

        $warehouse = AdminWarehouse::find($id);

        if(!$warehouse){
            throw new \Exception('P2-Warehouse not found for ID:'.$id);
        }

        return $warehouse;
    }

    public function deleteWarehouse($id){
        $warehouse = $this->getWarehouseById($id);

        if(!$warehouse->delete()){
            throw new \Exception('P3-Could not delete warehouse ID:'.$id);
        }

        return 1;
    }

}