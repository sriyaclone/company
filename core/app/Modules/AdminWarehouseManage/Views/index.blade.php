@extends('layouts.back_master') @section('title','Admin - Warehouse Management') 
@section('css')
<style type="text/css">

    .chosen-single{
        height: 25px !important;
        border : 0px !important;
    }

    .btn-warning2 {
    background-color: #1ab394;
    border-color: #1ab394;
    color: #FFFFFF;
    }
    
</style>
@stop

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Warehouse Management</h2>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{url('admin/warehouse/index')}}">Warehouse</a></li>
            <li class="active">List Warehouse</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a href="{{url('admin/warehouse/create')}}" class="btn btn-warning2"><i class="fa fa-plus"></i> Add Warehouse</a>
            <a href="{{url('admin/warehouse/gmaps
            ')}}" class="btn btn-warning"><i class="fa fa-plus"></i> View on Map</a>
        </div>
    </div>
</div>

<!-- Content-->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>List Warehouse</h5>
            <div class="ibox-tools">
                
            </div>
        </div>

        <div class="ibox-content" style="display: block;">
                <table class="table table-striped table-bordered table-hover dataTables-example">
                    <thead>
                        <form id="form" role="form" method="get" action="{{url('admin/warehouse/search')}}">
                            <tr>
                                <th width="5%">#</th>
                                <th>Warehouse Code</br></br>
                                    <input type="text" class="form-control" name="code" placeholder="Enter Code" value="{{$code}}">
                                </th>
                                <th>Warehouse Name </br></br>
                                    <input type="text" class="form-control" name="name" placeholder="Enter Name" value="{{$name}}">
                                </th>
                                <th>Warehouse Type </br></br>
                                    {!! Form::select('type', $warehouse_types, $type,['class'=>'chosen','style'=>'width:100%;','required','data-placeholder'=>'Choose Warehouse Type','id'=>'type']) !!}
                                </th>
                                <th>City </br></br>
                                    {!! Form::select('city', $cities, $city,['class'=>'chosen','style'=>'width:100%;','required','data-placeholder'=>'Choose City','id'=>'city']) !!}
                                </th>
                                <th align="text-center">Warehouse Location </br>
                                    {!! Form::select('location', $locations, $location,['class'=>'chosen','style'=>'width:100%;','required','data-placeholder'=>'Choose Warehouse','id'=>'location']) !!}
                                </th>
                                <th>Contact</th>
                                <th>Address</th>
                                <th>Location</th>
                                <th width="10%">Action <br></br>
                                    <button type="submit" class="btn btn-default btn-sm" id="plan"><i class="fa fa-search" style="padding-right: 16px;width: 28px;"></i>Find</button>
                                </th>
                            </tr>
                        </form>
                    </thead>
                    <tbody>
                    @if(count($warehouse_details) > 0)
                        @foreach($warehouse_details as $key => $row)
                            <tr>                           
                                <td class="text-center">
                                    {{ (($warehouse_details->currentPage()-1)*$warehouse_details->perPage())+($key+1) }}</td>
                                <td> 
                                    {{ ($row->code !='') ? $row->code : '-' }}
                                </td>
                                <td> 
                                    {{ ($row->name !='') ? $row->name : '-' }}
                                </td>
                                <td class="text-center">
                                    {{ ($row->types != '') ? $row->types->name : '-' }}
                                </td>
                                <td class="text-center">
                                    {{ ($row->city != '') ? $row->city->name : '-' }}
                                </td>
                                <td class="text-center">
                                    {{ ($row->location != '') ? $row->location->name : '-' }}
                                </td>
                                <td>
                                    {{ ($row->contact_first != '') ? $row->contact_first : '-' }} <br>
                                    {{ ($row->contact_second != '') ? $row->contact_second : '-' }}
                                </td>
                                <td>
                                    {{ ($row->address != '') ? $row->address : '-' }}
                                </td>
                                <td>
                                    Lon : {{ ($row->lng != '') ? $row->lng : '-' }}<br>
                                    Lan : {{ ($row->lat != '') ? $row->lat : '-' }}
                                </td>
                                <td class="text-center">
                                    <div class="btn-group">
                                    @if($user->hasAnyAccess(['warehouse.edit', 'admin']))
                                        <button class="btn btn-default" onclick="window.location.href='{{url('admin/warehouse/edit/'.$row->id)}}'"><i class="fa fa-pencil"></i></button>
                                    @else
                                        <button class="btn btn-default" disabled><i class="fa fa-pencil"></i></button>
                                    @endif

                                    @if($user->hasAnyAccess(['warehouse.delete', 'admin']))
                                        <button class="btn btn-default btn-delete" data-id="{{$row->id}}" onclick="deleteData('{{url('admin/warehouse/delete?id='.$row->id)}}');"><i class="fa fa-trash-o"></i></button>
                                    @else
                                        <button class="btn btn-default" disabled><i class="fa fa-trash-o"></i></button>
                                    @endif
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9" align="center"> - No Data to Display - </td>
                        </tr>
                    @endif
                    </tbody>
                </table>
                @if($warehouse_details != null)
                <div class="row">
                    <div class="col-sm-12 text-left">
                         Row {{$warehouse_details->count()}} of {{$warehouse_details->total()}} 
                    </div>
                    <div class="col-sm-12 text-right">
                         {!! $warehouse_details->render() !!}
                    </div>
                </div>
                @endif
        </div>
    </div>
</div>

@stop

@section('js')
<script type="text/javascript">
    $(document).ready(function() {
        $(".chosen").chosen();

        $('#type').change(function(){
            $("form").submit();
        });
    });

    function deleteData(_url){
        swal({
            title: "Are you sure?",
            text: "you wanna delete this warehouse?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        },
        function(){
            $.ajax({
                url: _url,
                method: 'get',
                cache: false,
                data: [],
                success: function(response){
                    if(response.status == 1){
                        swal("Done!", "warehouse has been deleted!.", "success");
                        location.reload();
                    }else{
                        swal("Error!", response.message, "error");
                    }
                },
                error: function(xhr){
                    console.log(xhr);
                    swal("Error!", 'Error occurred. Please try again', "error");
                } 
            });
      
        });
    }
</script>
@stop
