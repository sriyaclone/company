@extends('layouts.back_master') @section('title','Admin - Warehouse Management')
@section('current_title','Warehouse Edit')

@section('css')
<style type="text/css">
  
</style>
@stop

@section('content')
    <!-- Content Header (Page header) -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Warehouse Management</h2>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{url('admin/warehouse/index')}}">Warehouse</a></li>
            <li class="active">Edit Warehouse</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a href="{{url('admin/warehouse/index')}}" class="btn btn-primary"><i class="fa fa-th"></i> List Warehouse</a>
            <a href="{{url('admin/warehouse/create')}}" class="btn btn-warning"><i class="fa fa-plus"></i> Add Warehouse</a>
        </div>
    </div>
</div>
<!-- !!Content Header (Page header) -->

<!-- Main content -->
<div class="wrapper wrapper-content animated fadenRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Edit Warehouse</h5>
            <div class="ibox-tools">
                
            </div>
        </div>

        <div class="ibox-content" style="display: block;">
            <form role="form" class="form-horizontal form-validation" method="post">
                {!!Form::token()!!}

                <div class="form-group">
                    <label class="col-sm-2 control-label required">Warehouse Type</label>
                    <div class="col-sm-10 @if($errors->has('type')) has-error @endif">
                        {!! Form::select('type', $warehouse_types, $warehouse_details[0]->type_id,['class'=>'chosen','style'=>'width:100%;','required','data-placeholder'=>'Choose Warehouse Type']) !!}
                        @if($errors->has('type'))
                            <span class="help-block">{{$errors->first('type')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class=" @if($errors->has('code')) has-error @endif">
                        <label class="col-sm-2 control-label required">Code</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @if($errors->has('code')) error @endif" name="code" placeholder="Code" value="{{$warehouse_details[0]->code}}">
                            @if($errors->has('code'))
                                <span class="help-block">{{$errors->first('code')}}</span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class=" @if($errors->has('name')) has-error @endif">
                        <label class="col-sm-2 control-label required">Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @if($errors->has('name')) error @endif" name="name" placeholder="Name" value="{{$warehouse_details[0]->name}}">
                            @if($errors->has('name'))
                                <span class="help-block">{{$errors->first('name')}}</span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label required">City</label>
                    <div class="col-sm-10 @if($errors->has('city')) has-error @endif">
                        {!! Form::select('city', $cities, $warehouse_details[0]->city_id,['class'=>'chosen','style'=>'width:100%;','required','data-placeholder'=>'Choose City']) !!}
                        @if($errors->has('city'))
                            <span class="help-block">{{$errors->first('city')}}</span>
                        @endif
                    </div>
                </div>
                    
                <div class="form-group">
                    <div class="@if($errors->has('email')) has-error @endif">
                        <label class="col-sm-2 control-label required">Email</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @if($errors->has('email')) error @endif" name="email" placeholder="Email" value="{{$warehouse_details[0]->email}}">
                            @if($errors->has('email'))
                                <span class="help-block">{{$errors->first('email')}}</span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="@if($errors->has('contact_first')) has-error @endif">
                        <label class="col-sm-2 control-label required">Contact First</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @if($errors->has('contact_first')) error @endif" name="contact_first" placeholder="Contact First" value="{{$warehouse_details[0]->contact_first}}">
                            @if($errors->has('contact_first'))
                                <span class="help-block">{{$errors->first('contact_first')}}</span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="@if($errors->has('contact_second')) has-error @endif">
                        <label class="col-sm-2 control-label">Contact Second</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @if($errors->has('contact_second')) error @endif" name="contact_second" placeholder="Contact Second" value="{{$warehouse_details[0]->contact_second}}">
                            @if($errors->has('contact_second'))
                                <span class="help-block">{{$errors->first('contact_second')}}</span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label required">Address</label>
                    <div class="col-sm-10">
                        <textarea name="address" class="form-control @if($errors->has('address')) has-error @endif" rows="3">{{$warehouse_details[0]->address}}</textarea>
                        @if($errors->has('address'))
                            <span class="help-block">{{$errors->first('address')}}</span>
                        @endif
                    </div>
                </div>
        
                <div class="form-group">
                    <label class="col-sm-2 control-label">Location</label>
                    <div class="col-sm-10 @if($errors->has('location')) has-error @endif">
                        {!! Form::select('location', $locations, $warehouse_details[0]->location_id,['class'=>'chosen','style'=>'width:100%;','required','data-placeholder'=>'Choose Location']) !!}
                        @if($errors->has('location'))
                            <span class="help-block">{{$errors->first('location')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="@if($errors->has('longtitude')) has-error @endif">
                        <label class="col-sm-2 control-label">Longtitude</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @if($errors->has('longtitude')) error @endif" name="longtitude" placeholder="Longtitude" value="{{$warehouse_details[0]->longtitude}}">
                            @if($errors->has('longtitude'))
                                <span class="help-block">{{$errors->first('longtitude')}}</span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="@if($errors->has('latitude')) has-error @endif">
                        <label class="col-sm-2 control-label">Latitude</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @if($errors->has('latitude')) error @endif" name="latitude" placeholder="Latitude" value="{{$warehouse_details[0]->latitude}}">
                            @if($errors->has('latitude'))
                                <span class="help-block">{{$errors->first('latitude')}}</span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-success pull-right"><i class="fa fa-floppy-o"></i> Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div> 
<!-- !!!Content -->

@stop
@section('js')
    <!-- CORE JS -->
  <script type="text/javascript">
      $(document).ready(function() {
        $(".chosen").chosen();
      });
  </script>
    <!-- //CORE JS -->
@stop
