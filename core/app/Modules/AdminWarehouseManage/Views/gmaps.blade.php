@extends('layouts.back_master') @section('title','Admin - Warehouse Management') 
@section('css')
<style type="text/css">

    #mymap {
            border:1px solid red;
            width: 700px;
            height: 500px;
    }

    .btn-warning2 {
    background-color: #1ab394;
    border-color: #1ab394;
    color: #FFFFFF;
    }
</style>
@stop

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Warehouse Management</h2>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{url('admin/warehouse/index')}}">Warehouse</a></li>
            <li class="active">Warehouse Map on view</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a href="{{url('admin/warehouse/create')}}" class="btn btn-warning2"><i class="fa fa-plus"></i> Add Warehouse</a>
            <a href="{{url('admin/warehouse/index')}}" class="btn btn-warning"><i class="fa fa-plus"></i>List Warehouse</a>
        </div>
    </div>
</div>

<!-- Content-->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Warehouse Locations</h5>
            <div class="ibox-tools">
                
            </div>
        </div>

        <div class="ibox-content" style="display: block;">

            <div id="mymap"></div>  

        </div>
    </div>
</div>


@stop

@section('js')
    <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD16kKkYS7Q9PkS922z71C4mLzDDYaKI"
    async defer></script> -->
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyD16kKkYS-7Q9PkS922z7lC4mLz-DDYaKI"></script>
    <script src="{{asset('assets/map/gmaps.js')}}"></script>
   

<script type="text/javascript">
    var warehouse = <?php print_r(json_encode($warehouse)) ?>;
    console.log(warehouse);


    var mymap = new GMaps({
      el: '#mymap',
      lat: 8.3114,
      lng: 80.4037,
      zoom:7
    });


    $.each( warehouse, function( index, value ){
        mymap.addMarker({
          lat: value.lat,
          lng: value.lng,
          title: value.name,
          click: function(e) {
            alert('This is '+value.name+',from Sri Lanka.');
          }
        });
   });
</script>
@stop
