@extends('layouts.back_master') @section('title','Admin - Warehouse Management')
@section('css')
<style type="text/css">
	
</style>
@stop
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Warehouse Management</h2>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{url('admin/warehouse/index')}}">Warehouse</a></li>
            <li class="active">List Warehouse</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a href="{{url('admin/warehouse/index')}}" class="btn btn-primary"><i class="fa fa-th"></i> List Warehouse</a>
        </div>
    </div>
</div>

<!-- Content-->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Add Warehouse</h5>
            <div class="ibox-tools">
                
            </div>
        </div>

        <div class="ibox-content" style="display: block;">
            <form role="form" class="form-horizontal form-validation" method="post" id="myform">
                {!!Form::token()!!}
                <div class="form-group">
                    <label class="col-sm-2 control-label required">Warehouse Type</label>
                    <div class="col-sm-8">                      
                        {!! Form::select('type', $warehouse_types, Input::old('type'),['class'=>'chosen','style'=>'width:100%;','required','data-placeholder'=>'Choose Warehouse Type']) !!}
                        @if($errors->has('type'))
                            <span class="help-block">{{$errors->first('type')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label required">Code</label>
                    <div class="col-sm-8 @if($errors->has('code')) has-error @endif">
                        <input type="text" class="form-control" id="code" name="code" placeholder="Code" value="{{Input::old('code')}}">
                        @if($errors->has('code'))
                            <span class="help-block">{{$errors->first('code')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label required">Name</label>
                    <div class="col-sm-8 @if($errors->has('name')) has-error @endif">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{Input::old('name')}}">
                        @if($errors->has('name'))
                            <span class="help-block">{{$errors->first('name')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label required">City</label>
                    <div class="col-sm-8">                      
                        {!! Form::select('city', $cities, Input::old('city'),['class'=>'chosen','style'=>'width:100%;','required','data-placeholder'=>'Choose City']) !!}
                        @if($errors->has('city'))
                            <span class="help-block">{{$errors->first('city')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label required">Email</label>
                    <div class="col-sm-8 @if($errors->has('email')) has-error @endif">
                        <input type="text" class="form-control" name="email" placeholder="Email" value="{{Input::old('email')}}">
                        @if($errors->has('email'))
                            <span class="help-block">{{$errors->first('email')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label required">Contact First</label>
                    <div class="col-sm-8 @if($errors->has('contact_first')) has-error @endif">
                        <input type="text" class="form-control" name="contact_first" placeholder="Contact First" value="{{Input::old('contact_first')}}">
                        @if($errors->has('contact_first'))
                            <span class="help-block">{{$errors->first('contact_first')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Contact Second</label>
                    <div class="col-sm-8 @if($errors->has('contact_second')) has-error @endif">
                        <input type="text" class="form-control" name="contact_second" placeholder="Contact Second" value="{{Input::old('contact_second')}}">
                        @if($errors->has('contact_second'))
                            <span class="help-block">{{$errors->first('contact_second')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label required">Address</label>
                    <div class="col-sm-8 @if($errors->has('address')) has-error @endif">
                       <textarea name="address" id="address" class="form-control" rows="3">{{old('address')}}</textarea>
                        @if($errors->has('address'))
                            <span class="help-block">{{$errors->first('address')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Location</label>
                    <div class="col-sm-8">                      
                        {!! Form::select('location', $locations, Input::old('location'),['class'=>'chosen','style'=>'width:100%;','required','data-placeholder'=>'Choose Location']) !!}
                        @if($errors->has('location'))
                            <span class="help-block">{{$errors->first('location')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Longtitude</label>
                    <div class="col-sm-8 @if($errors->has('longtitude')) has-error @endif">
                        <input type="text" class="form-control" name="longtitude" placeholder="Longtitude" value="{{Input::old('longtitude')}}">
                        @if($errors->has('longtitude'))
                            <span class="help-block">{{$errors->first('longtitude')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Latitude</label>
                    <div class="col-sm-8 @if($errors->has('latitude')) has-error @endif">
                        <input type="text" class="form-control" name="latitude" placeholder="Latitude" value="{{Input::old('latitude')}}">
                        @if($errors->has('latitude'))
                            <span class="help-block">{{$errors->first('latitude')}}</span>
                        @endif
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-success pull-right"><i class="fa fa-floppy-o"></i> Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


@stop

@section('js')
	<!-- CORE JS -->
	<script type="text/javascript">
    	$(document).ready(function() {
	  		$(".chosen").chosen();
	  	});

        $('#myform').submit(function() {
            var txt = $('#name').val();
            
            $('#name').val(txt.trim());

            $('#myform').submit();
        });
	</script>
  	<!-- //CORE JS -->
@stop
