<?php
namespace App\Modules\AdminWarehouseManage\Requests;

use App\Http\Requests\Request;

class WarehouseRequest extends Request {

	public function authorize(){
		return true;
	}

	public function rules(){
		$id = $this->id;

		if($this->is('admin/warehouse/create')){
			$rules = [
				'code'	=> 'required|alpha_dash|unique:warehouse,code,'.$this->code,
				'name'	=> 'required|unique:warehouse,name,'.$this->name,
				'city'	=> 'required|numeric|min:1',
				'email'	=> 'required|regex:/[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}/',
				'contact_first'	=> 'required',
				'address'	=> 'required',
				'type'	=> 'required|numeric|min:1',	
			];
		}else if($this->is('admin/warehouse/edit/'.$id)){
			$rules = [
				'code'	=> 'required|unique:warehouse,code,'.$id,
				'name'	=> 'required|unique:warehouse,name,'.$id,
				'city'	=> 'required|numeric|min:1',
				'email'	=> 'required|regex:/[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}/',
				'contact_first'	=> 'required',
				'address'	=> 'required|unique:warehouse,address,'.$id,
				'type'	=> 'required|numeric|min:1',
			];
		}

		return $rules;
	}

	public function messages(){

        return [
            'location.min' => 'The Location can not be blank'
        ];

    }

}
