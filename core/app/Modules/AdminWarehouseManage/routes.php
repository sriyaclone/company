<?php


Route::group(['middleware' => ['auth']], function()
{	
	Route::group(['prefix'=>'admin/warehouse/','namespace' => 'App\Modules\AdminWarehouseManage\Controllers'],function() {

	    Route::get('create', [
	      'as' => 'warehouse.create', 'uses' => 'AdminWarehouseController@create'
	    ]);

	    Route::post('create', [
	      'as' => 'warehouse.create', 'uses' => 'AdminWarehouseController@store'
	    ]);

	    Route::get('edit/{id}', [
	      'as' => 'warehouse.edit', 'uses' => 'AdminWarehouseController@edit'
	    ]);

	    Route::post('edit/{id}', [
            'as' => 'warehouse.edit','uses' => 'AdminWarehouseController@update'
        ]);

        Route::get('index', [
            'as' => 'warehouse.index', 'uses' => 'AdminWarehouseController@index'
        ]);

        Route::get('search', [
            'as' => 'warehouse.search', 'uses' => 'AdminWarehouseController@searchWarehouse'
        ]);
	    
	    Route::get('delete', [ 
            'as' => 'warehouse.delete', 'uses' => 'AdminWarehouseController@delete'
        ]);

        Route::get('gmaps', [ 
            'as' => 'warehouse.gmaps', 'uses' => 'AdminWarehouseController@gmaps'
        ]);
	});    
});