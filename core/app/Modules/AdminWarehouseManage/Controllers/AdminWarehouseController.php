<?php namespace App\Modules\AdminWarehouseManage\Controllers;


/**
* Controller class
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Modules\AdminWarehouseManage\Models\AdminWarehouse;
use App\Modules\AdminWarehouseManage\BusinessLogics\WarehouseLogic;
use App\Modules\AdminWarehouseManage\Requests\WarehouseRequest;

use App\Models\City;
use App\Models\WarehouseType;
use App\Modules\AdminLocationManage\Models\Location;

use DB;

class AdminWarehouseController extends Controller {

	protected $warehouse;

    public function __construct(WarehouseLogic $warehouseLogic){
        $this->warehouse = $warehouseLogic;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$warehouse_details = $this->warehouse->getAllWarehouses();
		
		$warehouse_types = WarehouseType::all()->lists('name', 'id')->prepend('All','0');

		$cities =City::all()->lists('name', 'id')->prepend('All','0');

		$locations=Location::all()->lists('name', 'id')->prepend('All','0');

		return view("AdminWarehouseManage::index")->with([
			'warehouse_details'		 	=> $warehouse_details,
			'warehouse_types' 			=> $warehouse_types,
			'cities'					=> $cities,
			'locations'					=> $locations,
			'type'						=> 0,
			'location'					=> 0,
			'city'						=> 0,
			'code'						=> '',
			'name' 						=> '',
			'contact_first' 			=> '',
			'contact_second' 			=> '',
			'address' 					=> '',
			'longtitude' 				=> '',
			'latitude' 					=> ''
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$warehouse_types = WarehouseType::all()->lists('name', 'id')->prepend('Select Warehouse Type','0');

		$cities =City::all()->lists('name', 'id')->prepend('Select City','0');

		$locations=Location::all()->lists('name', 'id')->prepend('Select Location','0');
		
		return view('AdminWarehouseManage::create')->with([ 
			'warehouse_types' => $warehouse_types,
			'cities'		  => $cities,
			'locations'		  => $locations,
		]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(WarehouseRequest $request)
	{
		try {
	        
	        $bool = $this->warehouse->add($request);

	        return redirect('admin/warehouse/index')->with([
                'success' => true,
                'success.message' => 'New warehouse added successfully!',
                'success.title'   => 'Success..!'
            ]);
	    }
	    catch(Exception $e){
            return redirect('admin/warehouse/index')->with([
                'error' => true,
                'error.title' => 'Error..!',
                'error.message' => $e->getMessage()
            ])->withInput();
        }
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function edit($id)
	{
        $warehouse_details = $this->warehouse->getWarehouseDetails($id);
        
        $warehouses = AdminWarehouse::all()->lists('name', 'id')->prepend('Root','0');

        $warehouse_types = WarehouseType::all()->lists('name', 'id')->prepend('Select Warehouse Type','0');

		$cities =City::all()->lists('name', 'id')->prepend('Select City','0');

		$locations=Location::all()->lists('name', 'id')->prepend('Select Location','0');

        if($warehouse_details) {
            return view("AdminWarehouseManage::edit")->with([
            	'warehouse_details' 	=> $warehouse_details,
            	'warehouses' 			=> $warehouses,
            	'warehouse_types' 		=> $warehouse_types,
            	'cities' 				=> $cities,
            	'locations' 			=> $locations
           	]);
        }else{
            return response()->view('errors.404');
        }
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(WarehouseRequest $request, $id)
	{
		try {
            $bool = $this->warehouse->updateWarehouse($id, $request);

            return redirect('admin/warehouse/index')->with([
                'success' 	      => true,
                'success.message' => 'Warehouse updated successfully!',
                'success.title'   => 'Success..!'
            ]);
        }catch(Exception $e){
            return back()->with([
                'error' 		=> true,
                'error.title'	=> 'Error..!',
                'error.message' => $e->getMessage()
            ])->withInput();
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function delete(Request $request)
	{
		try{
			$this->warehouse->deleteWarehouse($request->id);
			return response()->json([
				'status' => 1
			]);
		}catch(\Exception $e){
			return response()->json([
				'status'  => 0,
				'message' => $e->getMessage()
			]);
		}
	}

	/**
     * This function is used to search product category
     * @return Response
     */
    public function searchWarehouse(Request $request){
        $warehouse_details = $this->warehouse->searchWarehouse($request->get('code'),$request->get('name'), $request->get('type'), $request->get('location'),$request->get('city'));

        $warehouse_types = WarehouseType::all()->lists('name', 'id')->prepend('All','0');

		$locations		 =Location::all()->lists('name', 'id')->prepend('All','0');

		$cities 		 =City::all()->lists('name', 'id')->prepend('Select City','0');
        
        return view("AdminWarehouseManage::index")->with([
        	'warehouse_details' 		=> $warehouse_details,
        	'warehouse_types' 			=> $warehouse_types,
        	'locations' 				=> $locations,
        	'cities'					=> $cities,
        	'code' 						=> $request->get('code'),
        	'name' 						=> $request->get('name'),
        	'type'						=> $request->get('type'),
        	'location' 					=> $request->get('location'),
        	'city' 						=> $request->get('city')
        ]);
    }

    /**
     * This function is used to show map
     * @return Response
     */
    public function gmaps()
    {
    	$warehouse = DB::table('warehouse')->get();
    	return view('AdminWarehouseManage::gmaps',compact('warehouse'));
    }

}
