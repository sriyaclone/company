<?php namespace App\Modules\AdminWarehouseManage\Models;

/**
*
* Model
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdminWarehouse extends Model{

	/**
     * table row delete
     */
    use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'warehouse';

	// guard attributes from mass-assignment
	protected $guarded = array('id');

	/**
	 * Warehouse Type
	 * @return object warehouse type
	 */
	public function types() {
		return $this->belongsTo('App\Models\WarehouseType', 'type_id', 'id');
	}
	/**
	 * Location
	 * @return object location
	 */
	public function location() {
		return $this->belongsTo('App\Modules\AdminLocationManage\Models\Location', 'location_id', 'id');
	}
	/**
	 * City
	 * @return object city
	 */
	public function city() {
		return $this->belongsTo('App\Models\City', 'city_id', 'id');
	}

}
