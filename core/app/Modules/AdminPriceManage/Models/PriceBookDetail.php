<?php namespace App\Modules\AdminPriceManage\Models;

/**
*
* Model
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class PriceBookDetail extends Model {

	use SoftDeletes;

	protected $table = 'pricebook_details';

	protected $dates = ['deleted_at'];

	protected $fillable = [
		'product_id',
		'pricebook_id',
		'price',
	];

	public function product(){
		return $this->belongsTo('App\Modules\AdminProductManage\Models\Product', 'product_id', 'id');
	}

}
