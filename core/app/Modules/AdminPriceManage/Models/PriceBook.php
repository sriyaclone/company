<?php namespace App\Modules\AdminPriceManage\Models;

/**
*
* Model
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class PriceBook extends Model {

	use SoftDeletes;
	
	protected $dates = ['deleted_at'];

	protected $table = 'pricebook';


	protected $fillable = [
		'code',
		'name',
		'status',
	];

	public function details(){
		return $this->hasMany('App\Modules\AdminPriceManage\Models\PriceBookDetail', 'pricebook_id', 'id');
	}

	public function customers(){
		return $this->belongsToMany('App\Modules\AdminCustomerManage\Models\Customer', 'customer_pricebooks', 'pricebook_id', 'customer_id')
				->withPivot(['ended_at']);
	}

	public function current_customers(){
		return $this->hasMany('App\Modules\AdminCustomerManage\Models\CustomerPriceBook', 'pricebook_id', 'id')
			->whereNull('ended_at');
	}

}
