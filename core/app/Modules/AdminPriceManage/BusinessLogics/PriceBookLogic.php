<?php namespace App\Modules\AdminPriceManage\BusinessLogics;


/**
* Business Logics 
* Define all the busines logics in here
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use Illuminate\Database\Eloquent\Model;

use App\Modules\AdminProductManage\BusinessLogics\ProductLogic;

use App\Modules\AdminPriceManage\Models\PriceBook;
use App\Modules\AdminPriceManage\Models\PriceBookDetail;

use DB;
use Excel;

class PriceBookLogic{

	private $pricebook;
	private $detail;
	private $productLogic;
	private $customerLogic;

	public function __construct(PriceBook $pricebook, 
		PriceBookDetail $detail, 
		ProductLogic $productLogic){

		$this->pricebook = $pricebook;
		$this->detail = $detail;
		$this->productLogic = $productLogic;
	}

	public function savePriceBook($priceData, $isUpdate = false, $isUpload = false){
		$pricebook = $this->pricebook;

		if($priceData->code)
			$pricebook->code = $priceData->code;
		elseif($isUpdate && !$priceData->code)
			$pricebook->code = '';

		if($priceData->name)
			$pricebook->name = $priceData->name;
		elseif($isUpdate && !$priceData->name)
			$pricebook->name = '';

		if($priceData->status)
			$pricebook->status = $priceData->status;
		elseif($isUpdate && !$priceData->status)
			$pricebook->status = 0;

		if(!$pricebook->save()){
			throw new \Exception('P1-Could not save pricebook');
		}

		if(!$isUpload){
			foreach($priceData->product as $key => $pro){
				$data = new \StdClass;

				if($pro){
					$product = $this->productLogic->getProductById($pro);
					$data->product_id = $product->id;
				}else{
					throw new \Exception('P2-Product ID not found');
				}

				$data->price = $priceData->price[$key];

				$this->savePriceBookDetails($pricebook, $data, $isUpdate);
			}
		}

		return $pricebook;
	}

	public function savePriceBookDetails($priceBook, $detailData, $isUpdate = false){
		$detail = $this->detail;

		if($detailData->price)
			$detail->price = $detailData->price;
		elseif($isUpdate && !$detailData->price)
			$detail->price = '';

		$detail->pricebook_id = $priceBook->id;

		$detail->product_id = $detailData->product_id;

		if(!$detail->save()){
			throw new \Exception('Could not save price detail');
		}

		return 1;
	}

	public function getPriceBookById($id){

		$book = PriceBook::with(['details.product'])->find($id);

		if(!$book){
			throw new \Exception('P3-Pricebook not found for ID:'.$id);
		}

		return $book;
	}

	public function getPriceBookByCode($code){

		$book = PriceBook::where('code',$code)->first();

		if(!$book){
			throw new \Exception('P4-Pricebook not found for Code:'.$code);
		}

		return $book;
	}

	public function add($data){
		$dd = null;

		DB::transaction(function() use ($data, &$dd){
			$this->pricebook = new PriceBook;
			$priceBook = $this->savePriceBook($data, true, true);
			$dd = $this->uploadExcel($priceBook, $data->file);
		});

		return $dd;
	}

	public function updatePriceBook($id, $data){
		$dd = null;

		DB::transaction(function() use ($data, $id, &$dd){
			$book = $this->getPriceBookById($id);
			$this->pricebook = $book;
			$priceBook = $this->savePriceBook($data, false, true);
			$dd = $this->uploadExcel($priceBook, $data->file);
		});

		return $dd;
	}

	public function getDetailByBookAndProduct($pricebook_id, $product_id){
		$detail = PriceBookDetail::where('pricebook_id',$pricebook_id)
					->where('product_id', $product_id)->first();

		if(!$detail){
			throw new \Exception('Detail not found');
		}

		return $detail;
	}

	public function uploadExcel($pricebook, $file){
		$data = [];
		Excel::load($file, function($reader) use (&$data, $pricebook){

			$reader->each(function($row) use (&$data, $pricebook){
				$row->pricebook_id = $pricebook->id;
				$this->validateRequiredDetail($row);

				$product = $this->productLogic->getProductByCode($row->code);
				$row->product_id = $product->id;

				try{
					$detail = $this->getDetailByBookAndProduct($pricebook->id, $product->id);
				}catch(\Exception $e){
					$detail = null;
				}

				if(!$detail){
					$this->detail = new PriceBookDetail;
					$this->savePriceBookDetails($pricebook, $row);
					array_push($data, [
						'code' => $row->code, 
						'price' => $row->price,
						'status' => 1,
						'message' => 'Done'
					]);
				}else{
					$this->detail = $detail;
					$this->savePriceBookDetails($pricebook, $row, true);

					array_push($data, [
						'code' => $row->code, 
						'price' => $row->price,
						'status' => 1,
						'message' => 'Updated existing data.'
					]);
				}
			});
		});

		return $data;
	}

	public function validateRequiredData($data){
		if(!$data->code)
			throw new \Exception('PB1-Short Code is required');

		if(!$data->name)
			throw new \Exception('PB2-Price Book Name is required');

		return 1;
	}

	public function validateRequiredDetail($data){
		if(!$data->pricebook_id)
			throw new \Exception('PB1-Price Book is required');

		if(!$data->code)
			throw new \Exception('PB2-Product Code is required');

		if(!$data->price)
			throw new \Exception('PB3-Price is required');

		if(!is_numeric($data->price))
			throw new \Exception('PB4-Price should be numeric');

		return 1;
	}

	public function getProductPriceForCustomer($product_id, $customer_id){

		$product = $this->productLogic->getProductById($product_id);
		$customer = $this->customerLogic->getCustomerById($customer_id);

		if(!count($customer->pricebook) > 0){
			return null;
		}

		$price = $customer->pricebook[0]->details()
		->with(['product'])
		->where('product_id', $product->id)
		->first();


		return $price;
	}

	public function getRecords($filters, $perPage = 10){
		$books = PriceBook::orderBy('id','asc');

		if($filters->code != ''){
			$books->where('code','LIKE','%'.$filters->code.'%');
		}

		if($filters->keyword != ''){
			$books->where('name', 'LIKE', '%'.$filters->keyword.'%');
		}

		$books = $books->paginate($perPage);

		return $books;
	}

	public function assignCustomer($id, $data){

		$pBook = $this->getPriceBookById($id);

		if(!$data->is_checked){
			$book = DB::table('customer_pricebooks')
					->whereNull('ended_at')
					->where('customer_id',$data->customer)
					->update(['ended_at' => date('Y-m-d H:i:s')]);

			return [
				'status' => 1,
				'line' => 3,
				'message' => 'Customer detached from Pricebook '.$pBook->code
			];
		}else{

			$customer = $this->customerLogic->getCustomerById($data->customer);
			if(count($customer->pricebook) > 0){
				$dd = DB::table('customer_pricebooks')
					->whereNull('ended_at')
					->where('customer_id',$data->customer)
					->update(['ended_at' => date('Y-m-d H:i:s')]);

				$book = $this->getPriceBookById($id)->customers()->attach($data->customer);

				return [
					'status' => 1,
					'line' => 1,
					'message' => 'Customer detached from Pricebook '.$customer->pricebook[0]->code.' and attached to Pricebook '.$pBook->code
				];
			}else{
				$book = $this->getPriceBookById($id)->customers()->attach($data->customer);
				return [
					'status' => 1,
					'line' => 2,
					'message' => 'Customer attached to Pricebook '.$pBook->code
				];
			}
		}
	}

}
