@extends('layouts.back_master') @section('title','List Price Book')
@section('css')

<style type="text/css">
    
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Price Book Management</h2>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{url('admin/price/list')}}">Price Book Management</a></li>
            <li class="active">List Price Book</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a class="btn btn-warning" href="{{ url('admin/price/add') }}"><i class="fa fa-plus" aria-hidden="true"></i> Add Price Book</a>
        </div>
    </div>
</div>

<!-- Main content -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>List Price Book</h5>
            <div class="ibox-tools">
                
            </div>
        </div>

        <div class="ibox-content" style="display: block;">
            <table class="table table-bordered">
                <thead>
                    <form method="get">
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">Short Code <br>
                                <input type="text" class="form-control" placeholder="Enter Code" name="code" value="{{$old->code}}">
                            </th>
                            <th class="text-center">Name <br>
                                <input type="text" class="form-control" placeholder="Enter Name" name="keyword" value="{{$old->keyword}}">
                            </th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Actions <br>
                                <button class="btn btn-default" type="submit">Search</button>
                            </th>
                        </tr>
                    </form>
                </thead>
                <tbody>
                    @if(count($data) > 0)
                        @foreach($data as $key => $row)
                            <tr>
                                <td class="text-center">{{ (($data->currentPage()-1)*$data->perPage())+($key+1) }}</td>
                                <td>{{ ($row->code != '') ? $row->code : '-' }}</td>
                                <td>{{ ($row->name != '') ? $row->name : '-' }}</td>
                                <td class="text-center">{!! ($row->status == 1) ? '<span class="text-success"><i class="fa fa-check-circle-o"></i> Active</span>' : '<span class="text-danger"><i class="fa fa-times-circle-o"></i> Inactive</span>' !!}</td>
                                <td class="text-center" width="13%">
                                    <div class="btn-group">
                                        @if($user->hasAnyAccess(['price.view', 'admin']))
                                            <button class="btn btn-default" onclick="window.location.href='{{url('admin/price/view/'.$row->id)}}'"><i class="fa fa-eye"></i></button>
                                        @else
                                            <button class="btn btn-default" disabled><i class="fa fa-eye"></i></button>
                                        @endif

                                        @if($user->hasAnyAccess(['price.edit', 'admin']))
                                            <button class="btn btn-default" onclick="window.location.href='{{url('admin/price/edit/'.$row->id)}}'"><i class="fa fa-pencil"></i></button>
                                        @else
                                            <button class="btn btn-default" disabled><i class="fa fa-pencil"></i></button>
                                        @endif

                                        @if($user->hasAnyAccess(['price.delete', 'admin']))
                                            <!-- <button class="btn btn-default btn-delete" data-id="{{$row->id}}" onclick="deleteData('{{url('admin/price/delete?id='.$row->id)}}');"><i class="fa fa-trash-o"></i></button> -->
                                        @else
                                            <!-- <button class="btn btn-default" disabled><i class="fa fa-trash-o"></i></button> -->
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5" align="center"> - No Data to Display - </td>
                        </tr>
                    @endif
                </tbody>
            </table>
            <div class="row">
                <div class="col-sm-12 text-left">
                     Row {{$data->count()}} of {{$data->total()}} 
                </div>
                <div class="col-sm-12 text-right">
                    {!! $data->appends($old->except('page'))->render() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@stop
@section('js')

<script src="{{asset('assets/dist/chosen/chosen.jquery.min.js')}}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $(".chosen").chosen();
    });

    function deleteData(_url){
        swal({
            title: "Are you sure?",
            text: "you wanna delete this product?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        },
        function(){
            $.ajax({
                url: _url,
                method: 'get',
                cache: false,
                data: [],
                success: function(response){
                    if(response.status == 1){
                        swal("Done!", "product has been deleted!.", "success");
                        location.reload();
                    }else{
                        swal("Error!", response.message, "error");
                    }               
                },
                error: function(xhr){
                    console.log(xhr);
                    swal("Error!", 'Error occurred. Please try again', "error");
                } 
            });
            
        });
    }
</script>
@stop
