@extends('layouts.back_master') @section('title','View Price Book')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
<style type="text/css">
	.has-error .help-block, .has-error .control-label{
		color:#e41212;
	}
	.has-error .chosen-container{
		border:1px solid #e41212;
	}

	.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
		padding-top:5px;
	    padding-bottom:5px; 
	}

	.box .box-footer div.row > div {
	    border-top: 1px solid #eee;
	    padding-top: 6px;
	    padding-bottom: 6px;
	}
	.box .box-footer div.row:first-child > div {
		border-top: none;
	}

	.bpr{
		font-size: 10px;
	}

	.customer{
		font-size: 12px;
	}
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Price Book<small>Management</small></h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li><a href="{{url('admin/price/list')}}">Price Book Management</a></li>
		<li class="active">View Price Book</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="row">
		<div class="col-md-4">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Price Book</h3>
				</div>
				<br>
				<div class="box-body">
					<div class="row">
						<div class="col-md-4 text-right">
		              		<label for="" class="control-label"><strong>Short Code : </strong></label>
		              	</div>
		              	<div class="col-md-8">
	                		{{($data->code != '') ? $data->code: '-'}}
		              	</div>
					</div>
					<div class="row">
						<div class="col-md-4 text-right">
		              		<label for="" class="control-label"><strong>Book Name : </strong></label>
		              	</div>
		              	<div class="col-md-8">
	                		{{($data->name != '') ? $data->name: '-'}}
		              	</div>
					</div>
					<div class="row">
						<div class="col-md-4 text-right">
		              		<label for="" class="control-label"><strong>Status : </strong></label>
		              	</div>
		              	<div class="col-md-8">
	                		<span class="{{ ($data->status == 1)? 'text-success' : 'text-danger'}}">
	                			@if($data->status == 1)
	                			<i class="fa fa-check-circle-o"></i> Active
	                			@else
	                			<i class="fa fa-times-circle-o"></i> Inactive
	                			@endif
	                		</span>
		              	</div>
					</div>
				</div><!-- /.box-body -->
			</div><!-- /.box -->	
		</div>
		<div class="col-md-8">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Customers</h3>
				</div>
				<br>
				<div class="box-body">
					<form class="form-horizontal" method="get">
						<div class="form-group">
							<label class="control-label col-sm-2">Keyword</label>
							<div class="col-sm-8">
								<input type="text" name="keyword" class="form-control" value="{{$old->keyword}}">
							</div>
							<div class="col-sm-2">
								<button type="submit" class="btn btn-default"><i class="fa fa-search"></i> Filter</button>
							</div>
						</div>
					</form><br/>
		  		  	<table class="table table-bordered" align="center">
			          	<thead>
				            <tr>
				              <th class="text-center"></th>
				              <th class="text-center">Customer Code</th>
				              <th class="text-center">Customer Name</th>
				            </tr>
			          	</thead>
			          	<tbody>
			          	@if(count($customers) > 0)
			          		@foreach($customers as $customer)
				            	<tr>
				                	<td><input type="checkbox" class="btn-check" name="customers[]" @if($customer->current_pricebook) checked @endif value="{{$customer->id}}"></td>
				                	<td>{{($customer->code)? $customer->code : '-'}}</td>
				                	<td>{{($customer->first_name)? $customer->first_name : '-'}}</td>
				              	</tr>
				            @endforeach
				        @else
				        	<tr>
				        		<td class="text-center" colspan="3"> - No Customer Data - </td>
				        	</tr>
			            @endif
			          	</tbody>
			        </table>
			        @if(count($customers) > 0)
			        	<div class="pull-right">
			        	{!! $customers->appends($old->except('page'))->render() !!}
			        	</div>
			        @endif
				</div><!-- /.box-body -->
				<div class="overlay" style="display:none;">
			        <i class="fa fa-refresh fa-spin"></i>
				</div>
			</div><!-- /.box -->
		</div>
	</div>
</section><!-- /.content -->
@stop
@section('js')

<script src="{{asset('assets/dist/chosen/chosen.jquery.min.js')}}"></script>

<script type="text/javascript">
$(document).ready(function() {
  $(".chosen").chosen();

  $('.btn-check').click(function(){
  	$('.overlay').show();

  	var customer = $(this).val();
  	var isChecked = $(this).is(':checked');
  	$.ajax({
      url: '{{url('admin/price/assign/'.$data->id)}}',
      method: 'post',
      cache: false,
      data: {
      	customer:customer,
      	is_checked: (isChecked == true)?1:0
      },
      success: function(response){
        if(response.status == 1){
          swal("Done!", response.message, "success");
          $('.overlay').hide();
        }else{
          swal("Error!", 'Error occured. Try again', "error");
          $('.overlay').hide();
        }
      },
      error: function(xhr){
        console.log(xhr);
        swal("Error!", response.message, "error");
        $('.overlay').hide();
      } 
    });
  });
});
</script>
@stop
