@extends('layouts.back_master') @section('title','Add Price Book')
@section('css')

<style type="text/css">
	.has-error .help-block, .has-error .control-label{
		color:#e41212;
	}
	.has-error .chosen-container{
		border:1px solid #e41212;
	}
</style>
@stop
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Price Book Management</h2>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{url('admin/price/list')}}">Price Book Management</a></li>
            <li class="active">Add Price Book</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a class="btn btn-primary" href="{{ url('admin/price/list') }}"><i class="fa fa-th" aria-hidden="true"></i> Price Book List</a>
        </div>
    </div>
</div>

<!-- Main content -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Add Mrp</h5>
            <div class="ibox-tools">
                
            </div>
        </div>

        <div class="ibox-content" style="display: block;">
        	<form action="" class="form-horizontal" method="post" enctype="multipart/form-data">
				<div class="box-body">
					{!!Form::token()!!}
					<div class="form-group @if($errors->has('code')) has-error @endif">
          				<label for="" class="col-sm-2 control-label required">Short Code</label>
          				<div class="col-sm-10">
            				<input type="text" class="form-control" placeholder="Code"name="code" value="{{old('code')}}">
            				@if($errors->has('code'))
            					<span class="help-block">{{$errors->first('code')}}</span>
            				@endif
          				</div>
          			</div>
        			<div class="form-group @if($errors->has('name')) has-error @endif">
          				<label for="" class="col-sm-2 control-label required">Price Book Name</label>
          				<div class="col-sm-10">
	                		<input type="text" class="form-control" placeholder="Name"name="name" value="{{old('name')}}">
	                		@if($errors->has('name'))
	                		<span class="help-block">{{$errors->first('name')}}</span>
	                		@endif
          				</div>
        			</div>
        	
        			<div class="form-group @if($errors->has('status')) has-error @endif">
	              		<label for="" class="col-sm-2 control-label required">Status</label>
	              		<div class="col-sm-4">
	                		<select name="status" class="form-control chosen">
	                			<option value="1" @if(old('status') == 1) selected @endif>Active</option>
	                			<option value="0" @if(old('status') == 0) selected @endif>Inactive</option>
	                		</select>
	                		@if($errors->has('status'))
	                		<span class="help-block">{{$errors->first('status')}}</span>
	                		@endif
	              		</div>
	            	</div>

	            	<div class="form-group">
			            <label for="" class="col-sm-2 control-label">Download</label>
			            <div class="col-sm-10">
			              <a href="{{asset('assets/excel_formats/pricebook_upload.xls')}}">
			                <i class="fa fa-download"></i> Sample Excel
			              </a>
			            </div>
			        </div>

			        <div class="form-group @if($errors->has('file')) has-error @endif">
			            <label for="" class="col-sm-2 control-label required">Browse Excel</label>
			            <div class="col-sm-10">
			              <input type="file" name="file">
			              @if($errors->has('file'))
			              <span class="help-block">{{$errors->first('file')}}</span>
			              @endif
			            </div>
			        </div>
		        
			        @if(count(session('data')) > 0)
			  		  	<table class="table table-bordered" align="center" style="width:90%">
			          		<thead>
			            		<tr>
			              			<th>Code</th>
			              			<th>Price</th>
			              			<th>Status</th>
			            		</tr>
			          		</thead>
			          		<tbody>
				            	@foreach(session('data') as $row)
				              		<tr>
				                		<td width="20%">{{ $row['code'] }}</td>
				                		<td>{{ $row['price'] }}</td>
				                		<td><span class="{{($row['status'])? 'text-success':'text-danger'}}">{{ $row['message'] }}</span></td>
				              		</tr>
				            	@endforeach
			          		</tbody>
			        	</table>
			        @endif
				</div><!-- /.box-body -->
				<div class="row">
					<div class="col-sm-12">
						<button type="submit" class="btn btn-default pull-right">Save</button>
					</div>
				</div>
			</form>
        </div>
    </div>
</div>

@stop
@section('js')

<script type="text/javascript">
	$(document).ready(function() {
        $(".chosen").chosen();
      });

        $('#myform').submit(function() {
            var txt = $('#name').val();
            
            $('#name').val(txt.trim());

            $('#myform').submit();
        });;
</script>
@stop
