@extends('layouts.back_master') @section('title','View Price Book')
@section('css')
<style type="text/css">
	.has-error .help-block, .has-error .control-label{
		color:#e41212;
	}
	.has-error .chosen-container{
		border:1px solid #e41212;
	}

	.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
		padding-top:5px;
	    padding-bottom:5px; 
	}

	.box .box-footer div.row > div {
	    border-top: 1px solid #eee;
	    padding-top: 6px;
	    padding-bottom: 6px;
	}
	.box .box-footer div.row:first-child > div {
		border-top: none;
	}

	.bpr{
		font-size: 10px;
	}

	.customer{
		font-size: 12px;
	}
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Price Book Management</h2>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{url('admin/price/list')}}">Price Book Management</a></li>
            <li class="active">View Price Book</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a class="btn btn-primary" href="{{ url('admin/price/list') }}"><i class="fa fa-th" aria-hidden="true"></i> List Price Book</a>
        </div>
    </div>
</div>

<!-- Main content -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>View Price Book Details</h5>
            <div class="ibox-tools">
                
            </div>
        </div>

        <div class="ibox-content" style="display: block;">
        	<div class="row">
				<div class="col-md-4">
					<div class="box">
						<div class="box-header with-border">
							<h3 class="box-title">Price Book</h3>
						</div>
						<br>
						<div class="box-body">
							<div class="row">
								<div class="col-md-4 text-right">
				              		<label for="" class="control-label"><strong>Short Code : </strong></label>
				              	</div>
				              	<div class="col-md-8">
			                		{{($data->code != '') ? $data->code: '-'}}
				              	</div>
							</div>
							<div class="row">
								<div class="col-md-4 text-right">
				              		<label for="" class="control-label"><strong>Book Name : </strong></label>
				              	</div>
				              	<div class="col-md-8">
			                		{{($data->name != '') ? $data->name: '-'}}
				              	</div>
							</div>
							<div class="row">
								<div class="col-md-4 text-right">
				              		<label for="" class="control-label"><strong>Status : </strong></label>
				              	</div>
				              	<div class="col-md-8">
			                		<span class="{{ ($data->status == 1)? 'text-success' : 'text-danger'}}">
			                			@if($data->status == 1)
			                			<i class="fa fa-check-circle-o"></i> Active
			                			@else
			                			<i class="fa fa-times-circle-o"></i> Inactive
			                			@endif
			                		</span>
				              	</div>
							</div>
						</div><!-- /.box-body -->
						<div class="box-footer">
							
						</div>
					</div><!-- /.box -->	
				</div>
				<div class="col-md-8">
					<div class="box">
						<div class="box-header with-border">
							<h3 class="box-title">Product Details</h3>
						</div>
						<br>
						<div class="box-body">
				  		  <table class="table table-bordered" align="center">
					          <thead>
					            <tr>
					              <th class="text-center">Code</th>
					              <th class="text-center">Product Name</th>
					              <th class="text-center">Price</th>
					            </tr>
					          </thead>
					          <tbody>
					          	@if(count($data->details) > 0)
					          		@foreach($data->details as $row)
						            	<tr>
						                	<td>{{($row->product)? $row->product->code : '-'}}</td>
						                	<td>{{($row->product)? $row->product->name : '-'}}</td>
						                	<td class="text-right">{{($row->price)? number_format($row->price,2) : '0.00'}}</td>
						              	</tr>
						            @endforeach
						        @else
						        	<tr>
						        		<td colspan="3" class="text-center"> - No Price Data - </td>
						        	</tr>
					            @endif
					          </tbody>
					        </table>
						</div><!-- /.box-body -->
					</div><!-- /.box -->	
				</div>
			</div>
        </div>
    </div>
</div>

@stop
@section('js')

<script type="text/javascript">
	$(document).ready(function() {
	  	$(".chosen").chosen();
	});
</script>
@stop
