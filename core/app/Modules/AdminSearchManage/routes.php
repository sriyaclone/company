<?php

Route::group(['middleware' => ['auth']], function(){
	
    Route::group(['prefix' => 'admin/search', 'namespace' => 'App\Modules\AdminSearchManage\Controllers'],function(){

    	Route::get('/', [
        	'as' => 'admin.search', 'uses' => 'AdminSearchManageController@index' 
        ]);
	});  	
}); 


