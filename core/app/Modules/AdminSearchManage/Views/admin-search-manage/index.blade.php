@extends('layouts.back_master') @section('title','Add Menu')
@section('css')
<style type="text/css">
.search-results {
    list-style: outside none none;
    margin: 0px;
    padding: 0px;
}

.search-results h4 {
    font-weight: 400;
    line-height: 20px;
    padding: 0;
    margin: 0;
}

.search-results > li {
    margin-bottom: 30px;
}

.search-results .result-url {
    color: #239169;
    margin: 2px 0px;
    display: inline-block;
}
</style>
@stop
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
    Search 
    <small> Management</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
        <li class="active">Search</li>
    </ol>
</section>


<!-- SEARCH -->
<section class="content">

    
    <!-- Default box -->
    <div class="box">        
        <div class="box-body">
           {!! Form::open(['method' => 'GET', 'url' => '/admin/search', 'role' => 'search'])  !!}
            <div class="row">
                <div class="col-md-6">
                    <input type="text" class="form-control" name="keyword" placeholder="Search..." value="{{$keyword}}">  
                </div>
                 <div class="col-md-6">
                    <button class="btn btn-default pull-right" type="submit">
                        Find
                    </button>
               </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div> 



    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Search Result</h3>            
        </div>
        <div class="box-body">
            <ul class="search-results">
                @foreach($data as $inq)
                    <li>
                        <h4><a href="{{url('admin/inquiry/show/')}}/{{$inq->id}}">{{$inq->title}}</a></h4>
                        <a href="{{url('admin/inquiry/show/')}}/{{$inq->id}}" class="result-url">#{{$inq->inquiry_code}}</a>
                    </li>
                @endforeach
            </ul>          
        </div>
    </div>  
</section>



@stop
@section('js')

<script type="text/javascript">
$(document).ready(function() {
  
});
</script>
@stop


































