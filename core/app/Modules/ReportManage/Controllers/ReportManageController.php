<?php namespace App\Modules\ReportManage\Controllers;


/**
 * Controller class
 * @author Author <author@gmail.com>
 * @version x.x.x
 * @copyright Copyright (c) 2017, OITS.Dev+
 *
 */

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\StockTransaction;
use App\Models\Warehouse;
use App\Modules\AdminProductManage\Models\Product;
use Illuminate\Http\Request;

use App\Modules\ReportManage\BusinessLogics\ReportLogic;
use Sentinel;

class ReportManageController extends Controller
{


    protected $erpLogic;
    protected $reportLogic;

    public function __construct(ReportLogic $reportLogic)
    {
        $this->reportLogic = $reportLogic;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function commissionView(Request $request)
    {
        $orders = $this->reportLogic->getCommissions();
        $discountMatrix = $this->reportLogic->getDiscountMatrix();
        return view("ReportManage::commission", compact('orders', 'discountMatrix'));
    }

    public function stockLeadgerView(Request $request)
    {
        $warehouse = Warehouse::get();
        $products = Product::with('stock')->get();

        return view('ReportManage::stock_ledger')->with(['products' => $products, 'transactions' => [], 'input' => null, 'warehouses' => $warehouse]);
    }

    public function stockLeadger(Request $request)
    {
        $range = explode(" - ", $request->dateRange);
        $product_id = $request->product_id;
        $warehouse = $request->warehouse;
        $from = $range[0];
        $to = $range[1];
        $user = Sentinel::getUser();
        $data = $this->getStockMove($user, $product_id, $from, $to, $warehouse);
        $product = Product::find($product_id);


        $warehouse = Warehouse::get();

        $products = Product::with('stock')->get();
        return view('ReportManage::stock_ledger')->with(['products' => $products, 'transactions' => $data['data'], 'warehouses' => $warehouse, 'input' => $request->all(), 'open' => $data['open']]);
    }

    public function getStockMove($user,$product_id,$from,$to,$warehouse)
    {

        $warehouses = Warehouse::where('id',$warehouse)->lists('id');


        $union_stock = StockTransaction::with(['stock.product','type'])
            ->whereIn('stock_id',function($qry) use($product_id,$warehouses)
            {
                return $qry->select('id')->from('stock')
                    ->where('product_id',$product_id)
                    ->whereIn('warehouse_id',$warehouses);
            })
            ->whereNull('stock_transactions.deleted_at')
            ->whereBetween('created_at',[$from,$to])
            ->orderBy('created_at')
            ->get();


        $open  = $this->getOpen($from,$to,$product_id,$warehouses);

        return $grouped = [
            'data' => $union_stock->groupBy(function($date){ return $date->created_at->format('Y-m-d');}),
            'open' => $open
        ];
    }

    public function getOpen($from,$to,$product_id,$warehouses)
    {
        $aa = StockTransaction::selectRaw('ifnull(sum(qty),0) as qty')
            ->whereIn('stock_id',function($qry) use($product_id,$warehouses)
            {
                return $qry->select('id')->from('stock')
                    ->where('product_id',$product_id)
                    ->whereIn('warehouse_id',$warehouses);
            })
            ->whereNull('stock_transactions.deleted_at')
            ->where('created_at','<',$from)->first();

        return $aa->qty;
    }

    public function ledgerPrint(Request $request)
    {

        $product_id = $request->product_id;
        $from       = $request->from;
        $to         = $request->to;
        $warehouse     = $request->warehouse;
        $user       = Sentinel::getUser();
        $grouped    = $this->getStockMove($user,$product_id,$from,$to,$warehouse);
        $product    = Product::find($product_id);

        $page1      =  view('ReportManage::print.ledger')->with(['transactions'=>$grouped['data'],'product'=>$product,'open'=>$grouped['open']])->render();

        $pdf   = new \TCPDF();
        $pdf->SetMargins(5, 15, 5);
        $pdf->SetAutoPageBreak(TRUE, 35);
        $pdf->AddPage();
        $pdf->writeHtml($page1);
        $pdf->output("Stock Ledger.pdf", 'I');
    }


}
