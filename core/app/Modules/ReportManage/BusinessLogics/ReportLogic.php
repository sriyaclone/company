<?php namespace App\Modules\ReportManage\BusinessLogics;


/**
* Business Logics 
* Define all the busines logics in here
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use Illuminate\Database\Eloquent\Model;

use App\Modules\AdminOrderManage\Models\Order;
use App\Modules\AdminOrderManage\Models\OrderDetails;
use App\Models\DiscountMatrix;
use DB;

class ReportLogic extends Model {



	public function getCommissions()
	{
		
		$orders =  DB::select(DB::raw("select 
				`order`.*,
				order.initiate_user_type,
				CASE order.initiate_user_type
				    WHEN 'electrician' THEN concat(initiate_elect.first_name,' ',initiate_elect.last_name)
				    WHEN 'helper' THEN concat(initiate_helper.first_name,' ',initiate_helper.last_name)
				    WHEN 'dealer' THEN concat(initiate_dealer.first_name,' ',initiate_dealer.last_name)
				END AS initiated_by,
				concat(dealer.first_name,' ',dealer.last_name) as dealer,
				concat(dealer_helper.first_name,' ',dealer_helper.last_name) as helper
			from `order` 
			LEFT JOIN electrician as initiate_elect     ON order.order_initiate_by = initiate_elect.id
			LEFT JOIN dealer_helper as initiate_helper  ON order.order_initiate_by = initiate_helper.id
			LEFT JOIN dealer  as  initiate_dealer       ON  order.order_initiate_by = initiate_dealer.id
			LEFT JOIN dealer ON  order.dealer_id = dealer.id
			LEFT JOIN dealer_helper ON  order.dealer_helper_id = dealer_helper.id
			where `order`.status>0
			"
		));
		return $orders;
	}

	public function getDiscountMatrix()
	{
		
		$discountMatrix = DiscountMatrix::whereNull('ended_at')->first();
		return $discountMatrix;
	}

}
