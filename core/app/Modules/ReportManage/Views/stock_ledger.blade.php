@extends('layouts.back_master') @section('title','Ledger Report')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/bootstrap-daterangepicker/css/daterangepicker.css')}}">
    <style type="text/css">


        .running {
            background-color: #72cc2e;
        }

        strong {
            font-weight: 600;
        }

        table tr:hover {
            background-color: #f5f5f5;
        }
    </style>
@stop
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-8">
            <h2>Ledger Report</h2>
            <ol class="breadcrumb">
                <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
                <li><a href="{{url('/')}}">Stock Management</a></li>
                <li class="active">Ledger Report</li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Create Product</h5>
                <div class="ibox-tools">

                </div>
            </div>

            <div class="ibox-content" style="display: block;">

                <form class="form-horizontal" method="post" id="form">
                    {!!Form::token()!!}
                    <div class="row">
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            <div class="row">
                                <label class="col-sm-3 control-label required">Date</label>
                                <div class='col-sm-9'>
                                    <input type="text" class="form-control" name="dateRange" id="dateRange"
                                           data-session="{{$input['dateRange']}}" placeholder="Select Date Range"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                            <div class="row">
                                <label class="col-sm-3 control-label required">Product</label>
                                <div class="col-sm-9">

                                    <select name="product_id" id="inputProduct_id" class="form-control custom-chosen"
                                            required="required">
                                        @foreach($products as $product)
                                            <option @if($input['product_id']==$product->id) selected
                                                    @endif value="{{$product->id}}">({{$product->code}})
                                                -{{$product->name}} @if(sizeof($product->stock) == 0)
                                                    [no-stock]</span>@endif</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <div class="row">
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-right">
                                    <h6><strong>Warehouse</strong></h6>
                                </div>
                                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                    <select name="warehouse" class="form-control" required="required" id="warehouse">
                                        @if(count($user->roles)>0 && $user->roles[0]->id!=2)
                                            <option value="0">--ALL--</option>@endif
                                        @foreach($warehouses as $warehouse)
                                            <option value="{{$warehouse->id}}"
                                                    @if($input['warehouse']==$warehouse->id) selected @endif >
                                                {{$warehouse->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px" id="buttons">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
                                <button type="submit" class="btn btn-info">Find</button>
                                <a id="print" class="btn btn-success">Print</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-22 col-sm-12 col-md-12 col-lg-12 ">
                        @if(count($transactions)==0)
                            <div class="text-center">
                                <h3><strong style="font-weight: 600">! No Data</strong></h3>
                                <h5>No Data found for this product, please select range and product</h5>
                            </div>
                        @else
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th width="6%">#</th>
                                    <th width="20%" class="text-center">Date</th>
                                    <th width="16%">Transaction Type</th>
                                    <th width="14%" class="text-center">Transaction</th>
                                    <th width="12%" class="text-right">Open <br>QTY</th>
                                    <th width="12%" class="text-right">In <br>QTY</th>
                                    <th width="12%" class="text-right">Out <br>QTY</th>
                                    <th width="12%" class="text-right">Running <br>QTY</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $qty = $open ?>
                                @foreach($transactions as $key=>$transaction)
                                    <tr style="background-color: #f1f1f1">
                                        <td colspan="4"><strong>{{$key}}</strong></td>
                                        <td class="text-right"><span class="badge open">{{$qty}}</span></td>
                                        <td class="text-right">-</td>
                                        <td class="text-right">-</td>
                                        <td class="text-right"><span class="badge running">{{$qty}}</span></td>
                                    </tr>
                                    @foreach($transaction as $key=>$detail)
                                        <?php $qty = $detail->qty + $qty ?>
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td class="text-center">{{$detail->created_at->toDateTimeString()}}</td>
                                            <td>{{$detail->type->name}}</td>
                                            <td class="text-center"><a href="{{url('/')}}"
                                                                       style="color: blue;text-decoration: underline;">#{{str_pad($detail->reference_id,8,'0',STR_PAD_LEFT)}} </a>
                                            </td>
                                            <td class="text-right">-</td>
                                            <td class="text-right">@if($detail->qty>0){{ $detail->qty}}@else
                                                    - @endif</td>
                                            <td class="text-right">@if($detail->qty<0){{floatVal($detail->qty)*-1}}@else
                                                    - @endif</td>
                                            <td class="text-right">{{$qty}}</td>
                                        </tr>
                                    @endforeach
                                @endforeach
                                <tr>
                                    <td colspan="7" style="font-weight: 600;text-transform: capitalize;"
                                        class="text-right"><h5 style="margin-top: 5px">Running Stock</h5></td>
                                    <td class="text-right" style="font-weight: 600"><span
                                                class="badge running">{{$qty}}</span></td>
                                </tr>
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop
@section('js')
    <script src="{{url('/assets/angular/angular/angular.min.js')}}"></script>
    <script src="{{url('/assets/moment/moment.js')}}"></script>
    <script src="{{asset('assets/bootstrap-daterangepicker/js/daterangepicker.js')}}"></script>
    <script type="text/javascript">
        /* 
         * A CRAFT from MAC
         * Be a hacker but aint be a thief
         *
         */
        $(document).ready(function ($) {
            $(".custom-chosen").chosen({
                template: function (text, value, templateData) {
                    return [
                        "<img src='" + templateData.ico + "' height='16' width='16'></img> " + text + "</i> value: " + value
                    ].join("");
                }
            });

            var session_date = $('input[name="dateRange"]').attr('data-session');

            $('input[name="dateRange"]').daterangepicker({
                locale: {
                    format: 'YYYY-MM-DD'
                },
                startDate: session_date ? session_date : new Date()

            });

            $('#buttons').on('click', '#print', function () {
                var date_from = $('input[name="dateRange"]').data('daterangepicker').startDate.format('YYYY-MM-DD');
                var date_to = $('input[name="dateRange"]').data('daterangepicker').endDate.format('YYYY-MM-DD');
                var product_id = $('#inputProduct_id').val();
                var warehouse = $('#warehouse').val();

                if (date_from != null && date_to != null && product_id != null && warehouse != null) {
                    var url = "{{url('admin/report/ledger')}}" + "/print?product_id=" + product_id + "&from=" + date_from + "&to=" + date_to + "&warehouse=" + warehouse + "";
                    window.open(url);
                } else {
                    swal('oopz', 'select data and search then click print', 'error');
                }


            });
        });
    </script>
@stop
