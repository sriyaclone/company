	<style type="text/css">
	
td{
	font-weight: 400;
	font-size: 9px;
}

.details , .details> th ,  .details> td {
    border-collapse: collapse;   
    padding: 5px; 
}

th {
    text-align: left;
}	


.border{
    border-width:1px;
    border-style:solid;
}

.border-left{
    border-left-style:solid;
    border-width:1px;
}

.border-right{
    border-right-style:solid;
    border-width:1px;
}

.border-top{
    border-top-style:solid;
    border-width:1px;
}

.border-bottom: {
    border-bottom-style:solid;
    border-width:1px;
}

.text-center{
	text-align: center;
}

.text-right{
	text-align: right;
}

.text-left{
	text-align: left;
}

</style>

<table width="100%">
	<tbody>
		<tr><!--row1-->
			<td align="center">			
				<h2>STOCK LEDGER </h2>
				<h4>{{$product->product_name}}  - {{$product->short_code}}</h4>
			</td>
		</tr>

		<tr><td></td></tr>
		<tr><td></td></tr>
		<tr><td></td></tr>

		<tr>
			<td>
				<table>
		          	<thead>
			            <tr>
			              <th width="6%">#</th>
			              <th width="25%" class="text-center">Date</th>
			              <th width="17%">Type</th>	
			              <th width="13%" class="text-right">Open <br>QTY</th>
			              <th width="13%" class="text-right">In <br>QTY</th>
			              <th width="13%" class="text-right">Out <br>QTY</th>
			              <th width="13%" class="text-right">Running <br>QTY</th>
			            </tr>
		          	</thead>
		          	<tbody>
		              	<?php $qty=0 ?>
		              	@foreach($transactions as $key=>$transaction)                              
			              	<tr style="background-color: #f1f1f1">
				                <td colspan="3" style="line-height: 2"><strong>{{$key}}</strong></td>
				                <td class="text-right"><span class="badge open">{{$qty}}</span></td>
				                <td class="text-right">-</td>
				                <td class="text-right">-</td>
				                <td class="text-right">{{$qty}}</td>
			              	</tr>
			              	@foreach($transaction as $key=>$detail)  
			                	<?php $qty=$detail->qty+$qty ?>                                
				                <tr style="line-height: 3	">
				                  <td>{{$key+1}}</td>
				                  <td class="text-center">{{$detail->created_at->toDateTimeString()}}</td>
				                  <td>{{$detail->type->name}}</td>
				                  <td class="text-right">-</td>
				                  <td class="text-right">@if($detail->qty>0){{ $detail->qty}}@else - @endif</td>
				                  <td class="text-right">@if($detail->qty<0){{floatVal($detail->qty)*-1}}@else - @endif</td>
				                  <td class="text-right">{{$qty}}</td>
				                </tr>                                
			                @endforeach
		              	@endforeach
		              	<tr>
		                  	<td colspan="6" style="font-weight: 600;text-transform: capitalize;" class="text-right"><h4 style="margin-top: 5px">Running Stock</h4></td>
		                  	<td class="text-right" style="font-weight: 800">{{$qty}}</td>
		              	</tr>
		          	</tbody>
		        </table>
	        </td>
        </tr>

		
	</tbody>
</table>