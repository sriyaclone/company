@extends('layouts.back_master') @section('title','Admin - MRP Management')
@section('current_title','Reports')

@section('css')
<style type="text/css">
  .box-header, .box-body {
    padding: 20px;
  }
  .has-error .help-block, .has-error .control-label{
    color:#e41212;
  }
  .has-error .chosen-container{
    border:1px solid #e41212;
  }
</style>
@stop

@section('content')
<!-- Content-->
<section>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Report<small>Management</small></h1>
    <ol class="breadcrumb">
      	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      	<li>Report</li>
      	<li class="active">Commission</li>
    </ol>
  </section>
  <!-- !!Content Header (Page header) -->

  <!-- Main content -->
  <section class="content">

    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Commission Report</h3>
      </div>
      <div class="box-body">
        <table class="table table-bordered">
          <thead> 
            <tr>
              <th width="5%">#</th>
              <th>Order No</th>
              <th>Order Value</th>
              <th>Initiated By</th>
              <th>Initiated By Commission</th>
              <th>Dealer </th>
              <th>Dealer Commission</th>
              <th>Helper</th>
              <th>Helper Commission</th>
            </tr>
          </thead>
          <tbody>
            @foreach($orders as $key=>$order)
            <tr>
              <td>{{$key+1}}</td>
              <td>{{$order->reference_no}}</td>
              <td>
                {{$order->total_amount}}
              </td>
              <td>
               <strong> {{$order->initiated_by}}</strong><br>
               <small> {{$order->initiate_user_type}}</small>
              </td>
              <td>
                <?php $commission = (floatval($order->total_amount)*floatval($discountMatrix->direct_discount))/100 ?>
                Rs.{{number_format($commission,2)}}
                ({{$discountMatrix->direct_discount}}%)
              </td>
              <td>
                <strong> {{$order->dealer}}</strong>
              </td>
              <td>
                <?php $commission = (floatval($order->total_amount)*floatval($discountMatrix->dealer_discount))/100 ?>
                Rs.{{number_format($commission,2)}}
                ({{$discountMatrix->dealer_discount}}%)
              </td>
              <td>
                <strong> {{$order->helper}}</strong>
              </td>
              <td>
                 <?php $commission = (floatval($order->total_amount)*floatval($discountMatrix->indirect_discount))/100 ?>
                Rs.{{number_format($commission,2)}}
                ({{$discountMatrix->indirect_discount}}%)
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>

  </section>
  <!-- !!Main content -->

</section>
<!-- !!!Content -->

@stop
@section('js')  
  <!-- CORE JS -->
	<script type="text/javascript">
    	
	</script>
  <!-- //CORE JS -->
@stop
