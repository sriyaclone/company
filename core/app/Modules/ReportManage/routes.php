<?php


Route::group(['middleware' => ['auth']], function () {
    Route::group(['prefix' => 'admin/report/', 'namespace' => 'App\Modules\ReportManage\Controllers'], function () {

        Route::get('commission', [
            'as' => 'report.commission', 'uses' => 'ReportManageController@commissionView'
        ]);

        Route::get('ledger/timeline', [
            'as' => 'report.stock_timeline', 'uses' => 'ReportManageController@stockLeadgerTimeLineView'
        ]);

        Route::get('ledger', [
            'as' => 'report.stock_view', 'uses' => 'ReportManageController@stockLeadgerView'
        ]);

        Route::post('ledger', [
            'as' => 'report.stock_view', 'uses' => 'ReportManageController@stockLeadger'
        ]);

        Route::get('ledger/print', [
            'as' => 'report.stock_view', 'uses' => 'ReportManageController@ledgerPrint'
        ]);

        Route::get('json/getStockTimeline', [
            'as' => 'report.stock_timeline', 'uses' => 'ReportManageController@getStockTimeline'
        ]);

    });
});