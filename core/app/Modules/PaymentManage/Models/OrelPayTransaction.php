<?php namespace App\Modules\PaymentManage\Models;

/**
*
* Model
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;


class OrelPayTransaction extends Model {

	 /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'orel_pay_transactions';

    /**
     * The attributes that are not assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

}
