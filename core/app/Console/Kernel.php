<?php namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		'App\Console\Commands\Inspire',
		'App\Console\Commands\PermissonFlush',
		'App\Console\Commands\PermissonWrite',
		'App\Console\Commands\PermissionHandler',
		'App\Console\Commands\DataSyncCustomer',
		'App\Console\Commands\DataSyncInvoice',
		'App\Console\Commands\DataSyncPrice',
		'App\Console\Commands\DataSyncProduct',
		'App\Console\Commands\AwsProductSync'
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
		$schedule->command('inspire')
				 ->hourly();	
	}

}
