<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Modules\AdminProductManage\BusinessLogics\ProductLogic;
use Illuminate\Http\Request;

class AwsProductSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:awsproduct';

    protected $productLogic;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'get all product from aws cpm server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ProductLogic $productLogic)
    {
        parent::__construct();
        $this->productLogic = $productLogic;
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('customer sync starting....');
        $request = new Request;
        $request->type = 1;
        $request->is_terminal = true;
        //$this->info($request->type);
        $data = $this->productLogic->syncProduct($request);
        // $this->info($data);
        $this->info('customer sync finished');
    }
}
