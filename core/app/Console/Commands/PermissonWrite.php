<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Route;
use DB;

class PermissonWrite extends Command
{
     /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'permisson:write';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'write Permissons from route to tables';
    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('this will insert all the route permissons to the permissons tables');
        $aa = $this->getAllPermisonsFromRoutes();
        $this->info(count($aa).' permissons detected writing...');
        $data = [] ;
        array_push($data,[
            'name'          => 'admin',
            'description'   => '-'
        ]);
        foreach ($aa as $key=>$value) {
            array_push($data,[
                'name'          => $value,
                'description'   => '-'
            ]);
        }        
        DB::table('permissions')->insert($data); // Query Builder
        $this->info('done');
    }

    public function getAllPermisonsFromRoutes()
    {
        $routeCollection = Route::getRoutes();
        $aa = [];
        foreach ($routeCollection as $value) {
            $aa[$value->getName()] = $value->getName();
        }
        return $aa;

    }
}
