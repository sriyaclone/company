<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::get('/admin', [
      'as' => 'index', 'uses' => 'WelcomeController@admin'
    ]);
    
    Route::get('/', [
      'as' => 'index', 'uses' => 'WelcomeController@admin'
    ]);

});

Route::get('privacy-policy', [
  'as' => 'index', 'uses' => 'WelcomeController@privacy'
]);


Route::get('user/login', [
  'as' => 'user.login', 'uses' => 'AuthController@loginView'
]);

Route::post('user/login', [
  'as' => 'user.login', 'uses' => 'AuthController@login'
]);

Route::get('user/logout', [
  'as' => 'user.logout', 'uses' => 'AuthController@logout'
]);

Route::get('test/stock_in', [
    'as' => 'index', 'uses' => 'WelcomeController@stock_in'
]);

Route::get('test/stock_out', [
    'as' => 'index', 'uses' => 'WelcomeController@stock_out'
]);