<?php namespace App\Http\Middleware;

use Closure;
use Sentinel;
use Session;
use Route;
use Request;
use Core\MenuManage\Models\Menu;
use Core\Permissions\Models\Permission;
use App\Classes\DynamicMenu;
use Core\UserRoles\Models\UserRole;

class AuthAdmin {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		try{
			if (!Sentinel::check()){
				Session::put('loginRedirect', $request->url());
				return redirect()->route('user.login');
			}else{
				$user = Sentinel::getUser();	
				$action = Route::currentRouteName();
				if (!$user->hasAnyAccess(['admin'])) {				    
					return response()->view('errors.404');
				}
			}
		}catch(\Exception $e){
			Session::put('loginRedirect', $request->url());
			return redirect()->route('user.login');;
		}

	    view()->share('user',$user);

		return $next($request);
	}

}
