<?php namespace App\Http\Controllers;

use Core\UserManage\Models\User;
use Sentinel;
use Response;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

use App\Classes\Functions;
use App\Models\AppUser;

use Core\EmployeeManage\Models\Employee;
use Core\EmployeeManage\Models\EmployeeType;
use App\Modules\AdminProductManage\BusinessLogics\ProductLogic;
use App\Modules\AdminOrderManage\BusinessLogics\OrderLogic;
use App\Modules\AdminCustomerManage\BusinessLogics\CustomerLogic;
use App\Models\City;
use App\Models\MainCategory;
use App\Modules\AdminProductCategory\Models\AdminProductCategory;

class ApiElectricianController extends Controller {


	protected $productLogic;
	protected $orderLogic;
	protected $customerLogic;

	function __construct(ProductLogic $productLogic,OrderLogic $orderLogic,CustomerLogic $customerLogic)
	{
		$this->productLogic = $productLogic;
		$this->orderLogic = $orderLogic;
		$this->customerLogic = $customerLogic;
	}

	public function updateFCMToken(Request $request)
    {
        if (!isset($_REQUEST['fcm_token']) || !isset($_REQUEST['user_id'])) {
            return 'invalid request, request parameters is in valid ';
        }

        $fcm_token             = $request->get('fcm_token');
        $user_id               = $request->get('user_id');
        $response              = array();
        $response["timestamp"] = Functions::unixTimePhpToJava(time());

        $result 			   = AppUser::with(['employee'])->find($user_id);
        $result->fcm_token = $fcm_token;
        $result->save();

        if (count($result) == 1) {
            $response["result"] = 1;
            $response["token"]  = $fcm_token;
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }
    }
		
	public function checkUserAuthorize(Request $request)
	{
		if (!isset($_REQUEST['username']) || !isset($_REQUEST['password'])) {
            return 'invalid request request parameters is in valid ';
        }

        $userName = $request->get('username');
        $passWord = $request->get('password');
        $response = array();
        $response["timestamp"] = Functions::unixTimePhpToJava(time());

        $result = AppUser::selectRaw('*,id as userId')
        			->where('username', $userName)
		            ->where('password', md5($passWord))
		            ->where('refernece_type', 'electrician')
		            ->first();


        if (count($result) == 1) {
        	$response["result"] = 1;
        	$response["user"] = $result;
            $result->last_login =  date("Y/m/d H:i:s");
            $result->save();
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }
	}

	public function getProducts(Request $request){
		if (!isset($_REQUEST['token'])) {
            return 'invalid request request parameters is in valid ';
        }

        $token = $request->get('token');
        if ($token != NULL && $this->isToken($token)) {
			$response              = array();
			$response["timestamp"] = Functions::unixTimePhpToJava(time());
			$response["result"]    = 1;
			$user                  = $this->getTokenUser($token);			
			$result                = $this->productLogic->getAllProducts($request);
			$response["result"]    = count($result)>0?1:0;
			$response["products"]  = $result;
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }			
	}

	public function getCategories(Request $request){
		if (!isset($_REQUEST['token'])) {
            return 'invalid request request parameters is in valid ';
        }

        $token = $request->get('token');
        if ($token != NULL && $this->isToken($token)) {
			$response              = array();
			$response["timestamp"] = Functions::unixTimePhpToJava(time());
			$response["result"]    = 1;
			$user                  = $this->getTokenUser($token);			
			$result                = AdminProductCategory::selectRaw('id,name,deleted_at')->where('main_cat_id',$request->main_cat_id)->get();
			$response["result"]    = count($result)>0?1:0;
			$response["categories"]  = $result;
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }			
	}

	public function sendOrder(Request $request){
		if (!isset($_REQUEST['token'])) {
            return 'invalid request request parameters is in valid ';
        }

		$token = $request->get('token');
		$data  = file_get_contents('php://input');
		$data  = str_replace("\xe2\x80\x8b", "", $data);
        if ($token != NULL && $this->isToken($token)) {
			$response              = array();
			$response["timestamp"] = Functions::unixTimePhpToJava(time());
			$response["result"]    = 1;
			$user                  = $this->getTokenUser($token);			
			$order                 = json_decode(json_decode($data));

			$customer = $this->customerLogic->getCustomer($order->customer->firstName,$order->customer->lastName,$order->customer->contactNo);
			if(!$customer){
				$customer = $this->customerLogic->saveNewCustomer($order->customer);
			}
			
			if($customer){
				$result   = $this->orderLogic->createNewOrder($user,$order,$customer);
			}

			$response["order"]     = $result;
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }			
	}

	public function getOrders(Request $request){
		if (!isset($_REQUEST['token'])) {
            return 'invalid request request parameters is in valid ';
        }

		$token = $request->get('token');

        if ($token != NULL && $this->isToken($token)) {
			$response              = array();
			$response["timestamp"] = Functions::unixTimePhpToJava(time());
			$response["result"]    = 1;
			$user                  = $this->getTokenUser($token);					

			$result 			   = $this->orderLogic->getOrdersElectrican($user->reference_no); 						

			$response["orders"]     = $result;
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }			
	}


	public function getCities(Request $request){
		if (!isset($_REQUEST['token'])) {
            return 'invalid request request parameters is in valid ';
        }

		$token = $request->get('token');

        if ($token != NULL && $this->isToken($token)) {
			$response              = array();
			$response["timestamp"] = Functions::unixTimePhpToJava(time());
			$response["result"]    = 1;
			$user                  = $this->getTokenUser($token);					

			$result 			    = City::selectRaw('id,name_en,name_si')->get(); 						

			$response["cities"]     = $result;
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }			
	}

	public function getMainCategories(Request $request){
		if (!isset($_REQUEST['token'])) {
            return 'invalid request request parameters is in valid ';
        }

		$token = $request->get('token');

        if ($token != NULL && $this->isToken($token)) {
			$response              = array();
			$response["timestamp"] = Functions::unixTimePhpToJava(time());
			$response["result"]    = 1;
			$user                  = $this->getTokenUser($token);					

			$result 			    = MainCategory::selectRaw('id,name,deleted_at')->get(); 						

			$response["categories"]     = $result;
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }			
	}

	public function getImagesForProduct(Request $request){
		if (!isset($_REQUEST['token']) && !isset($_REQUEST['uuid'])) {
            return 'invalid request request parameters is in valid ';
        }

		$token = $request->get('token');

        if ($token != NULL && $this->isToken($token)) {
			$response              = array();
			$response["timestamp"] = Functions::unixTimePhpToJava(time());
			$response["result"]    = 1;
			$user                  = $this->getTokenUser($token);	

			$client = new \GuzzleHttp\Client();
	        $request = $client->get('http://13.250.41.29/orel_web/api/v1/product/get_images?uuid='.$request->uuid);
	        $response = $request->getBody()->getContents();
			return response()->json(json_decode($response));        
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }			
	}


	public function getOrderPdf(Request $request){
		if (!isset($_REQUEST['token']) && !isset($_REQUEST['order_id'])) {
            return 'invalid request request parameters is in valid ';
        }

		$token = $request->get('token');
		$order_id = $request->get('order_id');

        if ($token != NULL && $this->isToken($token)) {
			$response              = array();
			$response["timestamp"] = Functions::unixTimePhpToJava(time());
			$response["result"]    = 1;
			$user                  = $this->getTokenUser($token);	

			return $this->orderLogic->getOrderPdfOutput($order_id);        
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }			
	}


	public function isToken($token)
	{	
		return AppUser::where('token',$token)->count()>0;
	}

	public function getTokenUser($token)
	{	
		return AppUser::where('token',$token)->first();
	}


}
