<?php namespace App\Http\Controllers;

use App\Classes\Contracts\BaseStockHandler;
use App\Classes\Contracts\Handler;
use App\Classes\StockHandler;
use Core\MenuManage\Models\Menu;
use Core\UserManage\Models\User;
use Core\UserRoles\Models\UserRole;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use App\Classes\Functions;
use Illuminate\Http\Request;
use Sentinel;
use DB;

class WelcomeController extends Controller
{


    private $handler;

    public function __construct(BaseStockHandler $handler)
    {
        $this->handler = $handler;
    }
    /*
    |--------------------------------------------------------------------------
    | Welcome Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders the "marketing page" for the application and
    | is configured to only allow guests. Like most of the other sample
    | controllers, you are free to modify or remove it as you desire.
    |
    */


    /**
     * Show the application welcome screen to the user.
     *
     * @return Response
     */
    public function front()
    {
        return view('front');
    }

    /**
     * Show the application welcome screen to the user.
     *
     * @return Response
     */
    public function admin(Request $request)
    {

        return view('index');
    }

    public function stock_in(Request $request)
    {
        return $this->handler->stockIn(1,15,1,7,1,1.00);
    }

    public function stock_out(Request $request)
    {
        return $this->handler->stockOut(1,3,0,1,2,1,BaseStockHandler::FIRST_IN_FIRST_OUT);
    }
}
