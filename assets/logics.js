
function calculateNbtPrice(unitPrice, nbtPer){
	unitPrice = parseFloat(unitPrice);
	nbtPer = parseFloat(nbtPer);
	return ((unitPrice * (100 + nbtPer)) / 100);
}

function calculateNbtValue(unitPrice, nbtPer){
	unitPrice = parseFloat(unitPrice);
	nbtPer = parseFloat(nbtPer);
	return ((unitPrice * nbtPer) / 100);
}

function calculateVatPrice(price, vatPer){
	price = parseFloat(price);
	vatPer = parseFloat(vatPer);
	return ((price * (100 + vatPer)) / 100);
}

function calculateVatValue(price, vatPer){
	price = parseFloat(price);
	vatPer = parseFloat(vatPer);
	return ((price * vatPer) / 100);
}

function calculateDiscountPrice(price, per){
	price = parseFloat(price);
	per = parseFloat(per);
	return ((price * (100 - per)) / 100);
}

function calculateDiscountValue(price, per){
	price = parseFloat(price);
	per = parseFloat(per);
	return ((price * per) / 100);
}

function calculateVatCustomerQuotation(details, vat, nbt){

	if(details.length > 0){
		details.forEach(function(productRow){
			if(productRow.is_nbt){
				unitPrice = calculateNbtPrice(productRow.price, nbt);
			}else{
				unitPrice = productRow.price;
			}

			if(isNaN(parseFloat(productRow.discount))){
				productRow.discount = 0;				
			}else{
				productRow.discount = parseFloat(productRow.discount);
			}

			discount = parseFloat(productRow.discount)+parseFloat(productRow.udp);

			if(discount > 0){
				discountAmount = calculateDiscountValue(unitPrice, discount);
				discountPrice = calculateDiscountPrice(unitPrice, discount);
			}else{
				discountAmount = 0;
				discountPrice = unitPrice;
			}

			if(productRow.is_vat){
				vatAmount = calculateVatValue(discountPrice, vat);
				vatPrice = calculateVatPrice(discountPrice, vat);
			}else{
				vatAmount = 0;
				vatPrice = discountPrice;
			}

			productRow.unitPrice 		= unitPrice;
			productRow.discountAmount 	= discountAmount;
			productRow.discountPrice 	= discountPrice;
			productRow.vatAmount 		= vatAmount;
			productRow.vatPrice 		= vatPrice;
			productRow.aa 		= discount;
			productRow.totalAmount 		= unitPrice * productRow.qty;
			productRow.totalDiscount 	= discountAmount * productRow.qty;
			productRow.totalVat 		= vatAmount * productRow.qty;
		});
	}

	return details;
}

function calculateNonVatCustomerQuotation(details, vat, nbt){
	if(details.length > 0){
		details.forEach(function(productRow){
			if(productRow.is_nbt){
				unitPrice = calculateNbtPrice(productRow.price, nbt);
			}else{
				unitPrice = productRow.price;
			}

			if(productRow.is_vat){
				vatAmount	= calculateVatValue(unitPrice, vat);
				vatPrice 	= calculateVatPrice(unitPrice, vat);
				unitPrice 	= vatPrice;
			}else{
				vatAmount 	= 0;
				vatPrice 	= unitPrice;
				unitPrice 	= vatPrice;
			}

			if(isNaN(parseFloat(productRow.discount))){
				productRow.discount = 0;				
			}else{
				productRow.discount = parseFloat(productRow.discount);
			}

			discount = parseFloat(productRow.discount)+parseFloat(productRow.udp);

			if(discount > 0){
				discountAmount 	= calculateDiscountValue(unitPrice, discount);
				discountPrice 	= calculateDiscountPrice(unitPrice, discount);
			}else{
				discountAmount 	= 0;
				discountPrice 	= unitPrice;
			}

			productRow.unitPrice 		= unitPrice;
			productRow.discountAmount 	= discountAmount;
			productRow.discountPrice 	= discountPrice;
			productRow.vatAmount 		= 0;
			productRow.vatPrice 		= 0;
			productRow.aa 		= discount;
			productRow.totalAmount 		= unitPrice * productRow.qty;
			productRow.totalDiscount 	= discountAmount * productRow.qty;
			productRow.totalVat 		= 0;
		});
	}

	return details;
}

function calculateGrnDetails(details){

	if(details.length > 0){
		
		details.forEach(function(productRow){
			
			productRow.totalAmount = productRow.mrp * productRow.qty;
			
		});

	}

	return details;
}

function calculateOrderDetails(details){

	if(details.length > 0){
		
		details.forEach(function(productRow){
			
			productRow.totalAmount = productRow.mrp * productRow.qty;
			
		});

	}

	return details;
}

function calculateInvoiceDetails(details){

	if(details.length > 0){
		
		details.forEach(function(productRow){
			
			productRow.totalAmount = productRow.mrp * productRow.qty;
			
		});

	}

	return details;
}